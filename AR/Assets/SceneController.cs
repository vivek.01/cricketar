using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneController : MonoBehaviour
{
    //public enum UiScreens
    //{
    //    pitchDropDownScreen,
    //    bowlingTypeDropDownScreen,
    //    battingShotsDropDownScreen,
    //}



    //public Dictionary<UiScreens, GameObject> screens = new Dictionary<UiScreens, GameObject>();


    //private void Awake()
    //{
    //    foreach(UiScreens uiScreens in System.Enum.GetValues(typeof(UiScreens)))
    //    {
    //        screens.Add(uiScreens, new GameObject());
    //    }
    //}
    //public void EnableScreen(UiScreens i, bool isVisible)
    //{
    //    screens[i].SetActive(isVisible);
    //}
    public GameObject enableAnimator;
    public GameObject batsman;
    public List<GameObject> screens = new List<GameObject>();
    
    public void EnableScreen( int i)
    {
        screens[i].SetActive(true);
    }
    public void DisableScreen(int i)
    {
        screens[i].SetActive(false);
    }
    public void Coroutine()
    {
        StartCoroutine(EnableBatsman());
    }
    IEnumerator EnableBatsman()
    {
        yield return new WaitForSeconds(2.7f);
        
        screens[1].SetActive(true);
        Time.timeScale = 0;
    }

    public void PlayBatting()
    {
        Time.timeScale = 1;
    }
    public void EnableAnimation()
    {
        batsman.transform.localRotation *= Quaternion.Euler(0, 0, 0); 
        enableAnimator.GetComponent<Animator>().enabled = true;
        
    }
}
