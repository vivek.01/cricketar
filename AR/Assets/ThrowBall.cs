using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowBall : MonoBehaviour
{
    public Transform BallTransform;
    GameObject BallInit;
    public GameObject BallInitPrefab;
    public GameObject startBowling;
    Rigidbody rb;
    public float thrust = 100f;
    public void SpawnBall()
    {
        BallInit = Instantiate(BallInitPrefab, new Vector3(BallTransform.position.x, BallTransform.position.y, BallTransform.position.z), Quaternion.identity);
       
        rb = BallInit.GetComponent<Rigidbody>();
        rb.AddForce(8, 0, thrust, ForceMode.Force);
    }
    public void DestroyBall()
    {
        Destroy(startBowling);
    }
}
