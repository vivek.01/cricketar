using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PitchController : MonoBehaviour
{
    public List<GameObject> m_DropOptions = new List<GameObject>();
    private GameObject currentActive;
    private GameObject lastActive;
    private void Start()
    {
        DeActivate(0);
    }
    public void DeActivate(int activate)
    {
        if (lastActive != null)
        {
            lastActive.SetActive(false);
        }
        currentActive = m_DropOptions[activate];
        lastActive = currentActive;
        currentActive.SetActive(true);
    }
}
