﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// UnityEngine.AddComponentMenu
struct AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// UnityEngine.ContextMenu
struct ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// UnityEngine.DefaultExecutionOrder
struct DefaultExecutionOrder_t8495D3D4ECDFC3590621D31C3677D234D8A9BB1F;
// UnityEngine.DisallowMultipleComponent
struct DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E;
// UnityEngine.ExecuteInEditMode
struct ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173;
// UnityEngine.Serialization.FormerlySerializedAsAttribute
struct FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210;
// UnityEngine.HelpURLAttribute
struct HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023;
// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// UnityEngine.MultilineAttribute
struct MultilineAttribute_tA4DCA9D8519E0C1B05636F2EFF74F81335A7F291;
// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5;
// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// System.String
struct String_t;
// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* ARPlaneMeshVisualizer_t4C981AECE943267AD0363C57F2F3D32273E932F2_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* ARPlane_t6336725EC68CE9029844CBE72A7FE7374AD74891_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* ARSession_t0ECAFEF41CF9183D33E40174F849B07943DC5D3B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Light_tA2F349FE839781469A0344CF6039B51512394275_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CEnableBatsmanU3Ed__6_t9F28F6ED1F3B9EFF2BF57316D59057D3F25751C3_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__55_tAD522FF1E9F588E49FF94436005D673167775F79_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// UnityEngine.AddComponentMenu
struct AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.AddComponentMenu::m_AddComponentMenu
	String_t* ___m_AddComponentMenu_0;
	// System.Int32 UnityEngine.AddComponentMenu::m_Ordering
	int32_t ___m_Ordering_1;

public:
	inline static int32_t get_offset_of_m_AddComponentMenu_0() { return static_cast<int32_t>(offsetof(AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100, ___m_AddComponentMenu_0)); }
	inline String_t* get_m_AddComponentMenu_0() const { return ___m_AddComponentMenu_0; }
	inline String_t** get_address_of_m_AddComponentMenu_0() { return &___m_AddComponentMenu_0; }
	inline void set_m_AddComponentMenu_0(String_t* value)
	{
		___m_AddComponentMenu_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AddComponentMenu_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Ordering_1() { return static_cast<int32_t>(offsetof(AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100, ___m_Ordering_1)); }
	inline int32_t get_m_Ordering_1() const { return ___m_Ordering_1; }
	inline int32_t* get_address_of_m_Ordering_1() { return &___m_Ordering_1; }
	inline void set_m_Ordering_1(int32_t value)
	{
		___m_Ordering_1 = value;
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.ContextMenu
struct ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.ContextMenu::menuItem
	String_t* ___menuItem_0;
	// System.Boolean UnityEngine.ContextMenu::validate
	bool ___validate_1;
	// System.Int32 UnityEngine.ContextMenu::priority
	int32_t ___priority_2;

public:
	inline static int32_t get_offset_of_menuItem_0() { return static_cast<int32_t>(offsetof(ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861, ___menuItem_0)); }
	inline String_t* get_menuItem_0() const { return ___menuItem_0; }
	inline String_t** get_address_of_menuItem_0() { return &___menuItem_0; }
	inline void set_menuItem_0(String_t* value)
	{
		___menuItem_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___menuItem_0), (void*)value);
	}

	inline static int32_t get_offset_of_validate_1() { return static_cast<int32_t>(offsetof(ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861, ___validate_1)); }
	inline bool get_validate_1() const { return ___validate_1; }
	inline bool* get_address_of_validate_1() { return &___validate_1; }
	inline void set_validate_1(bool value)
	{
		___validate_1 = value;
	}

	inline static int32_t get_offset_of_priority_2() { return static_cast<int32_t>(offsetof(ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861, ___priority_2)); }
	inline int32_t get_priority_2() const { return ___priority_2; }
	inline int32_t* get_address_of_priority_2() { return &___priority_2; }
	inline void set_priority_2(int32_t value)
	{
		___priority_2 = value;
	}
};


// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.DefaultExecutionOrder
struct DefaultExecutionOrder_t8495D3D4ECDFC3590621D31C3677D234D8A9BB1F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 UnityEngine.DefaultExecutionOrder::m_Order
	int32_t ___m_Order_0;

public:
	inline static int32_t get_offset_of_m_Order_0() { return static_cast<int32_t>(offsetof(DefaultExecutionOrder_t8495D3D4ECDFC3590621D31C3677D234D8A9BB1F, ___m_Order_0)); }
	inline int32_t get_m_Order_0() const { return ___m_Order_0; }
	inline int32_t* get_address_of_m_Order_0() { return &___m_Order_0; }
	inline void set_m_Order_0(int32_t value)
	{
		___m_Order_0 = value;
	}
};


// UnityEngine.DisallowMultipleComponent
struct DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// UnityEngine.ExecuteInEditMode
struct ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.Serialization.FormerlySerializedAsAttribute
struct FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.Serialization.FormerlySerializedAsAttribute::m_oldName
	String_t* ___m_oldName_0;

public:
	inline static int32_t get_offset_of_m_oldName_0() { return static_cast<int32_t>(offsetof(FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210, ___m_oldName_0)); }
	inline String_t* get_m_oldName_0() const { return ___m_oldName_0; }
	inline String_t** get_address_of_m_oldName_0() { return &___m_oldName_0; }
	inline void set_m_oldName_0(String_t* value)
	{
		___m_oldName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_oldName_0), (void*)value);
	}
};


// UnityEngine.HelpURLAttribute
struct HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.HelpURLAttribute::m_Url
	String_t* ___m_Url_0;
	// System.Boolean UnityEngine.HelpURLAttribute::m_Dispatcher
	bool ___m_Dispatcher_1;
	// System.String UnityEngine.HelpURLAttribute::m_DispatchingFieldName
	String_t* ___m_DispatchingFieldName_2;

public:
	inline static int32_t get_offset_of_m_Url_0() { return static_cast<int32_t>(offsetof(HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023, ___m_Url_0)); }
	inline String_t* get_m_Url_0() const { return ___m_Url_0; }
	inline String_t** get_address_of_m_Url_0() { return &___m_Url_0; }
	inline void set_m_Url_0(String_t* value)
	{
		___m_Url_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Url_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Dispatcher_1() { return static_cast<int32_t>(offsetof(HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023, ___m_Dispatcher_1)); }
	inline bool get_m_Dispatcher_1() const { return ___m_Dispatcher_1; }
	inline bool* get_address_of_m_Dispatcher_1() { return &___m_Dispatcher_1; }
	inline void set_m_Dispatcher_1(bool value)
	{
		___m_Dispatcher_1 = value;
	}

	inline static int32_t get_offset_of_m_DispatchingFieldName_2() { return static_cast<int32_t>(offsetof(HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023, ___m_DispatchingFieldName_2)); }
	inline String_t* get_m_DispatchingFieldName_2() const { return ___m_DispatchingFieldName_2; }
	inline String_t** get_address_of_m_DispatchingFieldName_2() { return &___m_DispatchingFieldName_2; }
	inline void set_m_DispatchingFieldName_2(String_t* value)
	{
		___m_DispatchingFieldName_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DispatchingFieldName_2), (void*)value);
	}
};


// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.PropertyAttribute
struct PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type UnityEngine.RequireComponent::m_Type0
	Type_t * ___m_Type0_0;
	// System.Type UnityEngine.RequireComponent::m_Type1
	Type_t * ___m_Type1_1;
	// System.Type UnityEngine.RequireComponent::m_Type2
	Type_t * ___m_Type2_2;

public:
	inline static int32_t get_offset_of_m_Type0_0() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type0_0)); }
	inline Type_t * get_m_Type0_0() const { return ___m_Type0_0; }
	inline Type_t ** get_address_of_m_Type0_0() { return &___m_Type0_0; }
	inline void set_m_Type0_0(Type_t * value)
	{
		___m_Type0_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type0_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type1_1() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type1_1)); }
	inline Type_t * get_m_Type1_1() const { return ___m_Type1_1; }
	inline Type_t ** get_address_of_m_Type1_1() { return &___m_Type1_1; }
	inline void set_m_Type1_1(Type_t * value)
	{
		___m_Type1_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type1_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type2_2() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type2_2)); }
	inline Type_t * get_m_Type2_2() const { return ___m_Type2_2; }
	inline Type_t ** get_address_of_m_Type2_2() { return &___m_Type2_2; }
	inline void set_m_Type2_2(Type_t * value)
	{
		___m_Type2_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type2_2), (void*)value);
	}
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// UnityEngine.MultilineAttribute
struct MultilineAttribute_tA4DCA9D8519E0C1B05636F2EFF74F81335A7F291  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Int32 UnityEngine.MultilineAttribute::lines
	int32_t ___lines_0;

public:
	inline static int32_t get_offset_of_lines_0() { return static_cast<int32_t>(offsetof(MultilineAttribute_tA4DCA9D8519E0C1B05636F2EFF74F81335A7F291, ___lines_0)); }
	inline int32_t get_lines_0() const { return ___lines_0; }
	inline int32_t* get_address_of_lines_0() { return &___lines_0; }
	inline void set_lines_0(int32_t value)
	{
		___lines_0 = value;
	}
};


// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Single UnityEngine.RangeAttribute::min
	float ___min_0;
	// System.Single UnityEngine.RangeAttribute::max
	float ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___min_0)); }
	inline float get_min_0() const { return ___min_0; }
	inline float* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(float value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___max_1)); }
	inline float get_max_1() const { return ___max_1; }
	inline float* get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(float value)
	{
		___max_1 = value;
	}
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.TooltipAttribute::tooltip
	String_t* ___tooltip_0;

public:
	inline static int32_t get_offset_of_tooltip_0() { return static_cast<int32_t>(offsetof(TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B, ___tooltip_0)); }
	inline String_t* get_tooltip_0() const { return ___tooltip_0; }
	inline String_t** get_address_of_tooltip_0() { return &___tooltip_0; }
	inline void set_tooltip_0(String_t* value)
	{
		___tooltip_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tooltip_0), (void*)value);
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void UnityEngine.RequireComponent::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4 (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * __this, Type_t * ___requiredComponent0, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.TooltipAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042 (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * __this, String_t* ___tooltip0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.RequireComponent::.ctor(System.Type,System.Type,System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RequireComponent__ctor_m1D311DE3AB4EF7FE3D80292644967218D92B00B6 (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * __this, Type_t * ___requiredComponent0, Type_t * ___requiredComponent21, Type_t * ___requiredComponent32, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.HelpURLAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215 (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * __this, String_t* ___url0, const RuntimeMethod* method);
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549 (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * __this, String_t* ___menuName0, const RuntimeMethod* method);
// System.Void UnityEngine.MultilineAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MultilineAttribute__ctor_m353DC377C95D4C341F5DDD0F9894878F64E5703E (MultilineAttribute_tA4DCA9D8519E0C1B05636F2EFF74F81335A7F291 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Serialization.FormerlySerializedAsAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * __this, String_t* ___oldName0, const RuntimeMethod* method);
// System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000 (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * __this, float ___min0, float ___max1, const RuntimeMethod* method);
// System.Void UnityEngine.HideInInspector::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9 (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * __this, const RuntimeMethod* method);
// System.Void UnityEngine.ContextMenu::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ContextMenu__ctor_m8A50A6FFBB330E0DB6F6587FBAAA7850D8DBDA7B (ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 * __this, String_t* ___itemName0, const RuntimeMethod* method);
// System.Void UnityEngine.ExecuteInEditMode::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.DisallowMultipleComponent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * __this, const RuntimeMethod* method);
// System.Void UnityEngine.DefaultExecutionOrder::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DefaultExecutionOrder__ctor_m18F4188D26702C2E3EC0AB3C1FF4AA4F5329E7A9 (DefaultExecutionOrder_t8495D3D4ECDFC3590621D31C3677D234D8A9BB1F * __this, int32_t ___order0, const RuntimeMethod* method);
static void AssemblyU2DCSharp_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[0];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[1];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[2];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
}
static void LightEstimation_t9843B4C3725AE20D953D7D4AFA849B024DC06925_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Light_tA2F349FE839781469A0344CF6039B51512394275_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(Light_tA2F349FE839781469A0344CF6039B51512394275_0_0_0_var), NULL);
	}
}
static void LightEstimation_t9843B4C3725AE20D953D7D4AFA849B024DC06925_CustomAttributesCacheGenerator_m_CameraManager(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x41\x52\x43\x61\x6D\x65\x72\x61\x4D\x61\x6E\x61\x67\x65\x72\x20\x77\x68\x69\x63\x68\x20\x77\x69\x6C\x6C\x20\x70\x72\x6F\x64\x75\x63\x65\x20\x66\x72\x61\x6D\x65\x20\x65\x76\x65\x6E\x74\x73\x20\x63\x6F\x6E\x74\x61\x69\x6E\x69\x6E\x67\x20\x6C\x69\x67\x68\x74\x20\x65\x73\x74\x69\x6D\x61\x74\x69\x6F\x6E\x20\x69\x6E\x66\x6F\x72\x6D\x61\x74\x69\x6F\x6E\x2E"), NULL);
	}
}
static void LightEstimation_t9843B4C3725AE20D953D7D4AFA849B024DC06925_CustomAttributesCacheGenerator_U3CbrightnessU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LightEstimation_t9843B4C3725AE20D953D7D4AFA849B024DC06925_CustomAttributesCacheGenerator_U3CcolorTemperatureU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LightEstimation_t9843B4C3725AE20D953D7D4AFA849B024DC06925_CustomAttributesCacheGenerator_U3CcolorCorrectionU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LightEstimation_t9843B4C3725AE20D953D7D4AFA849B024DC06925_CustomAttributesCacheGenerator_U3CmainLightDirectionU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LightEstimation_t9843B4C3725AE20D953D7D4AFA849B024DC06925_CustomAttributesCacheGenerator_U3CmainLightColorU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LightEstimation_t9843B4C3725AE20D953D7D4AFA849B024DC06925_CustomAttributesCacheGenerator_U3CmainLightIntensityLumensU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LightEstimation_t9843B4C3725AE20D953D7D4AFA849B024DC06925_CustomAttributesCacheGenerator_U3CsphericalHarmonicsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LightEstimation_t9843B4C3725AE20D953D7D4AFA849B024DC06925_CustomAttributesCacheGenerator_m_BrightnessMod(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LightEstimation_t9843B4C3725AE20D953D7D4AFA849B024DC06925_CustomAttributesCacheGenerator_LightEstimation_get_brightness_m81B8B41717C95613B6B530C7A4DA253B82D41251(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LightEstimation_t9843B4C3725AE20D953D7D4AFA849B024DC06925_CustomAttributesCacheGenerator_LightEstimation_set_brightness_mF9D7CF55B64154A0E24B60520F2D2FB0EBB03370(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LightEstimation_t9843B4C3725AE20D953D7D4AFA849B024DC06925_CustomAttributesCacheGenerator_LightEstimation_get_colorTemperature_mFC6A5877B4551367A9DA5FEBE1F809D3E97C9CA4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LightEstimation_t9843B4C3725AE20D953D7D4AFA849B024DC06925_CustomAttributesCacheGenerator_LightEstimation_set_colorTemperature_m2094B346618FB1A940F97CCD138109BB438FF420(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LightEstimation_t9843B4C3725AE20D953D7D4AFA849B024DC06925_CustomAttributesCacheGenerator_LightEstimation_get_colorCorrection_m1DE07508BFE7757692C7831FAC0E0F58BDB64941(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LightEstimation_t9843B4C3725AE20D953D7D4AFA849B024DC06925_CustomAttributesCacheGenerator_LightEstimation_set_colorCorrection_mD59CF43A84D0DE0089C83409838DE311AA902D23(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LightEstimation_t9843B4C3725AE20D953D7D4AFA849B024DC06925_CustomAttributesCacheGenerator_LightEstimation_get_mainLightDirection_m9F63CCA98C50343FB0332DD4AA330F8678A9C02C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LightEstimation_t9843B4C3725AE20D953D7D4AFA849B024DC06925_CustomAttributesCacheGenerator_LightEstimation_set_mainLightDirection_mE2E416FED9DC9D5768A8A57A02A2F90C6DE0F105(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LightEstimation_t9843B4C3725AE20D953D7D4AFA849B024DC06925_CustomAttributesCacheGenerator_LightEstimation_get_mainLightColor_m33449C2C041A80E61B6318A06C33623E6FAEB5C8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LightEstimation_t9843B4C3725AE20D953D7D4AFA849B024DC06925_CustomAttributesCacheGenerator_LightEstimation_set_mainLightColor_m701E00A2FECDFF842A1138EF07899BF23D63AAB4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LightEstimation_t9843B4C3725AE20D953D7D4AFA849B024DC06925_CustomAttributesCacheGenerator_LightEstimation_get_mainLightIntensityLumens_mDF8695D4DEEBBF8E325DF72E87AB929772E077C0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LightEstimation_t9843B4C3725AE20D953D7D4AFA849B024DC06925_CustomAttributesCacheGenerator_LightEstimation_set_mainLightIntensityLumens_m7606C8F109CBE83D3A4B8BFFD83E50134064B280(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LightEstimation_t9843B4C3725AE20D953D7D4AFA849B024DC06925_CustomAttributesCacheGenerator_LightEstimation_get_sphericalHarmonics_m6FFAA5F42C14D0620BA4B570ED63A0B3AD72EA95(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LightEstimation_t9843B4C3725AE20D953D7D4AFA849B024DC06925_CustomAttributesCacheGenerator_LightEstimation_set_sphericalHarmonics_m8E39570E4DED98CD4C5C05DC0C15B0D0F8036ED0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PlacementReticle_t70B1486B426AD43423264CD3429D6F7319CE5B76_CustomAttributesCacheGenerator_m_SnapToMesh(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlacementReticle_t70B1486B426AD43423264CD3429D6F7319CE5B76_CustomAttributesCacheGenerator_m_RaycastManager(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlacementReticle_t70B1486B426AD43423264CD3429D6F7319CE5B76_CustomAttributesCacheGenerator_m_ReticlePrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlacementReticle_t70B1486B426AD43423264CD3429D6F7319CE5B76_CustomAttributesCacheGenerator_m_DistanceScale(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlacementReticle_t70B1486B426AD43423264CD3429D6F7319CE5B76_CustomAttributesCacheGenerator_m_CameraTransform(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ProbePlacement_t4CB974E563B208E7E4AFDDA906A01E34E7827702_CustomAttributesCacheGenerator_m_ProbeManager(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ProbePlacement_t4CB974E563B208E7E4AFDDA906A01E34E7827702_CustomAttributesCacheGenerator_m_RaycastManager(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ARFeatheredPlaneMeshVisualizer_t7B2E22615047B9FDFC4D5015F59BFE2A17E3140E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ARPlaneMeshVisualizer_t4C981AECE943267AD0363C57F2F3D32273E932F2_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ARPlane_t6336725EC68CE9029844CBE72A7FE7374AD74891_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m1D311DE3AB4EF7FE3D80292644967218D92B00B6(tmp, il2cpp_codegen_type_get_object(ARPlaneMeshVisualizer_t4C981AECE943267AD0363C57F2F3D32273E932F2_0_0_0_var), il2cpp_codegen_type_get_object(MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_0_0_0_var), il2cpp_codegen_type_get_object(ARPlane_t6336725EC68CE9029844CBE72A7FE7374AD74891_0_0_0_var), NULL);
	}
}
static void ARFeatheredPlaneMeshVisualizer_t7B2E22615047B9FDFC4D5015F59BFE2A17E3140E_CustomAttributesCacheGenerator_m_FeatheringWidth(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x77\x69\x64\x74\x68\x20\x6F\x66\x20\x74\x68\x65\x20\x74\x65\x78\x74\x75\x72\x65\x20\x66\x65\x61\x74\x68\x65\x72\x69\x6E\x67\x20\x28\x69\x6E\x20\x77\x6F\x72\x6C\x64\x20\x75\x6E\x69\x74\x73\x29\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FadePlaneOnBoundaryChange_t505F6F32EC6CD33C0DAF386F378B0C0968D786CA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ARPlane_t6336725EC68CE9029844CBE72A7FE7374AD74891_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ARPlane_t6336725EC68CE9029844CBE72A7FE7374AD74891_0_0_0_var), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_0_0_0_var), NULL);
	}
}
static void PlaceObjectsOnPlane_tBC077C6ABB6A15D8E3AE99CCC9966B4E562F5417_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F_0_0_0_var), NULL);
	}
}
static void PlaceObjectsOnPlane_tBC077C6ABB6A15D8E3AE99CCC9966B4E562F5417_CustomAttributesCacheGenerator_m_PlacedPrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x73\x74\x61\x6E\x74\x69\x61\x74\x65\x73\x20\x74\x68\x69\x73\x20\x70\x72\x65\x66\x61\x62\x20\x6F\x6E\x20\x61\x20\x70\x6C\x61\x6E\x65\x20\x61\x74\x20\x74\x68\x65\x20\x74\x6F\x75\x63\x68\x20\x6C\x6F\x63\x61\x74\x69\x6F\x6E\x2E"), NULL);
	}
}
static void PlaceObjectsOnPlane_tBC077C6ABB6A15D8E3AE99CCC9966B4E562F5417_CustomAttributesCacheGenerator_U3CspawnedObjectU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PlaceObjectsOnPlane_tBC077C6ABB6A15D8E3AE99CCC9966B4E562F5417_CustomAttributesCacheGenerator_onPlacedObject(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PlaceObjectsOnPlane_tBC077C6ABB6A15D8E3AE99CCC9966B4E562F5417_CustomAttributesCacheGenerator_m_MaxNumberOfObjectsToPlace(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlaceObjectsOnPlane_tBC077C6ABB6A15D8E3AE99CCC9966B4E562F5417_CustomAttributesCacheGenerator_m_CanReposition(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlaceObjectsOnPlane_tBC077C6ABB6A15D8E3AE99CCC9966B4E562F5417_CustomAttributesCacheGenerator_PlaceObjectsOnPlane_get_spawnedObject_m0C450181B9312C9BDFEBFF7724566880F8C54F16(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PlaceObjectsOnPlane_tBC077C6ABB6A15D8E3AE99CCC9966B4E562F5417_CustomAttributesCacheGenerator_PlaceObjectsOnPlane_set_spawnedObject_m5E0C55D3C00530D85D1D71ECA5F8B076664EBAC5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PlaceObjectsOnPlane_tBC077C6ABB6A15D8E3AE99CCC9966B4E562F5417_CustomAttributesCacheGenerator_PlaceObjectsOnPlane_add_onPlacedObject_mBE87A45F356D35BEE6369C97B9C679C26988B896(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PlaceObjectsOnPlane_tBC077C6ABB6A15D8E3AE99CCC9966B4E562F5417_CustomAttributesCacheGenerator_PlaceObjectsOnPlane_remove_onPlacedObject_mD603DEC49025DCB32DEBD81498F8FF12A11DC818(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ARKitCoachingOverlay_tE7AD6D9D6A4520CB18270A5B846153A84B35F629_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ARSession_t0ECAFEF41CF9183D33E40174F849B07943DC5D3B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ARSession_t0ECAFEF41CF9183D33E40174F849B07943DC5D3B_0_0_0_var), NULL);
	}
}
static void ARKitCoachingOverlay_tE7AD6D9D6A4520CB18270A5B846153A84B35F629_CustomAttributesCacheGenerator_m_Goal(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x63\x6F\x61\x63\x68\x69\x6E\x67\x20\x67\x6F\x61\x6C\x20\x61\x73\x73\x6F\x63\x69\x61\x74\x65\x64\x20\x77\x69\x74\x68\x20\x74\x68\x65\x20\x63\x6F\x61\x63\x68\x69\x6E\x67\x20\x6F\x76\x65\x72\x6C\x61\x79\x2E"), NULL);
	}
}
static void ARKitCoachingOverlay_tE7AD6D9D6A4520CB18270A5B846153A84B35F629_CustomAttributesCacheGenerator_m_ActivatesAutomatically(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x57\x68\x65\x74\x68\x65\x72\x20\x74\x68\x65\x20\x63\x6F\x61\x63\x68\x69\x6E\x67\x20\x6F\x76\x65\x72\x6C\x61\x79\x20\x61\x63\x74\x69\x76\x61\x74\x65\x73\x20\x61\x75\x74\x6F\x6D\x61\x74\x69\x63\x61\x6C\x6C\x79\x2E"), NULL);
	}
}
static void ARUXAnimationManager_tBFA785140C2D44C6B8F4AAB9540BD574B1013FA0_CustomAttributesCacheGenerator_m_InstructionText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x73\x74\x72\x75\x63\x74\x69\x6F\x6E\x61\x6C\x20\x74\x65\x73\x74\x20\x66\x6F\x72\x20\x76\x69\x73\x75\x61\x6C\x20\x55\x49"), NULL);
	}
}
static void ARUXAnimationManager_tBFA785140C2D44C6B8F4AAB9540BD574B1013FA0_CustomAttributesCacheGenerator_m_FindAPlaneClip(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x76\x65\x20\x64\x65\x76\x69\x63\x65\x20\x61\x6E\x69\x6D\x61\x74\x69\x6F\x6E"), NULL);
	}
}
static void ARUXAnimationManager_tBFA785140C2D44C6B8F4AAB9540BD574B1013FA0_CustomAttributesCacheGenerator_m_TapToPlaceClip(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x61\x70\x20\x74\x6F\x20\x70\x6C\x61\x63\x65\x20\x61\x6E\x69\x6D\x61\x74\x69\x6F\x6E"), NULL);
	}
}
static void ARUXAnimationManager_tBFA785140C2D44C6B8F4AAB9540BD574B1013FA0_CustomAttributesCacheGenerator_m_FindImageClip(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x46\x69\x6E\x64\x20\x43\x6C\x69\x70\x20\x61\x6E\x69\x6D\x61\x74\x69\x6F\x6E"), NULL);
	}
}
static void ARUXAnimationManager_tBFA785140C2D44C6B8F4AAB9540BD574B1013FA0_CustomAttributesCacheGenerator_m_FindBodyClip(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x46\x69\x6E\x64\x20\x62\x6F\x64\x79\x20\x61\x6E\x69\x6D\x61\x74\x69\x6F\x6E"), NULL);
	}
}
static void ARUXAnimationManager_tBFA785140C2D44C6B8F4AAB9540BD574B1013FA0_CustomAttributesCacheGenerator_m_FindObjectClip(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x46\x69\x6E\x64\x20\x6F\x62\x6A\x65\x63\x74\x20\x61\x6E\x69\x6D\x61\x74\x69\x6F\x6E"), NULL);
	}
}
static void ARUXAnimationManager_tBFA785140C2D44C6B8F4AAB9540BD574B1013FA0_CustomAttributesCacheGenerator_m_FindFaceClip(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x46\x69\x6E\x64\x20\x66\x61\x63\x65\x20\x61\x6E\x69\x6D\x61\x74\x69\x6F\x6E"), NULL);
	}
}
static void ARUXAnimationManager_tBFA785140C2D44C6B8F4AAB9540BD574B1013FA0_CustomAttributesCacheGenerator_m_CoachingOverlay(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x52\x4B\x69\x74\x20\x43\x6F\x61\x63\x68\x69\x6E\x67\x20\x6F\x76\x65\x72\x6C\x61\x79\x20\x72\x65\x66\x65\x72\x65\x6E\x63\x65"), NULL);
	}
}
static void ARUXAnimationManager_tBFA785140C2D44C6B8F4AAB9540BD574B1013FA0_CustomAttributesCacheGenerator_m_VideoPlayer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x56\x69\x64\x65\x6F\x20\x70\x6C\x61\x79\x65\x72\x20\x72\x65\x66\x65\x72\x65\x6E\x63\x65"), NULL);
	}
}
static void ARUXAnimationManager_tBFA785140C2D44C6B8F4AAB9540BD574B1013FA0_CustomAttributesCacheGenerator_m_RawImage(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x52\x61\x77\x20\x69\x6D\x61\x67\x65\x20\x75\x73\x65\x64\x20\x66\x6F\x72\x20\x76\x69\x64\x65\x6F\x70\x6C\x61\x79\x65\x72\x20\x72\x65\x66\x65\x72\x65\x6E\x63\x65"), NULL);
	}
}
static void ARUXAnimationManager_tBFA785140C2D44C6B8F4AAB9540BD574B1013FA0_CustomAttributesCacheGenerator_m_FadeOnDuration(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x69\x6D\x65\x20\x74\x68\x65\x20\x55\x49\x20\x74\x61\x6B\x65\x73\x20\x74\x6F\x20\x66\x61\x64\x65\x20\x6F\x6E"), NULL);
	}
}
static void ARUXAnimationManager_tBFA785140C2D44C6B8F4AAB9540BD574B1013FA0_CustomAttributesCacheGenerator_m_FadeOffDuration(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x69\x6D\x65\x20\x74\x68\x65\x20\x55\x49\x20\x74\x61\x6B\x65\x73\x20\x74\x6F\x20\x66\x61\x64\x65\x20\x6F\x66\x66"), NULL);
	}
}
static void ARUXAnimationManager_tBFA785140C2D44C6B8F4AAB9540BD574B1013FA0_CustomAttributesCacheGenerator_onFadeOffComplete(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ARUXAnimationManager_tBFA785140C2D44C6B8F4AAB9540BD574B1013FA0_CustomAttributesCacheGenerator_m_Transparent(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ARUXAnimationManager_tBFA785140C2D44C6B8F4AAB9540BD574B1013FA0_CustomAttributesCacheGenerator_m_LocalizationManager(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ARUXAnimationManager_tBFA785140C2D44C6B8F4AAB9540BD574B1013FA0_CustomAttributesCacheGenerator_m_LocalizeText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ARUXAnimationManager_tBFA785140C2D44C6B8F4AAB9540BD574B1013FA0_CustomAttributesCacheGenerator_ARUXAnimationManager_add_onFadeOffComplete_m988A1D4D19242C1067E194A5BA32C5D1988C282D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ARUXAnimationManager_tBFA785140C2D44C6B8F4AAB9540BD574B1013FA0_CustomAttributesCacheGenerator_ARUXAnimationManager_remove_onFadeOffComplete_m7D39849FBC6A57AB6D63D48A5E7B3DF2695D89DB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ARUXReasonsManager_t96AAD657A399018801E18CAEE6C4BAFC4C4704EB_CustomAttributesCacheGenerator_m_ShowNotTrackingReasons(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ARUXReasonsManager_t96AAD657A399018801E18CAEE6C4BAFC4C4704EB_CustomAttributesCacheGenerator_m_ReasonDisplayText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ARUXReasonsManager_t96AAD657A399018801E18CAEE6C4BAFC4C4704EB_CustomAttributesCacheGenerator_m_ReasonParent(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ARUXReasonsManager_t96AAD657A399018801E18CAEE6C4BAFC4C4704EB_CustomAttributesCacheGenerator_m_ReasonIcon(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ARUXReasonsManager_t96AAD657A399018801E18CAEE6C4BAFC4C4704EB_CustomAttributesCacheGenerator_m_InitRelocalSprite(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ARUXReasonsManager_t96AAD657A399018801E18CAEE6C4BAFC4C4704EB_CustomAttributesCacheGenerator_m_MotionSprite(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ARUXReasonsManager_t96AAD657A399018801E18CAEE6C4BAFC4C4704EB_CustomAttributesCacheGenerator_m_LightSprite(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ARUXReasonsManager_t96AAD657A399018801E18CAEE6C4BAFC4C4704EB_CustomAttributesCacheGenerator_m_FeaturesSprite(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ARUXReasonsManager_t96AAD657A399018801E18CAEE6C4BAFC4C4704EB_CustomAttributesCacheGenerator_m_UnsupportedSprite(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ARUXReasonsManager_t96AAD657A399018801E18CAEE6C4BAFC4C4704EB_CustomAttributesCacheGenerator_m_NoneSprite(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ARUXReasonsManager_t96AAD657A399018801E18CAEE6C4BAFC4C4704EB_CustomAttributesCacheGenerator_m_LocalizationManager(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ARUXReasonsManager_t96AAD657A399018801E18CAEE6C4BAFC4C4704EB_CustomAttributesCacheGenerator_m_LocalizeText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DisableTrackedVisuals_t9D362704CB23AA8F6AD79808444DB585E268A915_CustomAttributesCacheGenerator_m_DisableFeaturePoints(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x69\x73\x61\x62\x6C\x65\x73\x20\x73\x70\x61\x77\x6E\x65\x64\x20\x66\x65\x61\x74\x75\x72\x65\x20\x70\x6F\x69\x6E\x74\x73\x20\x61\x6E\x64\x20\x74\x68\x65\x20\x41\x52\x50\x6F\x69\x6E\x74\x43\x6C\x6F\x75\x64\x4D\x61\x6E\x61\x67\x65\x72"), NULL);
	}
}
static void DisableTrackedVisuals_t9D362704CB23AA8F6AD79808444DB585E268A915_CustomAttributesCacheGenerator_m_DisablePlaneRendering(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x69\x73\x61\x62\x6C\x65\x73\x20\x73\x70\x61\x77\x6E\x65\x64\x20\x70\x6C\x61\x6E\x65\x73\x20\x61\x6E\x64\x20\x41\x52\x50\x6C\x61\x6E\x65\x4D\x61\x6E\x61\x67\x65\x72"), NULL);
	}
}
static void DisableTrackedVisuals_t9D362704CB23AA8F6AD79808444DB585E268A915_CustomAttributesCacheGenerator_m_PointCloudManager(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DisableTrackedVisuals_t9D362704CB23AA8F6AD79808444DB585E268A915_CustomAttributesCacheGenerator_m_PlaneManager(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LocalizationManager_t9A271A1BB9769E489F800511B98E6F10E345C36C_CustomAttributesCacheGenerator_m_SimplifiedChineseFont(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LocalizationManager_t9A271A1BB9769E489F800511B98E6F10E345C36C_CustomAttributesCacheGenerator_m_JapaneseFont(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LocalizationManager_t9A271A1BB9769E489F800511B98E6F10E345C36C_CustomAttributesCacheGenerator_m_KoreanFont(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LocalizationManager_t9A271A1BB9769E489F800511B98E6F10E345C36C_CustomAttributesCacheGenerator_m_HindiFont(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LocalizationManager_t9A271A1BB9769E489F800511B98E6F10E345C36C_CustomAttributesCacheGenerator_m_InstructionText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LocalizationManager_t9A271A1BB9769E489F800511B98E6F10E345C36C_CustomAttributesCacheGenerator_m_ReasonText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LocalizationManager_t9A271A1BB9769E489F800511B98E6F10E345C36C_CustomAttributesCacheGenerator_LocalizationManager_Start_mCCC2E00354521D23FD5CBDB28FD347B27956BC61(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__55_tAD522FF1E9F588E49FF94436005D673167775F79_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__55_tAD522FF1E9F588E49FF94436005D673167775F79_0_0_0_var), NULL);
	}
}
static void U3CStartU3Ed__55_tAD522FF1E9F588E49FF94436005D673167775F79_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__55_tAD522FF1E9F588E49FF94436005D673167775F79_CustomAttributesCacheGenerator_U3CStartU3Ed__55__ctor_m987504CCE2A04E56489F015DDF3279DC89C0F080(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__55_tAD522FF1E9F588E49FF94436005D673167775F79_CustomAttributesCacheGenerator_U3CStartU3Ed__55_System_IDisposable_Dispose_m9728BAF46DD084B007DFFE1D4F6F87B2DB1E1554(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__55_tAD522FF1E9F588E49FF94436005D673167775F79_CustomAttributesCacheGenerator_U3CStartU3Ed__55_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3EC035A1A71FEA9171B22B400ABDAB7FFF2FB3ED(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__55_tAD522FF1E9F588E49FF94436005D673167775F79_CustomAttributesCacheGenerator_U3CStartU3Ed__55_System_Collections_IEnumerator_Reset_mE1713764C96D07B62E7DE7F5EA52D344AC64C15A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__55_tAD522FF1E9F588E49FF94436005D673167775F79_CustomAttributesCacheGenerator_U3CStartU3Ed__55_System_Collections_IEnumerator_get_Current_m1F76D144731727E498C37AD026E5F6D941A40D57(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_m_StartWithInstructionalUI(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_m_InstructionalUI(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_m_InstructionalGoal(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_m_ShowSecondaryInstructionalUI(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_m_SecondaryInstructionUI(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_m_SecondaryGoal(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_m_CoachingOverlayFallback(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x46\x61\x6C\x6C\x62\x61\x63\x6B\x20\x74\x6F\x20\x63\x72\x6F\x73\x73\x20\x70\x6C\x61\x74\x66\x6F\x72\x6D\x20\x55\x49\x20\x69\x66\x20\x41\x52\x4B\x69\x74\x20\x63\x6F\x61\x63\x68\x69\x6E\x67\x20\x6F\x76\x65\x72\x6C\x61\x79\x20\x69\x73\x20\x6E\x6F\x74\x20\x73\x75\x70\x70\x6F\x72\x74\x65\x64"), NULL);
	}
}
static void UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_m_ARSessionOrigin(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_m_PlaneManager(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_m_FaceManager(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_m_BodyManager(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_m_ImageManager(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_m_ObjectManager(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_m_AnimationManager(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_m_LocalizationManager(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_UIManager_U3COnEnableU3Eb__69_0_m6E36F214C2EC57CB40BEDD772DA012EE40D56CBC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_tF3B5E6F42A0418BAF09788FA0CE0E381B0383490_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SceneController_t8611277038809278C31AE35555719D7AE5438CA9_CustomAttributesCacheGenerator_SceneController_EnableBatsman_m583C8D3A4A1A27E0F5E121DCD72731F8CC063DA5(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CEnableBatsmanU3Ed__6_t9F28F6ED1F3B9EFF2BF57316D59057D3F25751C3_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CEnableBatsmanU3Ed__6_t9F28F6ED1F3B9EFF2BF57316D59057D3F25751C3_0_0_0_var), NULL);
	}
}
static void U3CEnableBatsmanU3Ed__6_t9F28F6ED1F3B9EFF2BF57316D59057D3F25751C3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CEnableBatsmanU3Ed__6_t9F28F6ED1F3B9EFF2BF57316D59057D3F25751C3_CustomAttributesCacheGenerator_U3CEnableBatsmanU3Ed__6__ctor_mEEC4B8F973D62718060076D669597F838C324FD9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEnableBatsmanU3Ed__6_t9F28F6ED1F3B9EFF2BF57316D59057D3F25751C3_CustomAttributesCacheGenerator_U3CEnableBatsmanU3Ed__6_System_IDisposable_Dispose_mB0483598E83315D30E92EAAD43D5AF10D57FBF54(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEnableBatsmanU3Ed__6_t9F28F6ED1F3B9EFF2BF57316D59057D3F25751C3_CustomAttributesCacheGenerator_U3CEnableBatsmanU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3679DAB9B0B5B9B44605F19D1D8D3ECB2DA8AE26(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEnableBatsmanU3Ed__6_t9F28F6ED1F3B9EFF2BF57316D59057D3F25751C3_CustomAttributesCacheGenerator_U3CEnableBatsmanU3Ed__6_System_Collections_IEnumerator_Reset_m5A6F7084CEF295AA235E9F6D9241FFA8D7D394B5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEnableBatsmanU3Ed__6_t9F28F6ED1F3B9EFF2BF57316D59057D3F25751C3_CustomAttributesCacheGenerator_U3CEnableBatsmanU3Ed__6_System_Collections_IEnumerator_get_Current_m3ACB8AAEC04E99894B01D1FDEA401A4A881093DD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void LeanInfoText_t508B956A97587F4ECF20EE95947A49736F76D477_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x23\x4C\x65\x61\x6E\x49\x6E\x66\x6F\x54\x65\x78\x74"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x49\x6E\x66\x6F\x20\x54\x65\x78\x74"), NULL);
	}
}
static void LeanInfoText_t508B956A97587F4ECF20EE95947A49736F76D477_CustomAttributesCacheGenerator_Format(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x66\x69\x6E\x61\x6C\x20\x74\x65\x78\x74\x20\x77\x69\x6C\x6C\x20\x75\x73\x65\x20\x74\x68\x69\x73\x20\x73\x74\x72\x69\x6E\x67\x20\x66\x6F\x72\x6D\x61\x74\x74\x69\x6E\x67\x2E"), NULL);
	}
	{
		MultilineAttribute_tA4DCA9D8519E0C1B05636F2EFF74F81335A7F291 * tmp = (MultilineAttribute_tA4DCA9D8519E0C1B05636F2EFF74F81335A7F291 *)cache->attributes[1];
		MultilineAttribute__ctor_m353DC377C95D4C341F5DDD0F9894878F64E5703E(tmp, NULL);
	}
}
static void LeanPulseScale_t984988192A1106BE7E5B31BAC1DA86CDE07F2A4E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x23\x4C\x65\x61\x6E\x50\x75\x6C\x73\x65\x53\x63\x61\x6C\x65"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x50\x75\x6C\x73\x65\x20\x53\x63\x61\x6C\x65"), NULL);
	}
}
static void LeanPulseScale_t984988192A1106BE7E5B31BAC1DA86CDE07F2A4E_CustomAttributesCacheGenerator_BaseScale(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x74\x72\x61\x6E\x73\x66\x6F\x72\x6D\x2E\x6C\x6F\x63\x61\x6C\x53\x63\x61\x6C\x65\x20\x62\x65\x66\x6F\x72\x65\x20\x70\x75\x6C\x73\x69\x6E\x67\x2E"), NULL);
	}
}
static void LeanPulseScale_t984988192A1106BE7E5B31BAC1DA86CDE07F2A4E_CustomAttributesCacheGenerator_Size(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x63\x75\x72\x72\x65\x6E\x74\x20\x73\x63\x61\x6C\x65\x20\x6D\x75\x6C\x74\x69\x70\x6C\x69\x65\x72\x2E"), NULL);
	}
}
static void LeanPulseScale_t984988192A1106BE7E5B31BAC1DA86CDE07F2A4E_CustomAttributesCacheGenerator_PulseInterval(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x69\x6E\x74\x65\x72\x76\x61\x6C\x20\x62\x65\x74\x77\x65\x65\x6E\x20\x65\x61\x63\x68\x20\x70\x75\x6C\x73\x65\x20\x69\x6E\x20\x73\x65\x63\x6F\x6E\x64\x73\x2E"), NULL);
	}
}
static void LeanPulseScale_t984988192A1106BE7E5B31BAC1DA86CDE07F2A4E_CustomAttributesCacheGenerator_PulseSize(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x61\x6D\x6F\x75\x6E\x74\x20\x53\x69\x7A\x65\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x69\x6E\x63\x72\x65\x6D\x65\x6E\x74\x65\x64\x20\x65\x61\x63\x68\x20\x70\x75\x6C\x73\x65\x2E"), NULL);
	}
}
static void LeanPulseScale_t984988192A1106BE7E5B31BAC1DA86CDE07F2A4E_CustomAttributesCacheGenerator_Damping(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x79\x6F\x75\x20\x77\x61\x6E\x74\x20\x74\x68\x69\x73\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x20\x74\x6F\x20\x63\x68\x61\x6E\x67\x65\x20\x73\x6D\x6F\x6F\x74\x68\x6C\x79\x20\x6F\x76\x65\x72\x20\x74\x69\x6D\x65\x2C\x20\x74\x68\x65\x6E\x20\x74\x68\x69\x73\x20\x61\x6C\x6C\x6F\x77\x73\x20\x79\x6F\x75\x20\x74\x6F\x20\x63\x6F\x6E\x74\x72\x6F\x6C\x20\x68\x6F\x77\x20\x71\x75\x69\x63\x6B\x20\x74\x68\x65\x20\x63\x68\x61\x6E\x67\x65\x73\x20\x72\x65\x61\x63\x68\x20\x74\x68\x65\x69\x72\x20\x74\x61\x72\x67\x65\x74\x20\x76\x61\x6C\x75\x65\x2E\xA\xA\x2D\x31\x20\x3D\x20\x49\x6E\x73\x74\x61\x6E\x74\x6C\x79\x20\x63\x68\x61\x6E\x67\x65\x2E\xA\xA\x31\x20\x3D\x20\x53\x6C\x6F\x77\x6C\x79\x20\x63\x68\x61\x6E\x67\x65\x2E\xA\xA\x31\x30\x20\x3D\x20\x51\x75\x69\x63\x6B\x6C\x79\x20\x63\x68\x61\x6E\x67\x65\x2E"), NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x61\x6D\x70\x65\x6E\x69\x6E\x67"), NULL);
	}
}
static void LeanTouchEvents_t1C2799365501C570FC4F5A45761CEF3330D4B12B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x23\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x45\x76\x65\x6E\x74\x73"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x54\x6F\x75\x63\x68\x20\x45\x76\x65\x6E\x74\x73"), NULL);
	}
}
static void LeanDragCamera_t16CF0D4D777F2F3B0C5A76111C8BA13A2698419E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x23\x4C\x65\x61\x6E\x44\x72\x61\x67\x43\x61\x6D\x65\x72\x61"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x44\x72\x61\x67\x20\x43\x61\x6D\x65\x72\x61"), NULL);
	}
}
static void LeanDragCamera_t16CF0D4D777F2F3B0C5A76111C8BA13A2698419E_CustomAttributesCacheGenerator_Sensitivity(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x6D\x6F\x76\x65\x6D\x65\x6E\x74\x20\x73\x70\x65\x65\x64\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x6D\x75\x6C\x74\x69\x70\x6C\x69\x65\x64\x20\x62\x79\x20\x74\x68\x69\x73\x2E\xA\xA\x2D\x31\x20\x3D\x20\x49\x6E\x76\x65\x72\x74\x65\x64\x20\x43\x6F\x6E\x74\x72\x6F\x6C\x73\x2E"), NULL);
	}
}
static void LeanDragCamera_t16CF0D4D777F2F3B0C5A76111C8BA13A2698419E_CustomAttributesCacheGenerator_Damping(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x79\x6F\x75\x20\x77\x61\x6E\x74\x20\x74\x68\x69\x73\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x20\x74\x6F\x20\x63\x68\x61\x6E\x67\x65\x20\x73\x6D\x6F\x6F\x74\x68\x6C\x79\x20\x6F\x76\x65\x72\x20\x74\x69\x6D\x65\x2C\x20\x74\x68\x65\x6E\x20\x74\x68\x69\x73\x20\x61\x6C\x6C\x6F\x77\x73\x20\x79\x6F\x75\x20\x74\x6F\x20\x63\x6F\x6E\x74\x72\x6F\x6C\x20\x68\x6F\x77\x20\x71\x75\x69\x63\x6B\x20\x74\x68\x65\x20\x63\x68\x61\x6E\x67\x65\x73\x20\x72\x65\x61\x63\x68\x20\x74\x68\x65\x69\x72\x20\x74\x61\x72\x67\x65\x74\x20\x76\x61\x6C\x75\x65\x2E\xA\xA\x2D\x31\x20\x3D\x20\x49\x6E\x73\x74\x61\x6E\x74\x6C\x79\x20\x63\x68\x61\x6E\x67\x65\x2E\xA\xA\x31\x20\x3D\x20\x53\x6C\x6F\x77\x6C\x79\x20\x63\x68\x61\x6E\x67\x65\x2E\xA\xA\x31\x30\x20\x3D\x20\x51\x75\x69\x63\x6B\x6C\x79\x20\x63\x68\x61\x6E\x67\x65\x2E"), NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x61\x6D\x70\x65\x6E\x69\x6E\x67"), NULL);
	}
}
static void LeanDragCamera_t16CF0D4D777F2F3B0C5A76111C8BA13A2698419E_CustomAttributesCacheGenerator_Inertia(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x61\x6C\x6C\x6F\x77\x73\x20\x79\x6F\x75\x20\x74\x6F\x20\x63\x6F\x6E\x74\x72\x6F\x6C\x20\x68\x6F\x77\x20\x6D\x75\x63\x68\x20\x6D\x6F\x6D\x65\x6E\x75\x6D\x20\x69\x73\x20\x72\x65\x74\x61\x69\x6E\x65\x64\x20\x77\x68\x65\x6E\x20\x74\x68\x65\x20\x64\x72\x61\x67\x67\x69\x6E\x67\x20\x66\x69\x6E\x67\x65\x72\x73\x20\x61\x72\x65\x20\x61\x6C\x6C\x20\x72\x65\x6C\x65\x61\x73\x65\x64\x2E\xA\xA\x4E\x4F\x54\x45\x3A\x20\x54\x68\x69\x73\x20\x72\x65\x71\x75\x69\x72\x65\x73\x20\x3C\x62\x3E\x44\x61\x6D\x70\x65\x6E\x69\x6E\x67\x3C\x2F\x62\x3E\x20\x74\x6F\x20\x62\x65\x20\x61\x62\x6F\x76\x65\x20\x30\x2E"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void LeanDragCamera_t16CF0D4D777F2F3B0C5A76111C8BA13A2698419E_CustomAttributesCacheGenerator_remainingDelta(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanDragCamera_t16CF0D4D777F2F3B0C5A76111C8BA13A2698419E_CustomAttributesCacheGenerator_LeanDragCamera_MoveToSelection_mC17CA5F136F00D57EDEF2F3193F53116ABC918D6(CustomAttributesCache* cache)
{
	{
		ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 * tmp = (ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 *)cache->attributes[0];
		ContextMenu__ctor_m8A50A6FFBB330E0DB6F6587FBAAA7850D8DBDA7B(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x76\x65\x20\x54\x6F\x20\x53\x65\x6C\x65\x63\x74\x69\x6F\x6E"), NULL);
	}
}
static void LeanDragTrail_t72B928B6E60E86D0845027C361672322C2305841_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x23\x4C\x65\x61\x6E\x44\x72\x61\x67\x54\x72\x61\x69\x6C"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[2];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x44\x72\x61\x67\x20\x54\x72\x61\x69\x6C"), NULL);
	}
}
static void LeanDragTrail_t72B928B6E60E86D0845027C361672322C2305841_CustomAttributesCacheGenerator_fingerDatas(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void LeanDragTranslate_tB311852063395381489EFEB52FC6849E0350EFBC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x23\x4C\x65\x61\x6E\x44\x72\x61\x67\x54\x72\x61\x6E\x73\x6C\x61\x74\x65"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x44\x72\x61\x67\x20\x54\x72\x61\x6E\x73\x6C\x61\x74\x65"), NULL);
	}
}
static void LeanDragTranslate_tB311852063395381489EFEB52FC6849E0350EFBC_CustomAttributesCacheGenerator_Camera(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x63\x61\x6D\x65\x72\x61\x20\x74\x68\x65\x20\x74\x72\x61\x6E\x73\x6C\x61\x74\x69\x6F\x6E\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x63\x61\x6C\x63\x75\x6C\x61\x74\x65\x64\x20\x75\x73\x69\x6E\x67\x2E\xA\xA\x4E\x6F\x6E\x65\x20\x3D\x20\x4D\x61\x69\x6E\x43\x61\x6D\x65\x72\x61\x2E"), NULL);
	}
}
static void LeanDragTranslate_tB311852063395381489EFEB52FC6849E0350EFBC_CustomAttributesCacheGenerator_Sensitivity(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x73\x65\x6E\x73\x69\x74\x69\x76\x69\x74\x79\x20\x6F\x66\x20\x74\x68\x65\x20\x74\x72\x61\x6E\x73\x6C\x61\x74\x69\x6F\x6E\x2E\xA\xA\x31\x20\x3D\x20\x44\x65\x66\x61\x75\x6C\x74\x2E\xA\x32\x20\x3D\x20\x44\x6F\x75\x62\x6C\x65\x2E"), NULL);
	}
}
static void LeanDragTranslate_tB311852063395381489EFEB52FC6849E0350EFBC_CustomAttributesCacheGenerator_Damping(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x79\x6F\x75\x20\x77\x61\x6E\x74\x20\x74\x68\x69\x73\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x20\x74\x6F\x20\x63\x68\x61\x6E\x67\x65\x20\x73\x6D\x6F\x6F\x74\x68\x6C\x79\x20\x6F\x76\x65\x72\x20\x74\x69\x6D\x65\x2C\x20\x74\x68\x65\x6E\x20\x74\x68\x69\x73\x20\x61\x6C\x6C\x6F\x77\x73\x20\x79\x6F\x75\x20\x74\x6F\x20\x63\x6F\x6E\x74\x72\x6F\x6C\x20\x68\x6F\x77\x20\x71\x75\x69\x63\x6B\x20\x74\x68\x65\x20\x63\x68\x61\x6E\x67\x65\x73\x20\x72\x65\x61\x63\x68\x20\x74\x68\x65\x69\x72\x20\x74\x61\x72\x67\x65\x74\x20\x76\x61\x6C\x75\x65\x2E\xA\xA\x2D\x31\x20\x3D\x20\x49\x6E\x73\x74\x61\x6E\x74\x6C\x79\x20\x63\x68\x61\x6E\x67\x65\x2E\xA\xA\x31\x20\x3D\x20\x53\x6C\x6F\x77\x6C\x79\x20\x63\x68\x61\x6E\x67\x65\x2E\xA\xA\x31\x30\x20\x3D\x20\x51\x75\x69\x63\x6B\x6C\x79\x20\x63\x68\x61\x6E\x67\x65\x2E"), NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x61\x6D\x70\x65\x6E\x69\x6E\x67"), NULL);
	}
}
static void LeanDragTranslate_tB311852063395381489EFEB52FC6849E0350EFBC_CustomAttributesCacheGenerator_Inertia(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x61\x6C\x6C\x6F\x77\x73\x20\x79\x6F\x75\x20\x74\x6F\x20\x63\x6F\x6E\x74\x72\x6F\x6C\x20\x68\x6F\x77\x20\x6D\x75\x63\x68\x20\x6D\x6F\x6D\x65\x6E\x75\x6D\x20\x69\x73\x20\x72\x65\x74\x61\x69\x6E\x65\x64\x20\x77\x68\x65\x6E\x20\x74\x68\x65\x20\x64\x72\x61\x67\x67\x69\x6E\x67\x20\x66\x69\x6E\x67\x65\x72\x73\x20\x61\x72\x65\x20\x61\x6C\x6C\x20\x72\x65\x6C\x65\x61\x73\x65\x64\x2E\xA\xA\x4E\x4F\x54\x45\x3A\x20\x54\x68\x69\x73\x20\x72\x65\x71\x75\x69\x72\x65\x73\x20\x3C\x62\x3E\x44\x61\x6D\x70\x65\x6E\x69\x6E\x67\x3C\x2F\x62\x3E\x20\x74\x6F\x20\x62\x65\x20\x61\x62\x6F\x76\x65\x20\x30\x2E"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void LeanDragTranslate_tB311852063395381489EFEB52FC6849E0350EFBC_CustomAttributesCacheGenerator_remainingTranslation(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanFingerDown_tBB4309FE0B73C4438020A3F4469626D1191939F2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x23\x4C\x65\x61\x6E\x46\x69\x6E\x67\x65\x72\x44\x6F\x77\x6E"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x46\x69\x6E\x67\x65\x72\x20\x44\x6F\x77\x6E"), NULL);
	}
}
static void LeanFingerDown_tBB4309FE0B73C4438020A3F4469626D1191939F2_CustomAttributesCacheGenerator_onFinger(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x6F\x6E\x44\x6F\x77\x6E"), NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x6E\x44\x6F\x77\x6E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[2];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanFingerDown_tBB4309FE0B73C4438020A3F4469626D1191939F2_CustomAttributesCacheGenerator_onWorld(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x6F\x6E\x50\x6F\x73\x69\x74\x69\x6F\x6E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanFingerDown_tBB4309FE0B73C4438020A3F4469626D1191939F2_CustomAttributesCacheGenerator_onScreen(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanFingerOld_t37E4543AACC95F6C05B7094CBCFE2541FA5D959F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x23\x4C\x65\x61\x6E\x46\x69\x6E\x67\x65\x72\x4F\x6C\x64"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x46\x69\x6E\x67\x65\x72\x20\x4F\x6C\x64"), NULL);
	}
}
static void LeanFingerOld_t37E4543AACC95F6C05B7094CBCFE2541FA5D959F_CustomAttributesCacheGenerator_onFinger(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanFingerOld_t37E4543AACC95F6C05B7094CBCFE2541FA5D959F_CustomAttributesCacheGenerator_onWorld(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x6F\x6E\x50\x6F\x73\x69\x74\x69\x6F\x6E"), NULL);
	}
}
static void LeanFingerOld_t37E4543AACC95F6C05B7094CBCFE2541FA5D959F_CustomAttributesCacheGenerator_onScreen(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanFingerSwipe_t4E40924EA323353C64A2F62A645C5EB8B4FB2912_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x23\x4C\x65\x61\x6E\x46\x69\x6E\x67\x65\x72\x53\x77\x69\x70\x65"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x46\x69\x6E\x67\x65\x72\x20\x53\x77\x69\x70\x65"), NULL);
	}
}
static void LeanFingerTap_t505333CF479CE38899CFC21A1943D0F3F0196CD7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x23\x4C\x65\x61\x6E\x46\x69\x6E\x67\x65\x72\x54\x61\x70"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x46\x69\x6E\x67\x65\x72\x20\x54\x61\x70"), NULL);
	}
}
static void LeanFingerTap_t505333CF479CE38899CFC21A1943D0F3F0196CD7_CustomAttributesCacheGenerator_onFinger(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x6F\x6E\x54\x61\x70"), NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x6E\x54\x61\x70"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[2];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanFingerTap_t505333CF479CE38899CFC21A1943D0F3F0196CD7_CustomAttributesCacheGenerator_onCount(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanFingerTap_t505333CF479CE38899CFC21A1943D0F3F0196CD7_CustomAttributesCacheGenerator_onWorld(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x6F\x6E\x50\x6F\x73\x69\x74\x69\x6F\x6E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanFingerTap_t505333CF479CE38899CFC21A1943D0F3F0196CD7_CustomAttributesCacheGenerator_onScreen(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanFingerUp_t8F38426263A3E2369766D376423034C82F9F5862_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x23\x4C\x65\x61\x6E\x46\x69\x6E\x67\x65\x72\x55\x70"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x46\x69\x6E\x67\x65\x72\x20\x55\x70"), NULL);
	}
}
static void LeanFingerUp_t8F38426263A3E2369766D376423034C82F9F5862_CustomAttributesCacheGenerator_onFinger(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x6F\x6E\x55\x70"), NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x6E\x55\x70"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[2];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanFingerUp_t8F38426263A3E2369766D376423034C82F9F5862_CustomAttributesCacheGenerator_onWorld(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x6F\x6E\x50\x6F\x73\x69\x74\x69\x6F\x6E"), NULL);
	}
}
static void LeanFingerUp_t8F38426263A3E2369766D376423034C82F9F5862_CustomAttributesCacheGenerator_onScreen(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanFingerUpdate_tA215228160659B8EA8BF59A748C9C44DC5DAB897_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x23\x4C\x65\x61\x6E\x46\x69\x6E\x67\x65\x72\x55\x70\x64\x61\x74\x65"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x46\x69\x6E\x67\x65\x72\x20\x55\x70\x64\x61\x74\x65"), NULL);
	}
}
static void LeanFingerUpdate_tA215228160659B8EA8BF59A748C9C44DC5DAB897_CustomAttributesCacheGenerator_onFinger(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x6F\x6E\x44\x72\x61\x67"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanFingerUpdate_tA215228160659B8EA8BF59A748C9C44DC5DAB897_CustomAttributesCacheGenerator_onDelta(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x6F\x6E\x44\x72\x61\x67\x44\x65\x6C\x74\x61"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanFingerUpdate_tA215228160659B8EA8BF59A748C9C44DC5DAB897_CustomAttributesCacheGenerator_onDistance(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanFingerUpdate_tA215228160659B8EA8BF59A748C9C44DC5DAB897_CustomAttributesCacheGenerator_onWorldFrom(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanFingerUpdate_tA215228160659B8EA8BF59A748C9C44DC5DAB897_CustomAttributesCacheGenerator_onWorldTo(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanFingerUpdate_tA215228160659B8EA8BF59A748C9C44DC5DAB897_CustomAttributesCacheGenerator_onWorldDelta(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanFingerUpdate_tA215228160659B8EA8BF59A748C9C44DC5DAB897_CustomAttributesCacheGenerator_onWorldFromTo(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanPinchScale_t06F48876FB3BF619EF69F2342975AB52C847FFF3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x23\x4C\x65\x61\x6E\x50\x69\x6E\x63\x68\x53\x63\x61\x6C\x65"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x50\x69\x6E\x63\x68\x20\x53\x63\x61\x6C\x65"), NULL);
	}
}
static void LeanPinchScale_t06F48876FB3BF619EF69F2342975AB52C847FFF3_CustomAttributesCacheGenerator_Camera(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x63\x61\x6D\x65\x72\x61\x20\x74\x68\x61\x74\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x75\x73\x65\x64\x20\x74\x6F\x20\x63\x61\x6C\x63\x75\x6C\x61\x74\x65\x20\x74\x68\x65\x20\x7A\x6F\x6F\x6D\x2E\xA\xA\x4E\x6F\x6E\x65\x20\x3D\x20\x4D\x61\x69\x6E\x43\x61\x6D\x65\x72\x61\x2E"), NULL);
	}
}
static void LeanPinchScale_t06F48876FB3BF619EF69F2342975AB52C847FFF3_CustomAttributesCacheGenerator_Relative(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x68\x6F\x75\x6C\x64\x20\x74\x68\x65\x20\x73\x63\x61\x6C\x69\x6E\x67\x20\x62\x65\x20\x70\x65\x72\x66\x6F\x72\x6D\x61\x6E\x63\x65\x64\x20\x72\x65\x6C\x61\x74\x69\x76\x65\x20\x74\x6F\x20\x74\x68\x65\x20\x66\x69\x6E\x67\x65\x72\x20\x63\x65\x6E\x74\x65\x72\x3F"), NULL);
	}
}
static void LeanPinchScale_t06F48876FB3BF619EF69F2342975AB52C847FFF3_CustomAttributesCacheGenerator_Sensitivity(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x73\x65\x6E\x73\x69\x74\x69\x76\x69\x74\x79\x20\x6F\x66\x20\x74\x68\x65\x20\x73\x63\x61\x6C\x69\x6E\x67\x2E\xA\xA\x31\x20\x3D\x20\x44\x65\x66\x61\x75\x6C\x74\x2E\xA\x32\x20\x3D\x20\x44\x6F\x75\x62\x6C\x65\x2E"), NULL);
	}
}
static void LeanPinchScale_t06F48876FB3BF619EF69F2342975AB52C847FFF3_CustomAttributesCacheGenerator_Damping(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x79\x6F\x75\x20\x77\x61\x6E\x74\x20\x74\x68\x69\x73\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x20\x74\x6F\x20\x63\x68\x61\x6E\x67\x65\x20\x73\x6D\x6F\x6F\x74\x68\x6C\x79\x20\x6F\x76\x65\x72\x20\x74\x69\x6D\x65\x2C\x20\x74\x68\x65\x6E\x20\x74\x68\x69\x73\x20\x61\x6C\x6C\x6F\x77\x73\x20\x79\x6F\x75\x20\x74\x6F\x20\x63\x6F\x6E\x74\x72\x6F\x6C\x20\x68\x6F\x77\x20\x71\x75\x69\x63\x6B\x20\x74\x68\x65\x20\x63\x68\x61\x6E\x67\x65\x73\x20\x72\x65\x61\x63\x68\x20\x74\x68\x65\x69\x72\x20\x74\x61\x72\x67\x65\x74\x20\x76\x61\x6C\x75\x65\x2E\xA\xA\x2D\x31\x20\x3D\x20\x49\x6E\x73\x74\x61\x6E\x74\x6C\x79\x20\x63\x68\x61\x6E\x67\x65\x2E\xA\xA\x31\x20\x3D\x20\x53\x6C\x6F\x77\x6C\x79\x20\x63\x68\x61\x6E\x67\x65\x2E\xA\xA\x31\x30\x20\x3D\x20\x51\x75\x69\x63\x6B\x6C\x79\x20\x63\x68\x61\x6E\x67\x65\x2E"), NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x61\x6D\x70\x65\x6E\x69\x6E\x67"), NULL);
	}
}
static void LeanPinchScale_t06F48876FB3BF619EF69F2342975AB52C847FFF3_CustomAttributesCacheGenerator_remainingScale(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSelect_t137EC6EEC0B436A563AD37A99842A7DF9D752A5B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x23\x4C\x65\x61\x6E\x53\x65\x6C\x65\x63\x74"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x53\x65\x6C\x65\x63\x74"), NULL);
	}
}
static void LeanSelect_t137EC6EEC0B436A563AD37A99842A7DF9D752A5B_CustomAttributesCacheGenerator_LeanSelect_DeselectAll_mD01A1B1C4EBA4A68E1E100411B80466440D71558(CustomAttributesCache* cache)
{
	{
		ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 * tmp = (ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 *)cache->attributes[0];
		ContextMenu__ctor_m8A50A6FFBB330E0DB6F6587FBAAA7850D8DBDA7B(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x73\x65\x6C\x65\x63\x74\x20\x41\x6C\x6C"), NULL);
	}
}
static void LeanSelectable_tDEC98CC1F068C6F567BD5E97A29892E6878548F4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[1];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[2];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x23\x4C\x65\x61\x6E\x53\x65\x6C\x65\x63\x74\x61\x62\x6C\x65"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[3];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x53\x65\x6C\x65\x63\x74\x61\x62\x6C\x65"), NULL);
	}
}
static void LeanSelectable_tDEC98CC1F068C6F567BD5E97A29892E6878548F4_CustomAttributesCacheGenerator_OnEnableGlobal(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LeanSelectable_tDEC98CC1F068C6F567BD5E97A29892E6878548F4_CustomAttributesCacheGenerator_OnDisableGlobal(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LeanSelectable_tDEC98CC1F068C6F567BD5E97A29892E6878548F4_CustomAttributesCacheGenerator_OnSelectGlobal(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LeanSelectable_tDEC98CC1F068C6F567BD5E97A29892E6878548F4_CustomAttributesCacheGenerator_OnSelectSetGlobal(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LeanSelectable_tDEC98CC1F068C6F567BD5E97A29892E6878548F4_CustomAttributesCacheGenerator_OnSelectUpGlobal(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LeanSelectable_tDEC98CC1F068C6F567BD5E97A29892E6878548F4_CustomAttributesCacheGenerator_OnDeselectGlobal(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LeanSelectable_tDEC98CC1F068C6F567BD5E97A29892E6878548F4_CustomAttributesCacheGenerator_onSelect(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x6E\x53\x65\x6C\x65\x63\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSelectable_tDEC98CC1F068C6F567BD5E97A29892E6878548F4_CustomAttributesCacheGenerator_onSelectUpdate(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x6F\x6E\x53\x65\x6C\x65\x63\x74\x53\x65\x74"), NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x6E\x53\x65\x6C\x65\x63\x74\x53\x65\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[2];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSelectable_tDEC98CC1F068C6F567BD5E97A29892E6878548F4_CustomAttributesCacheGenerator_onSelectUp(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x6E\x53\x65\x6C\x65\x63\x74\x55\x70"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSelectable_tDEC98CC1F068C6F567BD5E97A29892E6878548F4_CustomAttributesCacheGenerator_onDeselect(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x6E\x44\x65\x73\x65\x6C\x65\x63\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSelectable_tDEC98CC1F068C6F567BD5E97A29892E6878548F4_CustomAttributesCacheGenerator_isSelected(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSelectable_tDEC98CC1F068C6F567BD5E97A29892E6878548F4_CustomAttributesCacheGenerator_LeanSelectable_add_OnEnableGlobal_m9F457F205DDA6C06927E2085DF445E365E026816(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LeanSelectable_tDEC98CC1F068C6F567BD5E97A29892E6878548F4_CustomAttributesCacheGenerator_LeanSelectable_remove_OnEnableGlobal_mD41F740F27DBE3C58673A0D289FC741CBB119637(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LeanSelectable_tDEC98CC1F068C6F567BD5E97A29892E6878548F4_CustomAttributesCacheGenerator_LeanSelectable_add_OnDisableGlobal_mBF955FC5ACF3C04DE55AAF330E2EA836BB2C4F42(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LeanSelectable_tDEC98CC1F068C6F567BD5E97A29892E6878548F4_CustomAttributesCacheGenerator_LeanSelectable_remove_OnDisableGlobal_m48850D7E0A5B384458358B24209EED8B5E12772D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LeanSelectable_tDEC98CC1F068C6F567BD5E97A29892E6878548F4_CustomAttributesCacheGenerator_LeanSelectable_add_OnSelectGlobal_m26EC561CD6AF8AE8BFE48B3B6692A8221770FF2A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LeanSelectable_tDEC98CC1F068C6F567BD5E97A29892E6878548F4_CustomAttributesCacheGenerator_LeanSelectable_remove_OnSelectGlobal_mE31A611550B3C16DF21901A218FAF4823FAAC86A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LeanSelectable_tDEC98CC1F068C6F567BD5E97A29892E6878548F4_CustomAttributesCacheGenerator_LeanSelectable_add_OnSelectSetGlobal_mE22AC4FE50CDA36803F262FD6530888D05FFB5D6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LeanSelectable_tDEC98CC1F068C6F567BD5E97A29892E6878548F4_CustomAttributesCacheGenerator_LeanSelectable_remove_OnSelectSetGlobal_m2281234B22D87DA7AB917E92F8D7D2DC8CE7F405(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LeanSelectable_tDEC98CC1F068C6F567BD5E97A29892E6878548F4_CustomAttributesCacheGenerator_LeanSelectable_add_OnSelectUpGlobal_mB6012790E296C34FF4DE19F55CB0AAF1BEEE7AD1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LeanSelectable_tDEC98CC1F068C6F567BD5E97A29892E6878548F4_CustomAttributesCacheGenerator_LeanSelectable_remove_OnSelectUpGlobal_mBA331CE50DEB92F005AB5B80156D627CC8DC2994(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LeanSelectable_tDEC98CC1F068C6F567BD5E97A29892E6878548F4_CustomAttributesCacheGenerator_LeanSelectable_add_OnDeselectGlobal_m3C2708774D3E0D3DFB0FD2FD31DBA84DA41D83D1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LeanSelectable_tDEC98CC1F068C6F567BD5E97A29892E6878548F4_CustomAttributesCacheGenerator_LeanSelectable_remove_OnDeselectGlobal_m422D6B7107F1A8A9CC7E27742163B1222EF37B71(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LeanSelectable_tDEC98CC1F068C6F567BD5E97A29892E6878548F4_CustomAttributesCacheGenerator_LeanSelectable_Select_m846A32582C9696ED9046C46A5645BC6954415C3C(CustomAttributesCache* cache)
{
	{
		ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 * tmp = (ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 *)cache->attributes[0];
		ContextMenu__ctor_m8A50A6FFBB330E0DB6F6587FBAAA7850D8DBDA7B(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x6C\x65\x63\x74"), NULL);
	}
}
static void LeanSelectable_tDEC98CC1F068C6F567BD5E97A29892E6878548F4_CustomAttributesCacheGenerator_LeanSelectable_Deselect_m194A710A0CCCED6142EBCE699D6ED5CFA69309E9(CustomAttributesCache* cache)
{
	{
		ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 * tmp = (ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 *)cache->attributes[0];
		ContextMenu__ctor_m8A50A6FFBB330E0DB6F6587FBAAA7850D8DBDA7B(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x73\x65\x6C\x65\x63\x74"), NULL);
	}
}
static void LeanSelectableBehaviour_tC3D23A3120B57C535B1F35DC33E0AA359BD0854E_CustomAttributesCacheGenerator_LeanSelectableBehaviour_Register_m58EB6A8F0755B458F63BFF6D227A5E069F435021(CustomAttributesCache* cache)
{
	{
		ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 * tmp = (ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 *)cache->attributes[0];
		ContextMenu__ctor_m8A50A6FFBB330E0DB6F6587FBAAA7850D8DBDA7B(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x67\x69\x73\x74\x65\x72"), NULL);
	}
}
static void LeanSelectableBehaviour_tC3D23A3120B57C535B1F35DC33E0AA359BD0854E_CustomAttributesCacheGenerator_LeanSelectableBehaviour_Unregister_m6435986D4F6690B1985177ADDE732C82BAF20DC6(CustomAttributesCache* cache)
{
	{
		ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 * tmp = (ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 *)cache->attributes[0];
		ContextMenu__ctor_m8A50A6FFBB330E0DB6F6587FBAAA7850D8DBDA7B(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x72\x65\x67\x69\x73\x74\x65\x72"), NULL);
	}
}
static void LeanSelectableGraphicColor_t96DCEB3FA4B9F49177B551AAD1409552E69F893F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_0_0_0_var), NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x23\x4C\x65\x61\x6E\x53\x65\x6C\x65\x63\x74\x61\x62\x6C\x65\x47\x72\x61\x70\x68\x69\x63\x43\x6F\x6C\x6F\x72"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[2];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x53\x65\x6C\x65\x63\x74\x61\x62\x6C\x65\x20\x47\x72\x61\x70\x68\x69\x63\x20\x43\x6F\x6C\x6F\x72"), NULL);
	}
}
static void LeanSelectableGraphicColor_t96DCEB3FA4B9F49177B551AAD1409552E69F893F_CustomAttributesCacheGenerator_AutoGetDefaultColor(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x75\x74\x6F\x6D\x61\x74\x69\x63\x61\x6C\x6C\x79\x20\x72\x65\x61\x64\x20\x74\x68\x65\x20\x44\x65\x66\x61\x75\x6C\x74\x43\x6F\x6C\x6F\x72\x20\x66\x72\x6F\x6D\x20\x74\x68\x65\x20\x52\x65\x6E\x64\x65\x72\x65\x72\x2E\x6D\x61\x74\x65\x72\x69\x61\x6C\x3F"), NULL);
	}
}
static void LeanSelectableGraphicColor_t96DCEB3FA4B9F49177B551AAD1409552E69F893F_CustomAttributesCacheGenerator_DefaultColor(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x64\x65\x66\x61\x75\x6C\x74\x20\x63\x6F\x6C\x6F\x72\x20\x67\x69\x76\x65\x6E\x20\x74\x6F\x20\x74\x68\x65\x20\x52\x65\x6E\x64\x65\x72\x65\x72\x2E\x6D\x61\x74\x65\x72\x69\x61\x6C\x2E"), NULL);
	}
}
static void LeanSelectableGraphicColor_t96DCEB3FA4B9F49177B551AAD1409552E69F893F_CustomAttributesCacheGenerator_SelectedColor(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x63\x6F\x6C\x6F\x72\x20\x67\x69\x76\x65\x6E\x20\x74\x6F\x20\x74\x68\x65\x20\x52\x65\x6E\x64\x65\x72\x65\x72\x2E\x6D\x61\x74\x65\x72\x69\x61\x6C\x20\x77\x68\x65\x6E\x20\x73\x65\x6C\x65\x63\x74\x65\x64\x2E"), NULL);
	}
}
static void LeanSelectableRendererColor_t25C955AA97AE743D1C5EDE3D23F15AEF045B0C6E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_0_0_0_var), NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x23\x4C\x65\x61\x6E\x53\x65\x6C\x65\x63\x74\x61\x62\x6C\x65\x52\x65\x6E\x64\x65\x72\x65\x72\x43\x6F\x6C\x6F\x72"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[2];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x53\x65\x6C\x65\x63\x74\x61\x62\x6C\x65\x20\x52\x65\x6E\x64\x65\x72\x65\x72\x20\x43\x6F\x6C\x6F\x72"), NULL);
	}
}
static void LeanSelectableRendererColor_t25C955AA97AE743D1C5EDE3D23F15AEF045B0C6E_CustomAttributesCacheGenerator_AutoGetDefaultColor(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x75\x74\x6F\x6D\x61\x74\x69\x63\x61\x6C\x6C\x79\x20\x72\x65\x61\x64\x20\x74\x68\x65\x20\x44\x65\x66\x61\x75\x6C\x74\x43\x6F\x6C\x6F\x72\x20\x66\x72\x6F\x6D\x20\x74\x68\x65\x20\x6D\x61\x74\x65\x72\x69\x61\x6C\x3F"), NULL);
	}
}
static void LeanSelectableRendererColor_t25C955AA97AE743D1C5EDE3D23F15AEF045B0C6E_CustomAttributesCacheGenerator_DefaultColor(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x64\x65\x66\x61\x75\x6C\x74\x20\x63\x6F\x6C\x6F\x72\x20\x67\x69\x76\x65\x6E\x20\x74\x6F\x20\x74\x68\x65\x20\x6D\x61\x74\x65\x72\x69\x61\x6C\x73\x2E"), NULL);
	}
}
static void LeanSelectableRendererColor_t25C955AA97AE743D1C5EDE3D23F15AEF045B0C6E_CustomAttributesCacheGenerator_SelectedColor(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x63\x6F\x6C\x6F\x72\x20\x67\x69\x76\x65\x6E\x20\x74\x6F\x20\x74\x68\x65\x20\x6D\x61\x74\x65\x72\x69\x61\x6C\x73\x20\x77\x68\x65\x6E\x20\x73\x65\x6C\x65\x63\x74\x65\x64\x2E"), NULL);
	}
}
static void LeanSelectableRendererColor_t25C955AA97AE743D1C5EDE3D23F15AEF045B0C6E_CustomAttributesCacheGenerator_CloneMaterials(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x68\x6F\x75\x6C\x64\x20\x74\x68\x65\x20\x6D\x61\x74\x65\x72\x69\x61\x6C\x73\x20\x67\x65\x74\x20\x63\x6C\x6F\x6E\x65\x64\x20\x61\x74\x20\x74\x68\x65\x20\x73\x74\x61\x72\x74\x3F"), NULL);
	}
}
static void LeanSelectableSpriteRendererColor_tF5EB93CEB97B728667E0AC04CEC48D325C6FA7A6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_0_0_0_var), NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x23\x4C\x65\x61\x6E\x53\x65\x6C\x65\x63\x74\x61\x62\x6C\x65\x53\x70\x72\x69\x74\x65\x52\x65\x6E\x64\x65\x72\x65\x72\x43\x6F\x6C\x6F\x72"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[2];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x53\x65\x6C\x65\x63\x74\x61\x62\x6C\x65\x20\x53\x70\x72\x69\x74\x65\x52\x65\x6E\x64\x65\x72\x65\x72\x20\x43\x6F\x6C\x6F\x72"), NULL);
	}
}
static void LeanSelectableSpriteRendererColor_tF5EB93CEB97B728667E0AC04CEC48D325C6FA7A6_CustomAttributesCacheGenerator_AutoGetDefaultColor(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x75\x74\x6F\x6D\x61\x74\x69\x63\x61\x6C\x6C\x79\x20\x72\x65\x61\x64\x20\x74\x68\x65\x20\x44\x65\x66\x61\x75\x6C\x74\x43\x6F\x6C\x6F\x72\x20\x66\x72\x6F\x6D\x20\x74\x68\x65\x20\x53\x70\x72\x69\x74\x65\x52\x65\x6E\x64\x65\x72\x65\x72\x3F"), NULL);
	}
}
static void LeanSelectableSpriteRendererColor_tF5EB93CEB97B728667E0AC04CEC48D325C6FA7A6_CustomAttributesCacheGenerator_DefaultColor(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x64\x65\x66\x61\x75\x6C\x74\x20\x63\x6F\x6C\x6F\x72\x20\x67\x69\x76\x65\x6E\x20\x74\x6F\x20\x74\x68\x65\x20\x53\x70\x72\x69\x74\x65\x52\x65\x6E\x64\x65\x72\x65\x72\x2E"), NULL);
	}
}
static void LeanSelectableSpriteRendererColor_tF5EB93CEB97B728667E0AC04CEC48D325C6FA7A6_CustomAttributesCacheGenerator_SelectedColor(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x63\x6F\x6C\x6F\x72\x20\x67\x69\x76\x65\x6E\x20\x74\x6F\x20\x74\x68\x65\x20\x53\x70\x72\x69\x74\x65\x52\x65\x6E\x64\x65\x72\x65\x72\x20\x77\x68\x65\x6E\x20\x73\x65\x6C\x65\x63\x74\x65\x64\x2E"), NULL);
	}
}
static void LeanSwipeBase_t1B013BA10DE95C56E3B6460F3DF3CE38C918A699_CustomAttributesCacheGenerator_RequiredAngle(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6E\x67\x6C\x65"), NULL);
	}
}
static void LeanSwipeBase_t1B013BA10DE95C56E3B6460F3DF3CE38C918A699_CustomAttributesCacheGenerator_RequiredArc(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6E\x67\x6C\x65\x54\x68\x72\x65\x73\x68\x6F\x6C\x64"), NULL);
	}
}
static void LeanSwipeBase_t1B013BA10DE95C56E3B6460F3DF3CE38C918A699_CustomAttributesCacheGenerator_onFinger(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x6F\x6E\x53\x77\x69\x70\x65"), NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x6E\x53\x77\x69\x70\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[2];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSwipeBase_t1B013BA10DE95C56E3B6460F3DF3CE38C918A699_CustomAttributesCacheGenerator_Modify(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6C\x61\x6D\x70"), NULL);
	}
}
static void LeanSwipeBase_t1B013BA10DE95C56E3B6460F3DF3CE38C918A699_CustomAttributesCacheGenerator_onDelta(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x6F\x6E\x53\x77\x69\x70\x65\x44\x65\x6C\x74\x61"), NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x6E\x53\x77\x69\x70\x65\x44\x65\x6C\x74\x61"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[2];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSwipeBase_t1B013BA10DE95C56E3B6460F3DF3CE38C918A699_CustomAttributesCacheGenerator_onDistance(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSwipeBase_t1B013BA10DE95C56E3B6460F3DF3CE38C918A699_CustomAttributesCacheGenerator_onWorldFrom(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSwipeBase_t1B013BA10DE95C56E3B6460F3DF3CE38C918A699_CustomAttributesCacheGenerator_onWorldTo(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSwipeBase_t1B013BA10DE95C56E3B6460F3DF3CE38C918A699_CustomAttributesCacheGenerator_onWorldDelta(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanSwipeBase_t1B013BA10DE95C56E3B6460F3DF3CE38C918A699_CustomAttributesCacheGenerator_onWorldFromTo(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x6F\x6E\x53\x77\x69\x70\x65\x46\x72\x6F\x6D\x54\x6F"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanTwistRotate_tFE8411F228C2CECB7893EE098C277A7A37C89A7F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x23\x4C\x65\x61\x6E\x54\x77\x69\x73\x74\x52\x6F\x74\x61\x74\x65"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x54\x77\x69\x73\x74\x20\x52\x6F\x74\x61\x74\x65"), NULL);
	}
}
static void LeanTwistRotate_tFE8411F228C2CECB7893EE098C277A7A37C89A7F_CustomAttributesCacheGenerator_Camera(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x63\x61\x6D\x65\x72\x61\x20\x77\x65\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x75\x73\x65\x64\x20\x74\x6F\x20\x63\x61\x6C\x63\x75\x6C\x61\x74\x65\x20\x72\x65\x6C\x61\x74\x69\x76\x65\x20\x72\x6F\x74\x61\x74\x69\x6F\x6E\x73\x2E\xA\xA\x4E\x6F\x6E\x65\x20\x3D\x20\x4D\x61\x69\x6E\x43\x61\x6D\x65\x72\x61\x2E"), NULL);
	}
}
static void LeanTwistRotate_tFE8411F228C2CECB7893EE098C277A7A37C89A7F_CustomAttributesCacheGenerator_Relative(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x68\x6F\x75\x6C\x64\x20\x74\x68\x65\x20\x72\x6F\x74\x61\x74\x69\x6F\x6E\x20\x62\x65\x20\x70\x65\x72\x66\x6F\x72\x6D\x61\x6E\x63\x65\x64\x20\x72\x65\x6C\x61\x74\x69\x76\x65\x20\x74\x6F\x20\x74\x68\x65\x20\x66\x69\x6E\x67\x65\x72\x20\x63\x65\x6E\x74\x65\x72\x3F"), NULL);
	}
}
static void LeanTwistRotate_tFE8411F228C2CECB7893EE098C277A7A37C89A7F_CustomAttributesCacheGenerator_Damping(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x79\x6F\x75\x20\x77\x61\x6E\x74\x20\x74\x68\x69\x73\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x20\x74\x6F\x20\x63\x68\x61\x6E\x67\x65\x20\x73\x6D\x6F\x6F\x74\x68\x6C\x79\x20\x6F\x76\x65\x72\x20\x74\x69\x6D\x65\x2C\x20\x74\x68\x65\x6E\x20\x74\x68\x69\x73\x20\x61\x6C\x6C\x6F\x77\x73\x20\x79\x6F\x75\x20\x74\x6F\x20\x63\x6F\x6E\x74\x72\x6F\x6C\x20\x68\x6F\x77\x20\x71\x75\x69\x63\x6B\x20\x74\x68\x65\x20\x63\x68\x61\x6E\x67\x65\x73\x20\x72\x65\x61\x63\x68\x20\x74\x68\x65\x69\x72\x20\x74\x61\x72\x67\x65\x74\x20\x76\x61\x6C\x75\x65\x2E\xA\xA\x2D\x31\x20\x3D\x20\x49\x6E\x73\x74\x61\x6E\x74\x6C\x79\x20\x63\x68\x61\x6E\x67\x65\x2E\xA\xA\x31\x20\x3D\x20\x53\x6C\x6F\x77\x6C\x79\x20\x63\x68\x61\x6E\x67\x65\x2E\xA\xA\x31\x30\x20\x3D\x20\x51\x75\x69\x63\x6B\x6C\x79\x20\x63\x68\x61\x6E\x67\x65\x2E"), NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x61\x6D\x70\x65\x6E\x69\x6E\x67"), NULL);
	}
}
static void LeanTwistRotate_tFE8411F228C2CECB7893EE098C277A7A37C89A7F_CustomAttributesCacheGenerator_remainingTranslation(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanTwistRotate_tFE8411F228C2CECB7893EE098C277A7A37C89A7F_CustomAttributesCacheGenerator_remainingRotation(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanTwistRotateAxis_t9DCE177FE4FA445368B0112319E88771C3A76F1F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x23\x4C\x65\x61\x6E\x54\x77\x69\x73\x74\x52\x6F\x74\x61\x74\x65\x41\x78\x69\x73"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x54\x77\x69\x73\x74\x20\x52\x6F\x74\x61\x74\x65\x20\x41\x78\x69\x73"), NULL);
	}
}
static void LeanTwistRotateAxis_t9DCE177FE4FA445368B0112319E88771C3A76F1F_CustomAttributesCacheGenerator_Axis(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x61\x78\x69\x73\x20\x6F\x66\x20\x72\x6F\x74\x61\x74\x69\x6F\x6E\x2E"), NULL);
	}
}
static void LeanTwistRotateAxis_t9DCE177FE4FA445368B0112319E88771C3A76F1F_CustomAttributesCacheGenerator_Space(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x52\x6F\x74\x61\x74\x65\x20\x6C\x6F\x63\x61\x6C\x6C\x79\x20\x6F\x72\x20\x67\x6C\x6F\x62\x61\x6C\x6C\x79\x3F"), NULL);
	}
}
static void LeanTwistRotateAxis_t9DCE177FE4FA445368B0112319E88771C3A76F1F_CustomAttributesCacheGenerator_Sensitivity(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x73\x65\x6E\x73\x69\x74\x69\x76\x69\x74\x79\x20\x6F\x66\x20\x74\x68\x65\x20\x72\x6F\x74\x61\x74\x69\x6F\x6E\x2E\xA\xA\x31\x20\x3D\x20\x44\x65\x66\x61\x75\x6C\x74\x2E\xA\x32\x20\x3D\x20\x44\x6F\x75\x62\x6C\x65\x2E"), NULL);
	}
}
static void LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
	{
		DefaultExecutionOrder_t8495D3D4ECDFC3590621D31C3677D234D8A9BB1F * tmp = (DefaultExecutionOrder_t8495D3D4ECDFC3590621D31C3677D234D8A9BB1F *)cache->attributes[1];
		DefaultExecutionOrder__ctor_m18F4188D26702C2E3EC0AB3C1FF4AA4F5329E7A9(tmp, -100LL, NULL);
	}
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[2];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[3];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68\x23\x4C\x65\x61\x6E\x54\x6F\x75\x63\x68"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[4];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x54\x6F\x75\x63\x68\x2F\x4C\x65\x61\x6E\x20\x54\x6F\x75\x63\x68"), NULL);
	}
}
static void LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_OnFingerDown(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_OnFingerUpdate(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_OnFingerUp(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_OnFingerOld(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_OnFingerTap(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_OnFingerSwipe(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_OnGesture(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_OnFingerExpired(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_OnFingerInactive(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_TapThreshold(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x61\x6C\x6C\x6F\x77\x73\x20\x79\x6F\x75\x20\x74\x6F\x20\x73\x65\x74\x20\x68\x6F\x77\x20\x6D\x61\x6E\x79\x20\x73\x65\x63\x6F\x6E\x64\x73\x20\x61\x72\x65\x20\x72\x65\x71\x75\x69\x72\x65\x64\x20\x62\x65\x74\x77\x65\x65\x6E\x20\x61\x20\x66\x69\x6E\x67\x65\x72\x20\x64\x6F\x77\x6E\x2F\x75\x70\x20\x66\x6F\x72\x20\x61\x20\x74\x61\x70\x20\x74\x6F\x20\x62\x65\x20\x72\x65\x67\x69\x73\x74\x65\x72\x65\x64\x2E"), NULL);
	}
}
static void LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_SwipeThreshold(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x61\x6C\x6C\x6F\x77\x73\x20\x79\x6F\x75\x20\x74\x6F\x20\x73\x65\x74\x20\x68\x6F\x77\x20\x6D\x61\x6E\x79\x20\x70\x69\x78\x65\x6C\x73\x20\x6F\x66\x20\x6D\x6F\x76\x65\x6D\x65\x6E\x74\x20\x28\x72\x65\x6C\x61\x74\x69\x76\x65\x20\x74\x6F\x20\x74\x68\x65\x20\x52\x65\x66\x65\x72\x65\x6E\x63\x65\x44\x70\x69\x29\x20\x61\x72\x65\x20\x72\x65\x71\x75\x69\x72\x65\x64\x20\x77\x69\x74\x68\x69\x6E\x20\x74\x68\x65\x20\x54\x61\x70\x54\x68\x72\x65\x73\x68\x6F\x6C\x64\x20\x66\x6F\x72\x20\x61\x20\x73\x77\x69\x70\x65\x20\x74\x6F\x20\x62\x65\x20\x74\x72\x69\x67\x67\x65\x72\x65\x64\x2E"), NULL);
	}
}
static void LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_ReferenceDpi(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x61\x6C\x6C\x6F\x77\x73\x20\x79\x6F\x75\x20\x74\x6F\x20\x73\x65\x74\x20\x74\x68\x65\x20\x64\x65\x66\x61\x75\x6C\x74\x20\x44\x50\x49\x20\x79\x6F\x75\x20\x77\x61\x6E\x74\x20\x74\x68\x65\x20\x69\x6E\x70\x75\x74\x20\x73\x63\x61\x6C\x69\x6E\x67\x20\x74\x6F\x20\x62\x65\x20\x62\x61\x73\x65\x64\x20\x6F\x6E\x2E"), NULL);
	}
}
static void LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_GuiLayers(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x61\x6C\x6C\x6F\x77\x73\x20\x79\x6F\x75\x20\x74\x6F\x20\x73\x65\x74\x20\x77\x68\x69\x63\x68\x20\x6C\x61\x79\x65\x72\x73\x20\x79\x6F\x75\x72\x20\x47\x55\x49\x20\x69\x73\x20\x6F\x6E\x2C\x20\x73\x6F\x20\x69\x74\x20\x63\x61\x6E\x20\x62\x65\x20\x69\x67\x6E\x6F\x72\x65\x64\x20\x62\x79\x20\x65\x61\x63\x68\x20\x66\x69\x6E\x67\x65\x72\x2E"), NULL);
	}
}
static void LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_RecordFingers(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x68\x6F\x75\x6C\x64\x20\x65\x61\x63\x68\x20\x66\x69\x6E\x67\x65\x72\x20\x72\x65\x63\x6F\x72\x64\x20\x73\x6E\x61\x70\x73\x68\x6F\x74\x73\x20\x6F\x66\x20\x74\x68\x65\x69\x72\x20\x73\x63\x72\x65\x65\x6E\x20\x70\x6F\x73\x69\x74\x69\x6F\x6E\x73\x3F"), NULL);
	}
}
static void LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_RecordThreshold(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x61\x6C\x6C\x6F\x77\x73\x20\x79\x6F\x75\x20\x74\x6F\x20\x73\x65\x74\x20\x74\x68\x65\x20\x61\x6D\x6F\x75\x6E\x74\x20\x6F\x66\x20\x70\x69\x78\x65\x6C\x73\x20\x61\x20\x66\x69\x6E\x67\x65\x72\x20\x6D\x75\x73\x74\x20\x6D\x6F\x76\x65\x20\x66\x6F\x72\x20\x61\x6E\x6F\x74\x68\x65\x72\x20\x73\x6E\x61\x70\x73\x68\x6F\x74\x20\x74\x6F\x20\x62\x65\x20\x73\x74\x6F\x72\x65\x64\x2E"), NULL);
	}
}
static void LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_RecordLimit(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x61\x6C\x6C\x6F\x77\x73\x20\x79\x6F\x75\x20\x74\x6F\x20\x73\x65\x74\x20\x74\x68\x65\x20\x6D\x61\x78\x69\x6D\x75\x6D\x20\x61\x6D\x6F\x75\x6E\x74\x20\x6F\x66\x20\x73\x65\x63\x6F\x6E\x64\x73\x20\x74\x68\x61\x74\x20\x63\x61\x6E\x20\x62\x65\x20\x72\x65\x63\x6F\x72\x64\x65\x64\x2C\x20\x30\x20\x3D\x20\x75\x6E\x6C\x69\x6D\x69\x74\x65\x64\x2E"), NULL);
	}
}
static void LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_SimulateMultiFingers(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x61\x6C\x6C\x6F\x77\x73\x20\x79\x6F\x75\x20\x74\x6F\x20\x73\x69\x6D\x75\x6C\x61\x74\x65\x20\x6D\x75\x6C\x74\x69\x20\x74\x6F\x75\x63\x68\x20\x69\x6E\x70\x75\x74\x73\x20\x6F\x6E\x20\x64\x65\x76\x69\x63\x65\x73\x20\x74\x68\x61\x74\x20\x64\x6F\x6E\x27\x74\x20\x73\x75\x70\x70\x6F\x72\x74\x20\x74\x68\x65\x6D\x20\x28\x65\x2E\x67\x2E\x20\x64\x65\x73\x6B\x74\x6F\x70\x29\x2E"), NULL);
	}
}
static void LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_PinchTwistKey(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x61\x6C\x6C\x6F\x77\x73\x20\x79\x6F\x75\x20\x74\x6F\x20\x73\x65\x74\x20\x77\x68\x69\x63\x68\x20\x6B\x65\x79\x20\x69\x73\x20\x72\x65\x71\x75\x69\x72\x65\x64\x20\x74\x6F\x20\x73\x69\x6D\x75\x6C\x61\x74\x65\x20\x6D\x75\x6C\x74\x69\x20\x6B\x65\x79\x20\x74\x77\x69\x73\x74\x69\x6E\x67\x2E"), NULL);
	}
}
static void LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_MovePivotKey(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x61\x6C\x6C\x6F\x77\x73\x20\x79\x6F\x75\x20\x74\x6F\x20\x73\x65\x74\x20\x77\x68\x69\x63\x68\x20\x6B\x65\x79\x20\x69\x73\x20\x72\x65\x71\x75\x69\x72\x65\x64\x20\x74\x6F\x20\x63\x68\x61\x6E\x67\x65\x20\x74\x68\x65\x20\x70\x69\x76\x6F\x74\x20\x70\x6F\x69\x6E\x74\x20\x6F\x66\x20\x74\x68\x65\x20\x70\x69\x6E\x63\x68\x20\x74\x77\x69\x73\x74\x20\x67\x65\x73\x74\x75\x72\x65\x2E"), NULL);
	}
}
static void LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_MultiDragKey(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x61\x6C\x6C\x6F\x77\x73\x20\x79\x6F\x75\x20\x74\x6F\x20\x73\x65\x74\x20\x77\x68\x69\x63\x68\x20\x6B\x65\x79\x20\x69\x73\x20\x72\x65\x71\x75\x69\x72\x65\x64\x20\x74\x6F\x20\x73\x69\x6D\x75\x6C\x61\x74\x65\x20\x6D\x75\x6C\x74\x69\x20\x6B\x65\x79\x20\x64\x72\x61\x67\x67\x69\x6E\x67\x2E"), NULL);
	}
}
static void LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_FingerTexture(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x61\x6C\x6C\x6F\x77\x73\x20\x79\x6F\x75\x20\x74\x6F\x20\x73\x65\x74\x20\x77\x68\x69\x63\x68\x20\x74\x65\x78\x74\x75\x72\x65\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x75\x73\x65\x64\x20\x74\x6F\x20\x73\x68\x6F\x77\x20\x74\x68\x65\x20\x73\x69\x6D\x75\x6C\x61\x74\x65\x64\x20\x66\x69\x6E\x67\x65\x72\x73\x2E"), NULL);
	}
}
static void LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_LeanTouch_add_OnFingerDown_m17FAE9CD13F1E43586D22F8195DCB4849D40EF9D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_LeanTouch_remove_OnFingerDown_m7D8D8908F245868EB3BA75FF2FE81363BD853BC5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_LeanTouch_add_OnFingerUpdate_mDD30E72D4128AC7F7DEE314AFDD1F63B3BC54255(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_LeanTouch_remove_OnFingerUpdate_m58B05A304E7E3A468A3967B4B826383F3FB57CF2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_LeanTouch_add_OnFingerUp_m63B6C30AB7B97D4009CF0611396C233A9B3C92BF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_LeanTouch_remove_OnFingerUp_mE1B669D2A145F54FC5147DE6374782507D803777(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_LeanTouch_add_OnFingerOld_mA1F82698C5926977AA0D01602C89FD70307EBD94(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_LeanTouch_remove_OnFingerOld_mA0BF26E8F3EB1309005BFA908CB823F20FAEA8A6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_LeanTouch_add_OnFingerTap_m5BA04B655B78782D12319DD143F4E01997E6594E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_LeanTouch_remove_OnFingerTap_m17033D5DBDA4CD4AF877E1D1C9C6EB1D9236E155(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_LeanTouch_add_OnFingerSwipe_mDEE2185B28C5117DCD3E7E9A0CEC5B18BA206BFA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_LeanTouch_remove_OnFingerSwipe_m36D61DE4F64B42907985C7E154FB3D9D7E1BE1DD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_LeanTouch_add_OnGesture_mCAFCA85BE9F5DCFC821AB3A702B592437BBF0589(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_LeanTouch_remove_OnGesture_mE803955E7B5635B8941DBBF930462EDCD1E0EBF2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_LeanTouch_add_OnFingerExpired_mDC3B4CAF9DEB8427D0CCDF27BEF598D4930D777F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_LeanTouch_remove_OnFingerExpired_m7C90A349CCA6B72C366D3F61B61E7AB286D6F17E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_LeanTouch_add_OnFingerInactive_m226A4E6851247A04C851E444A511372D056F4836(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_LeanTouch_remove_OnFingerInactive_mACEDB928D81225C7EEAE253E288E35111051BB84(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LeanDestroy_t142821BBC124216DE7C3E66C01E59FFC4FE15D40_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x43\x6F\x6D\x6D\x6F\x6E\x23\x4C\x65\x61\x6E\x44\x65\x73\x74\x72\x6F\x79"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x44\x65\x73\x74\x72\x6F\x79"), NULL);
	}
}
static void LeanPath_t5BEE07257CD2375F5278E36308C8DBE30EF7DBC5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x43\x6F\x6D\x6D\x6F\x6E\x23\x4C\x65\x61\x6E\x50\x61\x74\x68"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[2];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x50\x61\x74\x68"), NULL);
	}
}
static void LeanPath_t5BEE07257CD2375F5278E36308C8DBE30EF7DBC5_CustomAttributesCacheGenerator_Points(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x70\x6F\x69\x6E\x74\x73\x20\x61\x6C\x6F\x6E\x67\x20\x74\x68\x65\x20\x70\x61\x74\x68\x2E"), NULL);
	}
}
static void LeanPath_t5BEE07257CD2375F5278E36308C8DBE30EF7DBC5_CustomAttributesCacheGenerator_Loop(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x6F\x20\x74\x68\x65\x73\x65\x20\x70\x6F\x69\x6E\x74\x73\x20\x6C\x6F\x6F\x70\x20\x62\x61\x63\x6B\x20\x74\x6F\x20\x74\x68\x65\x20\x73\x74\x61\x72\x74\x3F"), NULL);
	}
}
static void LeanPath_t5BEE07257CD2375F5278E36308C8DBE30EF7DBC5_CustomAttributesCacheGenerator_Space(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x63\x6F\x6F\x72\x64\x69\x6E\x61\x74\x65\x20\x73\x79\x73\x74\x65\x6D\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x70\x6F\x69\x6E\x74\x73\x2E"), NULL);
	}
}
static void LeanPath_t5BEE07257CD2375F5278E36308C8DBE30EF7DBC5_CustomAttributesCacheGenerator_Smoothing(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x61\x6D\x6F\x75\x6E\x74\x20\x6F\x66\x20\x6C\x69\x6E\x65\x73\x20\x62\x65\x74\x77\x65\x65\x6E\x20\x65\x61\x63\x68\x20\x70\x61\x74\x68\x20\x70\x6F\x69\x6E\x74\x20\x77\x68\x65\x6E\x20\x72\x65\x61\x64\x20\x66\x72\x6F\x6D\x20\x4C\x65\x61\x6E\x53\x63\x72\x65\x65\x6E\x44\x65\x70\x74\x68\x2E"), NULL);
	}
}
static void LeanPath_t5BEE07257CD2375F5278E36308C8DBE30EF7DBC5_CustomAttributesCacheGenerator_Visual(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x61\x6C\x6C\x6F\x77\x73\x20\x79\x6F\x75\x20\x74\x6F\x20\x64\x72\x61\x77\x20\x61\x20\x76\x69\x73\x75\x61\x6C\x20\x6F\x66\x20\x74\x68\x65\x20\x70\x61\x74\x68\x20\x75\x73\x69\x6E\x67\x20\x61\x20\x4C\x69\x6E\x65\x52\x65\x6E\x64\x65\x72\x65\x72\x2E"), NULL);
	}
}
static void LeanPlane_t4F2B7AEF821A885E3E6762683CB0C4D3F63DE831_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x43\x6F\x6D\x6D\x6F\x6E\x23\x4C\x65\x61\x6E\x50\x6C\x61\x6E\x65"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x50\x6C\x61\x6E\x65"), NULL);
	}
}
static void LeanRoll_t673925EEFDF4F8F916A554552A0E4A8F3901D2CE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[1];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x43\x6F\x6D\x6D\x6F\x6E\x23\x4C\x65\x61\x6E\x52\x6F\x6C\x6C"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[2];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x52\x6F\x6C\x6C"), NULL);
	}
}
static void LeanRoll_t673925EEFDF4F8F916A554552A0E4A8F3901D2CE_CustomAttributesCacheGenerator_Damping(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x61\x6D\x70\x65\x6E\x69\x6E\x67"), NULL);
	}
}
static void LeanRoll_t673925EEFDF4F8F916A554552A0E4A8F3901D2CE_CustomAttributesCacheGenerator_currentAngle(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanRoll_t673925EEFDF4F8F916A554552A0E4A8F3901D2CE_CustomAttributesCacheGenerator_LeanRoll_SnapToTarget_m38645C71A54CE1A9346C3CB4FA7BBCEAB964CC29(CustomAttributesCache* cache)
{
	{
		ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 * tmp = (ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 *)cache->attributes[0];
		ContextMenu__ctor_m8A50A6FFBB330E0DB6F6587FBAAA7850D8DBDA7B(tmp, il2cpp_codegen_string_new_wrapper("\x53\x6E\x61\x70\x20\x54\x6F\x20\x54\x61\x72\x67\x65\x74"), NULL);
	}
}
static void LeanSpawn_t23AE573676D222DE373145A3F5ADD85CE7B715F3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 * tmp = (HelpURLAttribute_t0924A6D83FABA7B77780F7F9BBCBCB9E0EA15023 *)cache->attributes[0];
		HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215(tmp, il2cpp_codegen_string_new_wrapper("\x68\x74\x74\x70\x73\x3A\x2F\x2F\x63\x61\x72\x6C\x6F\x73\x77\x69\x6C\x6B\x65\x73\x2E\x67\x69\x74\x68\x75\x62\x2E\x69\x6F\x2F\x44\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2F\x4C\x65\x61\x6E\x43\x6F\x6D\x6D\x6F\x6E\x23\x4C\x65\x61\x6E\x53\x70\x61\x77\x6E"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x53\x70\x61\x77\x6E"), NULL);
	}
}
static void LeanLinkTo_t853CB80C0CCBFE9C9B216E39C9039043DDCE2872_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x43\x6F\x6D\x6D\x6F\x6E\x2F\x4C\x65\x61\x6E\x20\x4C\x69\x6E\x6B\x20\x54\x6F"), NULL);
	}
}
static void LeanLinkTo_t853CB80C0CCBFE9C9B216E39C9039043DDCE2872_CustomAttributesCacheGenerator_link(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LeanUpgradeEventSystem_tAE96AD3F97C431CDC61864BE43296269D9E7F0CC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x6E\x2F\x43\x6F\x6D\x6D\x6F\x6E\x2F\x55\x70\x67\x72\x61\x64\x65\x20\x45\x76\x65\x6E\x74\x53\x79\x73\x74\x65\x6D"), NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[293] = 
{
	LightEstimation_t9843B4C3725AE20D953D7D4AFA849B024DC06925_CustomAttributesCacheGenerator,
	ARFeatheredPlaneMeshVisualizer_t7B2E22615047B9FDFC4D5015F59BFE2A17E3140E_CustomAttributesCacheGenerator,
	FadePlaneOnBoundaryChange_t505F6F32EC6CD33C0DAF386F378B0C0968D786CA_CustomAttributesCacheGenerator,
	PlaceObjectsOnPlane_tBC077C6ABB6A15D8E3AE99CCC9966B4E562F5417_CustomAttributesCacheGenerator,
	ARKitCoachingOverlay_tE7AD6D9D6A4520CB18270A5B846153A84B35F629_CustomAttributesCacheGenerator,
	LeanInfoText_t508B956A97587F4ECF20EE95947A49736F76D477_CustomAttributesCacheGenerator,
	LeanPulseScale_t984988192A1106BE7E5B31BAC1DA86CDE07F2A4E_CustomAttributesCacheGenerator,
	LeanTouchEvents_t1C2799365501C570FC4F5A45761CEF3330D4B12B_CustomAttributesCacheGenerator,
	LeanDragCamera_t16CF0D4D777F2F3B0C5A76111C8BA13A2698419E_CustomAttributesCacheGenerator,
	LeanDragTrail_t72B928B6E60E86D0845027C361672322C2305841_CustomAttributesCacheGenerator,
	LeanDragTranslate_tB311852063395381489EFEB52FC6849E0350EFBC_CustomAttributesCacheGenerator,
	LeanFingerDown_tBB4309FE0B73C4438020A3F4469626D1191939F2_CustomAttributesCacheGenerator,
	LeanFingerOld_t37E4543AACC95F6C05B7094CBCFE2541FA5D959F_CustomAttributesCacheGenerator,
	LeanFingerSwipe_t4E40924EA323353C64A2F62A645C5EB8B4FB2912_CustomAttributesCacheGenerator,
	LeanFingerTap_t505333CF479CE38899CFC21A1943D0F3F0196CD7_CustomAttributesCacheGenerator,
	LeanFingerUp_t8F38426263A3E2369766D376423034C82F9F5862_CustomAttributesCacheGenerator,
	LeanFingerUpdate_tA215228160659B8EA8BF59A748C9C44DC5DAB897_CustomAttributesCacheGenerator,
	LeanPinchScale_t06F48876FB3BF619EF69F2342975AB52C847FFF3_CustomAttributesCacheGenerator,
	LeanSelect_t137EC6EEC0B436A563AD37A99842A7DF9D752A5B_CustomAttributesCacheGenerator,
	LeanSelectable_tDEC98CC1F068C6F567BD5E97A29892E6878548F4_CustomAttributesCacheGenerator,
	LeanSelectableGraphicColor_t96DCEB3FA4B9F49177B551AAD1409552E69F893F_CustomAttributesCacheGenerator,
	LeanSelectableRendererColor_t25C955AA97AE743D1C5EDE3D23F15AEF045B0C6E_CustomAttributesCacheGenerator,
	LeanSelectableSpriteRendererColor_tF5EB93CEB97B728667E0AC04CEC48D325C6FA7A6_CustomAttributesCacheGenerator,
	LeanTwistRotate_tFE8411F228C2CECB7893EE098C277A7A37C89A7F_CustomAttributesCacheGenerator,
	LeanTwistRotateAxis_t9DCE177FE4FA445368B0112319E88771C3A76F1F_CustomAttributesCacheGenerator,
	LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator,
	LeanDestroy_t142821BBC124216DE7C3E66C01E59FFC4FE15D40_CustomAttributesCacheGenerator,
	LeanPath_t5BEE07257CD2375F5278E36308C8DBE30EF7DBC5_CustomAttributesCacheGenerator,
	LeanPlane_t4F2B7AEF821A885E3E6762683CB0C4D3F63DE831_CustomAttributesCacheGenerator,
	LeanRoll_t673925EEFDF4F8F916A554552A0E4A8F3901D2CE_CustomAttributesCacheGenerator,
	LeanSpawn_t23AE573676D222DE373145A3F5ADD85CE7B715F3_CustomAttributesCacheGenerator,
	LeanLinkTo_t853CB80C0CCBFE9C9B216E39C9039043DDCE2872_CustomAttributesCacheGenerator,
	LeanUpgradeEventSystem_tAE96AD3F97C431CDC61864BE43296269D9E7F0CC_CustomAttributesCacheGenerator,
	U3CStartU3Ed__55_tAD522FF1E9F588E49FF94436005D673167775F79_CustomAttributesCacheGenerator,
	U3CU3Ec_tF3B5E6F42A0418BAF09788FA0CE0E381B0383490_CustomAttributesCacheGenerator,
	U3CEnableBatsmanU3Ed__6_t9F28F6ED1F3B9EFF2BF57316D59057D3F25751C3_CustomAttributesCacheGenerator,
	LightEstimation_t9843B4C3725AE20D953D7D4AFA849B024DC06925_CustomAttributesCacheGenerator_m_CameraManager,
	LightEstimation_t9843B4C3725AE20D953D7D4AFA849B024DC06925_CustomAttributesCacheGenerator_U3CbrightnessU3Ek__BackingField,
	LightEstimation_t9843B4C3725AE20D953D7D4AFA849B024DC06925_CustomAttributesCacheGenerator_U3CcolorTemperatureU3Ek__BackingField,
	LightEstimation_t9843B4C3725AE20D953D7D4AFA849B024DC06925_CustomAttributesCacheGenerator_U3CcolorCorrectionU3Ek__BackingField,
	LightEstimation_t9843B4C3725AE20D953D7D4AFA849B024DC06925_CustomAttributesCacheGenerator_U3CmainLightDirectionU3Ek__BackingField,
	LightEstimation_t9843B4C3725AE20D953D7D4AFA849B024DC06925_CustomAttributesCacheGenerator_U3CmainLightColorU3Ek__BackingField,
	LightEstimation_t9843B4C3725AE20D953D7D4AFA849B024DC06925_CustomAttributesCacheGenerator_U3CmainLightIntensityLumensU3Ek__BackingField,
	LightEstimation_t9843B4C3725AE20D953D7D4AFA849B024DC06925_CustomAttributesCacheGenerator_U3CsphericalHarmonicsU3Ek__BackingField,
	LightEstimation_t9843B4C3725AE20D953D7D4AFA849B024DC06925_CustomAttributesCacheGenerator_m_BrightnessMod,
	PlacementReticle_t70B1486B426AD43423264CD3429D6F7319CE5B76_CustomAttributesCacheGenerator_m_SnapToMesh,
	PlacementReticle_t70B1486B426AD43423264CD3429D6F7319CE5B76_CustomAttributesCacheGenerator_m_RaycastManager,
	PlacementReticle_t70B1486B426AD43423264CD3429D6F7319CE5B76_CustomAttributesCacheGenerator_m_ReticlePrefab,
	PlacementReticle_t70B1486B426AD43423264CD3429D6F7319CE5B76_CustomAttributesCacheGenerator_m_DistanceScale,
	PlacementReticle_t70B1486B426AD43423264CD3429D6F7319CE5B76_CustomAttributesCacheGenerator_m_CameraTransform,
	ProbePlacement_t4CB974E563B208E7E4AFDDA906A01E34E7827702_CustomAttributesCacheGenerator_m_ProbeManager,
	ProbePlacement_t4CB974E563B208E7E4AFDDA906A01E34E7827702_CustomAttributesCacheGenerator_m_RaycastManager,
	ARFeatheredPlaneMeshVisualizer_t7B2E22615047B9FDFC4D5015F59BFE2A17E3140E_CustomAttributesCacheGenerator_m_FeatheringWidth,
	PlaceObjectsOnPlane_tBC077C6ABB6A15D8E3AE99CCC9966B4E562F5417_CustomAttributesCacheGenerator_m_PlacedPrefab,
	PlaceObjectsOnPlane_tBC077C6ABB6A15D8E3AE99CCC9966B4E562F5417_CustomAttributesCacheGenerator_U3CspawnedObjectU3Ek__BackingField,
	PlaceObjectsOnPlane_tBC077C6ABB6A15D8E3AE99CCC9966B4E562F5417_CustomAttributesCacheGenerator_onPlacedObject,
	PlaceObjectsOnPlane_tBC077C6ABB6A15D8E3AE99CCC9966B4E562F5417_CustomAttributesCacheGenerator_m_MaxNumberOfObjectsToPlace,
	PlaceObjectsOnPlane_tBC077C6ABB6A15D8E3AE99CCC9966B4E562F5417_CustomAttributesCacheGenerator_m_CanReposition,
	ARKitCoachingOverlay_tE7AD6D9D6A4520CB18270A5B846153A84B35F629_CustomAttributesCacheGenerator_m_Goal,
	ARKitCoachingOverlay_tE7AD6D9D6A4520CB18270A5B846153A84B35F629_CustomAttributesCacheGenerator_m_ActivatesAutomatically,
	ARUXAnimationManager_tBFA785140C2D44C6B8F4AAB9540BD574B1013FA0_CustomAttributesCacheGenerator_m_InstructionText,
	ARUXAnimationManager_tBFA785140C2D44C6B8F4AAB9540BD574B1013FA0_CustomAttributesCacheGenerator_m_FindAPlaneClip,
	ARUXAnimationManager_tBFA785140C2D44C6B8F4AAB9540BD574B1013FA0_CustomAttributesCacheGenerator_m_TapToPlaceClip,
	ARUXAnimationManager_tBFA785140C2D44C6B8F4AAB9540BD574B1013FA0_CustomAttributesCacheGenerator_m_FindImageClip,
	ARUXAnimationManager_tBFA785140C2D44C6B8F4AAB9540BD574B1013FA0_CustomAttributesCacheGenerator_m_FindBodyClip,
	ARUXAnimationManager_tBFA785140C2D44C6B8F4AAB9540BD574B1013FA0_CustomAttributesCacheGenerator_m_FindObjectClip,
	ARUXAnimationManager_tBFA785140C2D44C6B8F4AAB9540BD574B1013FA0_CustomAttributesCacheGenerator_m_FindFaceClip,
	ARUXAnimationManager_tBFA785140C2D44C6B8F4AAB9540BD574B1013FA0_CustomAttributesCacheGenerator_m_CoachingOverlay,
	ARUXAnimationManager_tBFA785140C2D44C6B8F4AAB9540BD574B1013FA0_CustomAttributesCacheGenerator_m_VideoPlayer,
	ARUXAnimationManager_tBFA785140C2D44C6B8F4AAB9540BD574B1013FA0_CustomAttributesCacheGenerator_m_RawImage,
	ARUXAnimationManager_tBFA785140C2D44C6B8F4AAB9540BD574B1013FA0_CustomAttributesCacheGenerator_m_FadeOnDuration,
	ARUXAnimationManager_tBFA785140C2D44C6B8F4AAB9540BD574B1013FA0_CustomAttributesCacheGenerator_m_FadeOffDuration,
	ARUXAnimationManager_tBFA785140C2D44C6B8F4AAB9540BD574B1013FA0_CustomAttributesCacheGenerator_onFadeOffComplete,
	ARUXAnimationManager_tBFA785140C2D44C6B8F4AAB9540BD574B1013FA0_CustomAttributesCacheGenerator_m_Transparent,
	ARUXAnimationManager_tBFA785140C2D44C6B8F4AAB9540BD574B1013FA0_CustomAttributesCacheGenerator_m_LocalizationManager,
	ARUXAnimationManager_tBFA785140C2D44C6B8F4AAB9540BD574B1013FA0_CustomAttributesCacheGenerator_m_LocalizeText,
	ARUXReasonsManager_t96AAD657A399018801E18CAEE6C4BAFC4C4704EB_CustomAttributesCacheGenerator_m_ShowNotTrackingReasons,
	ARUXReasonsManager_t96AAD657A399018801E18CAEE6C4BAFC4C4704EB_CustomAttributesCacheGenerator_m_ReasonDisplayText,
	ARUXReasonsManager_t96AAD657A399018801E18CAEE6C4BAFC4C4704EB_CustomAttributesCacheGenerator_m_ReasonParent,
	ARUXReasonsManager_t96AAD657A399018801E18CAEE6C4BAFC4C4704EB_CustomAttributesCacheGenerator_m_ReasonIcon,
	ARUXReasonsManager_t96AAD657A399018801E18CAEE6C4BAFC4C4704EB_CustomAttributesCacheGenerator_m_InitRelocalSprite,
	ARUXReasonsManager_t96AAD657A399018801E18CAEE6C4BAFC4C4704EB_CustomAttributesCacheGenerator_m_MotionSprite,
	ARUXReasonsManager_t96AAD657A399018801E18CAEE6C4BAFC4C4704EB_CustomAttributesCacheGenerator_m_LightSprite,
	ARUXReasonsManager_t96AAD657A399018801E18CAEE6C4BAFC4C4704EB_CustomAttributesCacheGenerator_m_FeaturesSprite,
	ARUXReasonsManager_t96AAD657A399018801E18CAEE6C4BAFC4C4704EB_CustomAttributesCacheGenerator_m_UnsupportedSprite,
	ARUXReasonsManager_t96AAD657A399018801E18CAEE6C4BAFC4C4704EB_CustomAttributesCacheGenerator_m_NoneSprite,
	ARUXReasonsManager_t96AAD657A399018801E18CAEE6C4BAFC4C4704EB_CustomAttributesCacheGenerator_m_LocalizationManager,
	ARUXReasonsManager_t96AAD657A399018801E18CAEE6C4BAFC4C4704EB_CustomAttributesCacheGenerator_m_LocalizeText,
	DisableTrackedVisuals_t9D362704CB23AA8F6AD79808444DB585E268A915_CustomAttributesCacheGenerator_m_DisableFeaturePoints,
	DisableTrackedVisuals_t9D362704CB23AA8F6AD79808444DB585E268A915_CustomAttributesCacheGenerator_m_DisablePlaneRendering,
	DisableTrackedVisuals_t9D362704CB23AA8F6AD79808444DB585E268A915_CustomAttributesCacheGenerator_m_PointCloudManager,
	DisableTrackedVisuals_t9D362704CB23AA8F6AD79808444DB585E268A915_CustomAttributesCacheGenerator_m_PlaneManager,
	LocalizationManager_t9A271A1BB9769E489F800511B98E6F10E345C36C_CustomAttributesCacheGenerator_m_SimplifiedChineseFont,
	LocalizationManager_t9A271A1BB9769E489F800511B98E6F10E345C36C_CustomAttributesCacheGenerator_m_JapaneseFont,
	LocalizationManager_t9A271A1BB9769E489F800511B98E6F10E345C36C_CustomAttributesCacheGenerator_m_KoreanFont,
	LocalizationManager_t9A271A1BB9769E489F800511B98E6F10E345C36C_CustomAttributesCacheGenerator_m_HindiFont,
	LocalizationManager_t9A271A1BB9769E489F800511B98E6F10E345C36C_CustomAttributesCacheGenerator_m_InstructionText,
	LocalizationManager_t9A271A1BB9769E489F800511B98E6F10E345C36C_CustomAttributesCacheGenerator_m_ReasonText,
	UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_m_StartWithInstructionalUI,
	UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_m_InstructionalUI,
	UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_m_InstructionalGoal,
	UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_m_ShowSecondaryInstructionalUI,
	UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_m_SecondaryInstructionUI,
	UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_m_SecondaryGoal,
	UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_m_CoachingOverlayFallback,
	UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_m_ARSessionOrigin,
	UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_m_PlaneManager,
	UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_m_FaceManager,
	UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_m_BodyManager,
	UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_m_ImageManager,
	UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_m_ObjectManager,
	UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_m_AnimationManager,
	UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_m_LocalizationManager,
	LeanInfoText_t508B956A97587F4ECF20EE95947A49736F76D477_CustomAttributesCacheGenerator_Format,
	LeanPulseScale_t984988192A1106BE7E5B31BAC1DA86CDE07F2A4E_CustomAttributesCacheGenerator_BaseScale,
	LeanPulseScale_t984988192A1106BE7E5B31BAC1DA86CDE07F2A4E_CustomAttributesCacheGenerator_Size,
	LeanPulseScale_t984988192A1106BE7E5B31BAC1DA86CDE07F2A4E_CustomAttributesCacheGenerator_PulseInterval,
	LeanPulseScale_t984988192A1106BE7E5B31BAC1DA86CDE07F2A4E_CustomAttributesCacheGenerator_PulseSize,
	LeanPulseScale_t984988192A1106BE7E5B31BAC1DA86CDE07F2A4E_CustomAttributesCacheGenerator_Damping,
	LeanDragCamera_t16CF0D4D777F2F3B0C5A76111C8BA13A2698419E_CustomAttributesCacheGenerator_Sensitivity,
	LeanDragCamera_t16CF0D4D777F2F3B0C5A76111C8BA13A2698419E_CustomAttributesCacheGenerator_Damping,
	LeanDragCamera_t16CF0D4D777F2F3B0C5A76111C8BA13A2698419E_CustomAttributesCacheGenerator_Inertia,
	LeanDragCamera_t16CF0D4D777F2F3B0C5A76111C8BA13A2698419E_CustomAttributesCacheGenerator_remainingDelta,
	LeanDragTrail_t72B928B6E60E86D0845027C361672322C2305841_CustomAttributesCacheGenerator_fingerDatas,
	LeanDragTranslate_tB311852063395381489EFEB52FC6849E0350EFBC_CustomAttributesCacheGenerator_Camera,
	LeanDragTranslate_tB311852063395381489EFEB52FC6849E0350EFBC_CustomAttributesCacheGenerator_Sensitivity,
	LeanDragTranslate_tB311852063395381489EFEB52FC6849E0350EFBC_CustomAttributesCacheGenerator_Damping,
	LeanDragTranslate_tB311852063395381489EFEB52FC6849E0350EFBC_CustomAttributesCacheGenerator_Inertia,
	LeanDragTranslate_tB311852063395381489EFEB52FC6849E0350EFBC_CustomAttributesCacheGenerator_remainingTranslation,
	LeanFingerDown_tBB4309FE0B73C4438020A3F4469626D1191939F2_CustomAttributesCacheGenerator_onFinger,
	LeanFingerDown_tBB4309FE0B73C4438020A3F4469626D1191939F2_CustomAttributesCacheGenerator_onWorld,
	LeanFingerDown_tBB4309FE0B73C4438020A3F4469626D1191939F2_CustomAttributesCacheGenerator_onScreen,
	LeanFingerOld_t37E4543AACC95F6C05B7094CBCFE2541FA5D959F_CustomAttributesCacheGenerator_onFinger,
	LeanFingerOld_t37E4543AACC95F6C05B7094CBCFE2541FA5D959F_CustomAttributesCacheGenerator_onWorld,
	LeanFingerOld_t37E4543AACC95F6C05B7094CBCFE2541FA5D959F_CustomAttributesCacheGenerator_onScreen,
	LeanFingerTap_t505333CF479CE38899CFC21A1943D0F3F0196CD7_CustomAttributesCacheGenerator_onFinger,
	LeanFingerTap_t505333CF479CE38899CFC21A1943D0F3F0196CD7_CustomAttributesCacheGenerator_onCount,
	LeanFingerTap_t505333CF479CE38899CFC21A1943D0F3F0196CD7_CustomAttributesCacheGenerator_onWorld,
	LeanFingerTap_t505333CF479CE38899CFC21A1943D0F3F0196CD7_CustomAttributesCacheGenerator_onScreen,
	LeanFingerUp_t8F38426263A3E2369766D376423034C82F9F5862_CustomAttributesCacheGenerator_onFinger,
	LeanFingerUp_t8F38426263A3E2369766D376423034C82F9F5862_CustomAttributesCacheGenerator_onWorld,
	LeanFingerUp_t8F38426263A3E2369766D376423034C82F9F5862_CustomAttributesCacheGenerator_onScreen,
	LeanFingerUpdate_tA215228160659B8EA8BF59A748C9C44DC5DAB897_CustomAttributesCacheGenerator_onFinger,
	LeanFingerUpdate_tA215228160659B8EA8BF59A748C9C44DC5DAB897_CustomAttributesCacheGenerator_onDelta,
	LeanFingerUpdate_tA215228160659B8EA8BF59A748C9C44DC5DAB897_CustomAttributesCacheGenerator_onDistance,
	LeanFingerUpdate_tA215228160659B8EA8BF59A748C9C44DC5DAB897_CustomAttributesCacheGenerator_onWorldFrom,
	LeanFingerUpdate_tA215228160659B8EA8BF59A748C9C44DC5DAB897_CustomAttributesCacheGenerator_onWorldTo,
	LeanFingerUpdate_tA215228160659B8EA8BF59A748C9C44DC5DAB897_CustomAttributesCacheGenerator_onWorldDelta,
	LeanFingerUpdate_tA215228160659B8EA8BF59A748C9C44DC5DAB897_CustomAttributesCacheGenerator_onWorldFromTo,
	LeanPinchScale_t06F48876FB3BF619EF69F2342975AB52C847FFF3_CustomAttributesCacheGenerator_Camera,
	LeanPinchScale_t06F48876FB3BF619EF69F2342975AB52C847FFF3_CustomAttributesCacheGenerator_Relative,
	LeanPinchScale_t06F48876FB3BF619EF69F2342975AB52C847FFF3_CustomAttributesCacheGenerator_Sensitivity,
	LeanPinchScale_t06F48876FB3BF619EF69F2342975AB52C847FFF3_CustomAttributesCacheGenerator_Damping,
	LeanPinchScale_t06F48876FB3BF619EF69F2342975AB52C847FFF3_CustomAttributesCacheGenerator_remainingScale,
	LeanSelectable_tDEC98CC1F068C6F567BD5E97A29892E6878548F4_CustomAttributesCacheGenerator_OnEnableGlobal,
	LeanSelectable_tDEC98CC1F068C6F567BD5E97A29892E6878548F4_CustomAttributesCacheGenerator_OnDisableGlobal,
	LeanSelectable_tDEC98CC1F068C6F567BD5E97A29892E6878548F4_CustomAttributesCacheGenerator_OnSelectGlobal,
	LeanSelectable_tDEC98CC1F068C6F567BD5E97A29892E6878548F4_CustomAttributesCacheGenerator_OnSelectSetGlobal,
	LeanSelectable_tDEC98CC1F068C6F567BD5E97A29892E6878548F4_CustomAttributesCacheGenerator_OnSelectUpGlobal,
	LeanSelectable_tDEC98CC1F068C6F567BD5E97A29892E6878548F4_CustomAttributesCacheGenerator_OnDeselectGlobal,
	LeanSelectable_tDEC98CC1F068C6F567BD5E97A29892E6878548F4_CustomAttributesCacheGenerator_onSelect,
	LeanSelectable_tDEC98CC1F068C6F567BD5E97A29892E6878548F4_CustomAttributesCacheGenerator_onSelectUpdate,
	LeanSelectable_tDEC98CC1F068C6F567BD5E97A29892E6878548F4_CustomAttributesCacheGenerator_onSelectUp,
	LeanSelectable_tDEC98CC1F068C6F567BD5E97A29892E6878548F4_CustomAttributesCacheGenerator_onDeselect,
	LeanSelectable_tDEC98CC1F068C6F567BD5E97A29892E6878548F4_CustomAttributesCacheGenerator_isSelected,
	LeanSelectableGraphicColor_t96DCEB3FA4B9F49177B551AAD1409552E69F893F_CustomAttributesCacheGenerator_AutoGetDefaultColor,
	LeanSelectableGraphicColor_t96DCEB3FA4B9F49177B551AAD1409552E69F893F_CustomAttributesCacheGenerator_DefaultColor,
	LeanSelectableGraphicColor_t96DCEB3FA4B9F49177B551AAD1409552E69F893F_CustomAttributesCacheGenerator_SelectedColor,
	LeanSelectableRendererColor_t25C955AA97AE743D1C5EDE3D23F15AEF045B0C6E_CustomAttributesCacheGenerator_AutoGetDefaultColor,
	LeanSelectableRendererColor_t25C955AA97AE743D1C5EDE3D23F15AEF045B0C6E_CustomAttributesCacheGenerator_DefaultColor,
	LeanSelectableRendererColor_t25C955AA97AE743D1C5EDE3D23F15AEF045B0C6E_CustomAttributesCacheGenerator_SelectedColor,
	LeanSelectableRendererColor_t25C955AA97AE743D1C5EDE3D23F15AEF045B0C6E_CustomAttributesCacheGenerator_CloneMaterials,
	LeanSelectableSpriteRendererColor_tF5EB93CEB97B728667E0AC04CEC48D325C6FA7A6_CustomAttributesCacheGenerator_AutoGetDefaultColor,
	LeanSelectableSpriteRendererColor_tF5EB93CEB97B728667E0AC04CEC48D325C6FA7A6_CustomAttributesCacheGenerator_DefaultColor,
	LeanSelectableSpriteRendererColor_tF5EB93CEB97B728667E0AC04CEC48D325C6FA7A6_CustomAttributesCacheGenerator_SelectedColor,
	LeanSwipeBase_t1B013BA10DE95C56E3B6460F3DF3CE38C918A699_CustomAttributesCacheGenerator_RequiredAngle,
	LeanSwipeBase_t1B013BA10DE95C56E3B6460F3DF3CE38C918A699_CustomAttributesCacheGenerator_RequiredArc,
	LeanSwipeBase_t1B013BA10DE95C56E3B6460F3DF3CE38C918A699_CustomAttributesCacheGenerator_onFinger,
	LeanSwipeBase_t1B013BA10DE95C56E3B6460F3DF3CE38C918A699_CustomAttributesCacheGenerator_Modify,
	LeanSwipeBase_t1B013BA10DE95C56E3B6460F3DF3CE38C918A699_CustomAttributesCacheGenerator_onDelta,
	LeanSwipeBase_t1B013BA10DE95C56E3B6460F3DF3CE38C918A699_CustomAttributesCacheGenerator_onDistance,
	LeanSwipeBase_t1B013BA10DE95C56E3B6460F3DF3CE38C918A699_CustomAttributesCacheGenerator_onWorldFrom,
	LeanSwipeBase_t1B013BA10DE95C56E3B6460F3DF3CE38C918A699_CustomAttributesCacheGenerator_onWorldTo,
	LeanSwipeBase_t1B013BA10DE95C56E3B6460F3DF3CE38C918A699_CustomAttributesCacheGenerator_onWorldDelta,
	LeanSwipeBase_t1B013BA10DE95C56E3B6460F3DF3CE38C918A699_CustomAttributesCacheGenerator_onWorldFromTo,
	LeanTwistRotate_tFE8411F228C2CECB7893EE098C277A7A37C89A7F_CustomAttributesCacheGenerator_Camera,
	LeanTwistRotate_tFE8411F228C2CECB7893EE098C277A7A37C89A7F_CustomAttributesCacheGenerator_Relative,
	LeanTwistRotate_tFE8411F228C2CECB7893EE098C277A7A37C89A7F_CustomAttributesCacheGenerator_Damping,
	LeanTwistRotate_tFE8411F228C2CECB7893EE098C277A7A37C89A7F_CustomAttributesCacheGenerator_remainingTranslation,
	LeanTwistRotate_tFE8411F228C2CECB7893EE098C277A7A37C89A7F_CustomAttributesCacheGenerator_remainingRotation,
	LeanTwistRotateAxis_t9DCE177FE4FA445368B0112319E88771C3A76F1F_CustomAttributesCacheGenerator_Axis,
	LeanTwistRotateAxis_t9DCE177FE4FA445368B0112319E88771C3A76F1F_CustomAttributesCacheGenerator_Space,
	LeanTwistRotateAxis_t9DCE177FE4FA445368B0112319E88771C3A76F1F_CustomAttributesCacheGenerator_Sensitivity,
	LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_OnFingerDown,
	LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_OnFingerUpdate,
	LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_OnFingerUp,
	LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_OnFingerOld,
	LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_OnFingerTap,
	LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_OnFingerSwipe,
	LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_OnGesture,
	LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_OnFingerExpired,
	LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_OnFingerInactive,
	LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_TapThreshold,
	LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_SwipeThreshold,
	LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_ReferenceDpi,
	LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_GuiLayers,
	LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_RecordFingers,
	LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_RecordThreshold,
	LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_RecordLimit,
	LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_SimulateMultiFingers,
	LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_PinchTwistKey,
	LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_MovePivotKey,
	LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_MultiDragKey,
	LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_FingerTexture,
	LeanPath_t5BEE07257CD2375F5278E36308C8DBE30EF7DBC5_CustomAttributesCacheGenerator_Points,
	LeanPath_t5BEE07257CD2375F5278E36308C8DBE30EF7DBC5_CustomAttributesCacheGenerator_Loop,
	LeanPath_t5BEE07257CD2375F5278E36308C8DBE30EF7DBC5_CustomAttributesCacheGenerator_Space,
	LeanPath_t5BEE07257CD2375F5278E36308C8DBE30EF7DBC5_CustomAttributesCacheGenerator_Smoothing,
	LeanPath_t5BEE07257CD2375F5278E36308C8DBE30EF7DBC5_CustomAttributesCacheGenerator_Visual,
	LeanRoll_t673925EEFDF4F8F916A554552A0E4A8F3901D2CE_CustomAttributesCacheGenerator_Damping,
	LeanRoll_t673925EEFDF4F8F916A554552A0E4A8F3901D2CE_CustomAttributesCacheGenerator_currentAngle,
	LeanLinkTo_t853CB80C0CCBFE9C9B216E39C9039043DDCE2872_CustomAttributesCacheGenerator_link,
	LightEstimation_t9843B4C3725AE20D953D7D4AFA849B024DC06925_CustomAttributesCacheGenerator_LightEstimation_get_brightness_m81B8B41717C95613B6B530C7A4DA253B82D41251,
	LightEstimation_t9843B4C3725AE20D953D7D4AFA849B024DC06925_CustomAttributesCacheGenerator_LightEstimation_set_brightness_mF9D7CF55B64154A0E24B60520F2D2FB0EBB03370,
	LightEstimation_t9843B4C3725AE20D953D7D4AFA849B024DC06925_CustomAttributesCacheGenerator_LightEstimation_get_colorTemperature_mFC6A5877B4551367A9DA5FEBE1F809D3E97C9CA4,
	LightEstimation_t9843B4C3725AE20D953D7D4AFA849B024DC06925_CustomAttributesCacheGenerator_LightEstimation_set_colorTemperature_m2094B346618FB1A940F97CCD138109BB438FF420,
	LightEstimation_t9843B4C3725AE20D953D7D4AFA849B024DC06925_CustomAttributesCacheGenerator_LightEstimation_get_colorCorrection_m1DE07508BFE7757692C7831FAC0E0F58BDB64941,
	LightEstimation_t9843B4C3725AE20D953D7D4AFA849B024DC06925_CustomAttributesCacheGenerator_LightEstimation_set_colorCorrection_mD59CF43A84D0DE0089C83409838DE311AA902D23,
	LightEstimation_t9843B4C3725AE20D953D7D4AFA849B024DC06925_CustomAttributesCacheGenerator_LightEstimation_get_mainLightDirection_m9F63CCA98C50343FB0332DD4AA330F8678A9C02C,
	LightEstimation_t9843B4C3725AE20D953D7D4AFA849B024DC06925_CustomAttributesCacheGenerator_LightEstimation_set_mainLightDirection_mE2E416FED9DC9D5768A8A57A02A2F90C6DE0F105,
	LightEstimation_t9843B4C3725AE20D953D7D4AFA849B024DC06925_CustomAttributesCacheGenerator_LightEstimation_get_mainLightColor_m33449C2C041A80E61B6318A06C33623E6FAEB5C8,
	LightEstimation_t9843B4C3725AE20D953D7D4AFA849B024DC06925_CustomAttributesCacheGenerator_LightEstimation_set_mainLightColor_m701E00A2FECDFF842A1138EF07899BF23D63AAB4,
	LightEstimation_t9843B4C3725AE20D953D7D4AFA849B024DC06925_CustomAttributesCacheGenerator_LightEstimation_get_mainLightIntensityLumens_mDF8695D4DEEBBF8E325DF72E87AB929772E077C0,
	LightEstimation_t9843B4C3725AE20D953D7D4AFA849B024DC06925_CustomAttributesCacheGenerator_LightEstimation_set_mainLightIntensityLumens_m7606C8F109CBE83D3A4B8BFFD83E50134064B280,
	LightEstimation_t9843B4C3725AE20D953D7D4AFA849B024DC06925_CustomAttributesCacheGenerator_LightEstimation_get_sphericalHarmonics_m6FFAA5F42C14D0620BA4B570ED63A0B3AD72EA95,
	LightEstimation_t9843B4C3725AE20D953D7D4AFA849B024DC06925_CustomAttributesCacheGenerator_LightEstimation_set_sphericalHarmonics_m8E39570E4DED98CD4C5C05DC0C15B0D0F8036ED0,
	PlaceObjectsOnPlane_tBC077C6ABB6A15D8E3AE99CCC9966B4E562F5417_CustomAttributesCacheGenerator_PlaceObjectsOnPlane_get_spawnedObject_m0C450181B9312C9BDFEBFF7724566880F8C54F16,
	PlaceObjectsOnPlane_tBC077C6ABB6A15D8E3AE99CCC9966B4E562F5417_CustomAttributesCacheGenerator_PlaceObjectsOnPlane_set_spawnedObject_m5E0C55D3C00530D85D1D71ECA5F8B076664EBAC5,
	PlaceObjectsOnPlane_tBC077C6ABB6A15D8E3AE99CCC9966B4E562F5417_CustomAttributesCacheGenerator_PlaceObjectsOnPlane_add_onPlacedObject_mBE87A45F356D35BEE6369C97B9C679C26988B896,
	PlaceObjectsOnPlane_tBC077C6ABB6A15D8E3AE99CCC9966B4E562F5417_CustomAttributesCacheGenerator_PlaceObjectsOnPlane_remove_onPlacedObject_mD603DEC49025DCB32DEBD81498F8FF12A11DC818,
	ARUXAnimationManager_tBFA785140C2D44C6B8F4AAB9540BD574B1013FA0_CustomAttributesCacheGenerator_ARUXAnimationManager_add_onFadeOffComplete_m988A1D4D19242C1067E194A5BA32C5D1988C282D,
	ARUXAnimationManager_tBFA785140C2D44C6B8F4AAB9540BD574B1013FA0_CustomAttributesCacheGenerator_ARUXAnimationManager_remove_onFadeOffComplete_m7D39849FBC6A57AB6D63D48A5E7B3DF2695D89DB,
	LocalizationManager_t9A271A1BB9769E489F800511B98E6F10E345C36C_CustomAttributesCacheGenerator_LocalizationManager_Start_mCCC2E00354521D23FD5CBDB28FD347B27956BC61,
	UIManager_t77C2B965B55C450F7226A05FE391FF12B5CE7858_CustomAttributesCacheGenerator_UIManager_U3COnEnableU3Eb__69_0_m6E36F214C2EC57CB40BEDD772DA012EE40D56CBC,
	SceneController_t8611277038809278C31AE35555719D7AE5438CA9_CustomAttributesCacheGenerator_SceneController_EnableBatsman_m583C8D3A4A1A27E0F5E121DCD72731F8CC063DA5,
	LeanDragCamera_t16CF0D4D777F2F3B0C5A76111C8BA13A2698419E_CustomAttributesCacheGenerator_LeanDragCamera_MoveToSelection_mC17CA5F136F00D57EDEF2F3193F53116ABC918D6,
	LeanSelect_t137EC6EEC0B436A563AD37A99842A7DF9D752A5B_CustomAttributesCacheGenerator_LeanSelect_DeselectAll_mD01A1B1C4EBA4A68E1E100411B80466440D71558,
	LeanSelectable_tDEC98CC1F068C6F567BD5E97A29892E6878548F4_CustomAttributesCacheGenerator_LeanSelectable_add_OnEnableGlobal_m9F457F205DDA6C06927E2085DF445E365E026816,
	LeanSelectable_tDEC98CC1F068C6F567BD5E97A29892E6878548F4_CustomAttributesCacheGenerator_LeanSelectable_remove_OnEnableGlobal_mD41F740F27DBE3C58673A0D289FC741CBB119637,
	LeanSelectable_tDEC98CC1F068C6F567BD5E97A29892E6878548F4_CustomAttributesCacheGenerator_LeanSelectable_add_OnDisableGlobal_mBF955FC5ACF3C04DE55AAF330E2EA836BB2C4F42,
	LeanSelectable_tDEC98CC1F068C6F567BD5E97A29892E6878548F4_CustomAttributesCacheGenerator_LeanSelectable_remove_OnDisableGlobal_m48850D7E0A5B384458358B24209EED8B5E12772D,
	LeanSelectable_tDEC98CC1F068C6F567BD5E97A29892E6878548F4_CustomAttributesCacheGenerator_LeanSelectable_add_OnSelectGlobal_m26EC561CD6AF8AE8BFE48B3B6692A8221770FF2A,
	LeanSelectable_tDEC98CC1F068C6F567BD5E97A29892E6878548F4_CustomAttributesCacheGenerator_LeanSelectable_remove_OnSelectGlobal_mE31A611550B3C16DF21901A218FAF4823FAAC86A,
	LeanSelectable_tDEC98CC1F068C6F567BD5E97A29892E6878548F4_CustomAttributesCacheGenerator_LeanSelectable_add_OnSelectSetGlobal_mE22AC4FE50CDA36803F262FD6530888D05FFB5D6,
	LeanSelectable_tDEC98CC1F068C6F567BD5E97A29892E6878548F4_CustomAttributesCacheGenerator_LeanSelectable_remove_OnSelectSetGlobal_m2281234B22D87DA7AB917E92F8D7D2DC8CE7F405,
	LeanSelectable_tDEC98CC1F068C6F567BD5E97A29892E6878548F4_CustomAttributesCacheGenerator_LeanSelectable_add_OnSelectUpGlobal_mB6012790E296C34FF4DE19F55CB0AAF1BEEE7AD1,
	LeanSelectable_tDEC98CC1F068C6F567BD5E97A29892E6878548F4_CustomAttributesCacheGenerator_LeanSelectable_remove_OnSelectUpGlobal_mBA331CE50DEB92F005AB5B80156D627CC8DC2994,
	LeanSelectable_tDEC98CC1F068C6F567BD5E97A29892E6878548F4_CustomAttributesCacheGenerator_LeanSelectable_add_OnDeselectGlobal_m3C2708774D3E0D3DFB0FD2FD31DBA84DA41D83D1,
	LeanSelectable_tDEC98CC1F068C6F567BD5E97A29892E6878548F4_CustomAttributesCacheGenerator_LeanSelectable_remove_OnDeselectGlobal_m422D6B7107F1A8A9CC7E27742163B1222EF37B71,
	LeanSelectable_tDEC98CC1F068C6F567BD5E97A29892E6878548F4_CustomAttributesCacheGenerator_LeanSelectable_Select_m846A32582C9696ED9046C46A5645BC6954415C3C,
	LeanSelectable_tDEC98CC1F068C6F567BD5E97A29892E6878548F4_CustomAttributesCacheGenerator_LeanSelectable_Deselect_m194A710A0CCCED6142EBCE699D6ED5CFA69309E9,
	LeanSelectableBehaviour_tC3D23A3120B57C535B1F35DC33E0AA359BD0854E_CustomAttributesCacheGenerator_LeanSelectableBehaviour_Register_m58EB6A8F0755B458F63BFF6D227A5E069F435021,
	LeanSelectableBehaviour_tC3D23A3120B57C535B1F35DC33E0AA359BD0854E_CustomAttributesCacheGenerator_LeanSelectableBehaviour_Unregister_m6435986D4F6690B1985177ADDE732C82BAF20DC6,
	LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_LeanTouch_add_OnFingerDown_m17FAE9CD13F1E43586D22F8195DCB4849D40EF9D,
	LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_LeanTouch_remove_OnFingerDown_m7D8D8908F245868EB3BA75FF2FE81363BD853BC5,
	LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_LeanTouch_add_OnFingerUpdate_mDD30E72D4128AC7F7DEE314AFDD1F63B3BC54255,
	LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_LeanTouch_remove_OnFingerUpdate_m58B05A304E7E3A468A3967B4B826383F3FB57CF2,
	LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_LeanTouch_add_OnFingerUp_m63B6C30AB7B97D4009CF0611396C233A9B3C92BF,
	LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_LeanTouch_remove_OnFingerUp_mE1B669D2A145F54FC5147DE6374782507D803777,
	LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_LeanTouch_add_OnFingerOld_mA1F82698C5926977AA0D01602C89FD70307EBD94,
	LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_LeanTouch_remove_OnFingerOld_mA0BF26E8F3EB1309005BFA908CB823F20FAEA8A6,
	LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_LeanTouch_add_OnFingerTap_m5BA04B655B78782D12319DD143F4E01997E6594E,
	LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_LeanTouch_remove_OnFingerTap_m17033D5DBDA4CD4AF877E1D1C9C6EB1D9236E155,
	LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_LeanTouch_add_OnFingerSwipe_mDEE2185B28C5117DCD3E7E9A0CEC5B18BA206BFA,
	LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_LeanTouch_remove_OnFingerSwipe_m36D61DE4F64B42907985C7E154FB3D9D7E1BE1DD,
	LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_LeanTouch_add_OnGesture_mCAFCA85BE9F5DCFC821AB3A702B592437BBF0589,
	LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_LeanTouch_remove_OnGesture_mE803955E7B5635B8941DBBF930462EDCD1E0EBF2,
	LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_LeanTouch_add_OnFingerExpired_mDC3B4CAF9DEB8427D0CCDF27BEF598D4930D777F,
	LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_LeanTouch_remove_OnFingerExpired_m7C90A349CCA6B72C366D3F61B61E7AB286D6F17E,
	LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_LeanTouch_add_OnFingerInactive_m226A4E6851247A04C851E444A511372D056F4836,
	LeanTouch_tB1FBB211C6469AA4B59D127079571DE879C40B23_CustomAttributesCacheGenerator_LeanTouch_remove_OnFingerInactive_mACEDB928D81225C7EEAE253E288E35111051BB84,
	LeanRoll_t673925EEFDF4F8F916A554552A0E4A8F3901D2CE_CustomAttributesCacheGenerator_LeanRoll_SnapToTarget_m38645C71A54CE1A9346C3CB4FA7BBCEAB964CC29,
	U3CStartU3Ed__55_tAD522FF1E9F588E49FF94436005D673167775F79_CustomAttributesCacheGenerator_U3CStartU3Ed__55__ctor_m987504CCE2A04E56489F015DDF3279DC89C0F080,
	U3CStartU3Ed__55_tAD522FF1E9F588E49FF94436005D673167775F79_CustomAttributesCacheGenerator_U3CStartU3Ed__55_System_IDisposable_Dispose_m9728BAF46DD084B007DFFE1D4F6F87B2DB1E1554,
	U3CStartU3Ed__55_tAD522FF1E9F588E49FF94436005D673167775F79_CustomAttributesCacheGenerator_U3CStartU3Ed__55_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3EC035A1A71FEA9171B22B400ABDAB7FFF2FB3ED,
	U3CStartU3Ed__55_tAD522FF1E9F588E49FF94436005D673167775F79_CustomAttributesCacheGenerator_U3CStartU3Ed__55_System_Collections_IEnumerator_Reset_mE1713764C96D07B62E7DE7F5EA52D344AC64C15A,
	U3CStartU3Ed__55_tAD522FF1E9F588E49FF94436005D673167775F79_CustomAttributesCacheGenerator_U3CStartU3Ed__55_System_Collections_IEnumerator_get_Current_m1F76D144731727E498C37AD026E5F6D941A40D57,
	U3CEnableBatsmanU3Ed__6_t9F28F6ED1F3B9EFF2BF57316D59057D3F25751C3_CustomAttributesCacheGenerator_U3CEnableBatsmanU3Ed__6__ctor_mEEC4B8F973D62718060076D669597F838C324FD9,
	U3CEnableBatsmanU3Ed__6_t9F28F6ED1F3B9EFF2BF57316D59057D3F25751C3_CustomAttributesCacheGenerator_U3CEnableBatsmanU3Ed__6_System_IDisposable_Dispose_mB0483598E83315D30E92EAAD43D5AF10D57FBF54,
	U3CEnableBatsmanU3Ed__6_t9F28F6ED1F3B9EFF2BF57316D59057D3F25751C3_CustomAttributesCacheGenerator_U3CEnableBatsmanU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3679DAB9B0B5B9B44605F19D1D8D3ECB2DA8AE26,
	U3CEnableBatsmanU3Ed__6_t9F28F6ED1F3B9EFF2BF57316D59057D3F25751C3_CustomAttributesCacheGenerator_U3CEnableBatsmanU3Ed__6_System_Collections_IEnumerator_Reset_m5A6F7084CEF295AA235E9F6D9241FFA8D7D394B5,
	U3CEnableBatsmanU3Ed__6_t9F28F6ED1F3B9EFF2BF57316D59057D3F25751C3_CustomAttributesCacheGenerator_U3CEnableBatsmanU3Ed__6_System_Collections_IEnumerator_get_Current_m3ACB8AAEC04E99894B01D1FDEA401A4A881093DD,
	AssemblyU2DCSharp_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
