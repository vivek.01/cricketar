﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void CenterScreenHelper::Awake()
extern void CenterScreenHelper_Awake_m58C190250B1A7130B403349568E32464AE0C7544 (void);
// 0x00000002 UnityEngine.Vector2 CenterScreenHelper::GetCenterScreen()
extern void CenterScreenHelper_GetCenterScreen_mCD85AF7079FE1D3E940A5C48429962FE02FFEB94 (void);
// 0x00000003 System.Void CenterScreenHelper::.ctor()
extern void CenterScreenHelper__ctor_mCE4DCA550136049FC0DB7AC3CF171B16F13ABA10 (void);
// 0x00000004 UnityEngine.XR.ARFoundation.ARCameraManager LightEstimation::get_cameraManager()
extern void LightEstimation_get_cameraManager_mE01BC5E17D16A6843D648865853E23A07CF1543B (void);
// 0x00000005 System.Void LightEstimation::set_cameraManager(UnityEngine.XR.ARFoundation.ARCameraManager)
extern void LightEstimation_set_cameraManager_mF8BDE678B769ABCF7324064B1A4052F558934AE8 (void);
// 0x00000006 System.Nullable`1<System.Single> LightEstimation::get_brightness()
extern void LightEstimation_get_brightness_m81B8B41717C95613B6B530C7A4DA253B82D41251 (void);
// 0x00000007 System.Void LightEstimation::set_brightness(System.Nullable`1<System.Single>)
extern void LightEstimation_set_brightness_mF9D7CF55B64154A0E24B60520F2D2FB0EBB03370 (void);
// 0x00000008 System.Nullable`1<System.Single> LightEstimation::get_colorTemperature()
extern void LightEstimation_get_colorTemperature_mFC6A5877B4551367A9DA5FEBE1F809D3E97C9CA4 (void);
// 0x00000009 System.Void LightEstimation::set_colorTemperature(System.Nullable`1<System.Single>)
extern void LightEstimation_set_colorTemperature_m2094B346618FB1A940F97CCD138109BB438FF420 (void);
// 0x0000000A System.Nullable`1<UnityEngine.Color> LightEstimation::get_colorCorrection()
extern void LightEstimation_get_colorCorrection_m1DE07508BFE7757692C7831FAC0E0F58BDB64941 (void);
// 0x0000000B System.Void LightEstimation::set_colorCorrection(System.Nullable`1<UnityEngine.Color>)
extern void LightEstimation_set_colorCorrection_mD59CF43A84D0DE0089C83409838DE311AA902D23 (void);
// 0x0000000C System.Nullable`1<UnityEngine.Vector3> LightEstimation::get_mainLightDirection()
extern void LightEstimation_get_mainLightDirection_m9F63CCA98C50343FB0332DD4AA330F8678A9C02C (void);
// 0x0000000D System.Void LightEstimation::set_mainLightDirection(System.Nullable`1<UnityEngine.Vector3>)
extern void LightEstimation_set_mainLightDirection_mE2E416FED9DC9D5768A8A57A02A2F90C6DE0F105 (void);
// 0x0000000E System.Nullable`1<UnityEngine.Color> LightEstimation::get_mainLightColor()
extern void LightEstimation_get_mainLightColor_m33449C2C041A80E61B6318A06C33623E6FAEB5C8 (void);
// 0x0000000F System.Void LightEstimation::set_mainLightColor(System.Nullable`1<UnityEngine.Color>)
extern void LightEstimation_set_mainLightColor_m701E00A2FECDFF842A1138EF07899BF23D63AAB4 (void);
// 0x00000010 System.Nullable`1<System.Single> LightEstimation::get_mainLightIntensityLumens()
extern void LightEstimation_get_mainLightIntensityLumens_mDF8695D4DEEBBF8E325DF72E87AB929772E077C0 (void);
// 0x00000011 System.Void LightEstimation::set_mainLightIntensityLumens(System.Nullable`1<System.Single>)
extern void LightEstimation_set_mainLightIntensityLumens_m7606C8F109CBE83D3A4B8BFFD83E50134064B280 (void);
// 0x00000012 System.Nullable`1<UnityEngine.Rendering.SphericalHarmonicsL2> LightEstimation::get_sphericalHarmonics()
extern void LightEstimation_get_sphericalHarmonics_m6FFAA5F42C14D0620BA4B570ED63A0B3AD72EA95 (void);
// 0x00000013 System.Void LightEstimation::set_sphericalHarmonics(System.Nullable`1<UnityEngine.Rendering.SphericalHarmonicsL2>)
extern void LightEstimation_set_sphericalHarmonics_m8E39570E4DED98CD4C5C05DC0C15B0D0F8036ED0 (void);
// 0x00000014 System.Void LightEstimation::Awake()
extern void LightEstimation_Awake_m235FD397AF66B30969C55A03662A0AF3C939CEAA (void);
// 0x00000015 System.Void LightEstimation::OnEnable()
extern void LightEstimation_OnEnable_mD46CAFE75E1103FE8AA34C6CE94C6DC88F0BC39E (void);
// 0x00000016 System.Void LightEstimation::OnDisable()
extern void LightEstimation_OnDisable_mE6B9C5CD8516C5F09FCDA60AAC1E2FA6E58C9CEE (void);
// 0x00000017 System.Void LightEstimation::FrameChanged(UnityEngine.XR.ARFoundation.ARCameraFrameEventArgs)
extern void LightEstimation_FrameChanged_m1ACDB8AAC00FED1731FBD0F1FE81B8FC2B897840 (void);
// 0x00000018 System.Void LightEstimation::.ctor()
extern void LightEstimation__ctor_m8383629073F32A42EA866DE00BBBD46F66296032 (void);
// 0x00000019 System.Boolean PlacementReticle::get_snapToMesh()
extern void PlacementReticle_get_snapToMesh_mFCD2BF8AFDA208DD4889790B5C943E5C2BA51DA5 (void);
// 0x0000001A System.Void PlacementReticle::set_snapToMesh(System.Boolean)
extern void PlacementReticle_set_snapToMesh_mAC9E833925E3B85FEE8C833C26FFCD1CEECF13E5 (void);
// 0x0000001B UnityEngine.XR.ARFoundation.ARRaycastManager PlacementReticle::get_raycastManager()
extern void PlacementReticle_get_raycastManager_m63160F54CBBD75F8AB12238B5FCCBCD255FF90DB (void);
// 0x0000001C System.Void PlacementReticle::set_raycastManager(UnityEngine.XR.ARFoundation.ARRaycastManager)
extern void PlacementReticle_set_raycastManager_m60EA76792BDBFB10387B24F93A77B432DE70E906 (void);
// 0x0000001D UnityEngine.GameObject PlacementReticle::get_reticlePrefab()
extern void PlacementReticle_get_reticlePrefab_m68D1D0BA7E9C7DA7E1CEF303522A366ED2BF24A3 (void);
// 0x0000001E System.Void PlacementReticle::set_reticlePrefab(UnityEngine.GameObject)
extern void PlacementReticle_set_reticlePrefab_m1D0E650E3D544E428FC13D72B38101F67EF90603 (void);
// 0x0000001F System.Boolean PlacementReticle::get_distanceScale()
extern void PlacementReticle_get_distanceScale_mFC07759FC3540279CADA55DE80F6A1D2A33DA60F (void);
// 0x00000020 System.Void PlacementReticle::set_distanceScale(System.Boolean)
extern void PlacementReticle_set_distanceScale_m2F3D439A828914F8C11B700C874DF0FE5032FC09 (void);
// 0x00000021 UnityEngine.Transform PlacementReticle::get_cameraTransform()
extern void PlacementReticle_get_cameraTransform_mDEC424B4BCE0F314448E6ADDDF8A76F5B1A809BC (void);
// 0x00000022 System.Void PlacementReticle::set_cameraTransform(UnityEngine.Transform)
extern void PlacementReticle_set_cameraTransform_m16D0A3E9D0854C0583C84FAF3EC852D8582B89A9 (void);
// 0x00000023 System.Void PlacementReticle::Start()
extern void PlacementReticle_Start_m1C2FD5FFB0577E974F88B4A06B4328BBDD62FEE0 (void);
// 0x00000024 System.Void PlacementReticle::Update()
extern void PlacementReticle_Update_mE860D76D1869D685D4F18AFA996720486E0CC0BE (void);
// 0x00000025 UnityEngine.Transform PlacementReticle::GetReticlePosition()
extern void PlacementReticle_GetReticlePosition_m1F51136B5CC3F1265B5762BE6C733F9C5E175839 (void);
// 0x00000026 System.Void PlacementReticle::.ctor()
extern void PlacementReticle__ctor_mF0EA4954E8CDA6F19CDC34C742ED1CE371CB1C75 (void);
// 0x00000027 System.Void PlacementReticle::.cctor()
extern void PlacementReticle__cctor_mC8A11E65ABFDFF03ECD3911204B561F9E4EB84A4 (void);
// 0x00000028 UnityEngine.XR.ARFoundation.AREnvironmentProbeManager ProbePlacement::get_probeManager()
extern void ProbePlacement_get_probeManager_m54B80D3E77C43E104DAA54C8A0C49686615893B9 (void);
// 0x00000029 System.Void ProbePlacement::set_probeManager(UnityEngine.XR.ARFoundation.AREnvironmentProbeManager)
extern void ProbePlacement_set_probeManager_m556A87C8CBB842EE577814BEF1417DA3FF6FD71D (void);
// 0x0000002A UnityEngine.XR.ARFoundation.ARRaycastManager ProbePlacement::get_raycastManager()
extern void ProbePlacement_get_raycastManager_mB9EE8EC22BC493DF62CA6E2AD5A47F7D5AA64EAB (void);
// 0x0000002B System.Void ProbePlacement::set_raycastManager(UnityEngine.XR.ARFoundation.ARRaycastManager)
extern void ProbePlacement_set_raycastManager_mA34ED80F26318B24C791B4872BD2167ED72D3918 (void);
// 0x0000002C System.Void ProbePlacement::Update()
extern void ProbePlacement_Update_m22AA5CC6A30571E16ED67DE9C20D6DC94559E918 (void);
// 0x0000002D System.Void ProbePlacement::.ctor()
extern void ProbePlacement__ctor_m2CD3A6FF9CADFF7C5A709739BA6903A46696A0D3 (void);
// 0x0000002E System.Single ARFeatheredPlaneMeshVisualizer::get_featheringWidth()
extern void ARFeatheredPlaneMeshVisualizer_get_featheringWidth_m0AAD92A3D682C1DFC1B5630585550B6CAFBAFE32 (void);
// 0x0000002F System.Void ARFeatheredPlaneMeshVisualizer::set_featheringWidth(System.Single)
extern void ARFeatheredPlaneMeshVisualizer_set_featheringWidth_m4761377095817FA81908BC43FD08EF600F6CC231 (void);
// 0x00000030 System.Void ARFeatheredPlaneMeshVisualizer::Awake()
extern void ARFeatheredPlaneMeshVisualizer_Awake_m7FD9AC4C30D2F12A0A30CC3CAD9EC6C17B07C45D (void);
// 0x00000031 System.Void ARFeatheredPlaneMeshVisualizer::OnEnable()
extern void ARFeatheredPlaneMeshVisualizer_OnEnable_mEE0AFFC028F876DE841312FEB8CA15FEAD105FE1 (void);
// 0x00000032 System.Void ARFeatheredPlaneMeshVisualizer::OnDisable()
extern void ARFeatheredPlaneMeshVisualizer_OnDisable_m51213AC9D2DBD97C93CC41330F65D74661D5DC81 (void);
// 0x00000033 System.Void ARFeatheredPlaneMeshVisualizer::ARPlane_boundaryUpdated(UnityEngine.XR.ARFoundation.ARPlaneBoundaryChangedEventArgs)
extern void ARFeatheredPlaneMeshVisualizer_ARPlane_boundaryUpdated_m354D58D0B29B40FE6D3A6689590E271D9282323F (void);
// 0x00000034 System.Void ARFeatheredPlaneMeshVisualizer::GenerateBoundaryUVs(UnityEngine.Mesh)
extern void ARFeatheredPlaneMeshVisualizer_GenerateBoundaryUVs_m241EDA47E1C4DECA8CCBC61D8DF6EC300546CDF0 (void);
// 0x00000035 System.Void ARFeatheredPlaneMeshVisualizer::.ctor()
extern void ARFeatheredPlaneMeshVisualizer__ctor_mA27A238E88D5FBA54D4DDFE588080DD41B449A4C (void);
// 0x00000036 System.Void ARFeatheredPlaneMeshVisualizer::.cctor()
extern void ARFeatheredPlaneMeshVisualizer__cctor_mF5C9F857AC060614B46B9359B3F0859FBC309DF3 (void);
// 0x00000037 System.Void FadePlaneOnBoundaryChange::OnEnable()
extern void FadePlaneOnBoundaryChange_OnEnable_m9A290421CF90C45F885B14F3E1DA888BFD27A340 (void);
// 0x00000038 System.Void FadePlaneOnBoundaryChange::OnDisable()
extern void FadePlaneOnBoundaryChange_OnDisable_m0C62572D4B470C313E0B203F6AE948865F395825 (void);
// 0x00000039 System.Void FadePlaneOnBoundaryChange::Update()
extern void FadePlaneOnBoundaryChange_Update_mBB620E21C1BE2FE4EF8F45D1C14EBE6C5579F8A8 (void);
// 0x0000003A System.Void FadePlaneOnBoundaryChange::PlaneOnBoundaryChanged(UnityEngine.XR.ARFoundation.ARPlaneBoundaryChangedEventArgs)
extern void FadePlaneOnBoundaryChange_PlaneOnBoundaryChanged_mF362091F455EDBF9437C0B672D7CE83ED4CD20CC (void);
// 0x0000003B System.Void FadePlaneOnBoundaryChange::.ctor()
extern void FadePlaneOnBoundaryChange__ctor_m6625F1DF41CF81E94037383DFBEDE2FB0237096E (void);
// 0x0000003C UnityEngine.GameObject PlaceObjectsOnPlane::get_placedPrefab()
extern void PlaceObjectsOnPlane_get_placedPrefab_m1DF1DEA50DD1EB8FA27F91E9E36CD05F9996AD47 (void);
// 0x0000003D System.Void PlaceObjectsOnPlane::set_placedPrefab(UnityEngine.GameObject)
extern void PlaceObjectsOnPlane_set_placedPrefab_mF8AB158FD6274BDE9CD2CBCD8D07C363150AB9F4 (void);
// 0x0000003E UnityEngine.GameObject PlaceObjectsOnPlane::get_spawnedObject()
extern void PlaceObjectsOnPlane_get_spawnedObject_m0C450181B9312C9BDFEBFF7724566880F8C54F16 (void);
// 0x0000003F System.Void PlaceObjectsOnPlane::set_spawnedObject(UnityEngine.GameObject)
extern void PlaceObjectsOnPlane_set_spawnedObject_m5E0C55D3C00530D85D1D71ECA5F8B076664EBAC5 (void);
// 0x00000040 System.Void PlaceObjectsOnPlane::add_onPlacedObject(System.Action)
extern void PlaceObjectsOnPlane_add_onPlacedObject_mBE87A45F356D35BEE6369C97B9C679C26988B896 (void);
// 0x00000041 System.Void PlaceObjectsOnPlane::remove_onPlacedObject(System.Action)
extern void PlaceObjectsOnPlane_remove_onPlacedObject_mD603DEC49025DCB32DEBD81498F8FF12A11DC818 (void);
// 0x00000042 System.Boolean PlaceObjectsOnPlane::get_canReposition()
extern void PlaceObjectsOnPlane_get_canReposition_m23D1F0BD936618915374B516A3469A600A318180 (void);
// 0x00000043 System.Void PlaceObjectsOnPlane::set_canReposition(System.Boolean)
extern void PlaceObjectsOnPlane_set_canReposition_mEE47BA8AB9213401E15E5B7FE58C83F9759EA088 (void);
// 0x00000044 System.Void PlaceObjectsOnPlane::Awake()
extern void PlaceObjectsOnPlane_Awake_mA25B1581D947FA1688472EB160973D208A610A66 (void);
// 0x00000045 System.Void PlaceObjectsOnPlane::Update()
extern void PlaceObjectsOnPlane_Update_mB26AAB9031D9C19840C18DF08EAA14E81E526C72 (void);
// 0x00000046 System.Void PlaceObjectsOnPlane::DisableInstantiatedGameObject()
extern void PlaceObjectsOnPlane_DisableInstantiatedGameObject_m64B0DCEEB4BE05B0C43BB1DCB5625A185AC937BB (void);
// 0x00000047 System.Void PlaceObjectsOnPlane::PlacedObject()
extern void PlaceObjectsOnPlane_PlacedObject_m7888392D26B29C3AC0CD6B94611B0078026D96BA (void);
// 0x00000048 System.Void PlaceObjectsOnPlane::SetPrefabType(UnityEngine.GameObject)
extern void PlaceObjectsOnPlane_SetPrefabType_mF60094F82FFAE45850E948483B8F67BA4FF10D48 (void);
// 0x00000049 System.Void PlaceObjectsOnPlane::.ctor()
extern void PlaceObjectsOnPlane__ctor_mE4F19060ECAC129230030682148DA6D8BB2236CD (void);
// 0x0000004A System.Void PlaceObjectsOnPlane::.cctor()
extern void PlaceObjectsOnPlane__cctor_mD0EB872BDAA8E1DA31C77197F954EE6CC07AD776 (void);
// 0x0000004B System.Boolean ARKitCoachingOverlay::get_activatesAutomatically()
extern void ARKitCoachingOverlay_get_activatesAutomatically_m43FE71F33A96A596F554C5685B460307A22E6191 (void);
// 0x0000004C System.Void ARKitCoachingOverlay::set_activatesAutomatically(System.Boolean)
extern void ARKitCoachingOverlay_set_activatesAutomatically_mCA5679C29A9C0B788DF7877921582E751F6D30A9 (void);
// 0x0000004D System.Boolean ARKitCoachingOverlay::get_supported()
extern void ARKitCoachingOverlay_get_supported_mF8B542B3A56EDD453C0BA7CF443F1AED0A4F1782 (void);
// 0x0000004E System.Void ARKitCoachingOverlay::OnEnable()
extern void ARKitCoachingOverlay_OnEnable_m48102540113361551F51CE45E7285D493E9433A9 (void);
// 0x0000004F System.Void ARKitCoachingOverlay::ActivateCoaching(System.Boolean)
extern void ARKitCoachingOverlay_ActivateCoaching_m968D23AE1413DEA4E6D33BE8C3074CBDE5AA7954 (void);
// 0x00000050 System.Void ARKitCoachingOverlay::DisableCoaching(System.Boolean)
extern void ARKitCoachingOverlay_DisableCoaching_m13E7F6653A5C55B76207C6462E611642968E4B07 (void);
// 0x00000051 System.Void ARKitCoachingOverlay::.ctor()
extern void ARKitCoachingOverlay__ctor_m3A2BF1CA291CBE8B05FB58F88A35A2B48502AA35 (void);
// 0x00000052 TMPro.TMP_Text ARUXAnimationManager::get_instructionText()
extern void ARUXAnimationManager_get_instructionText_m514815741754D3316056F333967E6BB23CE8BEE4 (void);
// 0x00000053 System.Void ARUXAnimationManager::set_instructionText(TMPro.TMP_Text)
extern void ARUXAnimationManager_set_instructionText_m4DF1E2F1174EEB418AF1EDC5223CEC64D5022920 (void);
// 0x00000054 UnityEngine.Video.VideoClip ARUXAnimationManager::get_findAPlaneClip()
extern void ARUXAnimationManager_get_findAPlaneClip_m719B16B0422736480570DB4E439DBE01AA8C2114 (void);
// 0x00000055 System.Void ARUXAnimationManager::set_findAPlaneClip(UnityEngine.Video.VideoClip)
extern void ARUXAnimationManager_set_findAPlaneClip_m77EA535039AB1E3D9191CE99C5F7951FD21BF3C4 (void);
// 0x00000056 UnityEngine.Video.VideoClip ARUXAnimationManager::get_tapToPlaceClip()
extern void ARUXAnimationManager_get_tapToPlaceClip_mDCC5C54A2D0554975F37E9F3E2527C5A96DBA97B (void);
// 0x00000057 System.Void ARUXAnimationManager::set_tapToPlaceClip(UnityEngine.Video.VideoClip)
extern void ARUXAnimationManager_set_tapToPlaceClip_m2BDDC86C341E760C28A36536C9EC6B2E9830B3B8 (void);
// 0x00000058 UnityEngine.Video.VideoClip ARUXAnimationManager::get_findImageClip()
extern void ARUXAnimationManager_get_findImageClip_m17F43F94F6FDBF44038B525A056135DAF84DA3A6 (void);
// 0x00000059 System.Void ARUXAnimationManager::set_findImageClip(UnityEngine.Video.VideoClip)
extern void ARUXAnimationManager_set_findImageClip_m8708CC3233B186BC87286078748D780B0B0D6497 (void);
// 0x0000005A UnityEngine.Video.VideoClip ARUXAnimationManager::get_findBodyClip()
extern void ARUXAnimationManager_get_findBodyClip_mC9A93E71F45C759AF10C008E392575E5B27FBED1 (void);
// 0x0000005B System.Void ARUXAnimationManager::set_findBodyClip(UnityEngine.Video.VideoClip)
extern void ARUXAnimationManager_set_findBodyClip_mF086B22B4119D422855C50D1BE47372F308AD20D (void);
// 0x0000005C UnityEngine.Video.VideoClip ARUXAnimationManager::get_findObjectClip()
extern void ARUXAnimationManager_get_findObjectClip_m9AD91C9C5E2EFF367878E7AFE82AA3555A7EC6E7 (void);
// 0x0000005D System.Void ARUXAnimationManager::set_findObjectClip(UnityEngine.Video.VideoClip)
extern void ARUXAnimationManager_set_findObjectClip_mAB9A67C0313D3504BAB411495A9963865435773B (void);
// 0x0000005E UnityEngine.Video.VideoClip ARUXAnimationManager::get_findFaceClip()
extern void ARUXAnimationManager_get_findFaceClip_mCABE42C2DFB671055915EC5981E075A63CBA6552 (void);
// 0x0000005F System.Void ARUXAnimationManager::set_findFaceClip(UnityEngine.Video.VideoClip)
extern void ARUXAnimationManager_set_findFaceClip_mFAD93DF36221E5D60829DDEA80373F281CE42854 (void);
// 0x00000060 ARKitCoachingOverlay ARUXAnimationManager::get_coachingOverlay()
extern void ARUXAnimationManager_get_coachingOverlay_m5618DDC3B42F3839A2750D9F40E11EC9CC0D3988 (void);
// 0x00000061 System.Void ARUXAnimationManager::set_coachingOverlay(ARKitCoachingOverlay)
extern void ARUXAnimationManager_set_coachingOverlay_m814AB67F980DBE8AC8C8688046AFF42998B4D109 (void);
// 0x00000062 UnityEngine.Video.VideoPlayer ARUXAnimationManager::get_videoPlayer()
extern void ARUXAnimationManager_get_videoPlayer_m744C861A74C305AB9771AE5C165CE50BCDDDDF2C (void);
// 0x00000063 System.Void ARUXAnimationManager::set_videoPlayer(UnityEngine.Video.VideoPlayer)
extern void ARUXAnimationManager_set_videoPlayer_m0D3A4AEC9309AF981D131D6EF2B925C325B0B7B3 (void);
// 0x00000064 UnityEngine.UI.RawImage ARUXAnimationManager::get_rawImage()
extern void ARUXAnimationManager_get_rawImage_mCBFE65FAE8C8603CC638EA30FBA2EFEAD3A54DD4 (void);
// 0x00000065 System.Void ARUXAnimationManager::set_rawImage(UnityEngine.UI.RawImage)
extern void ARUXAnimationManager_set_rawImage_mEA87BEC0B7A5DB9C6958D66BDA6C6F4E46C0869A (void);
// 0x00000066 System.Void ARUXAnimationManager::add_onFadeOffComplete(System.Action)
extern void ARUXAnimationManager_add_onFadeOffComplete_m988A1D4D19242C1067E194A5BA32C5D1988C282D (void);
// 0x00000067 System.Void ARUXAnimationManager::remove_onFadeOffComplete(System.Action)
extern void ARUXAnimationManager_remove_onFadeOffComplete_m7D39849FBC6A57AB6D63D48A5E7B3DF2695D89DB (void);
// 0x00000068 UnityEngine.Texture ARUXAnimationManager::get_transparent()
extern void ARUXAnimationManager_get_transparent_m2EDE47D2661C42196C9AF99A1DBD664A8373B0D3 (void);
// 0x00000069 System.Void ARUXAnimationManager::set_transparent(UnityEngine.Texture)
extern void ARUXAnimationManager_set_transparent_m9F725BE8D2ADABEF4251247E3FB6CF4A88FB0BD2 (void);
// 0x0000006A LocalizationManager ARUXAnimationManager::get_localizationManager()
extern void ARUXAnimationManager_get_localizationManager_m48ED855EEEE5C97BDA3825B643AD24F5028C3887 (void);
// 0x0000006B System.Void ARUXAnimationManager::set_localizationManager(LocalizationManager)
extern void ARUXAnimationManager_set_localizationManager_mABFDF3C996AF72EBAE76D3D00EE00087644668B4 (void);
// 0x0000006C System.Boolean ARUXAnimationManager::get_localizeText()
extern void ARUXAnimationManager_get_localizeText_mFE8B410693D3E7C6A620F944004C1F2B7DF6FF8C (void);
// 0x0000006D System.Void ARUXAnimationManager::set_localizeText(System.Boolean)
extern void ARUXAnimationManager_set_localizeText_mE3E02C81B178C99CDEC9D5E7DB66FB7C3BAAE394 (void);
// 0x0000006E System.Void ARUXAnimationManager::Start()
extern void ARUXAnimationManager_Start_mD238B58A7F129816A2539587FD91D2445A9D2083 (void);
// 0x0000006F System.Void ARUXAnimationManager::Update()
extern void ARUXAnimationManager_Update_m95C600DC484FBB782C75DA7018A21965354B7015 (void);
// 0x00000070 System.Void ARUXAnimationManager::ShowTapToPlace()
extern void ARUXAnimationManager_ShowTapToPlace_mD164AD80CD34C083DC02CC0B918CDBA2F1E12E87 (void);
// 0x00000071 System.Void ARUXAnimationManager::ShowFindImage()
extern void ARUXAnimationManager_ShowFindImage_mDE5652F03937A048A2AB4EEE3894422897E6315E (void);
// 0x00000072 System.Void ARUXAnimationManager::ShowFindBody()
extern void ARUXAnimationManager_ShowFindBody_mFFEE3A8E28318CD2478A4198B96962DC7F1D6773 (void);
// 0x00000073 System.Void ARUXAnimationManager::ShowFindObject()
extern void ARUXAnimationManager_ShowFindObject_m5613130140BF7564B8026CC0B4FD80F2FC38BE5A (void);
// 0x00000074 System.Void ARUXAnimationManager::ShowFindFace()
extern void ARUXAnimationManager_ShowFindFace_m18EC5D247D9D3BD038495356704E8AFA3DB1BAD5 (void);
// 0x00000075 System.Void ARUXAnimationManager::ShowCrossPlatformFindAPlane()
extern void ARUXAnimationManager_ShowCrossPlatformFindAPlane_m7103C2C86FBD3CA361050BEA0ACBEB117EA99241 (void);
// 0x00000076 System.Void ARUXAnimationManager::ShowCoachingOverlay()
extern void ARUXAnimationManager_ShowCoachingOverlay_m6F6B24BA84550FE59F97FDF57D8FF50EEED919BC (void);
// 0x00000077 System.Boolean ARUXAnimationManager::ARKitCoachingOverlaySupported()
extern void ARUXAnimationManager_ARKitCoachingOverlaySupported_m09A1F1D76F4F2B0877678D97E3F2E440AB490400 (void);
// 0x00000078 System.Void ARUXAnimationManager::FadeOffCurrentUI()
extern void ARUXAnimationManager_FadeOffCurrentUI_m3C9C5A64B3CB75838522ED1F01CE5A99D4F5BFFB (void);
// 0x00000079 System.Void ARUXAnimationManager::.ctor()
extern void ARUXAnimationManager__ctor_m75E82BE7AC295093E53BF5E4F2037111C29F08F2 (void);
// 0x0000007A System.Boolean ARUXReasonsManager::get_showNotTrackingReasons()
extern void ARUXReasonsManager_get_showNotTrackingReasons_mA12725E27C8D70F1BDD90865A295895A57454F35 (void);
// 0x0000007B System.Void ARUXReasonsManager::set_showNotTrackingReasons(System.Boolean)
extern void ARUXReasonsManager_set_showNotTrackingReasons_m17262974FD31DA2843755C937F4A3B0CDA850382 (void);
// 0x0000007C TMPro.TMP_Text ARUXReasonsManager::get_reasonDisplayText()
extern void ARUXReasonsManager_get_reasonDisplayText_mC7C9A561D882C514D05FA6D5AD7C99CB874C4318 (void);
// 0x0000007D System.Void ARUXReasonsManager::set_reasonDisplayText(TMPro.TMP_Text)
extern void ARUXReasonsManager_set_reasonDisplayText_m753C3C84B6D26AB2A1563D1A569EEF8DE0B4C880 (void);
// 0x0000007E UnityEngine.GameObject ARUXReasonsManager::get_reasonParent()
extern void ARUXReasonsManager_get_reasonParent_m16292A2374CFD842048A28DE37529BA24DA41020 (void);
// 0x0000007F System.Void ARUXReasonsManager::set_reasonParent(UnityEngine.GameObject)
extern void ARUXReasonsManager_set_reasonParent_m61E6991F11599AEAAF528A6F8668B90EA9E9E25C (void);
// 0x00000080 UnityEngine.UI.Image ARUXReasonsManager::get_reasonIcon()
extern void ARUXReasonsManager_get_reasonIcon_mEEAD7FBC5DBBED8206948C9C543AA4C6C413637F (void);
// 0x00000081 System.Void ARUXReasonsManager::set_reasonIcon(UnityEngine.UI.Image)
extern void ARUXReasonsManager_set_reasonIcon_m71B9311EAEB1B5AD141EE840507A9A8F8E563A51 (void);
// 0x00000082 UnityEngine.Sprite ARUXReasonsManager::get_initRelocalSprite()
extern void ARUXReasonsManager_get_initRelocalSprite_m024F7B88DEFDB433F9838CDC0DC2ABA391EED2CC (void);
// 0x00000083 System.Void ARUXReasonsManager::set_initRelocalSprite(UnityEngine.Sprite)
extern void ARUXReasonsManager_set_initRelocalSprite_m409BF75B26CC2FC814E08FA1055A3947E5D2CE55 (void);
// 0x00000084 UnityEngine.Sprite ARUXReasonsManager::get_motionSprite()
extern void ARUXReasonsManager_get_motionSprite_m92EC24F8AEC3C7B6F6AF7178898E8C2AF21445C3 (void);
// 0x00000085 System.Void ARUXReasonsManager::set_motionSprite(UnityEngine.Sprite)
extern void ARUXReasonsManager_set_motionSprite_m72A97B8D0D40DF73089AD1C48347E6BED2F860D0 (void);
// 0x00000086 UnityEngine.Sprite ARUXReasonsManager::get_lightSprite()
extern void ARUXReasonsManager_get_lightSprite_mF8B82531BB0BCB59039A33B666832B02CDD0F8AF (void);
// 0x00000087 System.Void ARUXReasonsManager::set_lightSprite(UnityEngine.Sprite)
extern void ARUXReasonsManager_set_lightSprite_m9CDE50D4F5E15890CF86B9AD6DABFBD40BEC6A26 (void);
// 0x00000088 UnityEngine.Sprite ARUXReasonsManager::get_featuresSprite()
extern void ARUXReasonsManager_get_featuresSprite_mC7DBCB37FF3DFB3999227EF8AB67E6A1C52A025C (void);
// 0x00000089 System.Void ARUXReasonsManager::set_featuresSprite(UnityEngine.Sprite)
extern void ARUXReasonsManager_set_featuresSprite_m40BD82A19A7D5CD2BB75C33262EF5A969539BA45 (void);
// 0x0000008A UnityEngine.Sprite ARUXReasonsManager::get_unsupportedSprite()
extern void ARUXReasonsManager_get_unsupportedSprite_m025F5BF0BBFEC485A777315E20462C4A9AF4375C (void);
// 0x0000008B System.Void ARUXReasonsManager::set_unsupportedSprite(UnityEngine.Sprite)
extern void ARUXReasonsManager_set_unsupportedSprite_m055F4F1FF41C0F62E2043B6FFC09C6D74CA88229 (void);
// 0x0000008C UnityEngine.Sprite ARUXReasonsManager::get_noneSprite()
extern void ARUXReasonsManager_get_noneSprite_m194347B6F3A6CBE8E312585C80A2928D3B064404 (void);
// 0x0000008D System.Void ARUXReasonsManager::set_noneSprite(UnityEngine.Sprite)
extern void ARUXReasonsManager_set_noneSprite_mE8667B89B2DC3333ECBCDD9CBEE123ED6FC44BFB (void);
// 0x0000008E LocalizationManager ARUXReasonsManager::get_localizationManager()
extern void ARUXReasonsManager_get_localizationManager_m3F0954D20F9B4B11B15067B25DC5550A8A70F477 (void);
// 0x0000008F System.Void ARUXReasonsManager::set_localizationManager(LocalizationManager)
extern void ARUXReasonsManager_set_localizationManager_m4E0ACDF16477375C019A0A885E6C1B92B2525AD1 (void);
// 0x00000090 System.Boolean ARUXReasonsManager::get_localizeText()
extern void ARUXReasonsManager_get_localizeText_m57164DA09CA5DB119DD8E489267944728167096E (void);
// 0x00000091 System.Void ARUXReasonsManager::set_localizeText(System.Boolean)
extern void ARUXReasonsManager_set_localizeText_m474B446D80DE7DF1922CEADF7DE13F0C8274D106 (void);
// 0x00000092 System.Void ARUXReasonsManager::OnEnable()
extern void ARUXReasonsManager_OnEnable_m037BB47878613AB444B796B207909A8AE7AADAFF (void);
// 0x00000093 System.Void ARUXReasonsManager::OnDisable()
extern void ARUXReasonsManager_OnDisable_mD3734CBAEFB3D13FC4DE2F8593BF2443418EFF00 (void);
// 0x00000094 System.Void ARUXReasonsManager::Update()
extern void ARUXReasonsManager_Update_m02B85757D02646BB3DA2C71E665E938007A9943A (void);
// 0x00000095 System.Void ARUXReasonsManager::ARSessionOnstateChanged(UnityEngine.XR.ARFoundation.ARSessionStateChangedEventArgs)
extern void ARUXReasonsManager_ARSessionOnstateChanged_m5F8B8F570C9F232464AB456245F6B4C11A859A46 (void);
// 0x00000096 System.Void ARUXReasonsManager::ShowReason()
extern void ARUXReasonsManager_ShowReason_mB626102F2ADE5EA027C1C4CE66624A526E17F0B9 (void);
// 0x00000097 System.Void ARUXReasonsManager::SetReason()
extern void ARUXReasonsManager_SetReason_m43AEDCB23F444A9E4EB23013650CE855385F4F60 (void);
// 0x00000098 System.Void ARUXReasonsManager::TestForceShowReason(UnityEngine.XR.ARSubsystems.NotTrackingReason)
extern void ARUXReasonsManager_TestForceShowReason_m9FB72D1242FC69D56E8080ED336F50DA3B7C5F16 (void);
// 0x00000099 System.Void ARUXReasonsManager::.ctor()
extern void ARUXReasonsManager__ctor_mD738924E0F0097E1DBB5294259B002ACF7D06874 (void);
// 0x0000009A System.Boolean DisableTrackedVisuals::get_disableFeaturePoints()
extern void DisableTrackedVisuals_get_disableFeaturePoints_mF53F493674476B2E0ACEB5152FD25AB6D4324E77 (void);
// 0x0000009B System.Void DisableTrackedVisuals::set_disableFeaturePoints(System.Boolean)
extern void DisableTrackedVisuals_set_disableFeaturePoints_mBF30CAABA44924EBB8A5D5A891D2C6159A83EF96 (void);
// 0x0000009C System.Boolean DisableTrackedVisuals::get_disablePlaneRendering()
extern void DisableTrackedVisuals_get_disablePlaneRendering_mB6F791B4221BC644C4ECBDA00EB8327B5ED7D2DE (void);
// 0x0000009D System.Void DisableTrackedVisuals::set_disablePlaneRendering(System.Boolean)
extern void DisableTrackedVisuals_set_disablePlaneRendering_mBEF12A327B6F216033672A7349EC12732F6D29A8 (void);
// 0x0000009E UnityEngine.XR.ARFoundation.ARPointCloudManager DisableTrackedVisuals::get_pointCloudManager()
extern void DisableTrackedVisuals_get_pointCloudManager_m74DAA4E0AE2218B0443D0A2E85BFEA286E348625 (void);
// 0x0000009F System.Void DisableTrackedVisuals::set_pointCloudManager(UnityEngine.XR.ARFoundation.ARPointCloudManager)
extern void DisableTrackedVisuals_set_pointCloudManager_m794B67459916AA4755EBE9E69831315FAAEFD3AC (void);
// 0x000000A0 UnityEngine.XR.ARFoundation.ARPlaneManager DisableTrackedVisuals::get_planeManager()
extern void DisableTrackedVisuals_get_planeManager_mE926216C35ECC33F2632C969BD151779076E41BC (void);
// 0x000000A1 System.Void DisableTrackedVisuals::set_planeManager(UnityEngine.XR.ARFoundation.ARPlaneManager)
extern void DisableTrackedVisuals_set_planeManager_m76DA3841E86C5EEB571019CC18C48200E9015358 (void);
// 0x000000A2 System.Void DisableTrackedVisuals::OnEnable()
extern void DisableTrackedVisuals_OnEnable_mC6E0BA401F8837D9A0B93C81DDFED1B813BC6629 (void);
// 0x000000A3 System.Void DisableTrackedVisuals::OnDisable()
extern void DisableTrackedVisuals_OnDisable_m3F48D8070C2B378FE7228B29472C8280241CB74D (void);
// 0x000000A4 System.Void DisableTrackedVisuals::OnPlacedObject()
extern void DisableTrackedVisuals_OnPlacedObject_mEE39A9227DBA59A9D83C17B254620121909225F6 (void);
// 0x000000A5 System.Void DisableTrackedVisuals::.ctor()
extern void DisableTrackedVisuals__ctor_m4EAA5CB87C1AEE2A10C4DE08F21528FDBDF9C53A (void);
// 0x000000A6 System.Boolean LocalizationManager::get_localizationComplete()
extern void LocalizationManager_get_localizationComplete_m88BBD683DC9FD499DEE14C62EBFC4C83260ED012 (void);
// 0x000000A7 TMPro.TMP_FontAsset LocalizationManager::get_simplifiedChineseFont()
extern void LocalizationManager_get_simplifiedChineseFont_m27F640E8CFC2FF177B3FC0152FA51A5B2A4BB806 (void);
// 0x000000A8 System.Void LocalizationManager::set_simplifiedChineseFont(TMPro.TMP_FontAsset)
extern void LocalizationManager_set_simplifiedChineseFont_m8F94F4123033A1E2A57F1A96120C9366CADF5063 (void);
// 0x000000A9 TMPro.TMP_FontAsset LocalizationManager::get_japaneseFont()
extern void LocalizationManager_get_japaneseFont_mC243AFEB584DAC1C604FD2F4BC179DAB11557CF8 (void);
// 0x000000AA System.Void LocalizationManager::set_japaneseFont(TMPro.TMP_FontAsset)
extern void LocalizationManager_set_japaneseFont_m4362024A8975641F2238F307D2E9766B6F140A77 (void);
// 0x000000AB TMPro.TMP_FontAsset LocalizationManager::get_koreanFont()
extern void LocalizationManager_get_koreanFont_m4FA454CCD51ABE9CCF678AA8424AA663BCF3A33A (void);
// 0x000000AC System.Void LocalizationManager::set_koreanFont(TMPro.TMP_FontAsset)
extern void LocalizationManager_set_koreanFont_mDF1E79BF3B0BE776F1CC51D5BE691906FF29BBF3 (void);
// 0x000000AD TMPro.TMP_FontAsset LocalizationManager::get_hindiFont()
extern void LocalizationManager_get_hindiFont_mEBF1821DFAC3B889694DD757791CC7D66DD46054 (void);
// 0x000000AE System.Void LocalizationManager::set_hindiFont(TMPro.TMP_FontAsset)
extern void LocalizationManager_set_hindiFont_m7B28FD32EF9148CF19BDCDF9428A982DF7CA82AD (void);
// 0x000000AF TMPro.TMP_Text LocalizationManager::get_instructionText()
extern void LocalizationManager_get_instructionText_mC8E952E79C3B600158E59CA9B20DE200296BD1A1 (void);
// 0x000000B0 System.Void LocalizationManager::set_instructionText(TMPro.TMP_Text)
extern void LocalizationManager_set_instructionText_m3BAE0D8712A487880BAA51E37C7E2B5988B09C75 (void);
// 0x000000B1 TMPro.TMP_Text LocalizationManager::get_reasonText()
extern void LocalizationManager_get_reasonText_m7D99E2130A2E5733559195A148AC77B00CB44D44 (void);
// 0x000000B2 System.Void LocalizationManager::set_reasonText(TMPro.TMP_Text)
extern void LocalizationManager_set_reasonText_m22D6AC6256F13F55B2D8A533914C4C19DB775A22 (void);
// 0x000000B3 System.Collections.IEnumerator LocalizationManager::Start()
extern void LocalizationManager_Start_mCCC2E00354521D23FD5CBDB28FD347B27956BC61 (void);
// 0x000000B4 System.Void LocalizationManager::OnCompletedReasonsUX(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.Localization.Tables.StringTable>)
extern void LocalizationManager_OnCompletedReasonsUX_mE09AA2D2EB53ACFDE0142288B5A11E8BD6689126 (void);
// 0x000000B5 System.Void LocalizationManager::SwapFonts(LocalizationManager/SupportedLanguages)
extern void LocalizationManager_SwapFonts_mDC31F1203A1557DB0ED9DAD0D70E0533A0AE7720 (void);
// 0x000000B6 System.Void LocalizationManager::.ctor()
extern void LocalizationManager__ctor_mFDB7124E84B7565E30F7240BB010304AD49A9832 (void);
// 0x000000B7 System.Void UXHandle::.ctor(UIManager/InstructionUI,UIManager/InstructionGoals)
extern void UXHandle__ctor_mCCADFDD89B964F4412436FA3100256EE9305FFDA (void);
// 0x000000B8 System.Boolean UIManager::get_startWithInstructionalUI()
extern void UIManager_get_startWithInstructionalUI_mE40F1263E3AD20EF65D3F71FCD8BFB9A813CABB3 (void);
// 0x000000B9 System.Void UIManager::set_startWithInstructionalUI(System.Boolean)
extern void UIManager_set_startWithInstructionalUI_m95C06FFDEDC4F5DEEE9153C3D24F42B2268B9E09 (void);
// 0x000000BA UIManager/InstructionUI UIManager::get_instructionalUI()
extern void UIManager_get_instructionalUI_mEF9826C7692CA05F7FDD31C7F5580164082548BF (void);
// 0x000000BB System.Void UIManager::set_instructionalUI(UIManager/InstructionUI)
extern void UIManager_set_instructionalUI_mEA656FF6B11EE89CCAC2D6E94DDEE60F7C2AB4B2 (void);
// 0x000000BC UIManager/InstructionGoals UIManager::get_instructionalGoal()
extern void UIManager_get_instructionalGoal_m063CD11118574B3B21E30B3F98383281436F1A28 (void);
// 0x000000BD System.Void UIManager::set_instructionalGoal(UIManager/InstructionGoals)
extern void UIManager_set_instructionalGoal_m9E1EDD22FCD82308FC7E13874EC1D145123357BB (void);
// 0x000000BE System.Boolean UIManager::get_showSecondaryInstructionalUI()
extern void UIManager_get_showSecondaryInstructionalUI_m4C6793FD65814FFADD3F23F42622BEBBE3CCAC80 (void);
// 0x000000BF System.Void UIManager::set_showSecondaryInstructionalUI(System.Boolean)
extern void UIManager_set_showSecondaryInstructionalUI_m867F90CD6FA33B82761F1015DB71D42933FCD52A (void);
// 0x000000C0 UIManager/InstructionUI UIManager::get_secondaryInstructionUI()
extern void UIManager_get_secondaryInstructionUI_m999F4A70CB2A6FFB79F02B097D70CDCF3F1F2F1F (void);
// 0x000000C1 System.Void UIManager::set_secondaryInstructionUI(UIManager/InstructionUI)
extern void UIManager_set_secondaryInstructionUI_m8E83AD99333F787D243B0AA92D0D3C5882942AC1 (void);
// 0x000000C2 UIManager/InstructionGoals UIManager::get_secondaryGoal()
extern void UIManager_get_secondaryGoal_mA5492F33BA5BCD4FE5B801A4D11BC9886FE2424D (void);
// 0x000000C3 System.Void UIManager::set_secondaryGoal(UIManager/InstructionGoals)
extern void UIManager_set_secondaryGoal_m8632E8D0201EBD6810FB3FD9D207536182D6AB28 (void);
// 0x000000C4 System.Boolean UIManager::get_coachingOverlayFallback()
extern void UIManager_get_coachingOverlayFallback_m8696E4F7A0AA1EECB1ED97D07682D458BB8A2B2D (void);
// 0x000000C5 System.Void UIManager::set_coachingOverlayFallback(System.Boolean)
extern void UIManager_set_coachingOverlayFallback_mE8D20CC40A1C46BEFE9BBA520DE7172E3C632956 (void);
// 0x000000C6 UnityEngine.GameObject UIManager::get_arSessionOrigin()
extern void UIManager_get_arSessionOrigin_m05131191805F0EF37D5C10120826BE801E74F343 (void);
// 0x000000C7 System.Void UIManager::set_arSessionOrigin(UnityEngine.GameObject)
extern void UIManager_set_arSessionOrigin_m9F06555FA957BF21149830605BC9256246ACC7B6 (void);
// 0x000000C8 UnityEngine.XR.ARFoundation.ARPlaneManager UIManager::get_planeManager()
extern void UIManager_get_planeManager_mC295F7015EC0AFA6BF017218A3D50FBB5F8EC93B (void);
// 0x000000C9 System.Void UIManager::set_planeManager(UnityEngine.XR.ARFoundation.ARPlaneManager)
extern void UIManager_set_planeManager_m00D2D98C5B74891D8CE5F847317C28A7B4A992B5 (void);
// 0x000000CA UnityEngine.XR.ARFoundation.ARFaceManager UIManager::get_faceManager()
extern void UIManager_get_faceManager_mF4D5FEB0CBF74E1E6DB86981BA328D74159FACB7 (void);
// 0x000000CB System.Void UIManager::set_faceManager(UnityEngine.XR.ARFoundation.ARFaceManager)
extern void UIManager_set_faceManager_m6C34CA7DE94937B661CD1AA9D02F5D79B70713F6 (void);
// 0x000000CC UnityEngine.XR.ARFoundation.ARHumanBodyManager UIManager::get_bodyManager()
extern void UIManager_get_bodyManager_m72FAB838B954B870E4C9A53E9B1E4F4F884783C1 (void);
// 0x000000CD System.Void UIManager::set_bodyManager(UnityEngine.XR.ARFoundation.ARHumanBodyManager)
extern void UIManager_set_bodyManager_mB227C036D10CC66AD40F4E050D154179DBAEE50D (void);
// 0x000000CE UnityEngine.XR.ARFoundation.ARTrackedImageManager UIManager::get_imageManager()
extern void UIManager_get_imageManager_m105BE6CFB85332D12FF7C85E6FE656EBDFDF7F54 (void);
// 0x000000CF System.Void UIManager::set_imageManager(UnityEngine.XR.ARFoundation.ARTrackedImageManager)
extern void UIManager_set_imageManager_m0529429F9ABF451B6411103D9F8F1BB9DACDB933 (void);
// 0x000000D0 UnityEngine.XR.ARFoundation.ARTrackedObjectManager UIManager::get_objectManager()
extern void UIManager_get_objectManager_m50E8ED7DCF8BB014447D56D38044459AFC9BDFA5 (void);
// 0x000000D1 System.Void UIManager::set_objectManager(UnityEngine.XR.ARFoundation.ARTrackedObjectManager)
extern void UIManager_set_objectManager_m706C5829D9F5C97B121E9353EB152FA28C56F71D (void);
// 0x000000D2 ARUXAnimationManager UIManager::get_animationManager()
extern void UIManager_get_animationManager_mAC5459D921B5A8510BE1A70514155EEFB11E1DB3 (void);
// 0x000000D3 System.Void UIManager::set_animationManager(ARUXAnimationManager)
extern void UIManager_set_animationManager_m412348E166C87E988BBC2802B7DFBB952CA2273E (void);
// 0x000000D4 LocalizationManager UIManager::get_localizationManager()
extern void UIManager_get_localizationManager_m62EE0ECE37206FFEE471E84E47431807D395790C (void);
// 0x000000D5 System.Void UIManager::set_localizationManager(LocalizationManager)
extern void UIManager_set_localizationManager_mD354A3856BE6015679D2873FE57ECEE8E891A824 (void);
// 0x000000D6 System.Void UIManager::OnEnable()
extern void UIManager_OnEnable_mAFE10D24F19B379487455D635E4A3C65D4B64240 (void);
// 0x000000D7 System.Void UIManager::OnDisable()
extern void UIManager_OnDisable_mBAED7246344396BA861ADE4603C663A9ECFBCDEF (void);
// 0x000000D8 System.Void UIManager::Update()
extern void UIManager_Update_m8A7C5DF1B797CFD6937FD6961AB9CC7B1A90D385 (void);
// 0x000000D9 System.Void UIManager::GetManagers()
extern void UIManager_GetManagers_mD12BBFF5C86710EEFB1373A750D4568248EBF65F (void);
// 0x000000DA System.Func`1<System.Boolean> UIManager::GetGoal(UIManager/InstructionGoals)
extern void UIManager_GetGoal_mF9DF0A17B2A9B3469399AE5EB06BDD6E85D7B2B0 (void);
// 0x000000DB System.Void UIManager::FadeOnInstructionalUI(UIManager/InstructionUI)
extern void UIManager_FadeOnInstructionalUI_m54B2A2025926527856D13414C1133EB617BCD96D (void);
// 0x000000DC System.Boolean UIManager::PlanesFound()
extern void UIManager_PlanesFound_m4815E9F0871E527DBBCD7A190B7727D65D482622 (void);
// 0x000000DD System.Boolean UIManager::MultiplePlanesFound()
extern void UIManager_MultiplePlanesFound_mA354599925BC74F323F09A20D4B5DE5EDAF20CDB (void);
// 0x000000DE System.Boolean UIManager::FaceFound()
extern void UIManager_FaceFound_mB49F79714CA7296A5C344A8093B8AE33F3F5CA20 (void);
// 0x000000DF System.Boolean UIManager::BodyFound()
extern void UIManager_BodyFound_m2B28D4FAEDAC413FE77BC29838F9113E986FD67F (void);
// 0x000000E0 System.Boolean UIManager::ImageFound()
extern void UIManager_ImageFound_mBAA7C2BF6A1B7B643502EEC407AAC5504549CE38 (void);
// 0x000000E1 System.Boolean UIManager::ObjectFound()
extern void UIManager_ObjectFound_mAE0C6EB3C245C76BCDFD0479514C7B21A5808ABA (void);
// 0x000000E2 System.Void UIManager::FadeComplete()
extern void UIManager_FadeComplete_mDD5CFE580BA60FEE6E72E4911F3ED91A41BF6336 (void);
// 0x000000E3 System.Boolean UIManager::PlacedObject()
extern void UIManager_PlacedObject_mFD09B746476FFB9DB7DE431FDE1123C194694429 (void);
// 0x000000E4 System.Void UIManager::AddToQueue(UXHandle)
extern void UIManager_AddToQueue_m35E9C512CBECA09EE9784B45DFCCA278D77590D7 (void);
// 0x000000E5 System.Void UIManager::TestFlipPlacementBool()
extern void UIManager_TestFlipPlacementBool_mFCC1C4330588D546B16DF094ABEBD73D1E18D69D (void);
// 0x000000E6 System.Void UIManager::.ctor()
extern void UIManager__ctor_mDADE1D724D40AF63AE78D51FC1CF1FE4784B4D4B (void);
// 0x000000E7 System.Void UIManager::<OnEnable>b__69_0()
extern void UIManager_U3COnEnableU3Eb__69_0_m6E36F214C2EC57CB40BEDD772DA012EE40D56CBC (void);
// 0x000000E8 System.Void DestroyBall::Update()
extern void DestroyBall_Update_m2A7E36240B466E7E8CB94AC53CB54D5897E90ABC (void);
// 0x000000E9 System.Void DestroyBall::.ctor()
extern void DestroyBall__ctor_mFD7FB06FCC68CAD40CDF58B265BC62C3D266EFB0 (void);
// 0x000000EA System.Void BowlingAnimationScript::Start()
extern void BowlingAnimationScript_Start_m69D0ACB76A47B2DC82476F4FF42ADB59144C3F44 (void);
// 0x000000EB System.Void BowlingAnimationScript::Update()
extern void BowlingAnimationScript_Update_m099DB37FF04CFE7D98B8A5EA98BF06BB7E36E260 (void);
// 0x000000EC System.Void BowlingAnimationScript::OnTriggerExit(UnityEngine.Collider)
extern void BowlingAnimationScript_OnTriggerExit_mEC4925631254044BA58C308DB0EF838B2AD3D61C (void);
// 0x000000ED System.Void BowlingAnimationScript::.ctor()
extern void BowlingAnimationScript__ctor_m36FFC0F741C60916F981B21EDB95C9C00C408A2F (void);
// 0x000000EE System.Void StrokeScript::Start()
extern void StrokeScript_Start_mA94A8B80CFEB459F99DB734242E720CFB2671519 (void);
// 0x000000EF System.Void StrokeScript::FullHalfVolley()
extern void StrokeScript_FullHalfVolley_m5D1DD2D44175F114806C10C62D1E1B5044C634F2 (void);
// 0x000000F0 System.Void StrokeScript::GoodLength()
extern void StrokeScript_GoodLength_mF7C6F8DDA77B3FF941D1B0C82763EA54D2A2F525 (void);
// 0x000000F1 System.Void StrokeScript::ShortofaLength()
extern void StrokeScript_ShortofaLength_m790A51F3320596F0EAD8A0942F342D8DF1C338FB (void);
// 0x000000F2 System.Void StrokeScript::Short()
extern void StrokeScript_Short_m7B53338D491FC909DCDE68A25FFF01FF8E935BCF (void);
// 0x000000F3 System.Void StrokeScript::CoverDrive()
extern void StrokeScript_CoverDrive_mED4430DFED7AD5C626CE976FA2C84ED754CA0501 (void);
// 0x000000F4 System.Void StrokeScript::StraightOffDrive()
extern void StrokeScript_StraightOffDrive_m4058EC11A4703CA32CE1FF19E0DEEF4783B49B5C (void);
// 0x000000F5 System.Void StrokeScript::OnDriveGlance()
extern void StrokeScript_OnDriveGlance_m3CBF2DC8EFF65C6E5D50B53F5D30EA47864D93F7 (void);
// 0x000000F6 System.Void StrokeScript::ForwardDefenceSweep()
extern void StrokeScript_ForwardDefenceSweep_mCFEE5F00A178BD0FC26A2D8664A4A044CC2E103F (void);
// 0x000000F7 System.Void StrokeScript::BackFootGlance()
extern void StrokeScript_BackFootGlance_mD7085CECF9823D3C42A454DDD3604CDD619C138D (void);
// 0x000000F8 System.Void StrokeScript::BackFootdefencedrive()
extern void StrokeScript_BackFootdefencedrive_mD1F4493891F4C5956FDD94F85F881A3F49A37DF5 (void);
// 0x000000F9 System.Void StrokeScript::LateCut()
extern void StrokeScript_LateCut_m67EFAF1C4536A222C7C00F497FAB7B5D64600A35 (void);
// 0x000000FA System.Void StrokeScript::Pull()
extern void StrokeScript_Pull_m23E0F0E0735F960D468CDBD961C31068B65FBE28 (void);
// 0x000000FB System.Void StrokeScript::SquareCut()
extern void StrokeScript_SquareCut_m27E291C2C1F0C636F94EBC840316584732854462 (void);
// 0x000000FC System.Void StrokeScript::Hook()
extern void StrokeScript_Hook_m1D7899483726EB742058822BFBCB587CDA5E272F (void);
// 0x000000FD System.Void StrokeScript::.ctor()
extern void StrokeScript__ctor_m1F07DCEFC6C7AD72770BE6AFF496C37136703F2E (void);
// 0x000000FE System.Void StrokeScriptRevised::Start()
extern void StrokeScriptRevised_Start_mCE848AF2E345CE7E13F02F2865762C1510C9C864 (void);
// 0x000000FF System.Void StrokeScriptRevised::Update()
extern void StrokeScriptRevised_Update_m119F7723F1B3AB42906D8AC427F925ECD90A8574 (void);
// 0x00000100 System.Void StrokeScriptRevised::LenghtFun(UnityEngine.GameObject)
extern void StrokeScriptRevised_LenghtFun_mC349FDB9DE492C6F9D842B13B26270A79E99597E (void);
// 0x00000101 System.Void StrokeScriptRevised::.ctor()
extern void StrokeScriptRevised__ctor_m16B20B00337BB41C602391E8536C82D7BE41588C (void);
// 0x00000102 System.Void SceneController::EnableScreen(System.Int32)
extern void SceneController_EnableScreen_mC439D970C9780E7B92A7A398495BAD35C76EFB4B (void);
// 0x00000103 System.Void SceneController::DisableScreen(System.Int32)
extern void SceneController_DisableScreen_m53165CC1485293FA9DC836D19686DAA615C51450 (void);
// 0x00000104 System.Void SceneController::Coroutine()
extern void SceneController_Coroutine_mB4428FD5F1616CD3D0E62215165D926AEEB4289F (void);
// 0x00000105 System.Collections.IEnumerator SceneController::EnableBatsman()
extern void SceneController_EnableBatsman_m583C8D3A4A1A27E0F5E121DCD72731F8CC063DA5 (void);
// 0x00000106 System.Void SceneController::PlayBatting()
extern void SceneController_PlayBatting_m3E71EF8520EA3319AEB7755997BA5D84FFB316A4 (void);
// 0x00000107 System.Void SceneController::EnableAnimation()
extern void SceneController_EnableAnimation_m4E9F1D5E9EBEA1725B9005DDECCFFD74CEDAB60C (void);
// 0x00000108 System.Void SceneController::.ctor()
extern void SceneController__ctor_mD43BE19257E8699CFE380AD920050CE78721E604 (void);
// 0x00000109 System.Void PitchController::Start()
extern void PitchController_Start_m5EC2C8DBDC50B602151DDDF64210997B06BDDDC5 (void);
// 0x0000010A System.Void PitchController::DeActivate(System.Int32)
extern void PitchController_DeActivate_mE7CDAE178982C890E66E4A0FC04927937AE7B16F (void);
// 0x0000010B System.Void PitchController::.ctor()
extern void PitchController__ctor_mD02C8A2847DFF53EA2DFB7DF65D24D6CF94D401F (void);
// 0x0000010C System.Void ThrowBall::SpawnBall()
extern void ThrowBall_SpawnBall_m791B31511F591550A6DCFB66EDB0C444F2296829 (void);
// 0x0000010D System.Void ThrowBall::DestroyBall()
extern void ThrowBall_DestroyBall_mDB05C350CCA5641838E85877E6F917177C61E965 (void);
// 0x0000010E System.Void ThrowBall::.ctor()
extern void ThrowBall__ctor_mEB1D70209F5810666677C57F9403447FF2DEBBB1 (void);
// 0x0000010F System.Void Lean.Touch.LeanInfoText::Display(System.String)
extern void LeanInfoText_Display_mEC16B609CFCAE7A91048BE9164B60F44F4259ACC (void);
// 0x00000110 System.Void Lean.Touch.LeanInfoText::Display(System.String,System.String)
extern void LeanInfoText_Display_m596455DE1512899D9EF3F4C08084E985024DE27C (void);
// 0x00000111 System.Void Lean.Touch.LeanInfoText::Display(System.Int32)
extern void LeanInfoText_Display_m3DB84D5C44825D2E71303A5AF70F6F0C1A47915B (void);
// 0x00000112 System.Void Lean.Touch.LeanInfoText::Display(System.Single)
extern void LeanInfoText_Display_m9D2D15985191EA6B29A9FCF1880F25CD527A5CBC (void);
// 0x00000113 System.Void Lean.Touch.LeanInfoText::Display(UnityEngine.Vector2)
extern void LeanInfoText_Display_mE6CE8FEAB1625F409382E286F44B037D41F0AC6C (void);
// 0x00000114 System.Void Lean.Touch.LeanInfoText::Display(UnityEngine.Vector3)
extern void LeanInfoText_Display_m24289429D94C3375550C8413F63B97D7297E2CA1 (void);
// 0x00000115 System.Void Lean.Touch.LeanInfoText::Display(UnityEngine.Vector4)
extern void LeanInfoText_Display_mEDDA82DEB5A4715ACCC23FCEAA6B26C4AA736D7A (void);
// 0x00000116 System.Void Lean.Touch.LeanInfoText::Display(System.Int32,System.Int32)
extern void LeanInfoText_Display_m2C6B430C20897AAC31A2B793191DEA4519E51F2F (void);
// 0x00000117 System.Void Lean.Touch.LeanInfoText::.ctor()
extern void LeanInfoText__ctor_m51DF3EEC28AB11A72249A25EDFA6CEC93CF078FE (void);
// 0x00000118 System.Void Lean.Touch.LeanPulseScale::Update()
extern void LeanPulseScale_Update_m0DB57098D8856470F1BF3B31B8E168E75049CEBA (void);
// 0x00000119 System.Void Lean.Touch.LeanPulseScale::.ctor()
extern void LeanPulseScale__ctor_m37134467E74586BBDC07FC46A7355FEE8F17EBE8 (void);
// 0x0000011A System.Void Lean.Touch.LeanTouchEvents::OnEnable()
extern void LeanTouchEvents_OnEnable_mBD10B04C13B8CE8CECDD20589C3063CF585845FD (void);
// 0x0000011B System.Void Lean.Touch.LeanTouchEvents::OnDisable()
extern void LeanTouchEvents_OnDisable_m7177B1E836063A83A524EE3BB1681539D454DA21 (void);
// 0x0000011C System.Void Lean.Touch.LeanTouchEvents::HandleFingerDown(Lean.Touch.LeanFinger)
extern void LeanTouchEvents_HandleFingerDown_mA446C49881F440F3373DDDA11AC9E1CE23203A99 (void);
// 0x0000011D System.Void Lean.Touch.LeanTouchEvents::HandleFingerUpdate(Lean.Touch.LeanFinger)
extern void LeanTouchEvents_HandleFingerUpdate_m98F6BAFB50666EA4139BC572138A5D12616A2F05 (void);
// 0x0000011E System.Void Lean.Touch.LeanTouchEvents::HandleFingerUp(Lean.Touch.LeanFinger)
extern void LeanTouchEvents_HandleFingerUp_m3103EFCFEC77FE3FEC97644CA364A80F158D93F6 (void);
// 0x0000011F System.Void Lean.Touch.LeanTouchEvents::HandleFingerTap(Lean.Touch.LeanFinger)
extern void LeanTouchEvents_HandleFingerTap_mA8CE756F2FED184BAA124A56BEFA3718F2CF87BD (void);
// 0x00000120 System.Void Lean.Touch.LeanTouchEvents::HandleFingerSwipe(Lean.Touch.LeanFinger)
extern void LeanTouchEvents_HandleFingerSwipe_m06AC0962C6CA2B959BE9BB47B0A8864A35E095EF (void);
// 0x00000121 System.Void Lean.Touch.LeanTouchEvents::HandleGesture(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanTouchEvents_HandleGesture_mA112A90D6124C66468ED2CA1A9F765BBD5158728 (void);
// 0x00000122 System.Void Lean.Touch.LeanTouchEvents::.ctor()
extern void LeanTouchEvents__ctor_m934F1E2098DD13DDEF8501475EDDD511EF8D57E0 (void);
// 0x00000123 System.Void Lean.Touch.LeanDragCamera::MoveToSelection()
extern void LeanDragCamera_MoveToSelection_mC17CA5F136F00D57EDEF2F3193F53116ABC918D6 (void);
// 0x00000124 System.Void Lean.Touch.LeanDragCamera::AddFinger(Lean.Touch.LeanFinger)
extern void LeanDragCamera_AddFinger_m92B16CBD775F8554B2137EA46F99A6CB8E1D0EEA (void);
// 0x00000125 System.Void Lean.Touch.LeanDragCamera::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanDragCamera_RemoveFinger_m675A72D42C447AF49F1100B40A668477FC349808 (void);
// 0x00000126 System.Void Lean.Touch.LeanDragCamera::RemoveAllFingers()
extern void LeanDragCamera_RemoveAllFingers_m3265FCD465BF03B47B400D6998423FE926F31FD8 (void);
// 0x00000127 System.Void Lean.Touch.LeanDragCamera::Awake()
extern void LeanDragCamera_Awake_mEC7847822CA1D8490E4E41F871CF225555AB78C8 (void);
// 0x00000128 System.Void Lean.Touch.LeanDragCamera::LateUpdate()
extern void LeanDragCamera_LateUpdate_m26FB50F446756E95E3C30C8017B33B7D71009666 (void);
// 0x00000129 System.Void Lean.Touch.LeanDragCamera::.ctor()
extern void LeanDragCamera__ctor_mA64F0D615C7CDB775E9BF8DDEA8662CF68BDA68E (void);
// 0x0000012A System.Void Lean.Touch.LeanDragTrail::AddFinger(Lean.Touch.LeanFinger)
extern void LeanDragTrail_AddFinger_m61EE86BFDE5A2C99E90E10A1F49EA59D3EFF374B (void);
// 0x0000012B System.Void Lean.Touch.LeanDragTrail::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanDragTrail_RemoveFinger_mC0605B2A93C975E0DFA8D8BCB68C16DD81C0EBFD (void);
// 0x0000012C System.Void Lean.Touch.LeanDragTrail::RemoveAllFingers()
extern void LeanDragTrail_RemoveAllFingers_m8ACE3216CF9036CABF18D668849748F445297A3F (void);
// 0x0000012D System.Void Lean.Touch.LeanDragTrail::Awake()
extern void LeanDragTrail_Awake_m9CA3C99A1893E2A854E5D3C8D93E65A80E11A016 (void);
// 0x0000012E System.Void Lean.Touch.LeanDragTrail::OnEnable()
extern void LeanDragTrail_OnEnable_m22B084652219AD8BFF5F76BDB4229464ECAE1A13 (void);
// 0x0000012F System.Void Lean.Touch.LeanDragTrail::OnDisable()
extern void LeanDragTrail_OnDisable_m323C72CA681312C69869800EA9BCF93674E78EE5 (void);
// 0x00000130 System.Void Lean.Touch.LeanDragTrail::Update()
extern void LeanDragTrail_Update_m07ED1B0764CFDAD90689312B2599137718FD4B00 (void);
// 0x00000131 System.Void Lean.Touch.LeanDragTrail::UpdateLine(Lean.Touch.LeanDragTrail/FingerData,Lean.Touch.LeanFinger,UnityEngine.LineRenderer)
extern void LeanDragTrail_UpdateLine_m48F8CC5B559B492389409E1E574F52FC46A99898 (void);
// 0x00000132 System.Void Lean.Touch.LeanDragTrail::HandleFingerUp(Lean.Touch.LeanFinger)
extern void LeanDragTrail_HandleFingerUp_mE4219E175E86F618058F98D94832311917765B98 (void);
// 0x00000133 System.Void Lean.Touch.LeanDragTrail::.ctor()
extern void LeanDragTrail__ctor_m2BD26F778E504FF07C958368C90C16A38C666AED (void);
// 0x00000134 System.Void Lean.Touch.LeanDragTranslate::AddFinger(Lean.Touch.LeanFinger)
extern void LeanDragTranslate_AddFinger_mCFE1B2F819BF0A8765AA399E98FF74254459F715 (void);
// 0x00000135 System.Void Lean.Touch.LeanDragTranslate::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanDragTranslate_RemoveFinger_m87323CF954F4B0E75C84B8C247517E4B1E164E5A (void);
// 0x00000136 System.Void Lean.Touch.LeanDragTranslate::RemoveAllFingers()
extern void LeanDragTranslate_RemoveAllFingers_mF17AB71669CBE48CFD81A6DB0D7F77C1CBCA37A1 (void);
// 0x00000137 System.Void Lean.Touch.LeanDragTranslate::Awake()
extern void LeanDragTranslate_Awake_m0F4AAEA53B203CF6249A76D62EA4B449B4A1BD8F (void);
// 0x00000138 System.Void Lean.Touch.LeanDragTranslate::Update()
extern void LeanDragTranslate_Update_mB75065AEA06B7BC9E9F849E04162EA29A6D866F3 (void);
// 0x00000139 System.Void Lean.Touch.LeanDragTranslate::TranslateUI(UnityEngine.Vector2)
extern void LeanDragTranslate_TranslateUI_m58CB7C78A870916101F7CE6D92ED0683AD4D2752 (void);
// 0x0000013A System.Void Lean.Touch.LeanDragTranslate::Translate(UnityEngine.Vector2)
extern void LeanDragTranslate_Translate_mED0FA0DFFA1442D2008804A3098528596487446A (void);
// 0x0000013B System.Void Lean.Touch.LeanDragTranslate::.ctor()
extern void LeanDragTranslate__ctor_m6FDA2E53B76816E8D16ED6A7B6A309BC51D12081 (void);
// 0x0000013C System.Int32 Lean.Touch.LeanFingerData::Count(System.Collections.Generic.List`1<T>)
// 0x0000013D System.Boolean Lean.Touch.LeanFingerData::Exists(System.Collections.Generic.List`1<T>,Lean.Touch.LeanFinger)
// 0x0000013E System.Void Lean.Touch.LeanFingerData::Remove(System.Collections.Generic.List`1<T>,Lean.Touch.LeanFinger,System.Collections.Generic.Stack`1<T>)
// 0x0000013F System.Void Lean.Touch.LeanFingerData::RemoveAll(System.Collections.Generic.List`1<T>,System.Collections.Generic.Stack`1<T>)
// 0x00000140 T Lean.Touch.LeanFingerData::Find(System.Collections.Generic.List`1<T>,Lean.Touch.LeanFinger)
// 0x00000141 T Lean.Touch.LeanFingerData::FindOrCreate(System.Collections.Generic.List`1<T>&,Lean.Touch.LeanFinger)
// 0x00000142 System.Void Lean.Touch.LeanFingerData::.ctor()
extern void LeanFingerData__ctor_m9D0DEC41E54D8B6AAD78BF2C17F75C79161B2510 (void);
// 0x00000143 Lean.Touch.LeanFingerDown/LeanFingerEvent Lean.Touch.LeanFingerDown::get_OnFinger()
extern void LeanFingerDown_get_OnFinger_m09CEA5AF6CC62534767806C9188D4437BE855D10 (void);
// 0x00000144 Lean.Touch.LeanFingerDown/Vector3Event Lean.Touch.LeanFingerDown::get_OnWorld()
extern void LeanFingerDown_get_OnWorld_mABD0F3882E1F0CCAE3DE6430936DC55A8F008542 (void);
// 0x00000145 Lean.Touch.LeanFingerDown/Vector2Event Lean.Touch.LeanFingerDown::get_OnScreen()
extern void LeanFingerDown_get_OnScreen_mC1BA9CF61C1128070CEB90BB1803235373DB944A (void);
// 0x00000146 System.Void Lean.Touch.LeanFingerDown::Awake()
extern void LeanFingerDown_Awake_m4EBD6572E08174C7207F37000C8FFF28084342AC (void);
// 0x00000147 System.Void Lean.Touch.LeanFingerDown::OnEnable()
extern void LeanFingerDown_OnEnable_m7DCAA90561D159F13C9039EB226050028CAA276E (void);
// 0x00000148 System.Void Lean.Touch.LeanFingerDown::OnDisable()
extern void LeanFingerDown_OnDisable_mE3878C228ABD82E39F2E67B2CD12D2726C84895F (void);
// 0x00000149 System.Void Lean.Touch.LeanFingerDown::HandleFingerDown(Lean.Touch.LeanFinger)
extern void LeanFingerDown_HandleFingerDown_mF6E9A8EBD0D96B3168E45CD8262B040C10D67AA9 (void);
// 0x0000014A System.Void Lean.Touch.LeanFingerDown::.ctor()
extern void LeanFingerDown__ctor_m677DD6264855C58CC2C7B1DB8CD3E9B042AFB10F (void);
// 0x0000014B System.Void Lean.Touch.LeanFingerFilter::.ctor(System.Boolean)
extern void LeanFingerFilter__ctor_m9027A3427D07A922FCB51A04A5EFEF68C469A76A (void);
// 0x0000014C System.Void Lean.Touch.LeanFingerFilter::.ctor(Lean.Touch.LeanFingerFilter/FilterType,System.Boolean,System.Int32,System.Int32,Lean.Touch.LeanSelectable)
extern void LeanFingerFilter__ctor_mFCDCEED5234D11A36522A2F024497582BE8BC2C0 (void);
// 0x0000014D System.Void Lean.Touch.LeanFingerFilter::UpdateRequiredSelectable(UnityEngine.GameObject)
extern void LeanFingerFilter_UpdateRequiredSelectable_m1D1F442B0D35326DEFACB787BEDB201A8D604FFB (void);
// 0x0000014E System.Void Lean.Touch.LeanFingerFilter::AddFinger(Lean.Touch.LeanFinger)
extern void LeanFingerFilter_AddFinger_m62B1294665D1E02E8CD4BEEB7F75C95931EB686E (void);
// 0x0000014F System.Void Lean.Touch.LeanFingerFilter::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanFingerFilter_RemoveFinger_m53A0A1AEC724712089F6B826E1B8378BE8272FF0 (void);
// 0x00000150 System.Void Lean.Touch.LeanFingerFilter::RemoveAllFingers()
extern void LeanFingerFilter_RemoveAllFingers_m0473D5577085C0CF16F78479CD323338D53BBEF7 (void);
// 0x00000151 System.Collections.Generic.List`1<Lean.Touch.LeanFinger> Lean.Touch.LeanFingerFilter::GetFingers(System.Boolean)
extern void LeanFingerFilter_GetFingers_m36475339DF733ABE96A27D5F2CA102719C020222 (void);
// 0x00000152 System.Boolean Lean.Touch.LeanFingerFilter::SimulatedFingersExist(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanFingerFilter_SimulatedFingersExist_m551529DB41F184F231D8D6466AA37FFE47E2DA27 (void);
// 0x00000153 Lean.Touch.LeanFingerOld/LeanFingerEvent Lean.Touch.LeanFingerOld::get_OnFinger()
extern void LeanFingerOld_get_OnFinger_mD8E5956B120186C73579C6682224CD81CDADB2C5 (void);
// 0x00000154 Lean.Touch.LeanFingerOld/Vector3Event Lean.Touch.LeanFingerOld::get_OnWorld()
extern void LeanFingerOld_get_OnWorld_mFE784D2717DB51ECEC384A40CBE854439B102035 (void);
// 0x00000155 Lean.Touch.LeanFingerOld/Vector2Event Lean.Touch.LeanFingerOld::get_OnScreen()
extern void LeanFingerOld_get_OnScreen_mF044667491D141DE9FF3675B1B96877015EB2B40 (void);
// 0x00000156 System.Void Lean.Touch.LeanFingerOld::Start()
extern void LeanFingerOld_Start_m2B72B8EE75C8C02FCF55F4AA33CB30B467C109FB (void);
// 0x00000157 System.Void Lean.Touch.LeanFingerOld::OnEnable()
extern void LeanFingerOld_OnEnable_m0D463352D83EDEAFEEC1F9270129468F702D4C28 (void);
// 0x00000158 System.Void Lean.Touch.LeanFingerOld::OnDisable()
extern void LeanFingerOld_OnDisable_m2518FE71C1A2F1B359296E6A6108627179A2D9AE (void);
// 0x00000159 System.Void Lean.Touch.LeanFingerOld::HandleFingerOld(Lean.Touch.LeanFinger)
extern void LeanFingerOld_HandleFingerOld_m416BB8EA7C79CC1C003550E3F9439B5ED7668375 (void);
// 0x0000015A System.Void Lean.Touch.LeanFingerOld::.ctor()
extern void LeanFingerOld__ctor_m05EDF29F6C7D1812E64521535052CF71671CE793 (void);
// 0x0000015B System.Void Lean.Touch.LeanFingerSwipe::Awake()
extern void LeanFingerSwipe_Awake_mD9328660DA1B3B5D6D9D031A3167BFCB0C03A50B (void);
// 0x0000015C System.Void Lean.Touch.LeanFingerSwipe::OnEnable()
extern void LeanFingerSwipe_OnEnable_m10DA774A0B007814784A755C9CBD4140DA49AA00 (void);
// 0x0000015D System.Void Lean.Touch.LeanFingerSwipe::OnDisable()
extern void LeanFingerSwipe_OnDisable_m044DB93E492A1B81D08461B8EE1D3846CD5BB5D1 (void);
// 0x0000015E System.Void Lean.Touch.LeanFingerSwipe::HandleFingerSwipe(Lean.Touch.LeanFinger)
extern void LeanFingerSwipe_HandleFingerSwipe_mF76AAF439C0238799A27C9A0FCCB71C0C3310EC3 (void);
// 0x0000015F System.Void Lean.Touch.LeanFingerSwipe::.ctor()
extern void LeanFingerSwipe__ctor_m237BB98066A426F484660860ECEAF60190379B24 (void);
// 0x00000160 Lean.Touch.LeanFingerTap/LeanFingerEvent Lean.Touch.LeanFingerTap::get_OnFinger()
extern void LeanFingerTap_get_OnFinger_m9ACB31CD15CA32E506CBEBC483481953854776C8 (void);
// 0x00000161 Lean.Touch.LeanFingerTap/IntEvent Lean.Touch.LeanFingerTap::get_OnCount()
extern void LeanFingerTap_get_OnCount_mB1E3A750E9BB4A0E1CC5A8FA15396106416D5668 (void);
// 0x00000162 Lean.Touch.LeanFingerTap/Vector3Event Lean.Touch.LeanFingerTap::get_OnWorld()
extern void LeanFingerTap_get_OnWorld_mC269480BC6FCEE7BA1D319E80E6F031C351FF1A8 (void);
// 0x00000163 Lean.Touch.LeanFingerTap/Vector2Event Lean.Touch.LeanFingerTap::get_OnScreen()
extern void LeanFingerTap_get_OnScreen_mDB16551B09D618D08F597665EFF1FE4AE69410A9 (void);
// 0x00000164 System.Void Lean.Touch.LeanFingerTap::Start()
extern void LeanFingerTap_Start_mEFEAF7F401BCA21468B99FF61AC5041B663CCC0A (void);
// 0x00000165 System.Void Lean.Touch.LeanFingerTap::OnEnable()
extern void LeanFingerTap_OnEnable_m1DDC1AF0B80803ED7593BE55B11BCE7F1E7CDC58 (void);
// 0x00000166 System.Void Lean.Touch.LeanFingerTap::OnDisable()
extern void LeanFingerTap_OnDisable_m27980E600FF685528006FA2D68EC4AE5689E5098 (void);
// 0x00000167 System.Void Lean.Touch.LeanFingerTap::HandleFingerTap(Lean.Touch.LeanFinger)
extern void LeanFingerTap_HandleFingerTap_m391E3808AC33BAB6B186EB0D003B0FC049FF20D5 (void);
// 0x00000168 System.Void Lean.Touch.LeanFingerTap::.ctor()
extern void LeanFingerTap__ctor_m4FBC89B7AC00498ECC7A76291D54903118605B98 (void);
// 0x00000169 Lean.Touch.LeanFingerUp/LeanFingerEvent Lean.Touch.LeanFingerUp::get_OnFinger()
extern void LeanFingerUp_get_OnFinger_m13ED2351510968174A99E1930F5D98EE809EC58B (void);
// 0x0000016A Lean.Touch.LeanFingerUp/Vector3Event Lean.Touch.LeanFingerUp::get_OnWorld()
extern void LeanFingerUp_get_OnWorld_m337A230C59B548A4D1F49AC1BC829C7AC4B3D2C2 (void);
// 0x0000016B Lean.Touch.LeanFingerUp/Vector2Event Lean.Touch.LeanFingerUp::get_OnScreen()
extern void LeanFingerUp_get_OnScreen_m0898E22CC52628062051442AD3CCCD2C844A6E37 (void);
// 0x0000016C System.Void Lean.Touch.LeanFingerUp::Start()
extern void LeanFingerUp_Start_m9F6BB4CB86D769CFBF83F2282D3DF40284E25AAA (void);
// 0x0000016D System.Void Lean.Touch.LeanFingerUp::OnEnable()
extern void LeanFingerUp_OnEnable_m4700254C003310B0CE255EB65BD1B6B4FF9BA1C8 (void);
// 0x0000016E System.Void Lean.Touch.LeanFingerUp::OnDisable()
extern void LeanFingerUp_OnDisable_m7D8508BA58501993E9CB9A28C59BB43AEC0FD039 (void);
// 0x0000016F System.Void Lean.Touch.LeanFingerUp::HandleFingerUp(Lean.Touch.LeanFinger)
extern void LeanFingerUp_HandleFingerUp_m404D0864041DE3F7EB7FD970BE3D3F7F10203BFC (void);
// 0x00000170 System.Void Lean.Touch.LeanFingerUp::.ctor()
extern void LeanFingerUp__ctor_m73784348918E7E501160DBE2454F0384030D82EA (void);
// 0x00000171 Lean.Touch.LeanFingerUpdate/LeanFingerEvent Lean.Touch.LeanFingerUpdate::get_OnFinger()
extern void LeanFingerUpdate_get_OnFinger_m91602063824CF466F4A1BF8568C248C8FF4D33AC (void);
// 0x00000172 Lean.Touch.LeanFingerUpdate/Vector2Event Lean.Touch.LeanFingerUpdate::get_OnDelta()
extern void LeanFingerUpdate_get_OnDelta_m4EFD9A089CDE1C6EBFED856F78B07EE4BCB4CF9B (void);
// 0x00000173 Lean.Touch.LeanFingerUpdate/FloatEvent Lean.Touch.LeanFingerUpdate::get_OnDistance()
extern void LeanFingerUpdate_get_OnDistance_m5C3D628E2C95CFC2D306806B265EFD1971553697 (void);
// 0x00000174 Lean.Touch.LeanFingerUpdate/Vector3Event Lean.Touch.LeanFingerUpdate::get_OnWorldFrom()
extern void LeanFingerUpdate_get_OnWorldFrom_m3CDBD2FD0D9FED63188B4C3A797CC08F56BD8CA9 (void);
// 0x00000175 Lean.Touch.LeanFingerUpdate/Vector3Event Lean.Touch.LeanFingerUpdate::get_OnWorldTo()
extern void LeanFingerUpdate_get_OnWorldTo_m3FC07F86C2F3A4BB17B9377EEB93CD7D56E6EB78 (void);
// 0x00000176 Lean.Touch.LeanFingerUpdate/Vector3Event Lean.Touch.LeanFingerUpdate::get_OnWorldDelta()
extern void LeanFingerUpdate_get_OnWorldDelta_mF9626EC74455D7029FD75CFDA4F6A8FCB60DFA25 (void);
// 0x00000177 Lean.Touch.LeanFingerUpdate/Vector3Vector3Event Lean.Touch.LeanFingerUpdate::get_OnWorldFromTo()
extern void LeanFingerUpdate_get_OnWorldFromTo_mE113F08E1043681A4538CD1613B7F55A2ECA3AD7 (void);
// 0x00000178 System.Void Lean.Touch.LeanFingerUpdate::Awake()
extern void LeanFingerUpdate_Awake_mBD1BFA6C5FEE4C1C7E99CB84916B631D2DE62D25 (void);
// 0x00000179 System.Void Lean.Touch.LeanFingerUpdate::OnEnable()
extern void LeanFingerUpdate_OnEnable_m53E8D10FC7F1C49F3573DF410B17F6AEB39813FE (void);
// 0x0000017A System.Void Lean.Touch.LeanFingerUpdate::OnDisable()
extern void LeanFingerUpdate_OnDisable_m6A9474317AFD20C81963732BF1D3C0A13C7761B5 (void);
// 0x0000017B System.Void Lean.Touch.LeanFingerUpdate::HandleFingerUpdate(Lean.Touch.LeanFinger)
extern void LeanFingerUpdate_HandleFingerUpdate_mE7A1613E0DA00A5A8FE848C5FEC64333E6D33B19 (void);
// 0x0000017C System.Void Lean.Touch.LeanFingerUpdate::.ctor()
extern void LeanFingerUpdate__ctor_mDC2270DFA2E7A11E4EA048FDC24E2D0D0ADBBA08 (void);
// 0x0000017D System.Void Lean.Touch.LeanPinchScale::AddFinger(Lean.Touch.LeanFinger)
extern void LeanPinchScale_AddFinger_m6E63F6A4AAAF088F9B31A70D37E8C926B418E05A (void);
// 0x0000017E System.Void Lean.Touch.LeanPinchScale::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanPinchScale_RemoveFinger_m7570DF4028DA34BE78DDDF839AB2FBE0C39C60BB (void);
// 0x0000017F System.Void Lean.Touch.LeanPinchScale::RemoveAllFingers()
extern void LeanPinchScale_RemoveAllFingers_mD25443D486D77D1F252B1761B88DC3DADCF1EE26 (void);
// 0x00000180 System.Void Lean.Touch.LeanPinchScale::Awake()
extern void LeanPinchScale_Awake_m382090C744E0F461544135AB055C00DB159E2ABC (void);
// 0x00000181 System.Void Lean.Touch.LeanPinchScale::Update()
extern void LeanPinchScale_Update_mD074B060F96F1B4CA5B8973438527406F841D4FF (void);
// 0x00000182 System.Void Lean.Touch.LeanPinchScale::TranslateUI(System.Single,UnityEngine.Vector2)
extern void LeanPinchScale_TranslateUI_m92FB2B58544A3307F0DB40403A32589AACE04A77 (void);
// 0x00000183 System.Void Lean.Touch.LeanPinchScale::Translate(System.Single,UnityEngine.Vector2)
extern void LeanPinchScale_Translate_m93CA0FC32CECFDD7985836A23CDB8ACC4878223D (void);
// 0x00000184 System.Void Lean.Touch.LeanPinchScale::.ctor()
extern void LeanPinchScale__ctor_mF9221BF63C4774835A82B33603A83DB0211925A1 (void);
// 0x00000185 System.Void Lean.Touch.LeanScreenDepth::.ctor(Lean.Touch.LeanScreenDepth/ConversionType,System.Int32,System.Single)
extern void LeanScreenDepth__ctor_m8241C542FD0B47AB7B44559D5148FE58FA0E6BD7 (void);
// 0x00000186 UnityEngine.Vector3 Lean.Touch.LeanScreenDepth::Convert(UnityEngine.Vector2,UnityEngine.GameObject,UnityEngine.Transform)
extern void LeanScreenDepth_Convert_m2380FBFF96D3036FFEF582F64E30AD029E49E13F (void);
// 0x00000187 UnityEngine.Vector3 Lean.Touch.LeanScreenDepth::ConvertDelta(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.GameObject,UnityEngine.Transform)
extern void LeanScreenDepth_ConvertDelta_mC27AC769D401202E0BC4E4651DC0ADF035CF4071 (void);
// 0x00000188 System.Boolean Lean.Touch.LeanScreenDepth::TryConvert(UnityEngine.Vector3&,UnityEngine.Vector2,UnityEngine.GameObject,UnityEngine.Transform)
extern void LeanScreenDepth_TryConvert_m7586DA6106530AA370705E0D7795B04AB7E5887D (void);
// 0x00000189 System.Boolean Lean.Touch.LeanScreenDepth::Exists(UnityEngine.GameObject,T&)
// 0x0000018A System.Boolean Lean.Touch.LeanScreenDepth::IsChildOf(UnityEngine.Transform,UnityEngine.Transform)
extern void LeanScreenDepth_IsChildOf_m4FEEC8865B58163E828194B3780672BE719F4A64 (void);
// 0x0000018B System.Void Lean.Touch.LeanScreenDepth::.cctor()
extern void LeanScreenDepth__cctor_mFC88DAAFFE1F65BCC52EC9BCE7A3C66897E173B2 (void);
// 0x0000018C System.Void Lean.Touch.LeanSelect::TrySelect(Lean.Touch.LeanFinger,UnityEngine.Component,UnityEngine.Vector3)
extern void LeanSelect_TrySelect_m9DB5A381403F839F532286B39358926A5F737BC2 (void);
// 0x0000018D System.Void Lean.Touch.LeanSelect::Select(Lean.Touch.LeanFinger,Lean.Touch.LeanSelectable)
extern void LeanSelect_Select_m794CAD55F9934F27EE462DEA49F56792F6455B87 (void);
// 0x0000018E System.Void Lean.Touch.LeanSelect::DeselectAll()
extern void LeanSelect_DeselectAll_mD01A1B1C4EBA4A68E1E100411B80466440D71558 (void);
// 0x0000018F System.Void Lean.Touch.LeanSelect::OnEnable()
extern void LeanSelect_OnEnable_m2BCE2916757220B2A3ADD89DE89D96EBB644C3D8 (void);
// 0x00000190 System.Void Lean.Touch.LeanSelect::OnDisable()
extern void LeanSelect_OnDisable_mBEA3E720D573E5A8A849E40E82D33F20524113C1 (void);
// 0x00000191 System.Void Lean.Touch.LeanSelect::.ctor()
extern void LeanSelect__ctor_mE71CFDD87A172D0584344855AABF07B40A55D205 (void);
// 0x00000192 System.Void Lean.Touch.LeanSelect::.cctor()
extern void LeanSelect__cctor_mB909940D752232BC29CBFE7A730F80D91AFF334C (void);
// 0x00000193 System.Void Lean.Touch.LeanSelectBase::SelectStartScreenPosition(Lean.Touch.LeanFinger)
extern void LeanSelectBase_SelectStartScreenPosition_mC7F6D894E56F3765DE5039DA2507DD3014506FAE (void);
// 0x00000194 System.Void Lean.Touch.LeanSelectBase::SelectScreenPosition(Lean.Touch.LeanFinger)
extern void LeanSelectBase_SelectScreenPosition_mEE36F99A486EEC8C09BFC4FD7665819DE9DB0258 (void);
// 0x00000195 System.Void Lean.Touch.LeanSelectBase::SelectScreenPosition(Lean.Touch.LeanFinger,UnityEngine.Vector2)
extern void LeanSelectBase_SelectScreenPosition_mBFF0B9251F3F22575C4BE2117166F3BF71D33E8C (void);
// 0x00000196 System.Void Lean.Touch.LeanSelectBase::TrySelect(Lean.Touch.LeanFinger,UnityEngine.Component,UnityEngine.Vector3)
// 0x00000197 System.Void Lean.Touch.LeanSelectBase::TryGetComponent(Lean.Touch.LeanSelectBase/SelectType,UnityEngine.Vector2,UnityEngine.Component&,UnityEngine.Vector3&)
extern void LeanSelectBase_TryGetComponent_m7131E33DDCA54AC16B3655438E0152F78F364FCD (void);
// 0x00000198 System.Int32 Lean.Touch.LeanSelectBase::GetClosestRaycastHitsIndex(System.Int32)
extern void LeanSelectBase_GetClosestRaycastHitsIndex_m11C95C684E6D6FA54F152249C2DC03434C07F73D (void);
// 0x00000199 UnityEngine.Vector2 Lean.Touch.LeanSelectBase::GetScreenPoint(UnityEngine.Camera,UnityEngine.Transform)
extern void LeanSelectBase_GetScreenPoint_mFB4F0DC8FBC7D74AC82C771AEFED913CE72A77F6 (void);
// 0x0000019A System.Void Lean.Touch.LeanSelectBase::.ctor()
extern void LeanSelectBase__ctor_mD6D21949ABB48BFAC35F502F3854D74B27692521 (void);
// 0x0000019B System.Void Lean.Touch.LeanSelectBase::.cctor()
extern void LeanSelectBase__cctor_mB66EA6E33B85A9D567509F1BC4DC8E9094C2D9EA (void);
// 0x0000019C System.Void Lean.Touch.LeanSelectable::add_OnEnableGlobal(System.Action`1<Lean.Touch.LeanSelectable>)
extern void LeanSelectable_add_OnEnableGlobal_m9F457F205DDA6C06927E2085DF445E365E026816 (void);
// 0x0000019D System.Void Lean.Touch.LeanSelectable::remove_OnEnableGlobal(System.Action`1<Lean.Touch.LeanSelectable>)
extern void LeanSelectable_remove_OnEnableGlobal_mD41F740F27DBE3C58673A0D289FC741CBB119637 (void);
// 0x0000019E System.Void Lean.Touch.LeanSelectable::add_OnDisableGlobal(System.Action`1<Lean.Touch.LeanSelectable>)
extern void LeanSelectable_add_OnDisableGlobal_mBF955FC5ACF3C04DE55AAF330E2EA836BB2C4F42 (void);
// 0x0000019F System.Void Lean.Touch.LeanSelectable::remove_OnDisableGlobal(System.Action`1<Lean.Touch.LeanSelectable>)
extern void LeanSelectable_remove_OnDisableGlobal_m48850D7E0A5B384458358B24209EED8B5E12772D (void);
// 0x000001A0 System.Void Lean.Touch.LeanSelectable::add_OnSelectGlobal(System.Action`2<Lean.Touch.LeanSelectable,Lean.Touch.LeanFinger>)
extern void LeanSelectable_add_OnSelectGlobal_m26EC561CD6AF8AE8BFE48B3B6692A8221770FF2A (void);
// 0x000001A1 System.Void Lean.Touch.LeanSelectable::remove_OnSelectGlobal(System.Action`2<Lean.Touch.LeanSelectable,Lean.Touch.LeanFinger>)
extern void LeanSelectable_remove_OnSelectGlobal_mE31A611550B3C16DF21901A218FAF4823FAAC86A (void);
// 0x000001A2 System.Void Lean.Touch.LeanSelectable::add_OnSelectSetGlobal(System.Action`2<Lean.Touch.LeanSelectable,Lean.Touch.LeanFinger>)
extern void LeanSelectable_add_OnSelectSetGlobal_mE22AC4FE50CDA36803F262FD6530888D05FFB5D6 (void);
// 0x000001A3 System.Void Lean.Touch.LeanSelectable::remove_OnSelectSetGlobal(System.Action`2<Lean.Touch.LeanSelectable,Lean.Touch.LeanFinger>)
extern void LeanSelectable_remove_OnSelectSetGlobal_m2281234B22D87DA7AB917E92F8D7D2DC8CE7F405 (void);
// 0x000001A4 System.Void Lean.Touch.LeanSelectable::add_OnSelectUpGlobal(System.Action`2<Lean.Touch.LeanSelectable,Lean.Touch.LeanFinger>)
extern void LeanSelectable_add_OnSelectUpGlobal_mB6012790E296C34FF4DE19F55CB0AAF1BEEE7AD1 (void);
// 0x000001A5 System.Void Lean.Touch.LeanSelectable::remove_OnSelectUpGlobal(System.Action`2<Lean.Touch.LeanSelectable,Lean.Touch.LeanFinger>)
extern void LeanSelectable_remove_OnSelectUpGlobal_mBA331CE50DEB92F005AB5B80156D627CC8DC2994 (void);
// 0x000001A6 System.Void Lean.Touch.LeanSelectable::add_OnDeselectGlobal(System.Action`1<Lean.Touch.LeanSelectable>)
extern void LeanSelectable_add_OnDeselectGlobal_m3C2708774D3E0D3DFB0FD2FD31DBA84DA41D83D1 (void);
// 0x000001A7 System.Void Lean.Touch.LeanSelectable::remove_OnDeselectGlobal(System.Action`1<Lean.Touch.LeanSelectable>)
extern void LeanSelectable_remove_OnDeselectGlobal_m422D6B7107F1A8A9CC7E27742163B1222EF37B71 (void);
// 0x000001A8 Lean.Touch.LeanSelectable/LeanFingerEvent Lean.Touch.LeanSelectable::get_OnSelect()
extern void LeanSelectable_get_OnSelect_m8E3DBD00DDA294792632B17866B991491D581FB4 (void);
// 0x000001A9 Lean.Touch.LeanSelectable/LeanFingerEvent Lean.Touch.LeanSelectable::get_OnSelectUpdate()
extern void LeanSelectable_get_OnSelectUpdate_m464D13210498A07206E2B8F6582E64BF122ED2CA (void);
// 0x000001AA Lean.Touch.LeanSelectable/LeanFingerEvent Lean.Touch.LeanSelectable::get_OnSelectUp()
extern void LeanSelectable_get_OnSelectUp_m581FE3EDD9E61DD2799650B5232FF86BA60A2E4C (void);
// 0x000001AB UnityEngine.Events.UnityEvent Lean.Touch.LeanSelectable::get_OnDeselect()
extern void LeanSelectable_get_OnDeselect_mE71E83654EA5C5C5016B73A6DD4FB9BA7816BF36 (void);
// 0x000001AC System.Void Lean.Touch.LeanSelectable::set_IsSelected(System.Boolean)
extern void LeanSelectable_set_IsSelected_m3E3F7DD3FAB07C75DF2B22DC2C08FABBCC5E196D (void);
// 0x000001AD System.Boolean Lean.Touch.LeanSelectable::get_IsSelected()
extern void LeanSelectable_get_IsSelected_mA27BBA35905B07BCBDF9140C908C1866E49349A4 (void);
// 0x000001AE System.Boolean Lean.Touch.LeanSelectable::get_IsSelectedRaw()
extern void LeanSelectable_get_IsSelectedRaw_mEE4FBF8D25B0D4B288EA58D1A4D20E7CFB264F12 (void);
// 0x000001AF System.Int32 Lean.Touch.LeanSelectable::get_IsSelectedCount()
extern void LeanSelectable_get_IsSelectedCount_m4BF11670DCEF739799B526D06B9E6EA6D3DA4A19 (void);
// 0x000001B0 System.Int32 Lean.Touch.LeanSelectable::get_IsSelectedRawCount()
extern void LeanSelectable_get_IsSelectedRawCount_m4D2A4780D62557DD83521A6E36A0D2CA6C216A62 (void);
// 0x000001B1 Lean.Touch.LeanFinger Lean.Touch.LeanSelectable::get_SelectingFinger()
extern void LeanSelectable_get_SelectingFinger_m7A62C9DD7B9F7126C42071304F9740269104C371 (void);
// 0x000001B2 System.Collections.Generic.List`1<Lean.Touch.LeanFinger> Lean.Touch.LeanSelectable::get_SelectingFingers()
extern void LeanSelectable_get_SelectingFingers_m27554C124728051293C687BE19DBC1C13840D987 (void);
// 0x000001B3 System.Collections.Generic.List`1<Lean.Touch.LeanFinger> Lean.Touch.LeanSelectable::GetFingers(System.Boolean,System.Boolean,System.Int32,Lean.Touch.LeanSelectable)
extern void LeanSelectable_GetFingers_mBBC187B20CD5D6E01A21AF4C389204956C757B9D (void);
// 0x000001B4 System.Void Lean.Touch.LeanSelectable::GetSelected(System.Collections.Generic.List`1<Lean.Touch.LeanSelectable>)
extern void LeanSelectable_GetSelected_mD97EA73EA840F3302FFC4183924919F040B8D49F (void);
// 0x000001B5 System.Void Lean.Touch.LeanSelectable::Cull(System.Int32)
extern void LeanSelectable_Cull_mFE6613BA264DE143D809FDB43365C5A9270B7C47 (void);
// 0x000001B6 Lean.Touch.LeanSelectable Lean.Touch.LeanSelectable::FindSelectable(Lean.Touch.LeanFinger)
extern void LeanSelectable_FindSelectable_m06CEAEF90D3BC159F8009E1C15AA92E0D4188E8B (void);
// 0x000001B7 System.Void Lean.Touch.LeanSelectable::ReplaceSelection(Lean.Touch.LeanFinger,System.Collections.Generic.List`1<Lean.Touch.LeanSelectable>)
extern void LeanSelectable_ReplaceSelection_m845CDD957A1FD92F9E38C4FF178FC1E5B7FCD159 (void);
// 0x000001B8 System.Boolean Lean.Touch.LeanSelectable::IsSelectedBy(Lean.Touch.LeanFinger)
extern void LeanSelectable_IsSelectedBy_m508E6BB74CA6352735D60E613CFF1C39183433ED (void);
// 0x000001B9 System.Boolean Lean.Touch.LeanSelectable::GetIsSelected(System.Boolean)
extern void LeanSelectable_GetIsSelected_mF69B8EB9DD26D08C5A481897776D567C60CDDD80 (void);
// 0x000001BA System.Void Lean.Touch.LeanSelectable::Select()
extern void LeanSelectable_Select_m846A32582C9696ED9046C46A5645BC6954415C3C (void);
// 0x000001BB System.Void Lean.Touch.LeanSelectable::Select(Lean.Touch.LeanFinger)
extern void LeanSelectable_Select_mD703CF70A9033CB32635FF0C8ABBA4445FF9AD2F (void);
// 0x000001BC System.Void Lean.Touch.LeanSelectable::Deselect()
extern void LeanSelectable_Deselect_m194A710A0CCCED6142EBCE699D6ED5CFA69309E9 (void);
// 0x000001BD System.Void Lean.Touch.LeanSelectable::DeselectAll()
extern void LeanSelectable_DeselectAll_mB92F85B0B613EB2A0876545D01DAA4EB5847BF81 (void);
// 0x000001BE System.Void Lean.Touch.LeanSelectable::OnEnable()
extern void LeanSelectable_OnEnable_mDC06C36AB4D9BBF8BE346B9E3C126F59207153DD (void);
// 0x000001BF System.Void Lean.Touch.LeanSelectable::OnDisable()
extern void LeanSelectable_OnDisable_mDBB0F91EDA3651A99FC2616F74FC6FC66C617F5D (void);
// 0x000001C0 System.Void Lean.Touch.LeanSelectable::BuildTempSelectables(Lean.Touch.LeanFinger)
extern void LeanSelectable_BuildTempSelectables_mD78396F44702992D3F450875EA670BF50372146D (void);
// 0x000001C1 System.Void Lean.Touch.LeanSelectable::HandleFingerUpdate(Lean.Touch.LeanFinger)
extern void LeanSelectable_HandleFingerUpdate_mE169485133CADAF14A706ADAC5836410C920DA5F (void);
// 0x000001C2 System.Boolean Lean.Touch.LeanSelectable::get_AnyFingersSet()
extern void LeanSelectable_get_AnyFingersSet_mD93FE54507CCF5A6E716C884151415B0BAE1C747 (void);
// 0x000001C3 System.Void Lean.Touch.LeanSelectable::HandleFingerUp(Lean.Touch.LeanFinger)
extern void LeanSelectable_HandleFingerUp_m7E27A42F6CC1B8B05279F615DFD829BDA054AD15 (void);
// 0x000001C4 System.Void Lean.Touch.LeanSelectable::HandleFingerInactive(Lean.Touch.LeanFinger)
extern void LeanSelectable_HandleFingerInactive_mB1581C19AEBC3162DA1ACB0FD63599A646D3034D (void);
// 0x000001C5 System.Void Lean.Touch.LeanSelectable::.ctor()
extern void LeanSelectable__ctor_m53DB58A1274046458288BE890A87033E0DD0F78A (void);
// 0x000001C6 System.Void Lean.Touch.LeanSelectable::.cctor()
extern void LeanSelectable__cctor_m031C6481394CB34F214981D827F593531BB154B2 (void);
// 0x000001C7 Lean.Touch.LeanSelectable Lean.Touch.LeanSelectableBehaviour::get_Selectable()
extern void LeanSelectableBehaviour_get_Selectable_m87AAF88CB651F300DDF2604928B6313085E2A56E (void);
// 0x000001C8 System.Void Lean.Touch.LeanSelectableBehaviour::Register()
extern void LeanSelectableBehaviour_Register_m58EB6A8F0755B458F63BFF6D227A5E069F435021 (void);
// 0x000001C9 System.Void Lean.Touch.LeanSelectableBehaviour::Register(Lean.Touch.LeanSelectable)
extern void LeanSelectableBehaviour_Register_mFE11711D2B46EF97F9A65CFB2BA529DB3E192057 (void);
// 0x000001CA System.Void Lean.Touch.LeanSelectableBehaviour::Unregister()
extern void LeanSelectableBehaviour_Unregister_m6435986D4F6690B1985177ADDE732C82BAF20DC6 (void);
// 0x000001CB System.Void Lean.Touch.LeanSelectableBehaviour::OnEnable()
extern void LeanSelectableBehaviour_OnEnable_m03E270B708043089690EB6FA800647ECEB849D0D (void);
// 0x000001CC System.Void Lean.Touch.LeanSelectableBehaviour::Start()
extern void LeanSelectableBehaviour_Start_mFC34E21B27A0852A19DDBEF817CDC9FE10F9D41E (void);
// 0x000001CD System.Void Lean.Touch.LeanSelectableBehaviour::OnDisable()
extern void LeanSelectableBehaviour_OnDisable_m82CCD84E6CE7C3721A20CDBC64B5AA6BB8AFC71A (void);
// 0x000001CE System.Void Lean.Touch.LeanSelectableBehaviour::OnSelect(Lean.Touch.LeanFinger)
extern void LeanSelectableBehaviour_OnSelect_m59094BAE9D0C6819C36008D420C81411E5CECE78 (void);
// 0x000001CF System.Void Lean.Touch.LeanSelectableBehaviour::OnSelectUp(Lean.Touch.LeanFinger)
extern void LeanSelectableBehaviour_OnSelectUp_m1D1C272BDE43478432AF97FDB96E95CB96545DCE (void);
// 0x000001D0 System.Void Lean.Touch.LeanSelectableBehaviour::OnDeselect()
extern void LeanSelectableBehaviour_OnDeselect_m8AC245592C2348DF2DD6370D3E8DD86F6BEDEAC7 (void);
// 0x000001D1 System.Void Lean.Touch.LeanSelectableBehaviour::.ctor()
extern void LeanSelectableBehaviour__ctor_m1A67DE8233AAE5D95131BD11A0C4F51DBD125E40 (void);
// 0x000001D2 System.Void Lean.Touch.LeanSelectableGraphicColor::Awake()
extern void LeanSelectableGraphicColor_Awake_m47B10A31C5551601B170C55887DB43C3039F393C (void);
// 0x000001D3 System.Void Lean.Touch.LeanSelectableGraphicColor::OnSelect(Lean.Touch.LeanFinger)
extern void LeanSelectableGraphicColor_OnSelect_m0437B76E863066BFDAECB508C08FD612A51B1501 (void);
// 0x000001D4 System.Void Lean.Touch.LeanSelectableGraphicColor::OnDeselect()
extern void LeanSelectableGraphicColor_OnDeselect_mB654258EAD7A5DEED077C19D11C1644F2E3A259A (void);
// 0x000001D5 System.Void Lean.Touch.LeanSelectableGraphicColor::ChangeColor(UnityEngine.Color)
extern void LeanSelectableGraphicColor_ChangeColor_m831F6F88269CB96FF42FBBB90A0E8FA2E84073DF (void);
// 0x000001D6 System.Void Lean.Touch.LeanSelectableGraphicColor::.ctor()
extern void LeanSelectableGraphicColor__ctor_mF2F9A5D202409902F56C8AE707E1C4EFD0BBE49F (void);
// 0x000001D7 System.Void Lean.Touch.LeanSelectableRendererColor::Awake()
extern void LeanSelectableRendererColor_Awake_m214897D8FAA5072C15423ED6D4FE8494EE54F84A (void);
// 0x000001D8 System.Void Lean.Touch.LeanSelectableRendererColor::OnSelect(Lean.Touch.LeanFinger)
extern void LeanSelectableRendererColor_OnSelect_m7CFFA6A7D545A4F2CC4645513B336E8C5EC09788 (void);
// 0x000001D9 System.Void Lean.Touch.LeanSelectableRendererColor::OnDeselect()
extern void LeanSelectableRendererColor_OnDeselect_m09E2705952F68DFED31BE322F9D9322935574E94 (void);
// 0x000001DA System.Void Lean.Touch.LeanSelectableRendererColor::ChangeColor(UnityEngine.Color)
extern void LeanSelectableRendererColor_ChangeColor_m04BD36420463E81EC44AEF6D74B74F2F034B9821 (void);
// 0x000001DB System.Void Lean.Touch.LeanSelectableRendererColor::.ctor()
extern void LeanSelectableRendererColor__ctor_mB53D27CE1C3119E41B37BA4AD05033DB432060B0 (void);
// 0x000001DC System.Void Lean.Touch.LeanSelectableSpriteRendererColor::Awake()
extern void LeanSelectableSpriteRendererColor_Awake_mF533BF2BE789DC9641B1A075F21424148A304DFC (void);
// 0x000001DD System.Void Lean.Touch.LeanSelectableSpriteRendererColor::OnSelect(Lean.Touch.LeanFinger)
extern void LeanSelectableSpriteRendererColor_OnSelect_m7C1911DC66C6898BD5F7367A1AF7D48ADD5EA7FD (void);
// 0x000001DE System.Void Lean.Touch.LeanSelectableSpriteRendererColor::OnDeselect()
extern void LeanSelectableSpriteRendererColor_OnDeselect_mA3AFE7FE2400FD87DEADC5F651B4F6E17EAEA8D4 (void);
// 0x000001DF System.Void Lean.Touch.LeanSelectableSpriteRendererColor::ChangeColor(UnityEngine.Color)
extern void LeanSelectableSpriteRendererColor_ChangeColor_mA0219FA4548BE17A237373D47CF8962A3D3116F0 (void);
// 0x000001E0 System.Void Lean.Touch.LeanSelectableSpriteRendererColor::.ctor()
extern void LeanSelectableSpriteRendererColor__ctor_mC79C9E1BA2A257B2FB0A8AA241DD4723084785FD (void);
// 0x000001E1 Lean.Touch.LeanSwipeBase/LeanFingerEvent Lean.Touch.LeanSwipeBase::get_OnFinger()
extern void LeanSwipeBase_get_OnFinger_mE101F01D72CBC82AA47618B93C15923D218ACB2A (void);
// 0x000001E2 Lean.Touch.LeanSwipeBase/Vector2Event Lean.Touch.LeanSwipeBase::get_OnDelta()
extern void LeanSwipeBase_get_OnDelta_mD553D217539D026ACA504EF0F8A03AC6C9E23C5D (void);
// 0x000001E3 Lean.Touch.LeanSwipeBase/FloatEvent Lean.Touch.LeanSwipeBase::get_OnDistance()
extern void LeanSwipeBase_get_OnDistance_mD6FEF5D166EAED2FEBEAA14484A14D552D50A929 (void);
// 0x000001E4 Lean.Touch.LeanSwipeBase/Vector3Event Lean.Touch.LeanSwipeBase::get_OnWorldFrom()
extern void LeanSwipeBase_get_OnWorldFrom_mB4D4334D499F6C420A7694925AB80FC696A33428 (void);
// 0x000001E5 Lean.Touch.LeanSwipeBase/Vector3Event Lean.Touch.LeanSwipeBase::get_OnWorldTo()
extern void LeanSwipeBase_get_OnWorldTo_m58A5E3A86B5D16D2082EE229F79416A6C0ED5430 (void);
// 0x000001E6 Lean.Touch.LeanSwipeBase/Vector3Event Lean.Touch.LeanSwipeBase::get_OnWorldDelta()
extern void LeanSwipeBase_get_OnWorldDelta_m3E702656F54E2EB573F42A7D4D2A7499F0722ECA (void);
// 0x000001E7 Lean.Touch.LeanSwipeBase/Vector3Vector3Event Lean.Touch.LeanSwipeBase::get_OnWorldFromTo()
extern void LeanSwipeBase_get_OnWorldFromTo_mECA2C27D7693D94627B42B5B374206A27064E9A7 (void);
// 0x000001E8 System.Boolean Lean.Touch.LeanSwipeBase::AngleIsValid(UnityEngine.Vector2)
extern void LeanSwipeBase_AngleIsValid_mDD59824DAD6494B0B600778987E68B882DEEFB4E (void);
// 0x000001E9 System.Void Lean.Touch.LeanSwipeBase::HandleFingerSwipe(Lean.Touch.LeanFinger,UnityEngine.Vector2,UnityEngine.Vector2)
extern void LeanSwipeBase_HandleFingerSwipe_mD530BABFA688A2AC05D9112184C877545228ACEE (void);
// 0x000001EA System.Void Lean.Touch.LeanSwipeBase::.ctor()
extern void LeanSwipeBase__ctor_m1AD287D74A26E80ECBD3F16E3B66031F61A8802C (void);
// 0x000001EB System.Void Lean.Touch.LeanTwistRotate::AddFinger(Lean.Touch.LeanFinger)
extern void LeanTwistRotate_AddFinger_mB1ACE79A7BEC44DA4DF1546AA025486B307C40C3 (void);
// 0x000001EC System.Void Lean.Touch.LeanTwistRotate::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanTwistRotate_RemoveFinger_mBA43133CC4CD144B9067CA95D01DBD0BFAED1C89 (void);
// 0x000001ED System.Void Lean.Touch.LeanTwistRotate::RemoveAllFingers()
extern void LeanTwistRotate_RemoveAllFingers_m88C3C6735CCE7C1E26A822F0F9651C01259ABAEB (void);
// 0x000001EE System.Void Lean.Touch.LeanTwistRotate::Awake()
extern void LeanTwistRotate_Awake_m5A13FFC66B445FB3C95C2E268F8C6C14430F2596 (void);
// 0x000001EF System.Void Lean.Touch.LeanTwistRotate::Update()
extern void LeanTwistRotate_Update_m95502F02441034C7FE830985744630D2AA7AF061 (void);
// 0x000001F0 System.Void Lean.Touch.LeanTwistRotate::TranslateUI(System.Single,UnityEngine.Vector2)
extern void LeanTwistRotate_TranslateUI_m2A211B8D0DF239454A179B2649339322DE742F04 (void);
// 0x000001F1 System.Void Lean.Touch.LeanTwistRotate::Translate(System.Single,UnityEngine.Vector2)
extern void LeanTwistRotate_Translate_m40074D4F4627C9A11B9785B55481F0429C5BBE48 (void);
// 0x000001F2 System.Void Lean.Touch.LeanTwistRotate::RotateUI(System.Single)
extern void LeanTwistRotate_RotateUI_m4296C6FA8AC71E445E500C31D75A63FAC9BEB649 (void);
// 0x000001F3 System.Void Lean.Touch.LeanTwistRotate::Rotate(System.Single)
extern void LeanTwistRotate_Rotate_mC0D7068BCFC1F255E39B3AC0C14A147DB81469D7 (void);
// 0x000001F4 System.Void Lean.Touch.LeanTwistRotate::.ctor()
extern void LeanTwistRotate__ctor_m0FE60DC5C70DB36376C6C4E34E23D9D4D6F849CB (void);
// 0x000001F5 System.Void Lean.Touch.LeanTwistRotateAxis::AddFinger(Lean.Touch.LeanFinger)
extern void LeanTwistRotateAxis_AddFinger_mC1368C6FBFCA893FFF0BA6953EFDD4FFF5881B6F (void);
// 0x000001F6 System.Void Lean.Touch.LeanTwistRotateAxis::RemoveFinger(Lean.Touch.LeanFinger)
extern void LeanTwistRotateAxis_RemoveFinger_mFB98312754B17DFAF2092D95B2612197AB3654B6 (void);
// 0x000001F7 System.Void Lean.Touch.LeanTwistRotateAxis::RemoveAllFingers()
extern void LeanTwistRotateAxis_RemoveAllFingers_mBC2855CD0887C7C9E570B9D52C60E1A17925C276 (void);
// 0x000001F8 System.Void Lean.Touch.LeanTwistRotateAxis::Awake()
extern void LeanTwistRotateAxis_Awake_mEFD9F22D6DD1BEF03B942F82AA0CFE23B10285CE (void);
// 0x000001F9 System.Void Lean.Touch.LeanTwistRotateAxis::Update()
extern void LeanTwistRotateAxis_Update_m2931C5471CD6FC71E1F47139E3D6B164F1A7729B (void);
// 0x000001FA System.Void Lean.Touch.LeanTwistRotateAxis::.ctor()
extern void LeanTwistRotateAxis__ctor_m1C288EA0D475042810E60AC20F4C3C3BDBFE0831 (void);
// 0x000001FB System.Boolean Lean.Touch.LeanFinger::get_IsActive()
extern void LeanFinger_get_IsActive_m5EB3CF326192F0AA8895CD15B831F7DA828790F8 (void);
// 0x000001FC System.Single Lean.Touch.LeanFinger::get_SnapshotDuration()
extern void LeanFinger_get_SnapshotDuration_mA4EFDA357087D6BBBA47F7BB521E9B261ED8AB24 (void);
// 0x000001FD System.Boolean Lean.Touch.LeanFinger::get_IsOverGui()
extern void LeanFinger_get_IsOverGui_m3372F67DEC22FCCB3C908604C734D7D9E7B0CAFE (void);
// 0x000001FE System.Boolean Lean.Touch.LeanFinger::get_Down()
extern void LeanFinger_get_Down_m45FA5F31995CB4F344C7DD5271C1EE7387BF559D (void);
// 0x000001FF System.Boolean Lean.Touch.LeanFinger::get_Up()
extern void LeanFinger_get_Up_m4F56B9B87250871F9002AA569382DBA60E1B7186 (void);
// 0x00000200 UnityEngine.Vector2 Lean.Touch.LeanFinger::get_LastSnapshotScreenDelta()
extern void LeanFinger_get_LastSnapshotScreenDelta_mC93F47A72A467AB3049D90FDE0466CF821233F2E (void);
// 0x00000201 UnityEngine.Vector2 Lean.Touch.LeanFinger::get_LastSnapshotScaledDelta()
extern void LeanFinger_get_LastSnapshotScaledDelta_mC8D9A621CFE90E1A051B751CBCA0091816048940 (void);
// 0x00000202 UnityEngine.Vector2 Lean.Touch.LeanFinger::get_ScreenDelta()
extern void LeanFinger_get_ScreenDelta_mBD113E64DFA2132091B2591669BF8DA6478B9931 (void);
// 0x00000203 UnityEngine.Vector2 Lean.Touch.LeanFinger::get_ScaledDelta()
extern void LeanFinger_get_ScaledDelta_m2782A2B8D42CB104C94AD0E68A9892C1212CB912 (void);
// 0x00000204 UnityEngine.Vector2 Lean.Touch.LeanFinger::get_SwipeScreenDelta()
extern void LeanFinger_get_SwipeScreenDelta_m5E3ACE2A2701D86AEECE19DBEEC184A17648C1EF (void);
// 0x00000205 UnityEngine.Vector2 Lean.Touch.LeanFinger::get_SwipeScaledDelta()
extern void LeanFinger_get_SwipeScaledDelta_mFDDB667770B03555AC7A34D7F7A0C9FAF93A46DB (void);
// 0x00000206 UnityEngine.Vector2 Lean.Touch.LeanFinger::GetSmoothScreenPosition(System.Single)
extern void LeanFinger_GetSmoothScreenPosition_m0D6B9634EAD0A670197E5122B6AB88B7FE512A46 (void);
// 0x00000207 System.Single Lean.Touch.LeanFinger::get_SmoothScreenPositionDelta()
extern void LeanFinger_get_SmoothScreenPositionDelta_m7C2A661BE9279D115B8EC98DD9D72C548CE2337F (void);
// 0x00000208 UnityEngine.Ray Lean.Touch.LeanFinger::GetRay(UnityEngine.Camera)
extern void LeanFinger_GetRay_m9B5E75941CF31049CBBAF401B82696C488AE7B97 (void);
// 0x00000209 UnityEngine.Ray Lean.Touch.LeanFinger::GetStartRay(UnityEngine.Camera)
extern void LeanFinger_GetStartRay_mF5E172C33B4C0AC62E606721F7DA12C515E1661C (void);
// 0x0000020A UnityEngine.Vector2 Lean.Touch.LeanFinger::GetSnapshotScreenDelta(System.Single)
extern void LeanFinger_GetSnapshotScreenDelta_m26D595C875BEBD033004E89322860C6C13A9D14C (void);
// 0x0000020B UnityEngine.Vector2 Lean.Touch.LeanFinger::GetSnapshotScaledDelta(System.Single)
extern void LeanFinger_GetSnapshotScaledDelta_mC902C04E3C5CAB3F932E6B9D50160E5F9F815D81 (void);
// 0x0000020C UnityEngine.Vector2 Lean.Touch.LeanFinger::GetSnapshotScreenPosition(System.Single)
extern void LeanFinger_GetSnapshotScreenPosition_m302F4F09D5D0D1D20AEDB70852E1526FD9B4BA82 (void);
// 0x0000020D UnityEngine.Vector3 Lean.Touch.LeanFinger::GetSnapshotWorldPosition(System.Single,System.Single,UnityEngine.Camera)
extern void LeanFinger_GetSnapshotWorldPosition_m6C1FAD0CF8085CDC0247605FD0883D2E9A76D9F9 (void);
// 0x0000020E System.Single Lean.Touch.LeanFinger::GetRadians(UnityEngine.Vector2)
extern void LeanFinger_GetRadians_m4BAFEEF64D666F8AF00DDB7871285382E7E01884 (void);
// 0x0000020F System.Single Lean.Touch.LeanFinger::GetDegrees(UnityEngine.Vector2)
extern void LeanFinger_GetDegrees_m6FA56D8AB0F982315AE58A0E8CA34446625809F1 (void);
// 0x00000210 System.Single Lean.Touch.LeanFinger::GetLastRadians(UnityEngine.Vector2)
extern void LeanFinger_GetLastRadians_m0A92E21C3FD95C1640165755BA473E972DD56C6A (void);
// 0x00000211 System.Single Lean.Touch.LeanFinger::GetLastDegrees(UnityEngine.Vector2)
extern void LeanFinger_GetLastDegrees_m1A702042CCF603A4B07D89DADDD4D779E84DD68E (void);
// 0x00000212 System.Single Lean.Touch.LeanFinger::GetDeltaRadians(UnityEngine.Vector2)
extern void LeanFinger_GetDeltaRadians_mCE5E06780D363DF4F03DBBFA6036593A493D166A (void);
// 0x00000213 System.Single Lean.Touch.LeanFinger::GetDeltaRadians(UnityEngine.Vector2,UnityEngine.Vector2)
extern void LeanFinger_GetDeltaRadians_m80BE16ECA752BF81608B5236751621891A3FE956 (void);
// 0x00000214 System.Single Lean.Touch.LeanFinger::GetDeltaDegrees(UnityEngine.Vector2)
extern void LeanFinger_GetDeltaDegrees_m243F39475A3B3C86FF21075A49B3F831DA895CA7 (void);
// 0x00000215 System.Single Lean.Touch.LeanFinger::GetDeltaDegrees(UnityEngine.Vector2,UnityEngine.Vector2)
extern void LeanFinger_GetDeltaDegrees_mB59044477F1716F67A902CE84AB47C2BE1577D3D (void);
// 0x00000216 System.Single Lean.Touch.LeanFinger::GetScreenDistance(UnityEngine.Vector2)
extern void LeanFinger_GetScreenDistance_m45F7F0A5B2813B3A47614B442613144D5CEF12DA (void);
// 0x00000217 System.Single Lean.Touch.LeanFinger::GetScaledDistance(UnityEngine.Vector2)
extern void LeanFinger_GetScaledDistance_m7B930E0AF81E7277B7314C31AD28D2725F5856D1 (void);
// 0x00000218 System.Single Lean.Touch.LeanFinger::GetLastScreenDistance(UnityEngine.Vector2)
extern void LeanFinger_GetLastScreenDistance_mA37A8D2F7CEE0FB3D831058A8D4B218F0E24DA61 (void);
// 0x00000219 System.Single Lean.Touch.LeanFinger::GetLastScaledDistance(UnityEngine.Vector2)
extern void LeanFinger_GetLastScaledDistance_m7BEA3523794103912F21A39A1FF24EE68274E50D (void);
// 0x0000021A System.Single Lean.Touch.LeanFinger::GetStartScreenDistance(UnityEngine.Vector2)
extern void LeanFinger_GetStartScreenDistance_m987B0EBF0FE3F2B54BA07836B9D178F34036B498 (void);
// 0x0000021B System.Single Lean.Touch.LeanFinger::GetStartScaledDistance(UnityEngine.Vector2)
extern void LeanFinger_GetStartScaledDistance_m6762F2E2E471FBC79635227467155427A681644E (void);
// 0x0000021C UnityEngine.Vector3 Lean.Touch.LeanFinger::GetStartWorldPosition(System.Single,UnityEngine.Camera)
extern void LeanFinger_GetStartWorldPosition_m74F17B992848A4B44CF2A34B5789BA2F5D840293 (void);
// 0x0000021D UnityEngine.Vector3 Lean.Touch.LeanFinger::GetLastWorldPosition(System.Single,UnityEngine.Camera)
extern void LeanFinger_GetLastWorldPosition_m3B21E536AE5C040EBAB40A3FC06B60277C12AF94 (void);
// 0x0000021E UnityEngine.Vector3 Lean.Touch.LeanFinger::GetWorldPosition(System.Single,UnityEngine.Camera)
extern void LeanFinger_GetWorldPosition_mEB1802EA120D6575B841E9DD1C7786058F164142 (void);
// 0x0000021F UnityEngine.Vector3 Lean.Touch.LeanFinger::GetWorldDelta(System.Single,UnityEngine.Camera)
extern void LeanFinger_GetWorldDelta_mFF2EC7D038C9FFF1B00730CE7E224D0A62A1D396 (void);
// 0x00000220 UnityEngine.Vector3 Lean.Touch.LeanFinger::GetWorldDelta(System.Single,System.Single,UnityEngine.Camera)
extern void LeanFinger_GetWorldDelta_mA8D62CF16DE26590CA68D0DF8121374B0B3834B6 (void);
// 0x00000221 System.Void Lean.Touch.LeanFinger::ClearSnapshots(System.Int32)
extern void LeanFinger_ClearSnapshots_m8FC8E9DF887955A35B967A6285FE090BF1A45D1F (void);
// 0x00000222 System.Void Lean.Touch.LeanFinger::RecordSnapshot()
extern void LeanFinger_RecordSnapshot_m076E2EA776632A022A0A6C9CB1D07A81EDB78065 (void);
// 0x00000223 System.Void Lean.Touch.LeanFinger::.ctor()
extern void LeanFinger__ctor_m0AE5B46092084E7254B5EA83F59ED7660446A72D (void);
// 0x00000224 UnityEngine.Vector2 Lean.Touch.LeanGesture::GetScreenCenter()
extern void LeanGesture_GetScreenCenter_m95E4A5CFD4294984403D5A684E368DE450C7F6F6 (void);
// 0x00000225 UnityEngine.Vector2 Lean.Touch.LeanGesture::GetScreenCenter(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetScreenCenter_m517308B8AC4FDA32CAD68C21E886A297DF6842DB (void);
// 0x00000226 System.Boolean Lean.Touch.LeanGesture::TryGetScreenCenter(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2&)
extern void LeanGesture_TryGetScreenCenter_m8C9AB8BC6C12E7A2633EF4868A3BB586811B5AC2 (void);
// 0x00000227 UnityEngine.Vector2 Lean.Touch.LeanGesture::GetLastScreenCenter()
extern void LeanGesture_GetLastScreenCenter_mD79CEECF6E271274227C7A3153FC808F26D78CCA (void);
// 0x00000228 UnityEngine.Vector2 Lean.Touch.LeanGesture::GetLastScreenCenter(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetLastScreenCenter_mCE822AE69762752F4EA42893A10CDCA37564ACC8 (void);
// 0x00000229 System.Boolean Lean.Touch.LeanGesture::TryGetLastScreenCenter(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2&)
extern void LeanGesture_TryGetLastScreenCenter_mBDF2A0A5A23FAF327F4E127D2A14BE4BCB942238 (void);
// 0x0000022A UnityEngine.Vector2 Lean.Touch.LeanGesture::GetStartScreenCenter()
extern void LeanGesture_GetStartScreenCenter_m6963DECCD253804D478F31B25D3E9E53A812E58D (void);
// 0x0000022B UnityEngine.Vector2 Lean.Touch.LeanGesture::GetStartScreenCenter(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetStartScreenCenter_m38F5994614222FD5D4C4E25F5E2B0DFBBA23E95A (void);
// 0x0000022C System.Boolean Lean.Touch.LeanGesture::TryGetStartScreenCenter(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2&)
extern void LeanGesture_TryGetStartScreenCenter_m0BC29D7F6382B6E12ECFCD7359DED0679931ECA3 (void);
// 0x0000022D UnityEngine.Vector2 Lean.Touch.LeanGesture::GetScreenDelta()
extern void LeanGesture_GetScreenDelta_m2586E19CDB082EC1869286BE346D2A6B8D1D2540 (void);
// 0x0000022E UnityEngine.Vector2 Lean.Touch.LeanGesture::GetScreenDelta(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetScreenDelta_m271791F0317692A6C46E929FD2674ABCC94366D0 (void);
// 0x0000022F System.Boolean Lean.Touch.LeanGesture::TryGetScreenDelta(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2&)
extern void LeanGesture_TryGetScreenDelta_mC31B1C0FCFB1741F2C9B7BD69F71EA03CCBD42CB (void);
// 0x00000230 UnityEngine.Vector2 Lean.Touch.LeanGesture::GetScaledDelta()
extern void LeanGesture_GetScaledDelta_m531E6CCA0539EB003CA7559AE7055927F039D229 (void);
// 0x00000231 UnityEngine.Vector2 Lean.Touch.LeanGesture::GetScaledDelta(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetScaledDelta_mD6F7ACD36100B8BC79C94B740A20058207998756 (void);
// 0x00000232 System.Boolean Lean.Touch.LeanGesture::TryGetScaledDelta(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2&)
extern void LeanGesture_TryGetScaledDelta_mAB2E7F47841626D9C307DCE1DD57FC6CB0ED827E (void);
// 0x00000233 UnityEngine.Vector3 Lean.Touch.LeanGesture::GetWorldDelta(System.Single,UnityEngine.Camera)
extern void LeanGesture_GetWorldDelta_m12CF8BB9CD5A3A32B5B40B2DE7A295147B198EE0 (void);
// 0x00000234 UnityEngine.Vector3 Lean.Touch.LeanGesture::GetWorldDelta(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,System.Single,UnityEngine.Camera)
extern void LeanGesture_GetWorldDelta_m1BECA7E4778CBE77A2BAF2A285F0C22C91CF1819 (void);
// 0x00000235 System.Boolean Lean.Touch.LeanGesture::TryGetWorldDelta(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,System.Single,UnityEngine.Vector3&,UnityEngine.Camera)
extern void LeanGesture_TryGetWorldDelta_m92117853249392410C8F61D12681DBDA3F8D81A2 (void);
// 0x00000236 System.Single Lean.Touch.LeanGesture::GetScreenDistance()
extern void LeanGesture_GetScreenDistance_m041A4E76BAD112851E689CE92FAD9BB7729A659D (void);
// 0x00000237 System.Single Lean.Touch.LeanGesture::GetScreenDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetScreenDistance_m5EDE5221BC7FC0B14F9D1B666A0361A0225E8AC3 (void);
// 0x00000238 System.Single Lean.Touch.LeanGesture::GetScreenDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2)
extern void LeanGesture_GetScreenDistance_mC3D9786C488664524C56C58EF8D4A14AEA0C0AF3 (void);
// 0x00000239 System.Boolean Lean.Touch.LeanGesture::TryGetScreenDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2,System.Single&)
extern void LeanGesture_TryGetScreenDistance_mA6ACAEBFCB5591CB55FC240E9D87672B37016282 (void);
// 0x0000023A System.Single Lean.Touch.LeanGesture::GetScaledDistance()
extern void LeanGesture_GetScaledDistance_mB114972D4083CE74D2B93606D49880220EEBD44E (void);
// 0x0000023B System.Single Lean.Touch.LeanGesture::GetScaledDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetScaledDistance_mE32812460D0D8114344C561E904231EE39C3AC2B (void);
// 0x0000023C System.Single Lean.Touch.LeanGesture::GetScaledDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2)
extern void LeanGesture_GetScaledDistance_m713957BCF545EF6891B37DEE2EF4E3BB927A91D6 (void);
// 0x0000023D System.Boolean Lean.Touch.LeanGesture::TryGetScaledDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2,System.Single&)
extern void LeanGesture_TryGetScaledDistance_m9DAF40C53F2524592C3494D3A6CEA89017ED351A (void);
// 0x0000023E System.Single Lean.Touch.LeanGesture::GetLastScreenDistance()
extern void LeanGesture_GetLastScreenDistance_m581336A0617379A200FF2F4D896E7F2E4957E95A (void);
// 0x0000023F System.Single Lean.Touch.LeanGesture::GetLastScreenDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetLastScreenDistance_m7DD444BFDCAB4D07131C3B7E380B942CB3F66692 (void);
// 0x00000240 System.Single Lean.Touch.LeanGesture::GetLastScreenDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2)
extern void LeanGesture_GetLastScreenDistance_m6C2B7DF8B4BFB18CEDF4B136D20F7CDF98713387 (void);
// 0x00000241 System.Boolean Lean.Touch.LeanGesture::TryGetLastScreenDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2,System.Single&)
extern void LeanGesture_TryGetLastScreenDistance_mC42D372AA0368015BD2337BA374851E68F890079 (void);
// 0x00000242 System.Single Lean.Touch.LeanGesture::GetLastScaledDistance()
extern void LeanGesture_GetLastScaledDistance_mDFAAB510AFC86D6F8E606DA195372744DD451035 (void);
// 0x00000243 System.Single Lean.Touch.LeanGesture::GetLastScaledDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetLastScaledDistance_mD299A8A10850A0778FB96F0D122740284E58A372 (void);
// 0x00000244 System.Single Lean.Touch.LeanGesture::GetLastScaledDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2)
extern void LeanGesture_GetLastScaledDistance_mE1A6903DA4416208397A6B4491D186005B624ED4 (void);
// 0x00000245 System.Boolean Lean.Touch.LeanGesture::TryGetLastScaledDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2,System.Single&)
extern void LeanGesture_TryGetLastScaledDistance_m30080A54C2B2BF4367F861009CF3FE55F1898D3B (void);
// 0x00000246 System.Single Lean.Touch.LeanGesture::GetStartScreenDistance()
extern void LeanGesture_GetStartScreenDistance_m78E9B4545CBA13FE354A3EE4891C5B1F60CDB41E (void);
// 0x00000247 System.Single Lean.Touch.LeanGesture::GetStartScreenDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetStartScreenDistance_m9769BF0434980A1A9425A9D7C75A6B404A2E5EBC (void);
// 0x00000248 System.Single Lean.Touch.LeanGesture::GetStartScreenDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2)
extern void LeanGesture_GetStartScreenDistance_mCE3A06586A7B2696459964A9E59BE15D7D4742D4 (void);
// 0x00000249 System.Boolean Lean.Touch.LeanGesture::TryGetStartScreenDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2,System.Single&)
extern void LeanGesture_TryGetStartScreenDistance_mD8C36D11BA50FCA63433B2425D21D1F34BEBB1C8 (void);
// 0x0000024A System.Single Lean.Touch.LeanGesture::GetStartScaledDistance()
extern void LeanGesture_GetStartScaledDistance_m15B3C4A5944E1629294436AE40BB3AB4D98AFA5B (void);
// 0x0000024B System.Single Lean.Touch.LeanGesture::GetStartScaledDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetStartScaledDistance_m1F86FB03F36DCAA89CF651B5215A340C0289B50E (void);
// 0x0000024C System.Single Lean.Touch.LeanGesture::GetStartScaledDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2)
extern void LeanGesture_GetStartScaledDistance_mEC2C7084DF1BB078A3DE0CA24AD204BCBBCA08F3 (void);
// 0x0000024D System.Boolean Lean.Touch.LeanGesture::TryGetStartScaledDistance(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2,System.Single&)
extern void LeanGesture_TryGetStartScaledDistance_m87C7EB5FEE9B1C7A655B782B6DE38F90EF5314A0 (void);
// 0x0000024E System.Single Lean.Touch.LeanGesture::GetPinchScale(System.Single)
extern void LeanGesture_GetPinchScale_mBBA14D943A1900A80B5E6700892FB722146495EE (void);
// 0x0000024F System.Single Lean.Touch.LeanGesture::GetPinchScale(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,System.Single)
extern void LeanGesture_GetPinchScale_m6E8E7D883EBC69480C0C94E27D09EB5DA60E8E93 (void);
// 0x00000250 System.Boolean Lean.Touch.LeanGesture::TryGetPinchScale(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2,UnityEngine.Vector2,System.Single&,System.Single)
extern void LeanGesture_TryGetPinchScale_m4EF715ECD02922701D6E68F166B0B926558452E7 (void);
// 0x00000251 System.Single Lean.Touch.LeanGesture::GetPinchRatio(System.Single)
extern void LeanGesture_GetPinchRatio_mB86218C87ECF9479C408995DE4C7F6536A831497 (void);
// 0x00000252 System.Single Lean.Touch.LeanGesture::GetPinchRatio(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,System.Single)
extern void LeanGesture_GetPinchRatio_m3F13637706904928538A5CAD55137CB641E0C1E9 (void);
// 0x00000253 System.Boolean Lean.Touch.LeanGesture::TryGetPinchRatio(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2,UnityEngine.Vector2,System.Single&,System.Single)
extern void LeanGesture_TryGetPinchRatio_m333C57DAFC4704857F1267E3A990E00B43BBEE28 (void);
// 0x00000254 System.Single Lean.Touch.LeanGesture::GetTwistDegrees()
extern void LeanGesture_GetTwistDegrees_mBD3C57754D69F4F126FF164233677451FA0CDCFD (void);
// 0x00000255 System.Single Lean.Touch.LeanGesture::GetTwistDegrees(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetTwistDegrees_mF044F7F3EBCF55911FBB7C293CA49FDD79F8F8A0 (void);
// 0x00000256 System.Single Lean.Touch.LeanGesture::GetTwistDegrees(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2,UnityEngine.Vector2)
extern void LeanGesture_GetTwistDegrees_m42803C38CB7BD8962C2D2935EDDF843CF51E7449 (void);
// 0x00000257 System.Boolean Lean.Touch.LeanGesture::TryGetTwistDegrees(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2,UnityEngine.Vector2,System.Single&)
extern void LeanGesture_TryGetTwistDegrees_mE8D3A01B072D7E0D9882B99385ECA0DE99967E86 (void);
// 0x00000258 System.Single Lean.Touch.LeanGesture::GetTwistRadians()
extern void LeanGesture_GetTwistRadians_m9A006C388E2425A8DE7E66BED5DCFB31AF67BEC4 (void);
// 0x00000259 System.Single Lean.Touch.LeanGesture::GetTwistRadians(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern void LeanGesture_GetTwistRadians_m9E288C09F5AD708504C9CFF6834E2598FA5FF46B (void);
// 0x0000025A System.Single Lean.Touch.LeanGesture::GetTwistRadians(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2,UnityEngine.Vector2)
extern void LeanGesture_GetTwistRadians_m39D3B12CAC1AFE09FD5B093E13F6A79C45D379A8 (void);
// 0x0000025B System.Boolean Lean.Touch.LeanGesture::TryGetTwistRadians(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>,UnityEngine.Vector2,UnityEngine.Vector2,System.Single&)
extern void LeanGesture_TryGetTwistRadians_m89F8D8A4373895F38903F1E80B6C8E7E359292EB (void);
// 0x0000025C UnityEngine.Vector3 Lean.Touch.LeanSnapshot::GetWorldPosition(System.Single,UnityEngine.Camera)
extern void LeanSnapshot_GetWorldPosition_m60417EFAA6CEFD1C7C8FFE0ED11107B2A0406707 (void);
// 0x0000025D Lean.Touch.LeanSnapshot Lean.Touch.LeanSnapshot::Pop()
extern void LeanSnapshot_Pop_m64151FFB07B94AF29CCB8E223EA4E930E5706098 (void);
// 0x0000025E System.Boolean Lean.Touch.LeanSnapshot::TryGetScreenPosition(System.Collections.Generic.List`1<Lean.Touch.LeanSnapshot>,System.Single,UnityEngine.Vector2&)
extern void LeanSnapshot_TryGetScreenPosition_m5D7A02E4CFB5EB9DC44E103B86CBCE1A49BD8114 (void);
// 0x0000025F System.Boolean Lean.Touch.LeanSnapshot::TryGetSnapshot(System.Collections.Generic.List`1<Lean.Touch.LeanSnapshot>,System.Int32,System.Single&,UnityEngine.Vector2&)
extern void LeanSnapshot_TryGetSnapshot_m5A11CF6A5B7C5BB3023EE97F1C9E3A5BADAF667A (void);
// 0x00000260 System.Int32 Lean.Touch.LeanSnapshot::GetLowerIndex(System.Collections.Generic.List`1<Lean.Touch.LeanSnapshot>,System.Single)
extern void LeanSnapshot_GetLowerIndex_m5537A71CFD9BF7ABF4AEB96215DF0C27740EDCA6 (void);
// 0x00000261 System.Void Lean.Touch.LeanSnapshot::.ctor()
extern void LeanSnapshot__ctor_m4BDD5DA589D0EAD0EB1F2C60C5CFC23206F32BFD (void);
// 0x00000262 System.Void Lean.Touch.LeanSnapshot::.cctor()
extern void LeanSnapshot__cctor_m8D1F8A5AB81CFF484861C1B988B43A5F7DEC459D (void);
// 0x00000263 System.Void Lean.Touch.LeanTouch::add_OnFingerDown(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_add_OnFingerDown_m17FAE9CD13F1E43586D22F8195DCB4849D40EF9D (void);
// 0x00000264 System.Void Lean.Touch.LeanTouch::remove_OnFingerDown(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_remove_OnFingerDown_m7D8D8908F245868EB3BA75FF2FE81363BD853BC5 (void);
// 0x00000265 System.Void Lean.Touch.LeanTouch::add_OnFingerUpdate(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_add_OnFingerUpdate_mDD30E72D4128AC7F7DEE314AFDD1F63B3BC54255 (void);
// 0x00000266 System.Void Lean.Touch.LeanTouch::remove_OnFingerUpdate(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_remove_OnFingerUpdate_m58B05A304E7E3A468A3967B4B826383F3FB57CF2 (void);
// 0x00000267 System.Void Lean.Touch.LeanTouch::add_OnFingerUp(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_add_OnFingerUp_m63B6C30AB7B97D4009CF0611396C233A9B3C92BF (void);
// 0x00000268 System.Void Lean.Touch.LeanTouch::remove_OnFingerUp(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_remove_OnFingerUp_mE1B669D2A145F54FC5147DE6374782507D803777 (void);
// 0x00000269 System.Void Lean.Touch.LeanTouch::add_OnFingerOld(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_add_OnFingerOld_mA1F82698C5926977AA0D01602C89FD70307EBD94 (void);
// 0x0000026A System.Void Lean.Touch.LeanTouch::remove_OnFingerOld(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_remove_OnFingerOld_mA0BF26E8F3EB1309005BFA908CB823F20FAEA8A6 (void);
// 0x0000026B System.Void Lean.Touch.LeanTouch::add_OnFingerTap(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_add_OnFingerTap_m5BA04B655B78782D12319DD143F4E01997E6594E (void);
// 0x0000026C System.Void Lean.Touch.LeanTouch::remove_OnFingerTap(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_remove_OnFingerTap_m17033D5DBDA4CD4AF877E1D1C9C6EB1D9236E155 (void);
// 0x0000026D System.Void Lean.Touch.LeanTouch::add_OnFingerSwipe(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_add_OnFingerSwipe_mDEE2185B28C5117DCD3E7E9A0CEC5B18BA206BFA (void);
// 0x0000026E System.Void Lean.Touch.LeanTouch::remove_OnFingerSwipe(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_remove_OnFingerSwipe_m36D61DE4F64B42907985C7E154FB3D9D7E1BE1DD (void);
// 0x0000026F System.Void Lean.Touch.LeanTouch::add_OnGesture(System.Action`1<System.Collections.Generic.List`1<Lean.Touch.LeanFinger>>)
extern void LeanTouch_add_OnGesture_mCAFCA85BE9F5DCFC821AB3A702B592437BBF0589 (void);
// 0x00000270 System.Void Lean.Touch.LeanTouch::remove_OnGesture(System.Action`1<System.Collections.Generic.List`1<Lean.Touch.LeanFinger>>)
extern void LeanTouch_remove_OnGesture_mE803955E7B5635B8941DBBF930462EDCD1E0EBF2 (void);
// 0x00000271 System.Void Lean.Touch.LeanTouch::add_OnFingerExpired(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_add_OnFingerExpired_mDC3B4CAF9DEB8427D0CCDF27BEF598D4930D777F (void);
// 0x00000272 System.Void Lean.Touch.LeanTouch::remove_OnFingerExpired(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_remove_OnFingerExpired_m7C90A349CCA6B72C366D3F61B61E7AB286D6F17E (void);
// 0x00000273 System.Void Lean.Touch.LeanTouch::add_OnFingerInactive(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_add_OnFingerInactive_m226A4E6851247A04C851E444A511372D056F4836 (void);
// 0x00000274 System.Void Lean.Touch.LeanTouch::remove_OnFingerInactive(System.Action`1<Lean.Touch.LeanFinger>)
extern void LeanTouch_remove_OnFingerInactive_mACEDB928D81225C7EEAE253E288E35111051BB84 (void);
// 0x00000275 System.Single Lean.Touch.LeanTouch::get_CurrentTapThreshold()
extern void LeanTouch_get_CurrentTapThreshold_mF85E0469F5CBB0264CB9676ADD02A58CDC705165 (void);
// 0x00000276 System.Single Lean.Touch.LeanTouch::get_CurrentSwipeThreshold()
extern void LeanTouch_get_CurrentSwipeThreshold_mA02ECE3889A6A3432820777DB65AE05E735A19CF (void);
// 0x00000277 System.Int32 Lean.Touch.LeanTouch::get_CurrentReferenceDpi()
extern void LeanTouch_get_CurrentReferenceDpi_mE4C155E648A42D3A028EF95E9B14B55A5A0A5879 (void);
// 0x00000278 UnityEngine.LayerMask Lean.Touch.LeanTouch::get_CurrentGuiLayers()
extern void LeanTouch_get_CurrentGuiLayers_m44F0FB49CD0DF70ACA3566A1E3BF15DEF7451E6D (void);
// 0x00000279 Lean.Touch.LeanTouch Lean.Touch.LeanTouch::get_Instance()
extern void LeanTouch_get_Instance_mAEA48C7EC5A9F5D0EDE2EDECE57FE4BFC3D63B53 (void);
// 0x0000027A System.Single Lean.Touch.LeanTouch::get_ScalingFactor()
extern void LeanTouch_get_ScalingFactor_m5C19536EADF8F9156EF7D0F75E8FBB6FE439BA1B (void);
// 0x0000027B System.Single Lean.Touch.LeanTouch::get_ScreenFactor()
extern void LeanTouch_get_ScreenFactor_m1656DF31F790F1629F67010DE3FD3B523C05C127 (void);
// 0x0000027C System.Boolean Lean.Touch.LeanTouch::get_GuiInUse()
extern void LeanTouch_get_GuiInUse_m1E6DEF25C330F52E6C908112EB14ABB62E6F9578 (void);
// 0x0000027D System.Boolean Lean.Touch.LeanTouch::PointOverGui(UnityEngine.Vector2)
extern void LeanTouch_PointOverGui_m992D2317AE2CC30D8ADEAB3EDF7E8FA35E5CE22A (void);
// 0x0000027E System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult> Lean.Touch.LeanTouch::RaycastGui(UnityEngine.Vector2)
extern void LeanTouch_RaycastGui_m46D88B1478558D4848EA3F0DBC6EAA53668264AE (void);
// 0x0000027F System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult> Lean.Touch.LeanTouch::RaycastGui(UnityEngine.Vector2,UnityEngine.LayerMask)
extern void LeanTouch_RaycastGui_m834161B727454502A991C2BDF82A2D8569A6767C (void);
// 0x00000280 System.Collections.Generic.List`1<Lean.Touch.LeanFinger> Lean.Touch.LeanTouch::GetFingers(System.Boolean,System.Boolean,System.Int32)
extern void LeanTouch_GetFingers_m3E51F69DBAAC079C9BCCB56C606ABF7EC92043AF (void);
// 0x00000281 System.Void Lean.Touch.LeanTouch::SimulateTap(UnityEngine.Vector2,System.Single,System.Int32)
extern void LeanTouch_SimulateTap_mDC0A6EF2E1C8DB33CC9219F26EB097A4FF193249 (void);
// 0x00000282 System.Void Lean.Touch.LeanTouch::Clear()
extern void LeanTouch_Clear_m480D07C5023A3E59BFBE60DBAC551C2D58A6465F (void);
// 0x00000283 System.Void Lean.Touch.LeanTouch::OnEnable()
extern void LeanTouch_OnEnable_mA8F035E5096033DBECE8236BF231678D5782E47F (void);
// 0x00000284 System.Void Lean.Touch.LeanTouch::OnDisable()
extern void LeanTouch_OnDisable_mAE2E123278374A47A730EA0B33279177E1D84227 (void);
// 0x00000285 System.Void Lean.Touch.LeanTouch::Update()
extern void LeanTouch_Update_m29FA2C0DE55C014E59DCA36391901AB2D5E62163 (void);
// 0x00000286 System.Void Lean.Touch.LeanTouch::UpdateFingers(System.Single,System.Boolean)
extern void LeanTouch_UpdateFingers_m6DCB1BF789CB79A66D35A652CD7A14A90F056F3F (void);
// 0x00000287 System.Void Lean.Touch.LeanTouch::OnGUI()
extern void LeanTouch_OnGUI_m7596947DFED03B9DA2B089D2AD36E5F268B217E1 (void);
// 0x00000288 System.Void Lean.Touch.LeanTouch::BeginFingers(System.Single)
extern void LeanTouch_BeginFingers_m61C0B32B75F276F075B0BD837CDA9CA1791A7F05 (void);
// 0x00000289 System.Void Lean.Touch.LeanTouch::EndFingers(System.Single)
extern void LeanTouch_EndFingers_m6D50FF715FC09BED4628F2833E658FC8943C5D60 (void);
// 0x0000028A System.Void Lean.Touch.LeanTouch::PollFingers()
extern void LeanTouch_PollFingers_m122A92510537CEBFC795399532CA337854880F70 (void);
// 0x0000028B System.Void Lean.Touch.LeanTouch::UpdateEvents()
extern void LeanTouch_UpdateEvents_mAC600A293B5E74B4C26D64C4E6E17CACB49E7C2D (void);
// 0x0000028C Lean.Touch.LeanFinger Lean.Touch.LeanTouch::AddFinger(System.Int32,UnityEngine.Vector2,System.Single,System.Boolean)
extern void LeanTouch_AddFinger_mA4CCB5070C89882868523A635D6B589976F17FFC (void);
// 0x0000028D Lean.Touch.LeanFinger Lean.Touch.LeanTouch::FindFinger(System.Int32)
extern void LeanTouch_FindFinger_mE38C9C8A9AB76767A1B889CE506F7B1C1EEBBE26 (void);
// 0x0000028E System.Int32 Lean.Touch.LeanTouch::FindInactiveFingerIndex(System.Int32)
extern void LeanTouch_FindInactiveFingerIndex_m9679A3819CE196276D9073E213899C110AE26C4E (void);
// 0x0000028F System.Void Lean.Touch.LeanTouch::.ctor()
extern void LeanTouch__ctor_mB1EAE50B98F4AAD304A245A293C7276D590A79A7 (void);
// 0x00000290 System.Void Lean.Touch.LeanTouch::.cctor()
extern void LeanTouch__cctor_m0948815C556ADA180AFAE7A74BE3AAB829E5AD53 (void);
// 0x00000291 System.Void Lean.Common.LeanDestroy::Update()
extern void LeanDestroy_Update_m5A36AAEAA9F60FF6EA6DA6785E98DB80BD1361A2 (void);
// 0x00000292 System.Void Lean.Common.LeanDestroy::DestroyNow()
extern void LeanDestroy_DestroyNow_mB49A154BBD7F694CE45FE13501A88CC25947F037 (void);
// 0x00000293 System.Void Lean.Common.LeanDestroy::.ctor()
extern void LeanDestroy__ctor_m7F9ABFEF88110AD86E294B58474F7A43026C9884 (void);
// 0x00000294 System.Int32 Lean.Common.LeanPath::get_PointCount()
extern void LeanPath_get_PointCount_m295E15FDB24F3C912F65592B6252D5FA4C70EE01 (void);
// 0x00000295 System.Int32 Lean.Common.LeanPath::GetPointCount(System.Int32)
extern void LeanPath_GetPointCount_m852761EA7B6EBB224EE462F98D7CDB9FCE703D03 (void);
// 0x00000296 UnityEngine.Vector3 Lean.Common.LeanPath::GetSmoothedPoint(System.Single)
extern void LeanPath_GetSmoothedPoint_m3F4CD111912C3DCED5637C27A169C6CE06806B60 (void);
// 0x00000297 UnityEngine.Vector3 Lean.Common.LeanPath::GetPoint(System.Int32,System.Int32)
extern void LeanPath_GetPoint_mBD5235B3AABDBF0EFDEA526331BDEA7E721D8E2F (void);
// 0x00000298 UnityEngine.Vector3 Lean.Common.LeanPath::GetPointRaw(System.Int32,System.Int32)
extern void LeanPath_GetPointRaw_m8A40F8C9700D231F7A9DA78E2EAE91334BE27DCA (void);
// 0x00000299 System.Void Lean.Common.LeanPath::SetLine(UnityEngine.Vector3,UnityEngine.Vector3)
extern void LeanPath_SetLine_mA682EB28573A36B72AEB649EF5543E1261201EB2 (void);
// 0x0000029A System.Boolean Lean.Common.LeanPath::TryGetClosest(UnityEngine.Vector3,UnityEngine.Vector3&,System.Int32&,System.Int32&,System.Int32)
extern void LeanPath_TryGetClosest_mAA5A9DA17F25D4E3F607F84F9F7329CFC69BBF09 (void);
// 0x0000029B System.Boolean Lean.Common.LeanPath::TryGetClosest(UnityEngine.Vector3,UnityEngine.Vector3&,System.Int32)
extern void LeanPath_TryGetClosest_m7E47B73402206BD25A18AC83291C3FCC73EACED8 (void);
// 0x0000029C System.Boolean Lean.Common.LeanPath::TryGetClosest(UnityEngine.Ray,UnityEngine.Vector3&,System.Int32&,System.Int32&,System.Int32)
extern void LeanPath_TryGetClosest_m1E5BC7131252BC5B3ADC23A7ABD2DA8052156FD6 (void);
// 0x0000029D System.Boolean Lean.Common.LeanPath::TryGetClosest(UnityEngine.Ray,UnityEngine.Vector3&,System.Int32)
extern void LeanPath_TryGetClosest_mB3924F0BF36E35EDB8EB8C439E7C68D4A8A55A93 (void);
// 0x0000029E System.Boolean Lean.Common.LeanPath::TryGetClosest(UnityEngine.Ray,UnityEngine.Vector3&,System.Int32,System.Single)
extern void LeanPath_TryGetClosest_m5BEBB049EC449482C50CDBA473A296D416D8CD80 (void);
// 0x0000029F UnityEngine.Vector3 Lean.Common.LeanPath::GetClosestPoint(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void LeanPath_GetClosestPoint_m97ECB21743B20049558B40ACD00D4FB7EA005933 (void);
// 0x000002A0 UnityEngine.Vector3 Lean.Common.LeanPath::GetClosestPoint(UnityEngine.Ray,UnityEngine.Vector3,UnityEngine.Vector3)
extern void LeanPath_GetClosestPoint_mC39A2518B0E571E5C04334C691A4EDFDF5A872AE (void);
// 0x000002A1 System.Single Lean.Common.LeanPath::GetClosestDistance(UnityEngine.Ray,UnityEngine.Vector3)
extern void LeanPath_GetClosestDistance_m9DC0FD949C730887AD9269B106824A9A7009C79F (void);
// 0x000002A2 System.Int32 Lean.Common.LeanPath::Mod(System.Int32,System.Int32)
extern void LeanPath_Mod_m5FC65C1C0E1DB3411D777603FD31A6BFD47715FF (void);
// 0x000002A3 System.Single Lean.Common.LeanPath::CubicInterpolate(System.Single,System.Single,System.Single,System.Single,System.Single)
extern void LeanPath_CubicInterpolate_m51FB406ECA52DEE859C5D5A883586A05012253CA (void);
// 0x000002A4 System.Void Lean.Common.LeanPath::UpdateVisual()
extern void LeanPath_UpdateVisual_mF016D7C056774F6F81972E7C795E48A6E0070E98 (void);
// 0x000002A5 System.Void Lean.Common.LeanPath::Update()
extern void LeanPath_Update_m56B54EE220703300AF73C7E3C50AB1CF137783EC (void);
// 0x000002A6 System.Void Lean.Common.LeanPath::.ctor()
extern void LeanPath__ctor_m7FBF847F394ADBCF00773327655BF4FBA5FBB16D (void);
// 0x000002A7 System.Void Lean.Common.LeanPath::.cctor()
extern void LeanPath__cctor_m247F27420E99110F9A68CCAB8D9349FEB171EF06 (void);
// 0x000002A8 UnityEngine.Vector3 Lean.Common.LeanPlane::GetClosest(UnityEngine.Vector3,System.Single)
extern void LeanPlane_GetClosest_m3946B16CFFDD25A85EA7F63CCBB7AE96592C168D (void);
// 0x000002A9 System.Boolean Lean.Common.LeanPlane::TryRaycast(UnityEngine.Ray,UnityEngine.Vector3&,System.Single,System.Boolean)
extern void LeanPlane_TryRaycast_mA9F2BC215465A197DCB02CA386D6557D63CBF7D0 (void);
// 0x000002AA UnityEngine.Vector3 Lean.Common.LeanPlane::GetClosest(UnityEngine.Ray,System.Single)
extern void LeanPlane_GetClosest_m9738FE9106536F3481FFE84B5D1BA961A79E1188 (void);
// 0x000002AB System.Boolean Lean.Common.LeanPlane::RayToPlane(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Ray,System.Single&)
extern void LeanPlane_RayToPlane_m281B102EF4BABA2B484D1ECE9E40A3D78E84EB1B (void);
// 0x000002AC System.Void Lean.Common.LeanPlane::.ctor()
extern void LeanPlane__ctor_m0348CD522CCD3CED55AEC240108993F648ABD59E (void);
// 0x000002AD System.Void Lean.Common.LeanRoll::IncrementAngle(System.Single)
extern void LeanRoll_IncrementAngle_mE4996C593D7B5F5C549D35710ECD9040F149FB59 (void);
// 0x000002AE System.Void Lean.Common.LeanRoll::DecrementAngle(System.Single)
extern void LeanRoll_DecrementAngle_m666A2A2B35F1B24AEE1C58C67B1135C29829D461 (void);
// 0x000002AF System.Void Lean.Common.LeanRoll::RotateToDelta(UnityEngine.Vector2)
extern void LeanRoll_RotateToDelta_m7ABE9C10A41436BC5441F80538ED8CAB8EB43B00 (void);
// 0x000002B0 System.Void Lean.Common.LeanRoll::SnapToTarget()
extern void LeanRoll_SnapToTarget_m38645C71A54CE1A9346C3CB4FA7BBCEAB964CC29 (void);
// 0x000002B1 System.Void Lean.Common.LeanRoll::Start()
extern void LeanRoll_Start_mF26675531936F560695F444CC8F7210FDE6FEDEB (void);
// 0x000002B2 System.Void Lean.Common.LeanRoll::Update()
extern void LeanRoll_Update_mA1CAB75EDAD4D417BCECA41350920036E7350CA0 (void);
// 0x000002B3 System.Void Lean.Common.LeanRoll::.ctor()
extern void LeanRoll__ctor_mDCF984455BC2AA07BB1CA4542654139043080BA8 (void);
// 0x000002B4 System.Void Lean.Common.LeanSpawn::Spawn()
extern void LeanSpawn_Spawn_m88F6C987A1A5A6F6FDCC42C99EE54EEE48E7355C (void);
// 0x000002B5 System.Void Lean.Common.LeanSpawn::Spawn(UnityEngine.Vector3)
extern void LeanSpawn_Spawn_m3FBB61E53EF9FA3EFE8E1F1706FBCE2F8F3A49C2 (void);
// 0x000002B6 System.Void Lean.Common.LeanSpawn::.ctor()
extern void LeanSpawn__ctor_mEC414F4483787C2A13317C6265E5D15602AD6507 (void);
// 0x000002B7 T Lean.Common.LeanHelper::CreateElement(UnityEngine.Transform)
// 0x000002B8 System.Single Lean.Common.LeanHelper::GetDampenFactor(System.Single,System.Single)
extern void LeanHelper_GetDampenFactor_mB38E6C7D44DD03FCB6EA13FDF47D1759D70C79B0 (void);
// 0x000002B9 T Lean.Common.LeanHelper::Destroy(T)
// 0x000002BA UnityEngine.Camera Lean.Common.LeanHelper::GetCamera(UnityEngine.Camera,UnityEngine.GameObject)
extern void LeanHelper_GetCamera_mC6F8F819625E55F46066BC33BDB5B4E27B6624BC (void);
// 0x000002BB UnityEngine.Vector2 Lean.Common.LeanHelper::Hermite(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern void LeanHelper_Hermite_mC232448D90E4F66CAF8464858F96F9DE328F298F (void);
// 0x000002BC System.Single Lean.Common.LeanHelper::HermiteInterpolate(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void LeanHelper_HermiteInterpolate_mC10EC993296BA9C701997970C4096820844629AA (void);
// 0x000002BD System.Int32 Lean.Common.LeanInput::GetTouchCount()
extern void LeanInput_GetTouchCount_mF5B91FAD5A4A09E9EEDE8354C90B846A71546B3C (void);
// 0x000002BE System.Void Lean.Common.LeanInput::GetTouch(System.Int32,System.Int32&,UnityEngine.Vector2&,System.Single&,System.Boolean&)
extern void LeanInput_GetTouch_mEE4ADAD4FE780C27750315EBAC87ECEF25EBDB37 (void);
// 0x000002BF UnityEngine.Vector2 Lean.Common.LeanInput::GetMousePosition()
extern void LeanInput_GetMousePosition_m73BF6A8AB96C06A26E43EF9970C730477A23A7EA (void);
// 0x000002C0 System.Boolean Lean.Common.LeanInput::GetDown(UnityEngine.KeyCode)
extern void LeanInput_GetDown_mC3C5BBB80DB59C700B3CA731647D22B75519B857 (void);
// 0x000002C1 System.Boolean Lean.Common.LeanInput::GetPressed(UnityEngine.KeyCode)
extern void LeanInput_GetPressed_mBBD9D27684033A1C7E02C0A9C733793446691DE0 (void);
// 0x000002C2 System.Boolean Lean.Common.LeanInput::GetUp(UnityEngine.KeyCode)
extern void LeanInput_GetUp_m3EA98E00B634171954A034A44101A7A88BD312FB (void);
// 0x000002C3 System.Boolean Lean.Common.LeanInput::GetMouseDown(System.Int32)
extern void LeanInput_GetMouseDown_m07DCCD7120218CD03DD0499AFA56F5961F06808F (void);
// 0x000002C4 System.Boolean Lean.Common.LeanInput::GetMousePressed(System.Int32)
extern void LeanInput_GetMousePressed_m0A4158E416B7953A1900D8E2208A066061F0828E (void);
// 0x000002C5 System.Boolean Lean.Common.LeanInput::GetMouseUp(System.Int32)
extern void LeanInput_GetMouseUp_mA5275C36A357186AE051B783086137E1FD27B85B (void);
// 0x000002C6 System.Single Lean.Common.LeanInput::GetMouseWheelDelta()
extern void LeanInput_GetMouseWheelDelta_mED9FD054BDB1B70B7F5C0607A0D1E1AD354A240C (void);
// 0x000002C7 System.Boolean Lean.Common.LeanInput::GetMouseExists()
extern void LeanInput_GetMouseExists_m554C7E038B57994F726D17B15F9ADE20C87934B1 (void);
// 0x000002C8 System.Boolean Lean.Common.LeanInput::GetKeyboardExists()
extern void LeanInput_GetKeyboardExists_m099B1E603DAB8FA7BDEF32C225F3C13B8241F31A (void);
// 0x000002C9 UnityEngine.Texture2D Lean.Common.Examples.LeanGuide::get_Icon()
extern void LeanGuide_get_Icon_m4211CD2FB86EB611139E73B5F583F20DCB81F933 (void);
// 0x000002CA System.String Lean.Common.Examples.LeanGuide::get_Version()
extern void LeanGuide_get_Version_m24BB827A3234D301CD0DC575CFD6FC0F0397B2EB (void);
// 0x000002CB System.Void Lean.Common.Examples.LeanGuide::.ctor()
extern void LeanGuide__ctor_mD0DDFCBA8D1D3A635C5B61F902014A24A84D7CE6 (void);
// 0x000002CC System.Void Lean.Common.Examples.LeanLinkTo::set_Link(Lean.Common.Examples.LeanLinkTo/LinkType)
extern void LeanLinkTo_set_Link_m2D14C174B6D4937FA4E9C0DF31A7E30F5EF2F04A (void);
// 0x000002CD Lean.Common.Examples.LeanLinkTo/LinkType Lean.Common.Examples.LeanLinkTo::get_Link()
extern void LeanLinkTo_get_Link_mE54B29E4EB943CD720275D7BEDA7678A6085EF72 (void);
// 0x000002CE System.Void Lean.Common.Examples.LeanLinkTo::Update()
extern void LeanLinkTo_Update_m2FC888CBF8274A5A1BBE52A1E204F43EB5E2BE34 (void);
// 0x000002CF System.Void Lean.Common.Examples.LeanLinkTo::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void LeanLinkTo_OnPointerClick_m82FBBF604DAF9629AF74CC9A1F865FD6D1EE3A64 (void);
// 0x000002D0 System.Int32 Lean.Common.Examples.LeanLinkTo::GetCurrentLevel()
extern void LeanLinkTo_GetCurrentLevel_m9E6C482ACD4BAAD0C058EE2917E42E88235840AA (void);
// 0x000002D1 System.Int32 Lean.Common.Examples.LeanLinkTo::GetLevelCount()
extern void LeanLinkTo_GetLevelCount_m609DBD1FAABF53291D11BC03FF62CEA2A0B23685 (void);
// 0x000002D2 System.Void Lean.Common.Examples.LeanLinkTo::LoadLevel(System.Int32)
extern void LeanLinkTo_LoadLevel_m4B7CFCACB0C91837A89939AB792051F64969344D (void);
// 0x000002D3 System.Void Lean.Common.Examples.LeanLinkTo::.ctor()
extern void LeanLinkTo__ctor_mBB86CA187E2030D2156B69AB183C7BA4558B47DB (void);
// 0x000002D4 System.Void Lean.Common.Examples.LeanUpgradeEventSystem::.ctor()
extern void LeanUpgradeEventSystem__ctor_mFBA8F608DD6786DF5068AF0E837A0D29894983BC (void);
// 0x000002D5 System.Void LocalizationManager/<Start>d__55::.ctor(System.Int32)
extern void U3CStartU3Ed__55__ctor_m987504CCE2A04E56489F015DDF3279DC89C0F080 (void);
// 0x000002D6 System.Void LocalizationManager/<Start>d__55::System.IDisposable.Dispose()
extern void U3CStartU3Ed__55_System_IDisposable_Dispose_m9728BAF46DD084B007DFFE1D4F6F87B2DB1E1554 (void);
// 0x000002D7 System.Boolean LocalizationManager/<Start>d__55::MoveNext()
extern void U3CStartU3Ed__55_MoveNext_mAC6E79927F1429E573CE90D98D20C252559E1056 (void);
// 0x000002D8 System.Object LocalizationManager/<Start>d__55::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__55_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3EC035A1A71FEA9171B22B400ABDAB7FFF2FB3ED (void);
// 0x000002D9 System.Void LocalizationManager/<Start>d__55::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__55_System_Collections_IEnumerator_Reset_mE1713764C96D07B62E7DE7F5EA52D344AC64C15A (void);
// 0x000002DA System.Object LocalizationManager/<Start>d__55::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__55_System_Collections_IEnumerator_get_Current_m1F76D144731727E498C37AD026E5F6D941A40D57 (void);
// 0x000002DB System.Void UIManager/<>c::.cctor()
extern void U3CU3Ec__cctor_mBA5A8E7D4C803C8F80B71126C81B4760B987AD2A (void);
// 0x000002DC System.Void UIManager/<>c::.ctor()
extern void U3CU3Ec__ctor_m0A79C1A3BEA9231B3E275AEE65DA7EE93B137E15 (void);
// 0x000002DD System.Boolean UIManager/<>c::<GetGoal>b__73_0()
extern void U3CU3Ec_U3CGetGoalU3Eb__73_0_m2B8DFF8BDBE12F4F91E215807A71BA90C0403D11 (void);
// 0x000002DE System.Boolean UIManager/<>c::<GetGoal>b__73_1()
extern void U3CU3Ec_U3CGetGoalU3Eb__73_1_mB0E33D138F70F637F0DFE253D23D7C034D0300D0 (void);
// 0x000002DF System.Void SceneController/<EnableBatsman>d__6::.ctor(System.Int32)
extern void U3CEnableBatsmanU3Ed__6__ctor_mEEC4B8F973D62718060076D669597F838C324FD9 (void);
// 0x000002E0 System.Void SceneController/<EnableBatsman>d__6::System.IDisposable.Dispose()
extern void U3CEnableBatsmanU3Ed__6_System_IDisposable_Dispose_mB0483598E83315D30E92EAAD43D5AF10D57FBF54 (void);
// 0x000002E1 System.Boolean SceneController/<EnableBatsman>d__6::MoveNext()
extern void U3CEnableBatsmanU3Ed__6_MoveNext_m3014B24C7D3766282B566712F3E240E264939DC1 (void);
// 0x000002E2 System.Object SceneController/<EnableBatsman>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CEnableBatsmanU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3679DAB9B0B5B9B44605F19D1D8D3ECB2DA8AE26 (void);
// 0x000002E3 System.Void SceneController/<EnableBatsman>d__6::System.Collections.IEnumerator.Reset()
extern void U3CEnableBatsmanU3Ed__6_System_Collections_IEnumerator_Reset_m5A6F7084CEF295AA235E9F6D9241FFA8D7D394B5 (void);
// 0x000002E4 System.Object SceneController/<EnableBatsman>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CEnableBatsmanU3Ed__6_System_Collections_IEnumerator_get_Current_m3ACB8AAEC04E99894B01D1FDEA401A4A881093DD (void);
// 0x000002E5 System.Void Lean.Touch.LeanDragTrail/FingerData::.ctor()
extern void FingerData__ctor_mBD4FE27C75E0E29562797D1AB0492EB2AC358205 (void);
// 0x000002E6 System.Void Lean.Touch.LeanFingerDown/LeanFingerEvent::.ctor()
extern void LeanFingerEvent__ctor_m4CFB483CBD90B48A48C6A6D9728A8931735EF946 (void);
// 0x000002E7 System.Void Lean.Touch.LeanFingerDown/Vector3Event::.ctor()
extern void Vector3Event__ctor_m960B25A38B6931C4ACA3DFED800D0276810C1A88 (void);
// 0x000002E8 System.Void Lean.Touch.LeanFingerDown/Vector2Event::.ctor()
extern void Vector2Event__ctor_mAAD07EA5C6123E8AC5E2268E8CDB2B8C8E90BAB0 (void);
// 0x000002E9 System.Void Lean.Touch.LeanFingerOld/LeanFingerEvent::.ctor()
extern void LeanFingerEvent__ctor_m96403F70A4B50BCA5684358D655143220F82AC9C (void);
// 0x000002EA System.Void Lean.Touch.LeanFingerOld/Vector3Event::.ctor()
extern void Vector3Event__ctor_mBFBC35865569FC86BB7046BF3CB4164D1769B7B3 (void);
// 0x000002EB System.Void Lean.Touch.LeanFingerOld/Vector2Event::.ctor()
extern void Vector2Event__ctor_mB9D39DE9A6A88B04E1C0DDD4BC774BD88F53583B (void);
// 0x000002EC System.Void Lean.Touch.LeanFingerTap/LeanFingerEvent::.ctor()
extern void LeanFingerEvent__ctor_m2CAD3392293BE1E47A3910B4458A43971343CF82 (void);
// 0x000002ED System.Void Lean.Touch.LeanFingerTap/Vector3Event::.ctor()
extern void Vector3Event__ctor_m773F439BD8010721D145B4FF54EDDDC52289F776 (void);
// 0x000002EE System.Void Lean.Touch.LeanFingerTap/Vector2Event::.ctor()
extern void Vector2Event__ctor_mB35F5A5569DBDF1144AFB86DD63C167C2496455E (void);
// 0x000002EF System.Void Lean.Touch.LeanFingerTap/IntEvent::.ctor()
extern void IntEvent__ctor_mD33570F55569DEAB4F3B26D77AA355F1ED477AA8 (void);
// 0x000002F0 System.Void Lean.Touch.LeanFingerUp/LeanFingerEvent::.ctor()
extern void LeanFingerEvent__ctor_mFC34EB4DFB02276BA55C8614091190901DC1FF85 (void);
// 0x000002F1 System.Void Lean.Touch.LeanFingerUp/Vector3Event::.ctor()
extern void Vector3Event__ctor_mEEF80B16ACEC7A2C7B430F708D95AA3921E097EB (void);
// 0x000002F2 System.Void Lean.Touch.LeanFingerUp/Vector2Event::.ctor()
extern void Vector2Event__ctor_m97409F24DA4F910CBC0CA98DF011AACE799BC17A (void);
// 0x000002F3 System.Void Lean.Touch.LeanFingerUpdate/LeanFingerEvent::.ctor()
extern void LeanFingerEvent__ctor_m9702EEF46B05BDF0536F72ED69E57B49AE18258E (void);
// 0x000002F4 System.Void Lean.Touch.LeanFingerUpdate/FloatEvent::.ctor()
extern void FloatEvent__ctor_m5E7A15983A28F6853971B22DB6067ADB2406E0BD (void);
// 0x000002F5 System.Void Lean.Touch.LeanFingerUpdate/Vector2Event::.ctor()
extern void Vector2Event__ctor_m51EBA0E530C99B6FF6C9298ADACBAF19FDC4EE1C (void);
// 0x000002F6 System.Void Lean.Touch.LeanFingerUpdate/Vector3Event::.ctor()
extern void Vector3Event__ctor_m37B43623DEF8F13CE720616D73F44C599CA37486 (void);
// 0x000002F7 System.Void Lean.Touch.LeanFingerUpdate/Vector3Vector3Event::.ctor()
extern void Vector3Vector3Event__ctor_m48CA2CA2DD2481C73C707D5D91117979836D882C (void);
// 0x000002F8 System.Void Lean.Touch.LeanSelectable/LeanFingerEvent::.ctor()
extern void LeanFingerEvent__ctor_mC7D40C1C205EE69CFA2893EA85BA064D198F3F1B (void);
// 0x000002F9 System.Void Lean.Touch.LeanSwipeBase/LeanFingerEvent::.ctor()
extern void LeanFingerEvent__ctor_m9E9808207D2EEFD582F28CE19E3E0A9075234948 (void);
// 0x000002FA System.Void Lean.Touch.LeanSwipeBase/FloatEvent::.ctor()
extern void FloatEvent__ctor_m2BE488A19FA2FC596AD67BBBE5F92885A2B29654 (void);
// 0x000002FB System.Void Lean.Touch.LeanSwipeBase/Vector2Event::.ctor()
extern void Vector2Event__ctor_mA7B1074912DDF49096DFF039309D23D8539B3DE0 (void);
// 0x000002FC System.Void Lean.Touch.LeanSwipeBase/Vector3Event::.ctor()
extern void Vector3Event__ctor_mDF01E3CA8B5D11B0F08F155BAA9D291D5801A44E (void);
// 0x000002FD System.Void Lean.Touch.LeanSwipeBase/Vector3Vector3Event::.ctor()
extern void Vector3Vector3Event__ctor_mFA93A740F2E94269E2185B8A3E8F49D10C729F67 (void);
static Il2CppMethodPointer s_methodPointers[765] = 
{
	CenterScreenHelper_Awake_m58C190250B1A7130B403349568E32464AE0C7544,
	CenterScreenHelper_GetCenterScreen_mCD85AF7079FE1D3E940A5C48429962FE02FFEB94,
	CenterScreenHelper__ctor_mCE4DCA550136049FC0DB7AC3CF171B16F13ABA10,
	LightEstimation_get_cameraManager_mE01BC5E17D16A6843D648865853E23A07CF1543B,
	LightEstimation_set_cameraManager_mF8BDE678B769ABCF7324064B1A4052F558934AE8,
	LightEstimation_get_brightness_m81B8B41717C95613B6B530C7A4DA253B82D41251,
	LightEstimation_set_brightness_mF9D7CF55B64154A0E24B60520F2D2FB0EBB03370,
	LightEstimation_get_colorTemperature_mFC6A5877B4551367A9DA5FEBE1F809D3E97C9CA4,
	LightEstimation_set_colorTemperature_m2094B346618FB1A940F97CCD138109BB438FF420,
	LightEstimation_get_colorCorrection_m1DE07508BFE7757692C7831FAC0E0F58BDB64941,
	LightEstimation_set_colorCorrection_mD59CF43A84D0DE0089C83409838DE311AA902D23,
	LightEstimation_get_mainLightDirection_m9F63CCA98C50343FB0332DD4AA330F8678A9C02C,
	LightEstimation_set_mainLightDirection_mE2E416FED9DC9D5768A8A57A02A2F90C6DE0F105,
	LightEstimation_get_mainLightColor_m33449C2C041A80E61B6318A06C33623E6FAEB5C8,
	LightEstimation_set_mainLightColor_m701E00A2FECDFF842A1138EF07899BF23D63AAB4,
	LightEstimation_get_mainLightIntensityLumens_mDF8695D4DEEBBF8E325DF72E87AB929772E077C0,
	LightEstimation_set_mainLightIntensityLumens_m7606C8F109CBE83D3A4B8BFFD83E50134064B280,
	LightEstimation_get_sphericalHarmonics_m6FFAA5F42C14D0620BA4B570ED63A0B3AD72EA95,
	LightEstimation_set_sphericalHarmonics_m8E39570E4DED98CD4C5C05DC0C15B0D0F8036ED0,
	LightEstimation_Awake_m235FD397AF66B30969C55A03662A0AF3C939CEAA,
	LightEstimation_OnEnable_mD46CAFE75E1103FE8AA34C6CE94C6DC88F0BC39E,
	LightEstimation_OnDisable_mE6B9C5CD8516C5F09FCDA60AAC1E2FA6E58C9CEE,
	LightEstimation_FrameChanged_m1ACDB8AAC00FED1731FBD0F1FE81B8FC2B897840,
	LightEstimation__ctor_m8383629073F32A42EA866DE00BBBD46F66296032,
	PlacementReticle_get_snapToMesh_mFCD2BF8AFDA208DD4889790B5C943E5C2BA51DA5,
	PlacementReticle_set_snapToMesh_mAC9E833925E3B85FEE8C833C26FFCD1CEECF13E5,
	PlacementReticle_get_raycastManager_m63160F54CBBD75F8AB12238B5FCCBCD255FF90DB,
	PlacementReticle_set_raycastManager_m60EA76792BDBFB10387B24F93A77B432DE70E906,
	PlacementReticle_get_reticlePrefab_m68D1D0BA7E9C7DA7E1CEF303522A366ED2BF24A3,
	PlacementReticle_set_reticlePrefab_m1D0E650E3D544E428FC13D72B38101F67EF90603,
	PlacementReticle_get_distanceScale_mFC07759FC3540279CADA55DE80F6A1D2A33DA60F,
	PlacementReticle_set_distanceScale_m2F3D439A828914F8C11B700C874DF0FE5032FC09,
	PlacementReticle_get_cameraTransform_mDEC424B4BCE0F314448E6ADDDF8A76F5B1A809BC,
	PlacementReticle_set_cameraTransform_m16D0A3E9D0854C0583C84FAF3EC852D8582B89A9,
	PlacementReticle_Start_m1C2FD5FFB0577E974F88B4A06B4328BBDD62FEE0,
	PlacementReticle_Update_mE860D76D1869D685D4F18AFA996720486E0CC0BE,
	PlacementReticle_GetReticlePosition_m1F51136B5CC3F1265B5762BE6C733F9C5E175839,
	PlacementReticle__ctor_mF0EA4954E8CDA6F19CDC34C742ED1CE371CB1C75,
	PlacementReticle__cctor_mC8A11E65ABFDFF03ECD3911204B561F9E4EB84A4,
	ProbePlacement_get_probeManager_m54B80D3E77C43E104DAA54C8A0C49686615893B9,
	ProbePlacement_set_probeManager_m556A87C8CBB842EE577814BEF1417DA3FF6FD71D,
	ProbePlacement_get_raycastManager_mB9EE8EC22BC493DF62CA6E2AD5A47F7D5AA64EAB,
	ProbePlacement_set_raycastManager_mA34ED80F26318B24C791B4872BD2167ED72D3918,
	ProbePlacement_Update_m22AA5CC6A30571E16ED67DE9C20D6DC94559E918,
	ProbePlacement__ctor_m2CD3A6FF9CADFF7C5A709739BA6903A46696A0D3,
	ARFeatheredPlaneMeshVisualizer_get_featheringWidth_m0AAD92A3D682C1DFC1B5630585550B6CAFBAFE32,
	ARFeatheredPlaneMeshVisualizer_set_featheringWidth_m4761377095817FA81908BC43FD08EF600F6CC231,
	ARFeatheredPlaneMeshVisualizer_Awake_m7FD9AC4C30D2F12A0A30CC3CAD9EC6C17B07C45D,
	ARFeatheredPlaneMeshVisualizer_OnEnable_mEE0AFFC028F876DE841312FEB8CA15FEAD105FE1,
	ARFeatheredPlaneMeshVisualizer_OnDisable_m51213AC9D2DBD97C93CC41330F65D74661D5DC81,
	ARFeatheredPlaneMeshVisualizer_ARPlane_boundaryUpdated_m354D58D0B29B40FE6D3A6689590E271D9282323F,
	ARFeatheredPlaneMeshVisualizer_GenerateBoundaryUVs_m241EDA47E1C4DECA8CCBC61D8DF6EC300546CDF0,
	ARFeatheredPlaneMeshVisualizer__ctor_mA27A238E88D5FBA54D4DDFE588080DD41B449A4C,
	ARFeatheredPlaneMeshVisualizer__cctor_mF5C9F857AC060614B46B9359B3F0859FBC309DF3,
	FadePlaneOnBoundaryChange_OnEnable_m9A290421CF90C45F885B14F3E1DA888BFD27A340,
	FadePlaneOnBoundaryChange_OnDisable_m0C62572D4B470C313E0B203F6AE948865F395825,
	FadePlaneOnBoundaryChange_Update_mBB620E21C1BE2FE4EF8F45D1C14EBE6C5579F8A8,
	FadePlaneOnBoundaryChange_PlaneOnBoundaryChanged_mF362091F455EDBF9437C0B672D7CE83ED4CD20CC,
	FadePlaneOnBoundaryChange__ctor_m6625F1DF41CF81E94037383DFBEDE2FB0237096E,
	PlaceObjectsOnPlane_get_placedPrefab_m1DF1DEA50DD1EB8FA27F91E9E36CD05F9996AD47,
	PlaceObjectsOnPlane_set_placedPrefab_mF8AB158FD6274BDE9CD2CBCD8D07C363150AB9F4,
	PlaceObjectsOnPlane_get_spawnedObject_m0C450181B9312C9BDFEBFF7724566880F8C54F16,
	PlaceObjectsOnPlane_set_spawnedObject_m5E0C55D3C00530D85D1D71ECA5F8B076664EBAC5,
	PlaceObjectsOnPlane_add_onPlacedObject_mBE87A45F356D35BEE6369C97B9C679C26988B896,
	PlaceObjectsOnPlane_remove_onPlacedObject_mD603DEC49025DCB32DEBD81498F8FF12A11DC818,
	PlaceObjectsOnPlane_get_canReposition_m23D1F0BD936618915374B516A3469A600A318180,
	PlaceObjectsOnPlane_set_canReposition_mEE47BA8AB9213401E15E5B7FE58C83F9759EA088,
	PlaceObjectsOnPlane_Awake_mA25B1581D947FA1688472EB160973D208A610A66,
	PlaceObjectsOnPlane_Update_mB26AAB9031D9C19840C18DF08EAA14E81E526C72,
	PlaceObjectsOnPlane_DisableInstantiatedGameObject_m64B0DCEEB4BE05B0C43BB1DCB5625A185AC937BB,
	PlaceObjectsOnPlane_PlacedObject_m7888392D26B29C3AC0CD6B94611B0078026D96BA,
	PlaceObjectsOnPlane_SetPrefabType_mF60094F82FFAE45850E948483B8F67BA4FF10D48,
	PlaceObjectsOnPlane__ctor_mE4F19060ECAC129230030682148DA6D8BB2236CD,
	PlaceObjectsOnPlane__cctor_mD0EB872BDAA8E1DA31C77197F954EE6CC07AD776,
	ARKitCoachingOverlay_get_activatesAutomatically_m43FE71F33A96A596F554C5685B460307A22E6191,
	ARKitCoachingOverlay_set_activatesAutomatically_mCA5679C29A9C0B788DF7877921582E751F6D30A9,
	ARKitCoachingOverlay_get_supported_mF8B542B3A56EDD453C0BA7CF443F1AED0A4F1782,
	ARKitCoachingOverlay_OnEnable_m48102540113361551F51CE45E7285D493E9433A9,
	ARKitCoachingOverlay_ActivateCoaching_m968D23AE1413DEA4E6D33BE8C3074CBDE5AA7954,
	ARKitCoachingOverlay_DisableCoaching_m13E7F6653A5C55B76207C6462E611642968E4B07,
	ARKitCoachingOverlay__ctor_m3A2BF1CA291CBE8B05FB58F88A35A2B48502AA35,
	ARUXAnimationManager_get_instructionText_m514815741754D3316056F333967E6BB23CE8BEE4,
	ARUXAnimationManager_set_instructionText_m4DF1E2F1174EEB418AF1EDC5223CEC64D5022920,
	ARUXAnimationManager_get_findAPlaneClip_m719B16B0422736480570DB4E439DBE01AA8C2114,
	ARUXAnimationManager_set_findAPlaneClip_m77EA535039AB1E3D9191CE99C5F7951FD21BF3C4,
	ARUXAnimationManager_get_tapToPlaceClip_mDCC5C54A2D0554975F37E9F3E2527C5A96DBA97B,
	ARUXAnimationManager_set_tapToPlaceClip_m2BDDC86C341E760C28A36536C9EC6B2E9830B3B8,
	ARUXAnimationManager_get_findImageClip_m17F43F94F6FDBF44038B525A056135DAF84DA3A6,
	ARUXAnimationManager_set_findImageClip_m8708CC3233B186BC87286078748D780B0B0D6497,
	ARUXAnimationManager_get_findBodyClip_mC9A93E71F45C759AF10C008E392575E5B27FBED1,
	ARUXAnimationManager_set_findBodyClip_mF086B22B4119D422855C50D1BE47372F308AD20D,
	ARUXAnimationManager_get_findObjectClip_m9AD91C9C5E2EFF367878E7AFE82AA3555A7EC6E7,
	ARUXAnimationManager_set_findObjectClip_mAB9A67C0313D3504BAB411495A9963865435773B,
	ARUXAnimationManager_get_findFaceClip_mCABE42C2DFB671055915EC5981E075A63CBA6552,
	ARUXAnimationManager_set_findFaceClip_mFAD93DF36221E5D60829DDEA80373F281CE42854,
	ARUXAnimationManager_get_coachingOverlay_m5618DDC3B42F3839A2750D9F40E11EC9CC0D3988,
	ARUXAnimationManager_set_coachingOverlay_m814AB67F980DBE8AC8C8688046AFF42998B4D109,
	ARUXAnimationManager_get_videoPlayer_m744C861A74C305AB9771AE5C165CE50BCDDDDF2C,
	ARUXAnimationManager_set_videoPlayer_m0D3A4AEC9309AF981D131D6EF2B925C325B0B7B3,
	ARUXAnimationManager_get_rawImage_mCBFE65FAE8C8603CC638EA30FBA2EFEAD3A54DD4,
	ARUXAnimationManager_set_rawImage_mEA87BEC0B7A5DB9C6958D66BDA6C6F4E46C0869A,
	ARUXAnimationManager_add_onFadeOffComplete_m988A1D4D19242C1067E194A5BA32C5D1988C282D,
	ARUXAnimationManager_remove_onFadeOffComplete_m7D39849FBC6A57AB6D63D48A5E7B3DF2695D89DB,
	ARUXAnimationManager_get_transparent_m2EDE47D2661C42196C9AF99A1DBD664A8373B0D3,
	ARUXAnimationManager_set_transparent_m9F725BE8D2ADABEF4251247E3FB6CF4A88FB0BD2,
	ARUXAnimationManager_get_localizationManager_m48ED855EEEE5C97BDA3825B643AD24F5028C3887,
	ARUXAnimationManager_set_localizationManager_mABFDF3C996AF72EBAE76D3D00EE00087644668B4,
	ARUXAnimationManager_get_localizeText_mFE8B410693D3E7C6A620F944004C1F2B7DF6FF8C,
	ARUXAnimationManager_set_localizeText_mE3E02C81B178C99CDEC9D5E7DB66FB7C3BAAE394,
	ARUXAnimationManager_Start_mD238B58A7F129816A2539587FD91D2445A9D2083,
	ARUXAnimationManager_Update_m95C600DC484FBB782C75DA7018A21965354B7015,
	ARUXAnimationManager_ShowTapToPlace_mD164AD80CD34C083DC02CC0B918CDBA2F1E12E87,
	ARUXAnimationManager_ShowFindImage_mDE5652F03937A048A2AB4EEE3894422897E6315E,
	ARUXAnimationManager_ShowFindBody_mFFEE3A8E28318CD2478A4198B96962DC7F1D6773,
	ARUXAnimationManager_ShowFindObject_m5613130140BF7564B8026CC0B4FD80F2FC38BE5A,
	ARUXAnimationManager_ShowFindFace_m18EC5D247D9D3BD038495356704E8AFA3DB1BAD5,
	ARUXAnimationManager_ShowCrossPlatformFindAPlane_m7103C2C86FBD3CA361050BEA0ACBEB117EA99241,
	ARUXAnimationManager_ShowCoachingOverlay_m6F6B24BA84550FE59F97FDF57D8FF50EEED919BC,
	ARUXAnimationManager_ARKitCoachingOverlaySupported_m09A1F1D76F4F2B0877678D97E3F2E440AB490400,
	ARUXAnimationManager_FadeOffCurrentUI_m3C9C5A64B3CB75838522ED1F01CE5A99D4F5BFFB,
	ARUXAnimationManager__ctor_m75E82BE7AC295093E53BF5E4F2037111C29F08F2,
	ARUXReasonsManager_get_showNotTrackingReasons_mA12725E27C8D70F1BDD90865A295895A57454F35,
	ARUXReasonsManager_set_showNotTrackingReasons_m17262974FD31DA2843755C937F4A3B0CDA850382,
	ARUXReasonsManager_get_reasonDisplayText_mC7C9A561D882C514D05FA6D5AD7C99CB874C4318,
	ARUXReasonsManager_set_reasonDisplayText_m753C3C84B6D26AB2A1563D1A569EEF8DE0B4C880,
	ARUXReasonsManager_get_reasonParent_m16292A2374CFD842048A28DE37529BA24DA41020,
	ARUXReasonsManager_set_reasonParent_m61E6991F11599AEAAF528A6F8668B90EA9E9E25C,
	ARUXReasonsManager_get_reasonIcon_mEEAD7FBC5DBBED8206948C9C543AA4C6C413637F,
	ARUXReasonsManager_set_reasonIcon_m71B9311EAEB1B5AD141EE840507A9A8F8E563A51,
	ARUXReasonsManager_get_initRelocalSprite_m024F7B88DEFDB433F9838CDC0DC2ABA391EED2CC,
	ARUXReasonsManager_set_initRelocalSprite_m409BF75B26CC2FC814E08FA1055A3947E5D2CE55,
	ARUXReasonsManager_get_motionSprite_m92EC24F8AEC3C7B6F6AF7178898E8C2AF21445C3,
	ARUXReasonsManager_set_motionSprite_m72A97B8D0D40DF73089AD1C48347E6BED2F860D0,
	ARUXReasonsManager_get_lightSprite_mF8B82531BB0BCB59039A33B666832B02CDD0F8AF,
	ARUXReasonsManager_set_lightSprite_m9CDE50D4F5E15890CF86B9AD6DABFBD40BEC6A26,
	ARUXReasonsManager_get_featuresSprite_mC7DBCB37FF3DFB3999227EF8AB67E6A1C52A025C,
	ARUXReasonsManager_set_featuresSprite_m40BD82A19A7D5CD2BB75C33262EF5A969539BA45,
	ARUXReasonsManager_get_unsupportedSprite_m025F5BF0BBFEC485A777315E20462C4A9AF4375C,
	ARUXReasonsManager_set_unsupportedSprite_m055F4F1FF41C0F62E2043B6FFC09C6D74CA88229,
	ARUXReasonsManager_get_noneSprite_m194347B6F3A6CBE8E312585C80A2928D3B064404,
	ARUXReasonsManager_set_noneSprite_mE8667B89B2DC3333ECBCDD9CBEE123ED6FC44BFB,
	ARUXReasonsManager_get_localizationManager_m3F0954D20F9B4B11B15067B25DC5550A8A70F477,
	ARUXReasonsManager_set_localizationManager_m4E0ACDF16477375C019A0A885E6C1B92B2525AD1,
	ARUXReasonsManager_get_localizeText_m57164DA09CA5DB119DD8E489267944728167096E,
	ARUXReasonsManager_set_localizeText_m474B446D80DE7DF1922CEADF7DE13F0C8274D106,
	ARUXReasonsManager_OnEnable_m037BB47878613AB444B796B207909A8AE7AADAFF,
	ARUXReasonsManager_OnDisable_mD3734CBAEFB3D13FC4DE2F8593BF2443418EFF00,
	ARUXReasonsManager_Update_m02B85757D02646BB3DA2C71E665E938007A9943A,
	ARUXReasonsManager_ARSessionOnstateChanged_m5F8B8F570C9F232464AB456245F6B4C11A859A46,
	ARUXReasonsManager_ShowReason_mB626102F2ADE5EA027C1C4CE66624A526E17F0B9,
	ARUXReasonsManager_SetReason_m43AEDCB23F444A9E4EB23013650CE855385F4F60,
	ARUXReasonsManager_TestForceShowReason_m9FB72D1242FC69D56E8080ED336F50DA3B7C5F16,
	ARUXReasonsManager__ctor_mD738924E0F0097E1DBB5294259B002ACF7D06874,
	DisableTrackedVisuals_get_disableFeaturePoints_mF53F493674476B2E0ACEB5152FD25AB6D4324E77,
	DisableTrackedVisuals_set_disableFeaturePoints_mBF30CAABA44924EBB8A5D5A891D2C6159A83EF96,
	DisableTrackedVisuals_get_disablePlaneRendering_mB6F791B4221BC644C4ECBDA00EB8327B5ED7D2DE,
	DisableTrackedVisuals_set_disablePlaneRendering_mBEF12A327B6F216033672A7349EC12732F6D29A8,
	DisableTrackedVisuals_get_pointCloudManager_m74DAA4E0AE2218B0443D0A2E85BFEA286E348625,
	DisableTrackedVisuals_set_pointCloudManager_m794B67459916AA4755EBE9E69831315FAAEFD3AC,
	DisableTrackedVisuals_get_planeManager_mE926216C35ECC33F2632C969BD151779076E41BC,
	DisableTrackedVisuals_set_planeManager_m76DA3841E86C5EEB571019CC18C48200E9015358,
	DisableTrackedVisuals_OnEnable_mC6E0BA401F8837D9A0B93C81DDFED1B813BC6629,
	DisableTrackedVisuals_OnDisable_m3F48D8070C2B378FE7228B29472C8280241CB74D,
	DisableTrackedVisuals_OnPlacedObject_mEE39A9227DBA59A9D83C17B254620121909225F6,
	DisableTrackedVisuals__ctor_m4EAA5CB87C1AEE2A10C4DE08F21528FDBDF9C53A,
	LocalizationManager_get_localizationComplete_m88BBD683DC9FD499DEE14C62EBFC4C83260ED012,
	LocalizationManager_get_simplifiedChineseFont_m27F640E8CFC2FF177B3FC0152FA51A5B2A4BB806,
	LocalizationManager_set_simplifiedChineseFont_m8F94F4123033A1E2A57F1A96120C9366CADF5063,
	LocalizationManager_get_japaneseFont_mC243AFEB584DAC1C604FD2F4BC179DAB11557CF8,
	LocalizationManager_set_japaneseFont_m4362024A8975641F2238F307D2E9766B6F140A77,
	LocalizationManager_get_koreanFont_m4FA454CCD51ABE9CCF678AA8424AA663BCF3A33A,
	LocalizationManager_set_koreanFont_mDF1E79BF3B0BE776F1CC51D5BE691906FF29BBF3,
	LocalizationManager_get_hindiFont_mEBF1821DFAC3B889694DD757791CC7D66DD46054,
	LocalizationManager_set_hindiFont_m7B28FD32EF9148CF19BDCDF9428A982DF7CA82AD,
	LocalizationManager_get_instructionText_mC8E952E79C3B600158E59CA9B20DE200296BD1A1,
	LocalizationManager_set_instructionText_m3BAE0D8712A487880BAA51E37C7E2B5988B09C75,
	LocalizationManager_get_reasonText_m7D99E2130A2E5733559195A148AC77B00CB44D44,
	LocalizationManager_set_reasonText_m22D6AC6256F13F55B2D8A533914C4C19DB775A22,
	LocalizationManager_Start_mCCC2E00354521D23FD5CBDB28FD347B27956BC61,
	LocalizationManager_OnCompletedReasonsUX_mE09AA2D2EB53ACFDE0142288B5A11E8BD6689126,
	LocalizationManager_SwapFonts_mDC31F1203A1557DB0ED9DAD0D70E0533A0AE7720,
	LocalizationManager__ctor_mFDB7124E84B7565E30F7240BB010304AD49A9832,
	UXHandle__ctor_mCCADFDD89B964F4412436FA3100256EE9305FFDA,
	UIManager_get_startWithInstructionalUI_mE40F1263E3AD20EF65D3F71FCD8BFB9A813CABB3,
	UIManager_set_startWithInstructionalUI_m95C06FFDEDC4F5DEEE9153C3D24F42B2268B9E09,
	UIManager_get_instructionalUI_mEF9826C7692CA05F7FDD31C7F5580164082548BF,
	UIManager_set_instructionalUI_mEA656FF6B11EE89CCAC2D6E94DDEE60F7C2AB4B2,
	UIManager_get_instructionalGoal_m063CD11118574B3B21E30B3F98383281436F1A28,
	UIManager_set_instructionalGoal_m9E1EDD22FCD82308FC7E13874EC1D145123357BB,
	UIManager_get_showSecondaryInstructionalUI_m4C6793FD65814FFADD3F23F42622BEBBE3CCAC80,
	UIManager_set_showSecondaryInstructionalUI_m867F90CD6FA33B82761F1015DB71D42933FCD52A,
	UIManager_get_secondaryInstructionUI_m999F4A70CB2A6FFB79F02B097D70CDCF3F1F2F1F,
	UIManager_set_secondaryInstructionUI_m8E83AD99333F787D243B0AA92D0D3C5882942AC1,
	UIManager_get_secondaryGoal_mA5492F33BA5BCD4FE5B801A4D11BC9886FE2424D,
	UIManager_set_secondaryGoal_m8632E8D0201EBD6810FB3FD9D207536182D6AB28,
	UIManager_get_coachingOverlayFallback_m8696E4F7A0AA1EECB1ED97D07682D458BB8A2B2D,
	UIManager_set_coachingOverlayFallback_mE8D20CC40A1C46BEFE9BBA520DE7172E3C632956,
	UIManager_get_arSessionOrigin_m05131191805F0EF37D5C10120826BE801E74F343,
	UIManager_set_arSessionOrigin_m9F06555FA957BF21149830605BC9256246ACC7B6,
	UIManager_get_planeManager_mC295F7015EC0AFA6BF017218A3D50FBB5F8EC93B,
	UIManager_set_planeManager_m00D2D98C5B74891D8CE5F847317C28A7B4A992B5,
	UIManager_get_faceManager_mF4D5FEB0CBF74E1E6DB86981BA328D74159FACB7,
	UIManager_set_faceManager_m6C34CA7DE94937B661CD1AA9D02F5D79B70713F6,
	UIManager_get_bodyManager_m72FAB838B954B870E4C9A53E9B1E4F4F884783C1,
	UIManager_set_bodyManager_mB227C036D10CC66AD40F4E050D154179DBAEE50D,
	UIManager_get_imageManager_m105BE6CFB85332D12FF7C85E6FE656EBDFDF7F54,
	UIManager_set_imageManager_m0529429F9ABF451B6411103D9F8F1BB9DACDB933,
	UIManager_get_objectManager_m50E8ED7DCF8BB014447D56D38044459AFC9BDFA5,
	UIManager_set_objectManager_m706C5829D9F5C97B121E9353EB152FA28C56F71D,
	UIManager_get_animationManager_mAC5459D921B5A8510BE1A70514155EEFB11E1DB3,
	UIManager_set_animationManager_m412348E166C87E988BBC2802B7DFBB952CA2273E,
	UIManager_get_localizationManager_m62EE0ECE37206FFEE471E84E47431807D395790C,
	UIManager_set_localizationManager_mD354A3856BE6015679D2873FE57ECEE8E891A824,
	UIManager_OnEnable_mAFE10D24F19B379487455D635E4A3C65D4B64240,
	UIManager_OnDisable_mBAED7246344396BA861ADE4603C663A9ECFBCDEF,
	UIManager_Update_m8A7C5DF1B797CFD6937FD6961AB9CC7B1A90D385,
	UIManager_GetManagers_mD12BBFF5C86710EEFB1373A750D4568248EBF65F,
	UIManager_GetGoal_mF9DF0A17B2A9B3469399AE5EB06BDD6E85D7B2B0,
	UIManager_FadeOnInstructionalUI_m54B2A2025926527856D13414C1133EB617BCD96D,
	UIManager_PlanesFound_m4815E9F0871E527DBBCD7A190B7727D65D482622,
	UIManager_MultiplePlanesFound_mA354599925BC74F323F09A20D4B5DE5EDAF20CDB,
	UIManager_FaceFound_mB49F79714CA7296A5C344A8093B8AE33F3F5CA20,
	UIManager_BodyFound_m2B28D4FAEDAC413FE77BC29838F9113E986FD67F,
	UIManager_ImageFound_mBAA7C2BF6A1B7B643502EEC407AAC5504549CE38,
	UIManager_ObjectFound_mAE0C6EB3C245C76BCDFD0479514C7B21A5808ABA,
	UIManager_FadeComplete_mDD5CFE580BA60FEE6E72E4911F3ED91A41BF6336,
	UIManager_PlacedObject_mFD09B746476FFB9DB7DE431FDE1123C194694429,
	UIManager_AddToQueue_m35E9C512CBECA09EE9784B45DFCCA278D77590D7,
	UIManager_TestFlipPlacementBool_mFCC1C4330588D546B16DF094ABEBD73D1E18D69D,
	UIManager__ctor_mDADE1D724D40AF63AE78D51FC1CF1FE4784B4D4B,
	UIManager_U3COnEnableU3Eb__69_0_m6E36F214C2EC57CB40BEDD772DA012EE40D56CBC,
	DestroyBall_Update_m2A7E36240B466E7E8CB94AC53CB54D5897E90ABC,
	DestroyBall__ctor_mFD7FB06FCC68CAD40CDF58B265BC62C3D266EFB0,
	BowlingAnimationScript_Start_m69D0ACB76A47B2DC82476F4FF42ADB59144C3F44,
	BowlingAnimationScript_Update_m099DB37FF04CFE7D98B8A5EA98BF06BB7E36E260,
	BowlingAnimationScript_OnTriggerExit_mEC4925631254044BA58C308DB0EF838B2AD3D61C,
	BowlingAnimationScript__ctor_m36FFC0F741C60916F981B21EDB95C9C00C408A2F,
	StrokeScript_Start_mA94A8B80CFEB459F99DB734242E720CFB2671519,
	StrokeScript_FullHalfVolley_m5D1DD2D44175F114806C10C62D1E1B5044C634F2,
	StrokeScript_GoodLength_mF7C6F8DDA77B3FF941D1B0C82763EA54D2A2F525,
	StrokeScript_ShortofaLength_m790A51F3320596F0EAD8A0942F342D8DF1C338FB,
	StrokeScript_Short_m7B53338D491FC909DCDE68A25FFF01FF8E935BCF,
	StrokeScript_CoverDrive_mED4430DFED7AD5C626CE976FA2C84ED754CA0501,
	StrokeScript_StraightOffDrive_m4058EC11A4703CA32CE1FF19E0DEEF4783B49B5C,
	StrokeScript_OnDriveGlance_m3CBF2DC8EFF65C6E5D50B53F5D30EA47864D93F7,
	StrokeScript_ForwardDefenceSweep_mCFEE5F00A178BD0FC26A2D8664A4A044CC2E103F,
	StrokeScript_BackFootGlance_mD7085CECF9823D3C42A454DDD3604CDD619C138D,
	StrokeScript_BackFootdefencedrive_mD1F4493891F4C5956FDD94F85F881A3F49A37DF5,
	StrokeScript_LateCut_m67EFAF1C4536A222C7C00F497FAB7B5D64600A35,
	StrokeScript_Pull_m23E0F0E0735F960D468CDBD961C31068B65FBE28,
	StrokeScript_SquareCut_m27E291C2C1F0C636F94EBC840316584732854462,
	StrokeScript_Hook_m1D7899483726EB742058822BFBCB587CDA5E272F,
	StrokeScript__ctor_m1F07DCEFC6C7AD72770BE6AFF496C37136703F2E,
	StrokeScriptRevised_Start_mCE848AF2E345CE7E13F02F2865762C1510C9C864,
	StrokeScriptRevised_Update_m119F7723F1B3AB42906D8AC427F925ECD90A8574,
	StrokeScriptRevised_LenghtFun_mC349FDB9DE492C6F9D842B13B26270A79E99597E,
	StrokeScriptRevised__ctor_m16B20B00337BB41C602391E8536C82D7BE41588C,
	SceneController_EnableScreen_mC439D970C9780E7B92A7A398495BAD35C76EFB4B,
	SceneController_DisableScreen_m53165CC1485293FA9DC836D19686DAA615C51450,
	SceneController_Coroutine_mB4428FD5F1616CD3D0E62215165D926AEEB4289F,
	SceneController_EnableBatsman_m583C8D3A4A1A27E0F5E121DCD72731F8CC063DA5,
	SceneController_PlayBatting_m3E71EF8520EA3319AEB7755997BA5D84FFB316A4,
	SceneController_EnableAnimation_m4E9F1D5E9EBEA1725B9005DDECCFFD74CEDAB60C,
	SceneController__ctor_mD43BE19257E8699CFE380AD920050CE78721E604,
	PitchController_Start_m5EC2C8DBDC50B602151DDDF64210997B06BDDDC5,
	PitchController_DeActivate_mE7CDAE178982C890E66E4A0FC04927937AE7B16F,
	PitchController__ctor_mD02C8A2847DFF53EA2DFB7DF65D24D6CF94D401F,
	ThrowBall_SpawnBall_m791B31511F591550A6DCFB66EDB0C444F2296829,
	ThrowBall_DestroyBall_mDB05C350CCA5641838E85877E6F917177C61E965,
	ThrowBall__ctor_mEB1D70209F5810666677C57F9403447FF2DEBBB1,
	LeanInfoText_Display_mEC16B609CFCAE7A91048BE9164B60F44F4259ACC,
	LeanInfoText_Display_m596455DE1512899D9EF3F4C08084E985024DE27C,
	LeanInfoText_Display_m3DB84D5C44825D2E71303A5AF70F6F0C1A47915B,
	LeanInfoText_Display_m9D2D15985191EA6B29A9FCF1880F25CD527A5CBC,
	LeanInfoText_Display_mE6CE8FEAB1625F409382E286F44B037D41F0AC6C,
	LeanInfoText_Display_m24289429D94C3375550C8413F63B97D7297E2CA1,
	LeanInfoText_Display_mEDDA82DEB5A4715ACCC23FCEAA6B26C4AA736D7A,
	LeanInfoText_Display_m2C6B430C20897AAC31A2B793191DEA4519E51F2F,
	LeanInfoText__ctor_m51DF3EEC28AB11A72249A25EDFA6CEC93CF078FE,
	LeanPulseScale_Update_m0DB57098D8856470F1BF3B31B8E168E75049CEBA,
	LeanPulseScale__ctor_m37134467E74586BBDC07FC46A7355FEE8F17EBE8,
	LeanTouchEvents_OnEnable_mBD10B04C13B8CE8CECDD20589C3063CF585845FD,
	LeanTouchEvents_OnDisable_m7177B1E836063A83A524EE3BB1681539D454DA21,
	LeanTouchEvents_HandleFingerDown_mA446C49881F440F3373DDDA11AC9E1CE23203A99,
	LeanTouchEvents_HandleFingerUpdate_m98F6BAFB50666EA4139BC572138A5D12616A2F05,
	LeanTouchEvents_HandleFingerUp_m3103EFCFEC77FE3FEC97644CA364A80F158D93F6,
	LeanTouchEvents_HandleFingerTap_mA8CE756F2FED184BAA124A56BEFA3718F2CF87BD,
	LeanTouchEvents_HandleFingerSwipe_m06AC0962C6CA2B959BE9BB47B0A8864A35E095EF,
	LeanTouchEvents_HandleGesture_mA112A90D6124C66468ED2CA1A9F765BBD5158728,
	LeanTouchEvents__ctor_m934F1E2098DD13DDEF8501475EDDD511EF8D57E0,
	LeanDragCamera_MoveToSelection_mC17CA5F136F00D57EDEF2F3193F53116ABC918D6,
	LeanDragCamera_AddFinger_m92B16CBD775F8554B2137EA46F99A6CB8E1D0EEA,
	LeanDragCamera_RemoveFinger_m675A72D42C447AF49F1100B40A668477FC349808,
	LeanDragCamera_RemoveAllFingers_m3265FCD465BF03B47B400D6998423FE926F31FD8,
	LeanDragCamera_Awake_mEC7847822CA1D8490E4E41F871CF225555AB78C8,
	LeanDragCamera_LateUpdate_m26FB50F446756E95E3C30C8017B33B7D71009666,
	LeanDragCamera__ctor_mA64F0D615C7CDB775E9BF8DDEA8662CF68BDA68E,
	LeanDragTrail_AddFinger_m61EE86BFDE5A2C99E90E10A1F49EA59D3EFF374B,
	LeanDragTrail_RemoveFinger_mC0605B2A93C975E0DFA8D8BCB68C16DD81C0EBFD,
	LeanDragTrail_RemoveAllFingers_m8ACE3216CF9036CABF18D668849748F445297A3F,
	LeanDragTrail_Awake_m9CA3C99A1893E2A854E5D3C8D93E65A80E11A016,
	LeanDragTrail_OnEnable_m22B084652219AD8BFF5F76BDB4229464ECAE1A13,
	LeanDragTrail_OnDisable_m323C72CA681312C69869800EA9BCF93674E78EE5,
	LeanDragTrail_Update_m07ED1B0764CFDAD90689312B2599137718FD4B00,
	LeanDragTrail_UpdateLine_m48F8CC5B559B492389409E1E574F52FC46A99898,
	LeanDragTrail_HandleFingerUp_mE4219E175E86F618058F98D94832311917765B98,
	LeanDragTrail__ctor_m2BD26F778E504FF07C958368C90C16A38C666AED,
	LeanDragTranslate_AddFinger_mCFE1B2F819BF0A8765AA399E98FF74254459F715,
	LeanDragTranslate_RemoveFinger_m87323CF954F4B0E75C84B8C247517E4B1E164E5A,
	LeanDragTranslate_RemoveAllFingers_mF17AB71669CBE48CFD81A6DB0D7F77C1CBCA37A1,
	LeanDragTranslate_Awake_m0F4AAEA53B203CF6249A76D62EA4B449B4A1BD8F,
	LeanDragTranslate_Update_mB75065AEA06B7BC9E9F849E04162EA29A6D866F3,
	LeanDragTranslate_TranslateUI_m58CB7C78A870916101F7CE6D92ED0683AD4D2752,
	LeanDragTranslate_Translate_mED0FA0DFFA1442D2008804A3098528596487446A,
	LeanDragTranslate__ctor_m6FDA2E53B76816E8D16ED6A7B6A309BC51D12081,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	LeanFingerData__ctor_m9D0DEC41E54D8B6AAD78BF2C17F75C79161B2510,
	LeanFingerDown_get_OnFinger_m09CEA5AF6CC62534767806C9188D4437BE855D10,
	LeanFingerDown_get_OnWorld_mABD0F3882E1F0CCAE3DE6430936DC55A8F008542,
	LeanFingerDown_get_OnScreen_mC1BA9CF61C1128070CEB90BB1803235373DB944A,
	LeanFingerDown_Awake_m4EBD6572E08174C7207F37000C8FFF28084342AC,
	LeanFingerDown_OnEnable_m7DCAA90561D159F13C9039EB226050028CAA276E,
	LeanFingerDown_OnDisable_mE3878C228ABD82E39F2E67B2CD12D2726C84895F,
	LeanFingerDown_HandleFingerDown_mF6E9A8EBD0D96B3168E45CD8262B040C10D67AA9,
	LeanFingerDown__ctor_m677DD6264855C58CC2C7B1DB8CD3E9B042AFB10F,
	LeanFingerFilter__ctor_m9027A3427D07A922FCB51A04A5EFEF68C469A76A,
	LeanFingerFilter__ctor_mFCDCEED5234D11A36522A2F024497582BE8BC2C0,
	LeanFingerFilter_UpdateRequiredSelectable_m1D1F442B0D35326DEFACB787BEDB201A8D604FFB,
	LeanFingerFilter_AddFinger_m62B1294665D1E02E8CD4BEEB7F75C95931EB686E,
	LeanFingerFilter_RemoveFinger_m53A0A1AEC724712089F6B826E1B8378BE8272FF0,
	LeanFingerFilter_RemoveAllFingers_m0473D5577085C0CF16F78479CD323338D53BBEF7,
	LeanFingerFilter_GetFingers_m36475339DF733ABE96A27D5F2CA102719C020222,
	LeanFingerFilter_SimulatedFingersExist_m551529DB41F184F231D8D6466AA37FFE47E2DA27,
	LeanFingerOld_get_OnFinger_mD8E5956B120186C73579C6682224CD81CDADB2C5,
	LeanFingerOld_get_OnWorld_mFE784D2717DB51ECEC384A40CBE854439B102035,
	LeanFingerOld_get_OnScreen_mF044667491D141DE9FF3675B1B96877015EB2B40,
	LeanFingerOld_Start_m2B72B8EE75C8C02FCF55F4AA33CB30B467C109FB,
	LeanFingerOld_OnEnable_m0D463352D83EDEAFEEC1F9270129468F702D4C28,
	LeanFingerOld_OnDisable_m2518FE71C1A2F1B359296E6A6108627179A2D9AE,
	LeanFingerOld_HandleFingerOld_m416BB8EA7C79CC1C003550E3F9439B5ED7668375,
	LeanFingerOld__ctor_m05EDF29F6C7D1812E64521535052CF71671CE793,
	LeanFingerSwipe_Awake_mD9328660DA1B3B5D6D9D031A3167BFCB0C03A50B,
	LeanFingerSwipe_OnEnable_m10DA774A0B007814784A755C9CBD4140DA49AA00,
	LeanFingerSwipe_OnDisable_m044DB93E492A1B81D08461B8EE1D3846CD5BB5D1,
	LeanFingerSwipe_HandleFingerSwipe_mF76AAF439C0238799A27C9A0FCCB71C0C3310EC3,
	LeanFingerSwipe__ctor_m237BB98066A426F484660860ECEAF60190379B24,
	LeanFingerTap_get_OnFinger_m9ACB31CD15CA32E506CBEBC483481953854776C8,
	LeanFingerTap_get_OnCount_mB1E3A750E9BB4A0E1CC5A8FA15396106416D5668,
	LeanFingerTap_get_OnWorld_mC269480BC6FCEE7BA1D319E80E6F031C351FF1A8,
	LeanFingerTap_get_OnScreen_mDB16551B09D618D08F597665EFF1FE4AE69410A9,
	LeanFingerTap_Start_mEFEAF7F401BCA21468B99FF61AC5041B663CCC0A,
	LeanFingerTap_OnEnable_m1DDC1AF0B80803ED7593BE55B11BCE7F1E7CDC58,
	LeanFingerTap_OnDisable_m27980E600FF685528006FA2D68EC4AE5689E5098,
	LeanFingerTap_HandleFingerTap_m391E3808AC33BAB6B186EB0D003B0FC049FF20D5,
	LeanFingerTap__ctor_m4FBC89B7AC00498ECC7A76291D54903118605B98,
	LeanFingerUp_get_OnFinger_m13ED2351510968174A99E1930F5D98EE809EC58B,
	LeanFingerUp_get_OnWorld_m337A230C59B548A4D1F49AC1BC829C7AC4B3D2C2,
	LeanFingerUp_get_OnScreen_m0898E22CC52628062051442AD3CCCD2C844A6E37,
	LeanFingerUp_Start_m9F6BB4CB86D769CFBF83F2282D3DF40284E25AAA,
	LeanFingerUp_OnEnable_m4700254C003310B0CE255EB65BD1B6B4FF9BA1C8,
	LeanFingerUp_OnDisable_m7D8508BA58501993E9CB9A28C59BB43AEC0FD039,
	LeanFingerUp_HandleFingerUp_m404D0864041DE3F7EB7FD970BE3D3F7F10203BFC,
	LeanFingerUp__ctor_m73784348918E7E501160DBE2454F0384030D82EA,
	LeanFingerUpdate_get_OnFinger_m91602063824CF466F4A1BF8568C248C8FF4D33AC,
	LeanFingerUpdate_get_OnDelta_m4EFD9A089CDE1C6EBFED856F78B07EE4BCB4CF9B,
	LeanFingerUpdate_get_OnDistance_m5C3D628E2C95CFC2D306806B265EFD1971553697,
	LeanFingerUpdate_get_OnWorldFrom_m3CDBD2FD0D9FED63188B4C3A797CC08F56BD8CA9,
	LeanFingerUpdate_get_OnWorldTo_m3FC07F86C2F3A4BB17B9377EEB93CD7D56E6EB78,
	LeanFingerUpdate_get_OnWorldDelta_mF9626EC74455D7029FD75CFDA4F6A8FCB60DFA25,
	LeanFingerUpdate_get_OnWorldFromTo_mE113F08E1043681A4538CD1613B7F55A2ECA3AD7,
	LeanFingerUpdate_Awake_mBD1BFA6C5FEE4C1C7E99CB84916B631D2DE62D25,
	LeanFingerUpdate_OnEnable_m53E8D10FC7F1C49F3573DF410B17F6AEB39813FE,
	LeanFingerUpdate_OnDisable_m6A9474317AFD20C81963732BF1D3C0A13C7761B5,
	LeanFingerUpdate_HandleFingerUpdate_mE7A1613E0DA00A5A8FE848C5FEC64333E6D33B19,
	LeanFingerUpdate__ctor_mDC2270DFA2E7A11E4EA048FDC24E2D0D0ADBBA08,
	LeanPinchScale_AddFinger_m6E63F6A4AAAF088F9B31A70D37E8C926B418E05A,
	LeanPinchScale_RemoveFinger_m7570DF4028DA34BE78DDDF839AB2FBE0C39C60BB,
	LeanPinchScale_RemoveAllFingers_mD25443D486D77D1F252B1761B88DC3DADCF1EE26,
	LeanPinchScale_Awake_m382090C744E0F461544135AB055C00DB159E2ABC,
	LeanPinchScale_Update_mD074B060F96F1B4CA5B8973438527406F841D4FF,
	LeanPinchScale_TranslateUI_m92FB2B58544A3307F0DB40403A32589AACE04A77,
	LeanPinchScale_Translate_m93CA0FC32CECFDD7985836A23CDB8ACC4878223D,
	LeanPinchScale__ctor_mF9221BF63C4774835A82B33603A83DB0211925A1,
	LeanScreenDepth__ctor_m8241C542FD0B47AB7B44559D5148FE58FA0E6BD7,
	LeanScreenDepth_Convert_m2380FBFF96D3036FFEF582F64E30AD029E49E13F,
	LeanScreenDepth_ConvertDelta_mC27AC769D401202E0BC4E4651DC0ADF035CF4071,
	LeanScreenDepth_TryConvert_m7586DA6106530AA370705E0D7795B04AB7E5887D,
	NULL,
	LeanScreenDepth_IsChildOf_m4FEEC8865B58163E828194B3780672BE719F4A64,
	LeanScreenDepth__cctor_mFC88DAAFFE1F65BCC52EC9BCE7A3C66897E173B2,
	LeanSelect_TrySelect_m9DB5A381403F839F532286B39358926A5F737BC2,
	LeanSelect_Select_m794CAD55F9934F27EE462DEA49F56792F6455B87,
	LeanSelect_DeselectAll_mD01A1B1C4EBA4A68E1E100411B80466440D71558,
	LeanSelect_OnEnable_m2BCE2916757220B2A3ADD89DE89D96EBB644C3D8,
	LeanSelect_OnDisable_mBEA3E720D573E5A8A849E40E82D33F20524113C1,
	LeanSelect__ctor_mE71CFDD87A172D0584344855AABF07B40A55D205,
	LeanSelect__cctor_mB909940D752232BC29CBFE7A730F80D91AFF334C,
	LeanSelectBase_SelectStartScreenPosition_mC7F6D894E56F3765DE5039DA2507DD3014506FAE,
	LeanSelectBase_SelectScreenPosition_mEE36F99A486EEC8C09BFC4FD7665819DE9DB0258,
	LeanSelectBase_SelectScreenPosition_mBFF0B9251F3F22575C4BE2117166F3BF71D33E8C,
	NULL,
	LeanSelectBase_TryGetComponent_m7131E33DDCA54AC16B3655438E0152F78F364FCD,
	LeanSelectBase_GetClosestRaycastHitsIndex_m11C95C684E6D6FA54F152249C2DC03434C07F73D,
	LeanSelectBase_GetScreenPoint_mFB4F0DC8FBC7D74AC82C771AEFED913CE72A77F6,
	LeanSelectBase__ctor_mD6D21949ABB48BFAC35F502F3854D74B27692521,
	LeanSelectBase__cctor_mB66EA6E33B85A9D567509F1BC4DC8E9094C2D9EA,
	LeanSelectable_add_OnEnableGlobal_m9F457F205DDA6C06927E2085DF445E365E026816,
	LeanSelectable_remove_OnEnableGlobal_mD41F740F27DBE3C58673A0D289FC741CBB119637,
	LeanSelectable_add_OnDisableGlobal_mBF955FC5ACF3C04DE55AAF330E2EA836BB2C4F42,
	LeanSelectable_remove_OnDisableGlobal_m48850D7E0A5B384458358B24209EED8B5E12772D,
	LeanSelectable_add_OnSelectGlobal_m26EC561CD6AF8AE8BFE48B3B6692A8221770FF2A,
	LeanSelectable_remove_OnSelectGlobal_mE31A611550B3C16DF21901A218FAF4823FAAC86A,
	LeanSelectable_add_OnSelectSetGlobal_mE22AC4FE50CDA36803F262FD6530888D05FFB5D6,
	LeanSelectable_remove_OnSelectSetGlobal_m2281234B22D87DA7AB917E92F8D7D2DC8CE7F405,
	LeanSelectable_add_OnSelectUpGlobal_mB6012790E296C34FF4DE19F55CB0AAF1BEEE7AD1,
	LeanSelectable_remove_OnSelectUpGlobal_mBA331CE50DEB92F005AB5B80156D627CC8DC2994,
	LeanSelectable_add_OnDeselectGlobal_m3C2708774D3E0D3DFB0FD2FD31DBA84DA41D83D1,
	LeanSelectable_remove_OnDeselectGlobal_m422D6B7107F1A8A9CC7E27742163B1222EF37B71,
	LeanSelectable_get_OnSelect_m8E3DBD00DDA294792632B17866B991491D581FB4,
	LeanSelectable_get_OnSelectUpdate_m464D13210498A07206E2B8F6582E64BF122ED2CA,
	LeanSelectable_get_OnSelectUp_m581FE3EDD9E61DD2799650B5232FF86BA60A2E4C,
	LeanSelectable_get_OnDeselect_mE71E83654EA5C5C5016B73A6DD4FB9BA7816BF36,
	LeanSelectable_set_IsSelected_m3E3F7DD3FAB07C75DF2B22DC2C08FABBCC5E196D,
	LeanSelectable_get_IsSelected_mA27BBA35905B07BCBDF9140C908C1866E49349A4,
	LeanSelectable_get_IsSelectedRaw_mEE4FBF8D25B0D4B288EA58D1A4D20E7CFB264F12,
	LeanSelectable_get_IsSelectedCount_m4BF11670DCEF739799B526D06B9E6EA6D3DA4A19,
	LeanSelectable_get_IsSelectedRawCount_m4D2A4780D62557DD83521A6E36A0D2CA6C216A62,
	LeanSelectable_get_SelectingFinger_m7A62C9DD7B9F7126C42071304F9740269104C371,
	LeanSelectable_get_SelectingFingers_m27554C124728051293C687BE19DBC1C13840D987,
	LeanSelectable_GetFingers_mBBC187B20CD5D6E01A21AF4C389204956C757B9D,
	LeanSelectable_GetSelected_mD97EA73EA840F3302FFC4183924919F040B8D49F,
	LeanSelectable_Cull_mFE6613BA264DE143D809FDB43365C5A9270B7C47,
	LeanSelectable_FindSelectable_m06CEAEF90D3BC159F8009E1C15AA92E0D4188E8B,
	LeanSelectable_ReplaceSelection_m845CDD957A1FD92F9E38C4FF178FC1E5B7FCD159,
	LeanSelectable_IsSelectedBy_m508E6BB74CA6352735D60E613CFF1C39183433ED,
	LeanSelectable_GetIsSelected_mF69B8EB9DD26D08C5A481897776D567C60CDDD80,
	LeanSelectable_Select_m846A32582C9696ED9046C46A5645BC6954415C3C,
	LeanSelectable_Select_mD703CF70A9033CB32635FF0C8ABBA4445FF9AD2F,
	LeanSelectable_Deselect_m194A710A0CCCED6142EBCE699D6ED5CFA69309E9,
	LeanSelectable_DeselectAll_mB92F85B0B613EB2A0876545D01DAA4EB5847BF81,
	LeanSelectable_OnEnable_mDC06C36AB4D9BBF8BE346B9E3C126F59207153DD,
	LeanSelectable_OnDisable_mDBB0F91EDA3651A99FC2616F74FC6FC66C617F5D,
	LeanSelectable_BuildTempSelectables_mD78396F44702992D3F450875EA670BF50372146D,
	LeanSelectable_HandleFingerUpdate_mE169485133CADAF14A706ADAC5836410C920DA5F,
	LeanSelectable_get_AnyFingersSet_mD93FE54507CCF5A6E716C884151415B0BAE1C747,
	LeanSelectable_HandleFingerUp_m7E27A42F6CC1B8B05279F615DFD829BDA054AD15,
	LeanSelectable_HandleFingerInactive_mB1581C19AEBC3162DA1ACB0FD63599A646D3034D,
	LeanSelectable__ctor_m53DB58A1274046458288BE890A87033E0DD0F78A,
	LeanSelectable__cctor_m031C6481394CB34F214981D827F593531BB154B2,
	LeanSelectableBehaviour_get_Selectable_m87AAF88CB651F300DDF2604928B6313085E2A56E,
	LeanSelectableBehaviour_Register_m58EB6A8F0755B458F63BFF6D227A5E069F435021,
	LeanSelectableBehaviour_Register_mFE11711D2B46EF97F9A65CFB2BA529DB3E192057,
	LeanSelectableBehaviour_Unregister_m6435986D4F6690B1985177ADDE732C82BAF20DC6,
	LeanSelectableBehaviour_OnEnable_m03E270B708043089690EB6FA800647ECEB849D0D,
	LeanSelectableBehaviour_Start_mFC34E21B27A0852A19DDBEF817CDC9FE10F9D41E,
	LeanSelectableBehaviour_OnDisable_m82CCD84E6CE7C3721A20CDBC64B5AA6BB8AFC71A,
	LeanSelectableBehaviour_OnSelect_m59094BAE9D0C6819C36008D420C81411E5CECE78,
	LeanSelectableBehaviour_OnSelectUp_m1D1C272BDE43478432AF97FDB96E95CB96545DCE,
	LeanSelectableBehaviour_OnDeselect_m8AC245592C2348DF2DD6370D3E8DD86F6BEDEAC7,
	LeanSelectableBehaviour__ctor_m1A67DE8233AAE5D95131BD11A0C4F51DBD125E40,
	LeanSelectableGraphicColor_Awake_m47B10A31C5551601B170C55887DB43C3039F393C,
	LeanSelectableGraphicColor_OnSelect_m0437B76E863066BFDAECB508C08FD612A51B1501,
	LeanSelectableGraphicColor_OnDeselect_mB654258EAD7A5DEED077C19D11C1644F2E3A259A,
	LeanSelectableGraphicColor_ChangeColor_m831F6F88269CB96FF42FBBB90A0E8FA2E84073DF,
	LeanSelectableGraphicColor__ctor_mF2F9A5D202409902F56C8AE707E1C4EFD0BBE49F,
	LeanSelectableRendererColor_Awake_m214897D8FAA5072C15423ED6D4FE8494EE54F84A,
	LeanSelectableRendererColor_OnSelect_m7CFFA6A7D545A4F2CC4645513B336E8C5EC09788,
	LeanSelectableRendererColor_OnDeselect_m09E2705952F68DFED31BE322F9D9322935574E94,
	LeanSelectableRendererColor_ChangeColor_m04BD36420463E81EC44AEF6D74B74F2F034B9821,
	LeanSelectableRendererColor__ctor_mB53D27CE1C3119E41B37BA4AD05033DB432060B0,
	LeanSelectableSpriteRendererColor_Awake_mF533BF2BE789DC9641B1A075F21424148A304DFC,
	LeanSelectableSpriteRendererColor_OnSelect_m7C1911DC66C6898BD5F7367A1AF7D48ADD5EA7FD,
	LeanSelectableSpriteRendererColor_OnDeselect_mA3AFE7FE2400FD87DEADC5F651B4F6E17EAEA8D4,
	LeanSelectableSpriteRendererColor_ChangeColor_mA0219FA4548BE17A237373D47CF8962A3D3116F0,
	LeanSelectableSpriteRendererColor__ctor_mC79C9E1BA2A257B2FB0A8AA241DD4723084785FD,
	LeanSwipeBase_get_OnFinger_mE101F01D72CBC82AA47618B93C15923D218ACB2A,
	LeanSwipeBase_get_OnDelta_mD553D217539D026ACA504EF0F8A03AC6C9E23C5D,
	LeanSwipeBase_get_OnDistance_mD6FEF5D166EAED2FEBEAA14484A14D552D50A929,
	LeanSwipeBase_get_OnWorldFrom_mB4D4334D499F6C420A7694925AB80FC696A33428,
	LeanSwipeBase_get_OnWorldTo_m58A5E3A86B5D16D2082EE229F79416A6C0ED5430,
	LeanSwipeBase_get_OnWorldDelta_m3E702656F54E2EB573F42A7D4D2A7499F0722ECA,
	LeanSwipeBase_get_OnWorldFromTo_mECA2C27D7693D94627B42B5B374206A27064E9A7,
	LeanSwipeBase_AngleIsValid_mDD59824DAD6494B0B600778987E68B882DEEFB4E,
	LeanSwipeBase_HandleFingerSwipe_mD530BABFA688A2AC05D9112184C877545228ACEE,
	LeanSwipeBase__ctor_m1AD287D74A26E80ECBD3F16E3B66031F61A8802C,
	LeanTwistRotate_AddFinger_mB1ACE79A7BEC44DA4DF1546AA025486B307C40C3,
	LeanTwistRotate_RemoveFinger_mBA43133CC4CD144B9067CA95D01DBD0BFAED1C89,
	LeanTwistRotate_RemoveAllFingers_m88C3C6735CCE7C1E26A822F0F9651C01259ABAEB,
	LeanTwistRotate_Awake_m5A13FFC66B445FB3C95C2E268F8C6C14430F2596,
	LeanTwistRotate_Update_m95502F02441034C7FE830985744630D2AA7AF061,
	LeanTwistRotate_TranslateUI_m2A211B8D0DF239454A179B2649339322DE742F04,
	LeanTwistRotate_Translate_m40074D4F4627C9A11B9785B55481F0429C5BBE48,
	LeanTwistRotate_RotateUI_m4296C6FA8AC71E445E500C31D75A63FAC9BEB649,
	LeanTwistRotate_Rotate_mC0D7068BCFC1F255E39B3AC0C14A147DB81469D7,
	LeanTwistRotate__ctor_m0FE60DC5C70DB36376C6C4E34E23D9D4D6F849CB,
	LeanTwistRotateAxis_AddFinger_mC1368C6FBFCA893FFF0BA6953EFDD4FFF5881B6F,
	LeanTwistRotateAxis_RemoveFinger_mFB98312754B17DFAF2092D95B2612197AB3654B6,
	LeanTwistRotateAxis_RemoveAllFingers_mBC2855CD0887C7C9E570B9D52C60E1A17925C276,
	LeanTwistRotateAxis_Awake_mEFD9F22D6DD1BEF03B942F82AA0CFE23B10285CE,
	LeanTwistRotateAxis_Update_m2931C5471CD6FC71E1F47139E3D6B164F1A7729B,
	LeanTwistRotateAxis__ctor_m1C288EA0D475042810E60AC20F4C3C3BDBFE0831,
	LeanFinger_get_IsActive_m5EB3CF326192F0AA8895CD15B831F7DA828790F8,
	LeanFinger_get_SnapshotDuration_mA4EFDA357087D6BBBA47F7BB521E9B261ED8AB24,
	LeanFinger_get_IsOverGui_m3372F67DEC22FCCB3C908604C734D7D9E7B0CAFE,
	LeanFinger_get_Down_m45FA5F31995CB4F344C7DD5271C1EE7387BF559D,
	LeanFinger_get_Up_m4F56B9B87250871F9002AA569382DBA60E1B7186,
	LeanFinger_get_LastSnapshotScreenDelta_mC93F47A72A467AB3049D90FDE0466CF821233F2E,
	LeanFinger_get_LastSnapshotScaledDelta_mC8D9A621CFE90E1A051B751CBCA0091816048940,
	LeanFinger_get_ScreenDelta_mBD113E64DFA2132091B2591669BF8DA6478B9931,
	LeanFinger_get_ScaledDelta_m2782A2B8D42CB104C94AD0E68A9892C1212CB912,
	LeanFinger_get_SwipeScreenDelta_m5E3ACE2A2701D86AEECE19DBEEC184A17648C1EF,
	LeanFinger_get_SwipeScaledDelta_mFDDB667770B03555AC7A34D7F7A0C9FAF93A46DB,
	LeanFinger_GetSmoothScreenPosition_m0D6B9634EAD0A670197E5122B6AB88B7FE512A46,
	LeanFinger_get_SmoothScreenPositionDelta_m7C2A661BE9279D115B8EC98DD9D72C548CE2337F,
	LeanFinger_GetRay_m9B5E75941CF31049CBBAF401B82696C488AE7B97,
	LeanFinger_GetStartRay_mF5E172C33B4C0AC62E606721F7DA12C515E1661C,
	LeanFinger_GetSnapshotScreenDelta_m26D595C875BEBD033004E89322860C6C13A9D14C,
	LeanFinger_GetSnapshotScaledDelta_mC902C04E3C5CAB3F932E6B9D50160E5F9F815D81,
	LeanFinger_GetSnapshotScreenPosition_m302F4F09D5D0D1D20AEDB70852E1526FD9B4BA82,
	LeanFinger_GetSnapshotWorldPosition_m6C1FAD0CF8085CDC0247605FD0883D2E9A76D9F9,
	LeanFinger_GetRadians_m4BAFEEF64D666F8AF00DDB7871285382E7E01884,
	LeanFinger_GetDegrees_m6FA56D8AB0F982315AE58A0E8CA34446625809F1,
	LeanFinger_GetLastRadians_m0A92E21C3FD95C1640165755BA473E972DD56C6A,
	LeanFinger_GetLastDegrees_m1A702042CCF603A4B07D89DADDD4D779E84DD68E,
	LeanFinger_GetDeltaRadians_mCE5E06780D363DF4F03DBBFA6036593A493D166A,
	LeanFinger_GetDeltaRadians_m80BE16ECA752BF81608B5236751621891A3FE956,
	LeanFinger_GetDeltaDegrees_m243F39475A3B3C86FF21075A49B3F831DA895CA7,
	LeanFinger_GetDeltaDegrees_mB59044477F1716F67A902CE84AB47C2BE1577D3D,
	LeanFinger_GetScreenDistance_m45F7F0A5B2813B3A47614B442613144D5CEF12DA,
	LeanFinger_GetScaledDistance_m7B930E0AF81E7277B7314C31AD28D2725F5856D1,
	LeanFinger_GetLastScreenDistance_mA37A8D2F7CEE0FB3D831058A8D4B218F0E24DA61,
	LeanFinger_GetLastScaledDistance_m7BEA3523794103912F21A39A1FF24EE68274E50D,
	LeanFinger_GetStartScreenDistance_m987B0EBF0FE3F2B54BA07836B9D178F34036B498,
	LeanFinger_GetStartScaledDistance_m6762F2E2E471FBC79635227467155427A681644E,
	LeanFinger_GetStartWorldPosition_m74F17B992848A4B44CF2A34B5789BA2F5D840293,
	LeanFinger_GetLastWorldPosition_m3B21E536AE5C040EBAB40A3FC06B60277C12AF94,
	LeanFinger_GetWorldPosition_mEB1802EA120D6575B841E9DD1C7786058F164142,
	LeanFinger_GetWorldDelta_mFF2EC7D038C9FFF1B00730CE7E224D0A62A1D396,
	LeanFinger_GetWorldDelta_mA8D62CF16DE26590CA68D0DF8121374B0B3834B6,
	LeanFinger_ClearSnapshots_m8FC8E9DF887955A35B967A6285FE090BF1A45D1F,
	LeanFinger_RecordSnapshot_m076E2EA776632A022A0A6C9CB1D07A81EDB78065,
	LeanFinger__ctor_m0AE5B46092084E7254B5EA83F59ED7660446A72D,
	LeanGesture_GetScreenCenter_m95E4A5CFD4294984403D5A684E368DE450C7F6F6,
	LeanGesture_GetScreenCenter_m517308B8AC4FDA32CAD68C21E886A297DF6842DB,
	LeanGesture_TryGetScreenCenter_m8C9AB8BC6C12E7A2633EF4868A3BB586811B5AC2,
	LeanGesture_GetLastScreenCenter_mD79CEECF6E271274227C7A3153FC808F26D78CCA,
	LeanGesture_GetLastScreenCenter_mCE822AE69762752F4EA42893A10CDCA37564ACC8,
	LeanGesture_TryGetLastScreenCenter_mBDF2A0A5A23FAF327F4E127D2A14BE4BCB942238,
	LeanGesture_GetStartScreenCenter_m6963DECCD253804D478F31B25D3E9E53A812E58D,
	LeanGesture_GetStartScreenCenter_m38F5994614222FD5D4C4E25F5E2B0DFBBA23E95A,
	LeanGesture_TryGetStartScreenCenter_m0BC29D7F6382B6E12ECFCD7359DED0679931ECA3,
	LeanGesture_GetScreenDelta_m2586E19CDB082EC1869286BE346D2A6B8D1D2540,
	LeanGesture_GetScreenDelta_m271791F0317692A6C46E929FD2674ABCC94366D0,
	LeanGesture_TryGetScreenDelta_mC31B1C0FCFB1741F2C9B7BD69F71EA03CCBD42CB,
	LeanGesture_GetScaledDelta_m531E6CCA0539EB003CA7559AE7055927F039D229,
	LeanGesture_GetScaledDelta_mD6F7ACD36100B8BC79C94B740A20058207998756,
	LeanGesture_TryGetScaledDelta_mAB2E7F47841626D9C307DCE1DD57FC6CB0ED827E,
	LeanGesture_GetWorldDelta_m12CF8BB9CD5A3A32B5B40B2DE7A295147B198EE0,
	LeanGesture_GetWorldDelta_m1BECA7E4778CBE77A2BAF2A285F0C22C91CF1819,
	LeanGesture_TryGetWorldDelta_m92117853249392410C8F61D12681DBDA3F8D81A2,
	LeanGesture_GetScreenDistance_m041A4E76BAD112851E689CE92FAD9BB7729A659D,
	LeanGesture_GetScreenDistance_m5EDE5221BC7FC0B14F9D1B666A0361A0225E8AC3,
	LeanGesture_GetScreenDistance_mC3D9786C488664524C56C58EF8D4A14AEA0C0AF3,
	LeanGesture_TryGetScreenDistance_mA6ACAEBFCB5591CB55FC240E9D87672B37016282,
	LeanGesture_GetScaledDistance_mB114972D4083CE74D2B93606D49880220EEBD44E,
	LeanGesture_GetScaledDistance_mE32812460D0D8114344C561E904231EE39C3AC2B,
	LeanGesture_GetScaledDistance_m713957BCF545EF6891B37DEE2EF4E3BB927A91D6,
	LeanGesture_TryGetScaledDistance_m9DAF40C53F2524592C3494D3A6CEA89017ED351A,
	LeanGesture_GetLastScreenDistance_m581336A0617379A200FF2F4D896E7F2E4957E95A,
	LeanGesture_GetLastScreenDistance_m7DD444BFDCAB4D07131C3B7E380B942CB3F66692,
	LeanGesture_GetLastScreenDistance_m6C2B7DF8B4BFB18CEDF4B136D20F7CDF98713387,
	LeanGesture_TryGetLastScreenDistance_mC42D372AA0368015BD2337BA374851E68F890079,
	LeanGesture_GetLastScaledDistance_mDFAAB510AFC86D6F8E606DA195372744DD451035,
	LeanGesture_GetLastScaledDistance_mD299A8A10850A0778FB96F0D122740284E58A372,
	LeanGesture_GetLastScaledDistance_mE1A6903DA4416208397A6B4491D186005B624ED4,
	LeanGesture_TryGetLastScaledDistance_m30080A54C2B2BF4367F861009CF3FE55F1898D3B,
	LeanGesture_GetStartScreenDistance_m78E9B4545CBA13FE354A3EE4891C5B1F60CDB41E,
	LeanGesture_GetStartScreenDistance_m9769BF0434980A1A9425A9D7C75A6B404A2E5EBC,
	LeanGesture_GetStartScreenDistance_mCE3A06586A7B2696459964A9E59BE15D7D4742D4,
	LeanGesture_TryGetStartScreenDistance_mD8C36D11BA50FCA63433B2425D21D1F34BEBB1C8,
	LeanGesture_GetStartScaledDistance_m15B3C4A5944E1629294436AE40BB3AB4D98AFA5B,
	LeanGesture_GetStartScaledDistance_m1F86FB03F36DCAA89CF651B5215A340C0289B50E,
	LeanGesture_GetStartScaledDistance_mEC2C7084DF1BB078A3DE0CA24AD204BCBBCA08F3,
	LeanGesture_TryGetStartScaledDistance_m87C7EB5FEE9B1C7A655B782B6DE38F90EF5314A0,
	LeanGesture_GetPinchScale_mBBA14D943A1900A80B5E6700892FB722146495EE,
	LeanGesture_GetPinchScale_m6E8E7D883EBC69480C0C94E27D09EB5DA60E8E93,
	LeanGesture_TryGetPinchScale_m4EF715ECD02922701D6E68F166B0B926558452E7,
	LeanGesture_GetPinchRatio_mB86218C87ECF9479C408995DE4C7F6536A831497,
	LeanGesture_GetPinchRatio_m3F13637706904928538A5CAD55137CB641E0C1E9,
	LeanGesture_TryGetPinchRatio_m333C57DAFC4704857F1267E3A990E00B43BBEE28,
	LeanGesture_GetTwistDegrees_mBD3C57754D69F4F126FF164233677451FA0CDCFD,
	LeanGesture_GetTwistDegrees_mF044F7F3EBCF55911FBB7C293CA49FDD79F8F8A0,
	LeanGesture_GetTwistDegrees_m42803C38CB7BD8962C2D2935EDDF843CF51E7449,
	LeanGesture_TryGetTwistDegrees_mE8D3A01B072D7E0D9882B99385ECA0DE99967E86,
	LeanGesture_GetTwistRadians_m9A006C388E2425A8DE7E66BED5DCFB31AF67BEC4,
	LeanGesture_GetTwistRadians_m9E288C09F5AD708504C9CFF6834E2598FA5FF46B,
	LeanGesture_GetTwistRadians_m39D3B12CAC1AFE09FD5B093E13F6A79C45D379A8,
	LeanGesture_TryGetTwistRadians_m89F8D8A4373895F38903F1E80B6C8E7E359292EB,
	LeanSnapshot_GetWorldPosition_m60417EFAA6CEFD1C7C8FFE0ED11107B2A0406707,
	LeanSnapshot_Pop_m64151FFB07B94AF29CCB8E223EA4E930E5706098,
	LeanSnapshot_TryGetScreenPosition_m5D7A02E4CFB5EB9DC44E103B86CBCE1A49BD8114,
	LeanSnapshot_TryGetSnapshot_m5A11CF6A5B7C5BB3023EE97F1C9E3A5BADAF667A,
	LeanSnapshot_GetLowerIndex_m5537A71CFD9BF7ABF4AEB96215DF0C27740EDCA6,
	LeanSnapshot__ctor_m4BDD5DA589D0EAD0EB1F2C60C5CFC23206F32BFD,
	LeanSnapshot__cctor_m8D1F8A5AB81CFF484861C1B988B43A5F7DEC459D,
	LeanTouch_add_OnFingerDown_m17FAE9CD13F1E43586D22F8195DCB4849D40EF9D,
	LeanTouch_remove_OnFingerDown_m7D8D8908F245868EB3BA75FF2FE81363BD853BC5,
	LeanTouch_add_OnFingerUpdate_mDD30E72D4128AC7F7DEE314AFDD1F63B3BC54255,
	LeanTouch_remove_OnFingerUpdate_m58B05A304E7E3A468A3967B4B826383F3FB57CF2,
	LeanTouch_add_OnFingerUp_m63B6C30AB7B97D4009CF0611396C233A9B3C92BF,
	LeanTouch_remove_OnFingerUp_mE1B669D2A145F54FC5147DE6374782507D803777,
	LeanTouch_add_OnFingerOld_mA1F82698C5926977AA0D01602C89FD70307EBD94,
	LeanTouch_remove_OnFingerOld_mA0BF26E8F3EB1309005BFA908CB823F20FAEA8A6,
	LeanTouch_add_OnFingerTap_m5BA04B655B78782D12319DD143F4E01997E6594E,
	LeanTouch_remove_OnFingerTap_m17033D5DBDA4CD4AF877E1D1C9C6EB1D9236E155,
	LeanTouch_add_OnFingerSwipe_mDEE2185B28C5117DCD3E7E9A0CEC5B18BA206BFA,
	LeanTouch_remove_OnFingerSwipe_m36D61DE4F64B42907985C7E154FB3D9D7E1BE1DD,
	LeanTouch_add_OnGesture_mCAFCA85BE9F5DCFC821AB3A702B592437BBF0589,
	LeanTouch_remove_OnGesture_mE803955E7B5635B8941DBBF930462EDCD1E0EBF2,
	LeanTouch_add_OnFingerExpired_mDC3B4CAF9DEB8427D0CCDF27BEF598D4930D777F,
	LeanTouch_remove_OnFingerExpired_m7C90A349CCA6B72C366D3F61B61E7AB286D6F17E,
	LeanTouch_add_OnFingerInactive_m226A4E6851247A04C851E444A511372D056F4836,
	LeanTouch_remove_OnFingerInactive_mACEDB928D81225C7EEAE253E288E35111051BB84,
	LeanTouch_get_CurrentTapThreshold_mF85E0469F5CBB0264CB9676ADD02A58CDC705165,
	LeanTouch_get_CurrentSwipeThreshold_mA02ECE3889A6A3432820777DB65AE05E735A19CF,
	LeanTouch_get_CurrentReferenceDpi_mE4C155E648A42D3A028EF95E9B14B55A5A0A5879,
	LeanTouch_get_CurrentGuiLayers_m44F0FB49CD0DF70ACA3566A1E3BF15DEF7451E6D,
	LeanTouch_get_Instance_mAEA48C7EC5A9F5D0EDE2EDECE57FE4BFC3D63B53,
	LeanTouch_get_ScalingFactor_m5C19536EADF8F9156EF7D0F75E8FBB6FE439BA1B,
	LeanTouch_get_ScreenFactor_m1656DF31F790F1629F67010DE3FD3B523C05C127,
	LeanTouch_get_GuiInUse_m1E6DEF25C330F52E6C908112EB14ABB62E6F9578,
	LeanTouch_PointOverGui_m992D2317AE2CC30D8ADEAB3EDF7E8FA35E5CE22A,
	LeanTouch_RaycastGui_m46D88B1478558D4848EA3F0DBC6EAA53668264AE,
	LeanTouch_RaycastGui_m834161B727454502A991C2BDF82A2D8569A6767C,
	LeanTouch_GetFingers_m3E51F69DBAAC079C9BCCB56C606ABF7EC92043AF,
	LeanTouch_SimulateTap_mDC0A6EF2E1C8DB33CC9219F26EB097A4FF193249,
	LeanTouch_Clear_m480D07C5023A3E59BFBE60DBAC551C2D58A6465F,
	LeanTouch_OnEnable_mA8F035E5096033DBECE8236BF231678D5782E47F,
	LeanTouch_OnDisable_mAE2E123278374A47A730EA0B33279177E1D84227,
	LeanTouch_Update_m29FA2C0DE55C014E59DCA36391901AB2D5E62163,
	LeanTouch_UpdateFingers_m6DCB1BF789CB79A66D35A652CD7A14A90F056F3F,
	LeanTouch_OnGUI_m7596947DFED03B9DA2B089D2AD36E5F268B217E1,
	LeanTouch_BeginFingers_m61C0B32B75F276F075B0BD837CDA9CA1791A7F05,
	LeanTouch_EndFingers_m6D50FF715FC09BED4628F2833E658FC8943C5D60,
	LeanTouch_PollFingers_m122A92510537CEBFC795399532CA337854880F70,
	LeanTouch_UpdateEvents_mAC600A293B5E74B4C26D64C4E6E17CACB49E7C2D,
	LeanTouch_AddFinger_mA4CCB5070C89882868523A635D6B589976F17FFC,
	LeanTouch_FindFinger_mE38C9C8A9AB76767A1B889CE506F7B1C1EEBBE26,
	LeanTouch_FindInactiveFingerIndex_m9679A3819CE196276D9073E213899C110AE26C4E,
	LeanTouch__ctor_mB1EAE50B98F4AAD304A245A293C7276D590A79A7,
	LeanTouch__cctor_m0948815C556ADA180AFAE7A74BE3AAB829E5AD53,
	LeanDestroy_Update_m5A36AAEAA9F60FF6EA6DA6785E98DB80BD1361A2,
	LeanDestroy_DestroyNow_mB49A154BBD7F694CE45FE13501A88CC25947F037,
	LeanDestroy__ctor_m7F9ABFEF88110AD86E294B58474F7A43026C9884,
	LeanPath_get_PointCount_m295E15FDB24F3C912F65592B6252D5FA4C70EE01,
	LeanPath_GetPointCount_m852761EA7B6EBB224EE462F98D7CDB9FCE703D03,
	LeanPath_GetSmoothedPoint_m3F4CD111912C3DCED5637C27A169C6CE06806B60,
	LeanPath_GetPoint_mBD5235B3AABDBF0EFDEA526331BDEA7E721D8E2F,
	LeanPath_GetPointRaw_m8A40F8C9700D231F7A9DA78E2EAE91334BE27DCA,
	LeanPath_SetLine_mA682EB28573A36B72AEB649EF5543E1261201EB2,
	LeanPath_TryGetClosest_mAA5A9DA17F25D4E3F607F84F9F7329CFC69BBF09,
	LeanPath_TryGetClosest_m7E47B73402206BD25A18AC83291C3FCC73EACED8,
	LeanPath_TryGetClosest_m1E5BC7131252BC5B3ADC23A7ABD2DA8052156FD6,
	LeanPath_TryGetClosest_mB3924F0BF36E35EDB8EB8C439E7C68D4A8A55A93,
	LeanPath_TryGetClosest_m5BEBB049EC449482C50CDBA473A296D416D8CD80,
	LeanPath_GetClosestPoint_m97ECB21743B20049558B40ACD00D4FB7EA005933,
	LeanPath_GetClosestPoint_mC39A2518B0E571E5C04334C691A4EDFDF5A872AE,
	LeanPath_GetClosestDistance_m9DC0FD949C730887AD9269B106824A9A7009C79F,
	LeanPath_Mod_m5FC65C1C0E1DB3411D777603FD31A6BFD47715FF,
	LeanPath_CubicInterpolate_m51FB406ECA52DEE859C5D5A883586A05012253CA,
	LeanPath_UpdateVisual_mF016D7C056774F6F81972E7C795E48A6E0070E98,
	LeanPath_Update_m56B54EE220703300AF73C7E3C50AB1CF137783EC,
	LeanPath__ctor_m7FBF847F394ADBCF00773327655BF4FBA5FBB16D,
	LeanPath__cctor_m247F27420E99110F9A68CCAB8D9349FEB171EF06,
	LeanPlane_GetClosest_m3946B16CFFDD25A85EA7F63CCBB7AE96592C168D,
	LeanPlane_TryRaycast_mA9F2BC215465A197DCB02CA386D6557D63CBF7D0,
	LeanPlane_GetClosest_m9738FE9106536F3481FFE84B5D1BA961A79E1188,
	LeanPlane_RayToPlane_m281B102EF4BABA2B484D1ECE9E40A3D78E84EB1B,
	LeanPlane__ctor_m0348CD522CCD3CED55AEC240108993F648ABD59E,
	LeanRoll_IncrementAngle_mE4996C593D7B5F5C549D35710ECD9040F149FB59,
	LeanRoll_DecrementAngle_m666A2A2B35F1B24AEE1C58C67B1135C29829D461,
	LeanRoll_RotateToDelta_m7ABE9C10A41436BC5441F80538ED8CAB8EB43B00,
	LeanRoll_SnapToTarget_m38645C71A54CE1A9346C3CB4FA7BBCEAB964CC29,
	LeanRoll_Start_mF26675531936F560695F444CC8F7210FDE6FEDEB,
	LeanRoll_Update_mA1CAB75EDAD4D417BCECA41350920036E7350CA0,
	LeanRoll__ctor_mDCF984455BC2AA07BB1CA4542654139043080BA8,
	LeanSpawn_Spawn_m88F6C987A1A5A6F6FDCC42C99EE54EEE48E7355C,
	LeanSpawn_Spawn_m3FBB61E53EF9FA3EFE8E1F1706FBCE2F8F3A49C2,
	LeanSpawn__ctor_mEC414F4483787C2A13317C6265E5D15602AD6507,
	NULL,
	LeanHelper_GetDampenFactor_mB38E6C7D44DD03FCB6EA13FDF47D1759D70C79B0,
	NULL,
	LeanHelper_GetCamera_mC6F8F819625E55F46066BC33BDB5B4E27B6624BC,
	LeanHelper_Hermite_mC232448D90E4F66CAF8464858F96F9DE328F298F,
	LeanHelper_HermiteInterpolate_mC10EC993296BA9C701997970C4096820844629AA,
	LeanInput_GetTouchCount_mF5B91FAD5A4A09E9EEDE8354C90B846A71546B3C,
	LeanInput_GetTouch_mEE4ADAD4FE780C27750315EBAC87ECEF25EBDB37,
	LeanInput_GetMousePosition_m73BF6A8AB96C06A26E43EF9970C730477A23A7EA,
	LeanInput_GetDown_mC3C5BBB80DB59C700B3CA731647D22B75519B857,
	LeanInput_GetPressed_mBBD9D27684033A1C7E02C0A9C733793446691DE0,
	LeanInput_GetUp_m3EA98E00B634171954A034A44101A7A88BD312FB,
	LeanInput_GetMouseDown_m07DCCD7120218CD03DD0499AFA56F5961F06808F,
	LeanInput_GetMousePressed_m0A4158E416B7953A1900D8E2208A066061F0828E,
	LeanInput_GetMouseUp_mA5275C36A357186AE051B783086137E1FD27B85B,
	LeanInput_GetMouseWheelDelta_mED9FD054BDB1B70B7F5C0607A0D1E1AD354A240C,
	LeanInput_GetMouseExists_m554C7E038B57994F726D17B15F9ADE20C87934B1,
	LeanInput_GetKeyboardExists_m099B1E603DAB8FA7BDEF32C225F3C13B8241F31A,
	LeanGuide_get_Icon_m4211CD2FB86EB611139E73B5F583F20DCB81F933,
	LeanGuide_get_Version_m24BB827A3234D301CD0DC575CFD6FC0F0397B2EB,
	LeanGuide__ctor_mD0DDFCBA8D1D3A635C5B61F902014A24A84D7CE6,
	LeanLinkTo_set_Link_m2D14C174B6D4937FA4E9C0DF31A7E30F5EF2F04A,
	LeanLinkTo_get_Link_mE54B29E4EB943CD720275D7BEDA7678A6085EF72,
	LeanLinkTo_Update_m2FC888CBF8274A5A1BBE52A1E204F43EB5E2BE34,
	LeanLinkTo_OnPointerClick_m82FBBF604DAF9629AF74CC9A1F865FD6D1EE3A64,
	LeanLinkTo_GetCurrentLevel_m9E6C482ACD4BAAD0C058EE2917E42E88235840AA,
	LeanLinkTo_GetLevelCount_m609DBD1FAABF53291D11BC03FF62CEA2A0B23685,
	LeanLinkTo_LoadLevel_m4B7CFCACB0C91837A89939AB792051F64969344D,
	LeanLinkTo__ctor_mBB86CA187E2030D2156B69AB183C7BA4558B47DB,
	LeanUpgradeEventSystem__ctor_mFBA8F608DD6786DF5068AF0E837A0D29894983BC,
	U3CStartU3Ed__55__ctor_m987504CCE2A04E56489F015DDF3279DC89C0F080,
	U3CStartU3Ed__55_System_IDisposable_Dispose_m9728BAF46DD084B007DFFE1D4F6F87B2DB1E1554,
	U3CStartU3Ed__55_MoveNext_mAC6E79927F1429E573CE90D98D20C252559E1056,
	U3CStartU3Ed__55_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3EC035A1A71FEA9171B22B400ABDAB7FFF2FB3ED,
	U3CStartU3Ed__55_System_Collections_IEnumerator_Reset_mE1713764C96D07B62E7DE7F5EA52D344AC64C15A,
	U3CStartU3Ed__55_System_Collections_IEnumerator_get_Current_m1F76D144731727E498C37AD026E5F6D941A40D57,
	U3CU3Ec__cctor_mBA5A8E7D4C803C8F80B71126C81B4760B987AD2A,
	U3CU3Ec__ctor_m0A79C1A3BEA9231B3E275AEE65DA7EE93B137E15,
	U3CU3Ec_U3CGetGoalU3Eb__73_0_m2B8DFF8BDBE12F4F91E215807A71BA90C0403D11,
	U3CU3Ec_U3CGetGoalU3Eb__73_1_mB0E33D138F70F637F0DFE253D23D7C034D0300D0,
	U3CEnableBatsmanU3Ed__6__ctor_mEEC4B8F973D62718060076D669597F838C324FD9,
	U3CEnableBatsmanU3Ed__6_System_IDisposable_Dispose_mB0483598E83315D30E92EAAD43D5AF10D57FBF54,
	U3CEnableBatsmanU3Ed__6_MoveNext_m3014B24C7D3766282B566712F3E240E264939DC1,
	U3CEnableBatsmanU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3679DAB9B0B5B9B44605F19D1D8D3ECB2DA8AE26,
	U3CEnableBatsmanU3Ed__6_System_Collections_IEnumerator_Reset_m5A6F7084CEF295AA235E9F6D9241FFA8D7D394B5,
	U3CEnableBatsmanU3Ed__6_System_Collections_IEnumerator_get_Current_m3ACB8AAEC04E99894B01D1FDEA401A4A881093DD,
	FingerData__ctor_mBD4FE27C75E0E29562797D1AB0492EB2AC358205,
	LeanFingerEvent__ctor_m4CFB483CBD90B48A48C6A6D9728A8931735EF946,
	Vector3Event__ctor_m960B25A38B6931C4ACA3DFED800D0276810C1A88,
	Vector2Event__ctor_mAAD07EA5C6123E8AC5E2268E8CDB2B8C8E90BAB0,
	LeanFingerEvent__ctor_m96403F70A4B50BCA5684358D655143220F82AC9C,
	Vector3Event__ctor_mBFBC35865569FC86BB7046BF3CB4164D1769B7B3,
	Vector2Event__ctor_mB9D39DE9A6A88B04E1C0DDD4BC774BD88F53583B,
	LeanFingerEvent__ctor_m2CAD3392293BE1E47A3910B4458A43971343CF82,
	Vector3Event__ctor_m773F439BD8010721D145B4FF54EDDDC52289F776,
	Vector2Event__ctor_mB35F5A5569DBDF1144AFB86DD63C167C2496455E,
	IntEvent__ctor_mD33570F55569DEAB4F3B26D77AA355F1ED477AA8,
	LeanFingerEvent__ctor_mFC34EB4DFB02276BA55C8614091190901DC1FF85,
	Vector3Event__ctor_mEEF80B16ACEC7A2C7B430F708D95AA3921E097EB,
	Vector2Event__ctor_m97409F24DA4F910CBC0CA98DF011AACE799BC17A,
	LeanFingerEvent__ctor_m9702EEF46B05BDF0536F72ED69E57B49AE18258E,
	FloatEvent__ctor_m5E7A15983A28F6853971B22DB6067ADB2406E0BD,
	Vector2Event__ctor_m51EBA0E530C99B6FF6C9298ADACBAF19FDC4EE1C,
	Vector3Event__ctor_m37B43623DEF8F13CE720616D73F44C599CA37486,
	Vector3Vector3Event__ctor_m48CA2CA2DD2481C73C707D5D91117979836D882C,
	LeanFingerEvent__ctor_mC7D40C1C205EE69CFA2893EA85BA064D198F3F1B,
	LeanFingerEvent__ctor_m9E9808207D2EEFD582F28CE19E3E0A9075234948,
	FloatEvent__ctor_m2BE488A19FA2FC596AD67BBBE5F92885A2B29654,
	Vector2Event__ctor_mA7B1074912DDF49096DFF039309D23D8539B3DE0,
	Vector3Event__ctor_mDF01E3CA8B5D11B0F08F155BAA9D291D5801A44E,
	Vector3Vector3Event__ctor_mFA93A740F2E94269E2185B8A3E8F49D10C729F67,
};
extern void UXHandle__ctor_mCCADFDD89B964F4412436FA3100256EE9305FFDA_AdjustorThunk (void);
extern void LeanFingerFilter__ctor_m9027A3427D07A922FCB51A04A5EFEF68C469A76A_AdjustorThunk (void);
extern void LeanFingerFilter__ctor_mFCDCEED5234D11A36522A2F024497582BE8BC2C0_AdjustorThunk (void);
extern void LeanFingerFilter_UpdateRequiredSelectable_m1D1F442B0D35326DEFACB787BEDB201A8D604FFB_AdjustorThunk (void);
extern void LeanFingerFilter_AddFinger_m62B1294665D1E02E8CD4BEEB7F75C95931EB686E_AdjustorThunk (void);
extern void LeanFingerFilter_RemoveFinger_m53A0A1AEC724712089F6B826E1B8378BE8272FF0_AdjustorThunk (void);
extern void LeanFingerFilter_RemoveAllFingers_m0473D5577085C0CF16F78479CD323338D53BBEF7_AdjustorThunk (void);
extern void LeanFingerFilter_GetFingers_m36475339DF733ABE96A27D5F2CA102719C020222_AdjustorThunk (void);
extern void LeanScreenDepth__ctor_m8241C542FD0B47AB7B44559D5148FE58FA0E6BD7_AdjustorThunk (void);
extern void LeanScreenDepth_Convert_m2380FBFF96D3036FFEF582F64E30AD029E49E13F_AdjustorThunk (void);
extern void LeanScreenDepth_ConvertDelta_mC27AC769D401202E0BC4E4651DC0ADF035CF4071_AdjustorThunk (void);
extern void LeanScreenDepth_TryConvert_m7586DA6106530AA370705E0D7795B04AB7E5887D_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[12] = 
{
	{ 0x060000B7, UXHandle__ctor_mCCADFDD89B964F4412436FA3100256EE9305FFDA_AdjustorThunk },
	{ 0x0600014B, LeanFingerFilter__ctor_m9027A3427D07A922FCB51A04A5EFEF68C469A76A_AdjustorThunk },
	{ 0x0600014C, LeanFingerFilter__ctor_mFCDCEED5234D11A36522A2F024497582BE8BC2C0_AdjustorThunk },
	{ 0x0600014D, LeanFingerFilter_UpdateRequiredSelectable_m1D1F442B0D35326DEFACB787BEDB201A8D604FFB_AdjustorThunk },
	{ 0x0600014E, LeanFingerFilter_AddFinger_m62B1294665D1E02E8CD4BEEB7F75C95931EB686E_AdjustorThunk },
	{ 0x0600014F, LeanFingerFilter_RemoveFinger_m53A0A1AEC724712089F6B826E1B8378BE8272FF0_AdjustorThunk },
	{ 0x06000150, LeanFingerFilter_RemoveAllFingers_m0473D5577085C0CF16F78479CD323338D53BBEF7_AdjustorThunk },
	{ 0x06000151, LeanFingerFilter_GetFingers_m36475339DF733ABE96A27D5F2CA102719C020222_AdjustorThunk },
	{ 0x06000185, LeanScreenDepth__ctor_m8241C542FD0B47AB7B44559D5148FE58FA0E6BD7_AdjustorThunk },
	{ 0x06000186, LeanScreenDepth_Convert_m2380FBFF96D3036FFEF582F64E30AD029E49E13F_AdjustorThunk },
	{ 0x06000187, LeanScreenDepth_ConvertDelta_mC27AC769D401202E0BC4E4651DC0ADF035CF4071_AdjustorThunk },
	{ 0x06000188, LeanScreenDepth_TryConvert_m7586DA6106530AA370705E0D7795B04AB7E5887D_AdjustorThunk },
};
static const int32_t s_InvokerIndices[765] = 
{
	3683,
	3676,
	3683,
	3612,
	3027,
	3507,
	2903,
	3507,
	2903,
	3501,
	2898,
	3509,
	2905,
	3501,
	2898,
	3507,
	2903,
	3508,
	2904,
	3683,
	3683,
	3683,
	2934,
	3683,
	3643,
	3056,
	3612,
	3027,
	3612,
	3027,
	3643,
	3056,
	3612,
	3027,
	3683,
	3683,
	3612,
	3683,
	5859,
	3612,
	3027,
	3612,
	3027,
	3683,
	3683,
	3653,
	3065,
	3683,
	3683,
	3683,
	2946,
	3027,
	3683,
	5859,
	3683,
	3683,
	3683,
	2946,
	3683,
	3612,
	3027,
	3612,
	3027,
	5774,
	5774,
	3643,
	3056,
	3683,
	3683,
	3683,
	3683,
	3027,
	3683,
	5859,
	3643,
	3056,
	3643,
	3683,
	3056,
	3056,
	3683,
	3612,
	3027,
	3612,
	3027,
	3612,
	3027,
	3612,
	3027,
	3612,
	3027,
	3612,
	3027,
	3612,
	3027,
	3612,
	3027,
	3612,
	3027,
	3612,
	3027,
	5774,
	5774,
	3612,
	3027,
	3612,
	3027,
	3643,
	3056,
	3683,
	3683,
	3683,
	3683,
	3683,
	3683,
	3683,
	3683,
	3683,
	3643,
	3683,
	3683,
	3643,
	3056,
	3612,
	3027,
	3612,
	3027,
	3612,
	3027,
	3612,
	3027,
	3612,
	3027,
	3612,
	3027,
	3612,
	3027,
	3612,
	3027,
	3612,
	3027,
	3612,
	3027,
	3643,
	3056,
	3683,
	3683,
	3683,
	2953,
	3683,
	3683,
	3009,
	3683,
	3643,
	3056,
	3643,
	3056,
	3612,
	3027,
	3612,
	3027,
	3683,
	3683,
	3683,
	3683,
	3643,
	3612,
	3027,
	3612,
	3027,
	3612,
	3027,
	3612,
	3027,
	3612,
	3027,
	3612,
	3027,
	3612,
	2796,
	3009,
	3683,
	1533,
	3643,
	3056,
	3594,
	3009,
	3594,
	3009,
	3643,
	3056,
	3594,
	3009,
	3594,
	3009,
	3643,
	3056,
	3612,
	3027,
	3612,
	3027,
	3612,
	3027,
	3612,
	3027,
	3612,
	3027,
	3612,
	3027,
	3612,
	3027,
	3612,
	3027,
	3683,
	3683,
	3683,
	3683,
	2217,
	3009,
	3643,
	3643,
	3643,
	3643,
	3643,
	3643,
	3683,
	3643,
	3090,
	3683,
	3683,
	3683,
	3683,
	3683,
	3683,
	3683,
	3027,
	3683,
	3683,
	3683,
	3683,
	3683,
	3683,
	3683,
	3683,
	3683,
	3683,
	3683,
	3683,
	3683,
	3683,
	3683,
	3683,
	3683,
	3683,
	3683,
	3027,
	3683,
	3009,
	3009,
	3683,
	3612,
	3683,
	3683,
	3683,
	3683,
	3009,
	3683,
	3683,
	3683,
	3683,
	3027,
	1684,
	3009,
	3065,
	3092,
	3094,
	3095,
	1533,
	3683,
	3683,
	3683,
	3683,
	3683,
	3027,
	3027,
	3027,
	3027,
	3027,
	3027,
	3683,
	3683,
	3027,
	3027,
	3683,
	3683,
	3683,
	3683,
	3027,
	3027,
	3683,
	3683,
	3683,
	3683,
	3683,
	964,
	3027,
	3683,
	3027,
	3027,
	3683,
	3683,
	3683,
	3092,
	3092,
	3683,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	3683,
	3612,
	3612,
	3612,
	3683,
	3683,
	3683,
	3027,
	3683,
	3056,
	263,
	3027,
	3027,
	3027,
	3683,
	2225,
	5711,
	3612,
	3612,
	3612,
	3683,
	3683,
	3683,
	3027,
	3683,
	3683,
	3683,
	3683,
	3027,
	3683,
	3612,
	3612,
	3612,
	3612,
	3683,
	3683,
	3683,
	3027,
	3683,
	3612,
	3612,
	3612,
	3683,
	3683,
	3683,
	3027,
	3683,
	3612,
	3612,
	3612,
	3612,
	3612,
	3612,
	3612,
	3683,
	3683,
	3683,
	3027,
	3683,
	3027,
	3027,
	3683,
	3683,
	3683,
	1728,
	1728,
	3683,
	920,
	886,
	554,
	530,
	-1,
	5150,
	5859,
	969,
	1684,
	3683,
	3683,
	3683,
	3683,
	5859,
	3027,
	3027,
	1693,
	969,
	604,
	5584,
	5218,
	3683,
	5859,
	5774,
	5774,
	5774,
	5774,
	5774,
	5774,
	5774,
	5774,
	5774,
	5774,
	5774,
	5774,
	3612,
	3612,
	3612,
	3612,
	3056,
	3643,
	3643,
	5823,
	5823,
	3612,
	3612,
	4357,
	5774,
	5771,
	5673,
	5380,
	2567,
	2597,
	3683,
	3027,
	3683,
	5859,
	3683,
	3683,
	5774,
	5774,
	3643,
	5774,
	5774,
	3683,
	5859,
	3612,
	3683,
	3027,
	3683,
	3683,
	3683,
	3683,
	3027,
	3027,
	3683,
	3683,
	3683,
	3027,
	3683,
	2968,
	3683,
	3683,
	3027,
	3683,
	2968,
	3683,
	3683,
	3027,
	3683,
	2968,
	3683,
	3612,
	3612,
	3612,
	3612,
	3612,
	3612,
	3612,
	2634,
	982,
	3683,
	3027,
	3027,
	3683,
	3683,
	3683,
	1728,
	1728,
	3065,
	3065,
	3683,
	3027,
	3027,
	3683,
	3683,
	3683,
	3683,
	3643,
	3653,
	3643,
	3643,
	3643,
	3676,
	3676,
	3676,
	3676,
	3676,
	3676,
	2770,
	3653,
	2254,
	2254,
	2770,
	2770,
	2770,
	885,
	2734,
	2734,
	2734,
	2734,
	2734,
	1333,
	2734,
	1333,
	2734,
	2734,
	2734,
	2734,
	2734,
	2734,
	1342,
	1342,
	1342,
	1342,
	885,
	3009,
	3683,
	3683,
	5855,
	5746,
	5146,
	5855,
	5746,
	5146,
	5855,
	5746,
	5146,
	5855,
	5746,
	5146,
	5855,
	5746,
	5146,
	5228,
	4665,
	4393,
	5846,
	5732,
	5194,
	4638,
	5846,
	5732,
	5194,
	4638,
	5846,
	5732,
	5194,
	4638,
	5846,
	5732,
	5194,
	4638,
	5846,
	5732,
	5194,
	4638,
	5846,
	5732,
	5194,
	4638,
	5734,
	5193,
	4035,
	5734,
	5193,
	4035,
	5846,
	5732,
	4658,
	4396,
	5846,
	5732,
	4658,
	4396,
	1342,
	5830,
	4637,
	4382,
	4982,
	3683,
	5859,
	5774,
	5774,
	5774,
	5774,
	5774,
	5774,
	5774,
	5774,
	5774,
	5774,
	5774,
	5774,
	5774,
	5774,
	5774,
	5774,
	5774,
	5774,
	5846,
	5846,
	5823,
	5826,
	5830,
	5846,
	5846,
	5843,
	5717,
	5683,
	5051,
	4598,
	4884,
	3683,
	3683,
	3683,
	3683,
	1726,
	3683,
	3065,
	3065,
	3683,
	3683,
	459,
	2217,
	2068,
	3683,
	5859,
	3683,
	3683,
	3683,
	3594,
	2068,
	2777,
	1340,
	1340,
	1741,
	220,
	878,
	219,
	870,
	548,
	887,
	884,
	1330,
	1106,
	222,
	3683,
	3683,
	3683,
	5859,
	1344,
	550,
	1341,
	4403,
	3683,
	3065,
	3065,
	3092,
	3683,
	3683,
	3683,
	3683,
	3683,
	3094,
	3683,
	-1,
	5196,
	-1,
	5039,
	4042,
	3826,
	5823,
	4144,
	5855,
	5708,
	5708,
	5708,
	5708,
	5708,
	5708,
	5846,
	5843,
	5843,
	3612,
	3612,
	3683,
	3009,
	3594,
	3683,
	3027,
	5823,
	5823,
	5771,
	3683,
	3683,
	3009,
	3683,
	3643,
	3612,
	3683,
	3612,
	5859,
	3683,
	3643,
	3643,
	3009,
	3683,
	3643,
	3612,
	3683,
	3612,
	3683,
	3683,
	3683,
	3683,
	3683,
	3683,
	3683,
	3683,
	3683,
	3683,
	3683,
	3683,
	3683,
	3683,
	3683,
	3683,
	3683,
	3683,
	3683,
	3683,
	3683,
	3683,
	3683,
	3683,
	3683,
};
static const Il2CppTokenRangePair s_rgctxIndices[9] = 
{
	{ 0x0600013C, { 0, 3 } },
	{ 0x0600013D, { 3, 3 } },
	{ 0x0600013E, { 6, 5 } },
	{ 0x0600013F, { 11, 4 } },
	{ 0x06000140, { 15, 3 } },
	{ 0x06000141, { 18, 7 } },
	{ 0x06000189, { 25, 3 } },
	{ 0x060002B7, { 28, 3 } },
	{ 0x060002B9, { 31, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[32] = 
{
	{ (Il2CppRGCTXDataType)3, 12776 },
	{ (Il2CppRGCTXDataType)3, 12777 },
	{ (Il2CppRGCTXDataType)2, 197 },
	{ (Il2CppRGCTXDataType)3, 12774 },
	{ (Il2CppRGCTXDataType)3, 12775 },
	{ (Il2CppRGCTXDataType)2, 196 },
	{ (Il2CppRGCTXDataType)3, 12779 },
	{ (Il2CppRGCTXDataType)3, 12780 },
	{ (Il2CppRGCTXDataType)2, 198 },
	{ (Il2CppRGCTXDataType)3, 12778 },
	{ (Il2CppRGCTXDataType)3, 20986 },
	{ (Il2CppRGCTXDataType)3, 12782 },
	{ (Il2CppRGCTXDataType)3, 12783 },
	{ (Il2CppRGCTXDataType)3, 20987 },
	{ (Il2CppRGCTXDataType)3, 12781 },
	{ (Il2CppRGCTXDataType)3, 12784 },
	{ (Il2CppRGCTXDataType)3, 12785 },
	{ (Il2CppRGCTXDataType)2, 200 },
	{ (Il2CppRGCTXDataType)2, 3089 },
	{ (Il2CppRGCTXDataType)3, 12786 },
	{ (Il2CppRGCTXDataType)3, 12788 },
	{ (Il2CppRGCTXDataType)3, 12789 },
	{ (Il2CppRGCTXDataType)2, 201 },
	{ (Il2CppRGCTXDataType)3, 23084 },
	{ (Il2CppRGCTXDataType)3, 12787 },
	{ (Il2CppRGCTXDataType)2, 204 },
	{ (Il2CppRGCTXDataType)3, 26433 },
	{ (Il2CppRGCTXDataType)3, 26766 },
	{ (Il2CppRGCTXDataType)1, 202 },
	{ (Il2CppRGCTXDataType)3, 26367 },
	{ (Il2CppRGCTXDataType)2, 202 },
	{ (Il2CppRGCTXDataType)2, 203 },
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	765,
	s_methodPointers,
	12,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	9,
	s_rgctxIndices,
	32,
	s_rgctxValues,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
